# Build type
BUILD:=-DCMAKE_BUILD_TYPE=RelWithDebInfo

# Configuration
CONFIG:=-DFDBB_WITH_OCL:BOOL=ON

# Tests
TESTS:=-DFDBB_BUILD_TESTS:BOOL=ON \
	-DFDBB_BUILD_TESTS_ARRAYFIRE:BOOL=ON \
	-DFDBB_BUILD_TESTS_VEXCL:BOOL=ON \
	-DFDBB_BUILD_TESTS_VIENNACL:BOOL=ON

# Unittests
UNITTESTS:=-DFDBB_BUILD_UNITTESTS:BOOL=OFF

# Do not edit
CMAKE_ARGS:=$(BUILD) $(CONFIG) $(TESTS) $(UNITTESTS)

# Commands
CMAKE:=cmake
CD:=cd
MAKE:=make
MKDIR:=mkdir

all: gcc

# Clang compiler
clang:
	$(shell $(MKDIR) -p build.clang && \
		$(CD) build.clang && \
	  	CC=clang CXX=clang++ $(CMAKE) $(CMAKE_ARGS) .. && \
		$(MAKE) all && \
	  	$(MAKE) test)

# Gnu Compiler Suite
gcc:
	$(shell $(MKDIR) -p build.gcc && \
		$(CD) build.gcc && \
		CC=gcc CXX=g++ $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# GNU Compiler Suite 4.7
gcc-4.7:
	$(shell $(MKDIR) -p build.gcc-4.7 && \
		$(CD) build.gcc-4.7 && \
		CC=gcc-4.7 CXX=g++-4.7 $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# GNU Compiler Suite 4.8
gcc-4.8:
	$(shell $(MKDIR) -p build.gcc-4.8 && \
		$(CD) build.gcc-4.8 && \
		CC=gcc-4.8 CXX=g++-4.8 $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# GNU Compiler Suite 4.9
gcc-4.9:
	$(shell $(MKDIR) -p build.gcc-4.9 && \
		$(CD) build.gcc-4.9 && \
		CC=gcc-4.9 CXX=g++-4.9 $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# GNU Compiler Suite 5.x
gcc-5:
	$(shell $(MKDIR) -p build.gcc-5 && \
		$(CD) build.gcc-5 && \
		CC=gcc-5 CXX=g++-5 $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# GNU Compiler Suite 6.x
gcc-6:
	$(shell $(MKDIR) -p build.gcc-6 && \
		$(CD) build.gcc-6 && \
		CC=gcc-6 CXX=g++-6 $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# GNU Compiler Suite 7.x
gcc-7:
	$(shell $(MKDIR) -p build.gcc-7 && \
		$(CD) build.gcc-7 && \
		CC=gcc-7 CXX=g++-7 $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# Intel Compiler Suite
intel:
	$(shell $(MKDIR) -p build.intel && \
		$(CD) build.intel && \
		CC=icc CXX=icpc $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all ; \
		$(MAKE) test)

# Open64 Compiler Suite
open64:
	$(shell $(MKDIR) -p build.open64 && \
		$(CD) build.open64 && \
		CC=opencc CXX=openCC $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# Oracle Compiler Suite
oracle:
	$(shell $(MKDIR) -p build.oracle && \
		$(CD) build.oracle && \
		CC=cc CXX=CC $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)

# PGI Compiler Suite
pgi:
	$(shell $(MKDIR) -p build.pgi && \
		$(CD) build.pgi && \
		CC=pgcc CXX=pgCC $(CMAKE) $(CMAKE_ARGS) -DFDBB_WITH_OMP .. && \
		$(MAKE) all && \
		$(MAKE) test)
