FDBB (Fluid Dynamics Building Blocks) is a C++ expression template library for fluid dynamics.
-  The latest revision of the **FDBB** library is available at
   https://gitlab.com/mmoelle1/FDBB.
-  The latest documentation of the **FDBB** library is available at
   https://mmoelle1.gitlab.io/FDBB/.

# <a name="contents">Contents</a>

1. [Introduction](#introduction)
   * [Philosophy](#philosophy)
   * [Prerequisites](#prerequisites)
   * [Coding style](#coding-style)
   * [License](#license)
2. [Getting started](#getting-started)
   * [Obtaining the latest code](#obtaining-the-latest-code)
   * [Compiling and running the UnitTests](#compiling-and-running-the-unittests)
   * [Including FDBB into own codes](#including-fdbb-into-own-codes)
3. [Core components](#core-components)
   * [Equations of state](#equations-of-state)
   * [Variables](#variables)
   * [Fluxes](#fluxes)
   * [Flux-Jacobians](#flux-jacobians)
4. [The plain program](#the-plain-program)

___

# <a name="introduction">Introduction</a>

**FDBB** is a fluid dynamics expression template library written in
C++14 (C++11 by compiler flag) that provides the algorithmic building
blocks for developing computational fluid dynamics (CFD) tools. It is
designed as a header-only C++ library that can be easily integrated
into existing CFD codes by including the main header file `fdbb.h`.

## <a name="philosophy">Philosophy</a>

**FDBB** is implemented as expression template library that builds on
existing vector expression template libraries (ETL). The main features
of the **FDBB** library are:
* Unified API for [variables](#variables), [fluxes](#fluxes), and [flux-Jacobians](#flux-jacobians) for arbitrary ETLs
* Use of full functionality and computational efficiency (OpenMP/MPI, CUDA, OpenCL, etc.) of the underlying ETLs
* Minimal computational overhead
* Easy integration of new ETLs

**FDBB** currently supports the following ETLs and compilers (tested under Linux/macOS)

| ETL                                                                   | Remark                                 | Clang | GCC 4.9 | GCC 5.x | GCC 6.x | PGI 16.10 | OracleStudio 12.5 |
|-----------------------------------------------------------------------|----------------------------------------|-------|---------|---------|---------|-----------|-------------------|
| [Armadillo](http://arma.sourceforge.net)                              | not fully supported yet                |       |         |         |         |           |                   |
| [Arrayfire](http://arrayfire.com)                                     | version 3.3.x and better               | yes   | yes     | yes     | yes     |           |                   |
| [Blaze](https://bitbucket.org/blaze-lib/blaze)                        | version 3.3 and better                 | yes   | yes     | yes     | yes     |           |                   |
| [CMTL4](http://www.simunova.com/de/cmtl4)                             | not fully supported yet                |       |         |         |         |           |                   |
| [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)         | version 3.2.x and better with patching | yes   | yes     | yes     | yes     |           |                   |
| [IT++](http://itpp.sourceforge.net)                                   | version 4.3.1 with patching            | yes   | yes     | yes     | yes     |           |                   |
| [MTL4](http://www.simunova.com/de/mtl4)                               | not fully supported yet                |       |         |         |         |           |                   |
| [uBLAS](http://www.boost.org/doc/libs/1_60_0/libs/numeric/ublas/doc/) | not fully supported yet                |       |         |         |         |           |                   |
| [VexCL](https://github.com/ddemidov/vexcl)                            | developer version                      | yes   | yes     | yes     | yes     |           |                   |
| [ViennaCL](http://viennacl.sourceforge.net)                           | developer version (1.8.x)              | yes   | yes     | yes     | yes        |           |                   |


## <a name="prerequisites">Prerequisites</a>

The **FDBB** library makes extensively use of C++11 features (`auto`
keyword, variadic templates, etc.) so that a C++ compiler with full
C++11 support is a prerequisite. Some ETLs, like Blaze, even require
C++14 support. Beside that, additional prerequisites depend on the
specific ETL used.


## <a name="coding-style">Coding style</a>

The coding style of the **FDBB** library is based on [Google's C++
style guide](https://google.github.io/styleguide/cppguide.html). The
[ClangFormat](http://clang.llvm.org/docs/ClangFormat.html) tool is
used regularly to ensure a consistent code format.

## <a name="license">License</a>

The library is licensed under the [Mozilla Public License Version 2.0](https://www.mozilla.org/MPL/2.0).

The MPL is a simple copyleft license. The MPL's "file-level" copyleft
is designed to encourage contributors to share their modifications
with the library, while still allowing them to combine the library
with code under other licenses (open or proprietary) with minimal
restrictions.

___

# <a name="getting-started">Getting started</a>

## <a name="obtaining-the-latest-code">Obtaining the latest code</a>

*   On **Linux/MacOSX**, you may checkout the latest revision using
```shell
$ git clone https://gitlab.com/mmoelle1/FDBB.git FDBB
```
Once the initial checkout is complete you can keep the code up-to-date using
```shell
$ cd FDBB
$ git pull
```

*   On **Windows**, you can use any [git
client](https://git-scm.com/download/win) to checkout the **FDBB**
library and keep the code up-to-date, using the same URL as above.

## <a name="compiling-and-running-the-unittests">Compiling and running the UnitTests</a>

The **FDBB** library comes with unit tests for all supported ETLs. The
compilation requires configuration using [CMake](https://cmake.org) at
a new, empty folder (in-source builds are disabled).

*   On **Linux/MacOSX**, you need to create an empty `build` folder,
    execute CMake and start the compilation process inside that
    folder. If your source folder is `FDBB` then the sequence of commands

```shell
$ mkdir build
$ cd build
$ cmake ../path/to/FDBB -DFDBB_BUILD_UNITTESTS=ON -DFDBB_BUILD_UNITTESTS_<ETL>=ON -DFDBB_<FEATURE>=ON
 -- Build files have been written to: /path/to/build
$ make
 ...
 [100%] Built
```
  compiles the unit tests for the enabled ETLs and features. To see
  a complete list of supported ETLs and features run
```shell
$ mkdir build
$ cd build
$ ccmake ../path/to/FDBB
```
  and set/unset the configuration in the CMake GUI.

  After successful compilation executable unit tests are created at
  the `./unittests/<ETL>/` subdirectory of the build folder.

  All compiled unit tests can be run by executing
```shell
$ make test
```

  If Doxygen is available on your system, you can compile and open
  the Doxygen HTML pages by executing
```shell
$ cd build
$ make doc
 ...
  Built target doc
$ firefox doc/html/index.html
```

*   On **Windows**, you need to run the cmake-gui tool to generate a
    project file and open it with your IDE (e.g., [Visual
    Studio](https://www.visualstudio.com)). Please consider the
    documentation of your IDE how to compile and run executables.

### Example: Compiling UnitTests for VexCL

To compile the unit tests for the VexCL library under Linux/MacOSX you
need to call CMake as follows:
```shell
$ mkdir build
$ cd build
$ cmake ../path/to/FDBB -DFDBB_BUILD_UNITTESTS=ON -DFDBB_BUILD_UNITTESTS_VEXCL=ON -DFDBB_WITH_OCL=ON
  ...
  Options:
  FDBB_BUILD_TESTS...................: OFF
  FDBB_BUILD_UNITTESTS...............: ON

  Features:
  FDBB_CXX_STANDARD..................: 14
  FDBB_WITH_CUDA.....................: OFF
  FDBB_WITH_MIC......................: OFF
  FDBB_WITH_OCL......................: ON
  FDBB_WITH_OMP......................: OFF

  UnitTests:
  FDBB_BUILD_UNITTESTS_DEVICES.......: CPU
  FDBB_BUILD_UNITTESTS_PERFMODE......: OFF
  FDBB_BUILD_UNITTESTS_PRECISIONS....: ALL
  FDBB_BUILD_UNITTESTS_SIZES.........: 1000
  FDBB_BUILD_UNITTESTS_RUNS..........: 1
  FDBB_BUILD_UNITTESTS_TESTS.........: ALL

  FDBB_BUILD_UNITTESTS_ARMADILLO.....: OFF
  FDBB_BUILD_UNITTESTS_ARRAYFIRE.....: OFF
  FDBB_BUILD_UNITTESTS_BLAZE.........: OFF
  FDBB_BUILD_UNITTESTS_CMTL4.........: OFF
  FDBB_BUILD_UNITTESTS_EIGEN.........: OFF
  FDBB_BUILD_UNITTESTS_ITPP..........: OFF
  FDBB_BUILD_UNITTESTS_MTL4..........: OFF
  FDBB_BUILD_UNITTESTS_UBLAS.........: OFF
  FDBB_BUILD_UNITTESTS_VEXCL.........: ON
  FDBB_BUILD_UNITTESTS_VIENNACL......: OFF
  ...
 -- Build files have been written to: /path/to/build
$ make vexcl-unittest
 ...
 [100%] Built
$ make test
  Running tests...
  Test project /path/to/build
      Start 1: vexcl-unittest
      1/1 Test #1: vexcl-unittest ...............   Passed    0.02 sec

  100% tests passed, 0 tests failed out of 1

  Total Test time (real) =   0.03 sec
```

### Example: Compiling selection of UnitTests

It is also possible to specify a selection of unit tests that should
be run by specifying a list/file patterns of tests to be included and
a list/file pattern of tests to be excluded.

To perform all unit tests for element-wise operations except for the
power function you need to call CMake as follows:
```shell
$ mkdir build
$ cd build
$ cmake ../path/to/FDBB -DFDBB_BUILD_UNITTESTS=ON -DFDBB_BUILD_UNITTESTS_VEXCL=ON -DFDBB_WITH_OCL=ON \
        -DFDBB_UNITTESTS_TESTS=ALL \
        -DFDBB_UNITTESTS_TESTS_INCLUDE="test_op_elem_*" \
        -DFDBB_UNITTESTS_TESTS_EXCLUDE="test_op_elem_pow"
```

## <a name="including-fdbb-into-own-codes">Including FDBB into own codes</a>

The **FDBB** library is included in your program by including the
header file
```cpp
#include "fdbb.h"
```
By default, **FDBB** assumes
C++14 support enabled and all accelerators and parallelization
techniques disabled. This behavior can be changed by enabled one or
more of the following flags when compiling your program:
*   C++11 support:     `-DFDBB_WITH_CXX11`
*   CUDA support:      `-DFDBB_WITH_CUDA`
*   Intel MIC support: `-DFDBB_WITH_MIC`
*   OpenCL support:    `-DFDBB_WITH_OCL`
*   OpenMP support:    `-DFDBB_WITH_OMP`

___

# <a name="core-components">Core components</a>

The **FDBB** library provides the following core components:
*   [equation of states (EOS)](#equation-of-state)
*   [primary and secondary variables](#variables)
*   [inviscid and viscous fluxes](#fluxes)
*   [flux-Jacobians for inviscid and viscous fluxes](#flux-jacobians)

Core components are realized as `struct`'s with `static` member
functions that realize the evaluation of [variables](#variables),
[fluxes](#fluxes), and [flux-Jacobians](#flux-jacobians).

## <a name="equations-of-state">Equations of state</a>

The **FDBB** library support the following [equations of
state](https://en.wikipedia.org/wiki/Equation_of_state) (EOS)
implemented in the file `fdbbEOS.hpp`:
*   [ideal gas law](https://en.wikipedia.org/wiki/Equation_of_state#Classical_ideal_gas_law)

Further EOS are planned in an upcoming release making use of the [CoolProp](http://www.coolprop.org) project.

### Example: EOS for an ideal gas

The strucutre `fdbb::EOSidealGas` implements the EOS for an ideal gas
with specific heat at constant volume \f$c_v=\frac{7}{2}R\f$ and
specific heat at constant pressure \f$c_p=\frac{5}{2}R\f$ with
\f$R=8.3144598(48)\, \frac{J}{mol\cdot K}\f$ being the ideal gas
constant. The absolute pressure can then be computed by passing
density \f$\rho\f$ and internal energy per unit mass \f$e\f$

```cpp
// Equation of state for ideal gas (rho,e)-variant
typedef fdbb::EOSidealGas<double,
                          std::ratio<7,2>,
                          std::ratio<5,2> > eos;

// Compute absolute pressure from density and internal energy per unit mass
auto p = eos::p(rho, e);
```

### Example: user-defined EOS

To create a user-defined EOS, you have to implement a new `struct`
derived from the base class `fdbb::EOS_pVT`, which represents an
EOS of the form \f$f(p,V,T)=0\f$ with absolute pressure \f$p\f$,
volume \f$V\f$, and absolute temperature \f$T\f$

The base class is used internally to detect the interface of the
`eos::p` function that is used during the computation of
[fluxes](#fluxes) and [flux-Jacobians](#flux-jacobians).

```cpp
// Equation of state with user-defined calculate_pressure functions
struct fdbbEOS_new : public fdbbEOS_pVT_rhoe
{
    // Return pressure computed from density and energy
    template<class Td, class Te>
    static inline auto p(const Trho& rho, const Te& e)
    -> decltype(calculate_p(rho,e))
    {
        return calculate_p(rho,e);
    }

    // Print information about EOS
    static std::ostream& print(std::ostream& os)
    {
        os << "User-defined EOS of the form p=p(d,e)\n";
        return os;
    }
}
```

## <a name="variables">Variables</a>

The **FDBB** library supports the following variables in one, two and three spatial dimensions:
* conservative variables
* primitive variables

The `struct` `fdbb::Variables` provides `static` member functions
to evaluate both primary and secondary variables. The table below
gives an overview of available member functions

| Variable name                     | Symbol                      | Function name | SI base unit                          | SI derived units          |
|-----------------------------------|-----------------------------|---------------|---------------------------------------|---------------------------|
| (volumetric mass) density         | \f$\rho\f$                  | `rho`         | \f$\rm kg \cdot m^{-3}\f$             |                           |
| velocity                          | \f$\mathbf{v}\f$            | `v<idim>`     | \f$\rm m \cdot s^{-1}\f$              |                           |
| velocity magnitude                | \f$\|\mathbf{v}\|\f$        | `v_mag`       | \f$\rm m \cdot s^{-1}\f$              |                           |
| velocity magnitude squared        | \f$\|\mathbf{v}\|^2\f$      | `v_mag2`      | \f$\rm m \cdot s^{-1}\f$              |                           |
| momentum                          | \f$\rho \mathbf{v}\f$       | `rhov<idim>`  | \f$\rm kg \cdot m \cdot s^{-1}\f$     |                           |
| momentum magnitude                | \f$\|\rho \mathbf{v}\|\f$   | `rhov_mag`    | \f$\rm kg \cdot m \cdot s^{-1}\f$     |                           |
| momentum magnitude squared        | \f$\|\rho \mathbf{v}\|^2\f$ | `rhov_mag2`   | \f$\rm kg \cdot m \cdot s^{-1}\f$     |                           |
| total energy per unit volume      | \f$\rho E\f$                | `rhoE`        | \f$\rm m^{2} \cdot kg \cdot s^{-2}\f$ | \f$\rm J\f$               |
| total energy per unit mass        | \f$E\f$                     | `E`           | \f$\rm m^{2} \cdot s^{-2}\f$          | \f$\rm J \cdot kg^{-1}\f$ |
| internal energy per unit volume   | \f$\rho e\f$                | `rhoe`        | \f$\rm m^{2} \cdot kg \cdot s^{-2}\f$ | \f$\rm J\f$               |
| internal energy per unit mass     | \f$e\f$                     | `e`           | \f$\rm m^{2} \cdot s^{-2}\f$          | \f$\rm J \cdot kg^{-1}\f$ |
| kinetic energy per unit volume    | \f$\rho E_{\rm kin}\f$      | `rhoE_kin`    | \f$\rm m^{2} \cdot kg \cdot s^{-2}\f$ | \f$\rm J\f$               |
| kinetic energy per unit mass      | \f$E_{\rm kin}\f$           | `E_kin`       | \f$\rm m^{2} \cdot s^{-2}\f$          | \f$\rm J \cdot kg^{-1}\f$ |
| total enthalpy per unit volume    | \f$\rho H\f$                | `rhoH`        | \f$\rm m^{2} \cdot kg \cdot s^{-2}\f$ | \f$\rm J\f$               |
| total enthalpy per unit mass      | \f$H\f$                     | `H`           | \f$\rm m^{2} \cdot s^{-2}\f$          | \f$\rm J \cdot kg^{-1}\f$ |
| internal enthalpy per unit volume | \f$\rho h\f$                | `rhoh`        | \f$\rm m^{2} \cdot kg \cdot s^{-2}\f$ | \f$\rm J\f$               |
| internal enthalpy per unit mass   | \f$h\f$                     | `h`           | \f$\rm m^{2} \cdot s^{-2}\f$          | \f$\rm J \cdot kg^{-1}\f$ |

### Example: conservative variables

```cpp
// VexCL context
vex::Context ctx( vex::Filter::CPU && vex::Filter::DoublePrecision );

// Vector length
std::size_t N = 10;

// Conservative variables in 1D
vex::vector<double> u0(ctx, N); // density
vex::vector<double> u1(ctx, N); // momentum
vex::vector<double> u2(ctx, N); // total energy

// Initialization
u0 = 2.0;
u1 = 1.0;
u2 = 5.0;

// Conservative variables in 1D
typedef fdbb::Variables<eos, 1, fdbb::EnumForm::conservative> variables;

// Primary variables
auto d = variables::rho    (u0, u1, u2);
auto m = variables::rhov<0>(u0, u1, u2);
auto E = variables::rhoE   (u0, u1, u2);

// Secondary variables
auto v = variables::v<0>   (u0, u1, u2);
```

## <a name="fluxes">Fluxes</a>

tba

## <a name="flux-jacobians">Flux-Jacobians</a>

tba

___

# <a name="the-plain-program">The plain program</a>

```cpp
#include <vector>
#include <vexcl/vexcl.hpp>

#include "fdbb.h"

// VexCL context
vex::Context ctx( vex::Filter::CPU && vex::Filter::DoublePrecision );

// Vector length
std::size_t N = 10;

// Conservative variables in 1D
vex::vector<double> u0(ctx, N); // density
vex::vector<double> u1(ctx, N); // momentum
vex::vector<double> u2(ctx, N); // total energy 

// Initialization
u0 = 2.0;
u1 = 1.0;
u2 = 5.0;

// Equation of state for ideal gas (rho,e)-variant
typedef fdbb::EOSidealGas<double,
                          std::ratio<7,2>,
                          std::ratio<5,2> > eos;

// Conservative variables in 1D
typedef fdbb::Variables<eos, 1, fdbb::EnumForm::conservative> variables;

// Primary variables
auto d = variables::rho    (u0, u1, u2);
auto m = variables::rhov<0>(u0, u1, u2);
auto E = variables::rhoE   (u0, u1, u2);

// Secondary variables
auto v = variables::v<0>(u0, u1, u2);
auto e = vairable::e    (u0, u1, u2);

// Compute absolute pressure from density and internal energy
auto p = eos::p (d, e);
```

---

This project has received funding from the European's Horizon 2020
research and innovation programme under grant agreement No 678727,
project [MOTOR](https://motor-project.eu) (Multi-ObjecTive design
Optimization of fluid eneRgy machines).

___

Author: Matthias Möller
