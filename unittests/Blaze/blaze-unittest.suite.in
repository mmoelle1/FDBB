#if (@SIZE@ <= 128)
SUITE(suite_static_@SUITE_SPEC@)
{
  typedef FixtureBlaze<@SIZE@,
                       @PRECISION@,
                       blaze::StaticVector<@PRECISION@,@SIZE@>,
                       blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > Fixture;

#define FDBB_FIXTURE Fixture

  typedef BlockExpressionFixtureBlaze<@SIZE@,
                                      @PRECISION@,
                                      blaze::StaticVector<@PRECISION@,@SIZE@>,
                                      blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > BlockExpressionFixture;

#define FDBB_BLOCKEXPRESSION_FIXTURE BlockExpressionFixture

  typedef ConservativeFixture1dBlaze<@SIZE@,
                                     @PRECISION@,
                                     blaze::StaticVector<@PRECISION@,@SIZE@>,
                                     blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > ConservativeFixture1d;

#define FDBB_CONSERVATIVE_FIXTURE_1D ConservativeFixture1d

  typedef ConservativeFixture2dBlaze<@SIZE@,
                                     @PRECISION@,
                                     blaze::StaticVector<@PRECISION@,@SIZE@>,
                                     blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > ConservativeFixture2d;

#define FDBB_CONSERVATIVE_FIXTURE_2D ConservativeFixture2d

  typedef ConservativeFixture3dBlaze<@SIZE@,
                                     @PRECISION@,
                                     blaze::StaticVector<@PRECISION@,@SIZE@>,
                                     blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > ConservativeFixture3d;

#define FDBB_CONSERVATIVE_FIXTURE_3D ConservativeFixture3d

  typedef PrimitiveFixture1dBlaze<@SIZE@,
                                  @PRECISION@,
                                  blaze::StaticVector<@PRECISION@,@SIZE@>,
                                  blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > PrimitiveFixture1d;

#define FDBB_PRIMITIVE_FIXTURE_1D PrimitiveFixture1d

  typedef PrimitiveFixture2dBlaze<@SIZE@,
                                  @PRECISION@,
                                  blaze::StaticVector<@PRECISION@,@SIZE@>,
                                  blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > PrimitiveFixture2d;

#define FDBB_PRIMITIVE_FIXTURE_2D PrimitiveFixture2d

  typedef PrimitiveFixture3dBlaze<@SIZE@,
                                  @PRECISION@,
                                  blaze::StaticVector<@PRECISION@,@SIZE@>,
                                  blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > PrimitiveFixture3d;

#define FDBB_PRIMITIVE_FIXTURE_3D PrimitiveFixture3d

  typedef RiemannFixture1dBlaze<@SIZE@,
                                @PRECISION@,
                                blaze::StaticVector<@PRECISION@,@SIZE@>,
                                blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > RiemannFixture1d;

#define FDBB_RIEMANN_FIXTURE_1D RiemannFixture1d

  typedef RiemannFixture2dBlaze<@SIZE@,
                                @PRECISION@,
                                blaze::StaticVector<@PRECISION@,@SIZE@>,
                                blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > RiemannFixture2d;

#define FDBB_RIEMANN_FIXTURE_2D RiemannFixture2d

  typedef RiemannFixture3dBlaze<@SIZE@,
                                @PRECISION@,
                                blaze::StaticVector<@PRECISION@,@SIZE@>,
                                blaze::StaticMatrix<@PRECISION@,@SIZE@,@SIZE@> > RiemannFixture3d;

#define FDBB_RIEMANN_FIXTURE_3D RiemannFixture3d

  // Define macro to count the number of arguments
#define NARGS(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value

  // Define timer macros
#ifdef ENABLE_PERFMODE

#define TEST_INIT(flops,flops_size,memops,memops_size)                  \
  PerfCounter<NARGS(flops),NARGS(memops)>                               \
    perfCounter({flops},{flops_size},{memops},{memops_size},@RUNS@,100)
#define TEST_START(...)  perfCounter.start()
#define TEST_STOP(...)   perfCounter.stop()
#define TEST_TIMER(...)  perfCounter
#define TEST_RUNS(...)   perfCounter.getRuns()
#define TEST_REPORT(...) perfCounter.print(std::clog,                   \
                                           "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#else

#define TEST_INIT(...)   Timer timer(@RUNS@,100)
#define TEST_START(...) timer.start()
#define TEST_STOP(...)   timer.stop()
#define TEST_TIMER(...)  timer
#define TEST_RUNS(...)   timer.getRuns()
#define TEST_REPORT(...) timer.print(std::clog,                         \
                                     "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#endif

  // Include unittest header files
  @INCLUDE_UNITTESTS_HEADERS@

    // Undefine timer macros
#undef TEST_INIT
#undef TEST_START
#undef TEST_STOP
#undef TEST_TIMER
#undef TEST_RUNS
#undef TEST_REPORT
#undef NARGS

    } // suite_static_@SUITE_SPEC@
#endif

SUITE(suite_dynamic_@SUITE_SPEC@)
{
  typedef FixtureBlaze<@SIZE@,
                       @PRECISION@,
                       blaze::DynamicVector<@PRECISION@>,
                       blaze::DynamicMatrix<@PRECISION@> > Fixture;

#define FDBB_FIXTURE Fixture

  typedef BlockExpressionFixtureBlaze<@SIZE@,
                                      @PRECISION@,
                                      blaze::DynamicVector<@PRECISION@>,
                                      blaze::DynamicMatrix<@PRECISION@> > BlockExpressionFixture;

#define FDBB_BLOCKEXPRESSION_FIXTURE BlockExpressionFixture

  typedef ConservativeFixture1dBlaze<@SIZE@,
                                     @PRECISION@,
                                     blaze::DynamicVector<@PRECISION@>,
                                     blaze::DynamicMatrix<@PRECISION@> > ConservativeFixture1d;
    
#define FDBB_CONSERVATIVE_FIXTURE_1D ConservativeFixture1d

  typedef ConservativeFixture2dBlaze<@SIZE@,
                                     @PRECISION@,
                                     blaze::DynamicVector<@PRECISION@>,
                                     blaze::DynamicMatrix<@PRECISION@> > ConservativeFixture2d;
    
#define FDBB_CONSERVATIVE_FIXTURE_2D ConservativeFixture2d

  typedef ConservativeFixture3dBlaze<@SIZE@,
                                     @PRECISION@,
                                     blaze::DynamicVector<@PRECISION@>,
                                     blaze::DynamicMatrix<@PRECISION@> > ConservativeFixture3d;

#define FDBB_CONSERVATIVE_FIXTURE_3D ConservativeFixture3d

  typedef PrimitiveFixture1dBlaze<@SIZE@,
                                  @PRECISION@,
                                  blaze::DynamicVector<@PRECISION@>,
                                  blaze::DynamicMatrix<@PRECISION@> > PrimitiveFixture1d;
    
#define FDBB_PRIMITIVE_FIXTURE_1D PrimitiveFixture1d

  typedef PrimitiveFixture2dBlaze<@SIZE@,
                                  @PRECISION@,
                                  blaze::DynamicVector<@PRECISION@>,
                                  blaze::DynamicMatrix<@PRECISION@> > PrimitiveFixture2d;
    
#define FDBB_PRIMITIVE_FIXTURE_2D PrimitiveFixture2d

  typedef PrimitiveFixture3dBlaze<@SIZE@,
                                  @PRECISION@,
                                  blaze::DynamicVector<@PRECISION@>,
                                  blaze::DynamicMatrix<@PRECISION@> > PrimitiveFixture3d;

#define FDBB_PRIMITIVE_FIXTURE_3D PrimitiveFixture3d

  typedef RiemannFixture1dBlaze<@SIZE@,
                                @PRECISION@,
                                blaze::DynamicVector<@PRECISION@>,
                                blaze::DynamicMatrix<@PRECISION@> > RiemannFixture1d;
    
#define FDBB_RIEMANN_FIXTURE_1D RiemannFixture1d

  typedef RiemannFixture2dBlaze<@SIZE@,
                                @PRECISION@,
                                blaze::DynamicVector<@PRECISION@>,
                                blaze::DynamicMatrix<@PRECISION@> > RiemannFixture2d;
    
#define FDBB_RIEMANN_FIXTURE_2D RiemannFixture2d

  typedef RiemannFixture3dBlaze<@SIZE@,
                                @PRECISION@,
                                blaze::DynamicVector<@PRECISION@>,
                                blaze::DynamicMatrix<@PRECISION@> > RiemannFixture3d;

#define FDBB_RIEMANN_FIXTURE_3D RiemannFixture3d

  // Define macro to count the number of arguments
#define NARGS(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value

  // Define timer macros
#ifdef ENABLE_PERFMODE

#define TEST_INIT(flops,flops_size,memops,memops_size)                  \
  PerfCounter<NARGS(flops),NARGS(memops)>                               \
    perfCounter({flops},{flops_size},{memops},{memops_size},@RUNS@,100)
#define TEST_START(...)  perfCounter.start()
#define TEST_STOP(...)   perfCounter.stop()
#define TEST_TIMER(...)  perfCounter
#define TEST_RUNS(...)   perfCounter.getRuns()
#define TEST_REPORT(...) perfCounter.print(std::clog,                   \
                                           "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#else

#define TEST_INIT(...)   Timer timer(@RUNS@,100)
#define TEST_START(...)  timer.start()
#define TEST_STOP(...)   timer.stop()
#define TEST_TIMER(...)  timer
#define TEST_RUNS(...)   timer.getRuns()
#define TEST_REPORT(...) timer.print(std::clog,                         \
                                     "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#endif

  // Include test header files
  @INCLUDE_UNITTESTS_HEADERS@

    // Undefine timer macros
#undef TEST_INIT
#undef TEST_START
#undef TEST_STOP
#undef TEST_TIMER
#undef TEST_RUNS
#undef TEST_REPORT
#undef NARGS

    } // suite_dynamic_@SUITE_SPEC@
