SUITE(suite_col_@SUITE_SPEC@)
{
  typedef FixtureArmadillo<@SIZE@,
                           @PRECISION@,
                           arma::Col<@PRECISION@>,
                           arma::Mat<@PRECISION@> > Fixture;

#define FDBB_FIXTURE Fixture

  typedef BlockExpressionFixtureArmadillo<@SIZE@,
                                          @PRECISION@,
                                          arma::Col<@PRECISION@>,
                                          arma::Mat<@PRECISION@> > BlockExpressionFixture;
        
#define FDBB_BLOCKEXPRESSION_FIXTURE BlockExpressionFixture
        
  typedef ConservativeFixture1dArmadillo<@SIZE@,
                                         @PRECISION@,
                                         arma::Col<@PRECISION@>,
                                         arma::Mat<@PRECISION@> > ConservativeFixture1d;
        
#define FDBB_CONSERVATIVE_FIXTURE_1D ConservativeFixture1d

  typedef ConservativeFixture2dArmadillo<@SIZE@,
                                         @PRECISION@,
                                         arma::Col<@PRECISION@>,
                                         arma::Mat<@PRECISION@> > ConservativeFixture2d;

#define FDBB_CONSERVATIVE_FIXTURE_2D ConservativeFixture2d

  typedef ConservativeFixture3dArmadillo<@SIZE@,
                                         @PRECISION@,
                                         arma::Col<@PRECISION@>,
                                         arma::Mat<@PRECISION@> > ConservativeFixture3d;
    
#define FDBB_CONSERVATIVE_FIXTURE_3D ConservativeFixture3d

  typedef PrimitiveFixture1dArmadillo<@SIZE@,
                                      @PRECISION@,
                                      arma::Col<@PRECISION@>,
                                      arma::Mat<@PRECISION@> > PrimitiveFixture1d;
    
#define FDBB_PRIMITIVE_FIXTURE_1D PrimitiveFixture1d

  typedef PrimitiveFixture2dArmadillo<@SIZE@,
                                      @PRECISION@,
                                      arma::Col<@PRECISION@>,
                                      arma::Mat<@PRECISION@> > PrimitiveFixture2d;

#define FDBB_PRIMITIVE_FIXTURE_2D PrimitiveFixture2d

  typedef PrimitiveFixture3dArmadillo<@SIZE@,
                                      @PRECISION@,
                                      arma::Col<@PRECISION@>,
                                      arma::Mat<@PRECISION@> > PrimitiveFixture3d;
    
#define FDBB_PRIMITIVE_FIXTURE_3D PrimitiveFixture3d

  typedef RiemannFixture1dArmadillo<@SIZE@,
                                    @PRECISION@,
                                    arma::Col<@PRECISION@>,
                                    arma::Mat<@PRECISION@> > RiemannFixture1d;
    
#define FDBB_RIEMANN_FIXTURE_1D RiemannFixture1d

  typedef RiemannFixture2dArmadillo<@SIZE@,
                                    @PRECISION@,
                                    arma::Col<@PRECISION@>,
                                    arma::Mat<@PRECISION@> > RiemannFixture2d;

#define FDBB_RIEMANN_FIXTURE_2D RiemannFixture2d

  typedef RiemannFixture3dArmadillo<@SIZE@,
                                    @PRECISION@,
                                    arma::Col<@PRECISION@>,
                                    arma::Mat<@PRECISION@> > RiemannFixture3d;
    
#define FDBB_RIEMANN_FIXTURE_3D RiemannFixture3d

  // Define macro to count the number of arguments
#define NARGS(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value

  // Define timer macros
#ifdef ENABLE_PERFMODE

#define TEST_INIT(flops,flops_size,memops,memops_size)                  \
  PerfCounter<NARGS(flops),NARGS(memops)>                               \
    perfCounter({flops},{flops_size},{memops},{memops_size},@RUNS@,100)
#define TEST_START(...)  perfCounter.start()
#define TEST_STOP(...)   perfCounter.stop()
#define TEST_TIMER(...)  perfCounter
#define TEST_RUNS(...)   perfCounter.getRuns()
#define TEST_REPORT(...) perfCounter.print(std::clog,                   \
                                           "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#else

#define TEST_INIT(...)   Timer timer(@RUNS@,100)
#define TEST_START(...)  timer.start()
#define TEST_STOP(...)   timer.stop()
#define TEST_TIMER(...)  timer
#define TEST_RUNS(...)   timer.getRuns()
#define TEST_REPORT(...) timer.print(std::clog,                         \
                                     "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#endif

  // Include unittest header files
  @INCLUDE_UNITTESTS_HEADERS@

    // Undefine timer macros
#undef TEST_INIT
#undef TEST_START
#undef TEST_STOP
#undef TEST_TIMER
#undef TEST_RUNS
#undef TEST_REPORT
#undef NARGS

    } // suite_col_@SUITE_SPEC@

SUITE(suite_row_@SUITE_SPEC@)
{
  typedef FixtureArmadillo<@SIZE@,
                           @PRECISION@,
                           arma::Row<@PRECISION@>,
                           arma::Mat<@PRECISION@> > Fixture;

#define FDBB_FIXTURE Fixture

  typedef BlockExpressionFixtureArmadillo<@SIZE@,
                                          @PRECISION@,
                                          arma::Row<@PRECISION@>,
                                          arma::Mat<@PRECISION@> > BlockExpressionFixture;

#define FDBB_BLOCKEXPRESSION_FIXTURE BlockExpressionFixture

  typedef ConservativeFixture1dArmadillo<@SIZE@,
                                         @PRECISION@,
                                         arma::Row<@PRECISION@>,
                                         arma::Mat<@PRECISION@> > ConservativeFixture1d;

#define FDBB_CONSERVATIVE_FIXTURE_1D ConservativeFixture1d

  typedef ConservativeFixture2dArmadillo<@SIZE@,
                                         @PRECISION@,
                                         arma::Row<@PRECISION@>,
                                         arma::Mat<@PRECISION@> > ConservativeFixture2d;
    
#define FDBB_CONSERVATIVE_FIXTURE_2D ConservativeFixture2d

  typedef ConservativeFixture3dArmadillo<@SIZE@,
                                         @PRECISION@,
                                         arma::Row<@PRECISION@>,
                                         arma::Mat<@PRECISION@> > ConservativeFixture3d;

#define FDBB_CONSERVATIVE_FIXTURE_3D ConservativeFixture3d

  typedef PrimitiveFixture1dArmadillo<@SIZE@,
                                      @PRECISION@,
                                      arma::Row<@PRECISION@>,
                                      arma::Mat<@PRECISION@> > PrimitiveFixture1d;

#define FDBB_PRIMITIVE_FIXTURE_1D PrimitiveFixture1d

  typedef PrimitiveFixture2dArmadillo<@SIZE@,
                                      @PRECISION@,
                                      arma::Row<@PRECISION@>,
                                      arma::Mat<@PRECISION@> > PrimitiveFixture2d;
    
#define FDBB_PRIMITIVE_FIXTURE_2D PrimitiveFixture2d

  typedef PrimitiveFixture3dArmadillo<@SIZE@,
                                      @PRECISION@,
                                      arma::Row<@PRECISION@>,
                                      arma::Mat<@PRECISION@> > PrimitiveFixture3d;

#define FDBB_PRIMITIVE_FIXTURE_3D PrimitiveFixture3d

  typedef RiemannFixture1dArmadillo<@SIZE@,
                                    @PRECISION@,
                                    arma::Row<@PRECISION@>,
                                    arma::Mat<@PRECISION@> > RiemannFixture1d;

#define FDBB_RIEMANN_FIXTURE_1D RiemannFixture1d

  typedef RiemannFixture2dArmadillo<@SIZE@,
                                    @PRECISION@,
                                    arma::Row<@PRECISION@>,
                                    arma::Mat<@PRECISION@> > RiemannFixture2d;
    
#define FDBB_RIEMANN_FIXTURE_2D RiemannFixture2d

  typedef RiemannFixture3dArmadillo<@SIZE@,
                                    @PRECISION@,
                                    arma::Row<@PRECISION@>,
                                    arma::Mat<@PRECISION@> > RiemannFixture3d;

#define FDBB_RIEMANN_FIXTURE_3D RiemannFixture3d

  // Define macro to count the number of arguments
#define NARGS(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value

  // Define timer macros
#ifdef ENABLE_PERFMODE

#define TEST_INIT(flops,flops_size,memops,memops_size)                  \
  PerfCounter<NARGS(flops),NARGS(memops)>                               \
    perfCounter({flops},{flops_size},{memops},{memops_size},@RUNS@,100)
#define TEST_START(...)  perfCounter.start()
#define TEST_STOP(...)   perfCounter.stop()
#define TEST_TIMER(...)  perfCounter
#define TEST_RUNS(...)   perfCounter.getRuns()
#define TEST_REPORT(...) perfCounter.print(std::clog,                   \
                                           "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#else

#define TEST_INIT(...)   Timer timer(@RUNS@,100)
#define TEST_START(...)  timer.start()
#define TEST_STOP(...)   timer.stop()
#define TEST_TIMER(...)  timer
#define TEST_RUNS(...)   timer.getRuns()
#define TEST_REPORT(...) timer.print(std::clog,                         \
                                     "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#endif

  // Include unittest header files
  @INCLUDE_UNITTESTS_HEADERS@

    // Undefine timer macros
#undef TEST_INIT
#undef TEST_START
#undef TEST_STOP
#undef TEST_TIMER
#undef TEST_RUNS
#undef TEST_REPORT
#undef NARGS

    } // suite_row_@SUITE_SPEC@
