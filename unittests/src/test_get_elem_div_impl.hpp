/** @file test_get_elem_div_impl.hpp
 *
 *  @brief UnitTests++ get_elem_div_impl test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test get_elem_div_impl indicator
 */
TEST_FIXTURE(FDBB_FIXTURE, get_elem_div_impl)
{
  CHECK(check_get_elem_div_impl(result_d, result_d));
}
