/** @file UnitTests.hpp
 *
 *  @brief UnitTest++ interface of the Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#pragma once
#ifndef FDBB_UNITTESTS_HPP
#define FDBB_UNITTESTS_HPP

#include <array>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include "fdbb.h"

/**
 * @brief
 * High-resolution wallclock timer
 */
struct Timer
{
public:
  /**
   * @brief
   * Constructor
   */
  Timer(std::size_t runs = 1, std::size_t threshold = 1)
    : runs(runs)
    , threshold(threshold)
    , tstart(std::chrono::high_resolution_clock::now())
    , tstop(std::chrono::high_resolution_clock::now())
  {}

  /**
   * @brief
   * Start time measurement
   */
  void start() { tstart = std::chrono::high_resolution_clock::now(); }

  /**
   * @brief
   * Stop time measurement
   */
  void stop() { tstop = std::chrono::high_resolution_clock::now(); }

  /**
   * @brief
   * Set number of runs
   */
  void setRuns(std::size_t runs) { this->runs = runs; }

  /**
   * @brief
   * Get number of runs
   */
  std::size_t getRuns() const { return runs; }

  /**
   * @brief
   * Set threshold for time unit conversion
   */
  void setThreshold(std::size_t threshold) { this->threshold = threshold; }

  /**
   * @brief
   * Get threshold for time unit conversion
   */
  std::size_t getThreshold() const { return threshold; }

  /**
   * @brief
   * Print elapsed time
   */
  std::ostream& print(std::ostream& os,
                      const std::string& prefix = "",
                      const std::string& suffix = "") const
  {
    os << prefix << "Elapsed time: ";
    if (std::chrono::duration_cast<std::chrono::seconds>(tstop - tstart)
          .count() > threshold)
      os << std::chrono::duration_cast<std::chrono::seconds>(tstop - tstart)
              .count()
         << " sec";
    else if (std::chrono::duration_cast<std::chrono::milliseconds>(tstop -
                                                                   tstart)
               .count() > threshold)
      os << std::chrono::duration_cast<std::chrono::milliseconds>(tstop -
                                                                  tstart)
              .count()
         << " millisec";
    else if (std::chrono::duration_cast<std::chrono::microseconds>(tstop -
                                                                   tstart)
               .count() > threshold)
      os << std::chrono::duration_cast<std::chrono::microseconds>(tstop -
                                                                  tstart)
              .count()
         << " microsec";
    else
      os << std::chrono::duration_cast<std::chrono::nanoseconds>(tstop - tstart)
              .count()
         << " nanosec";
    return (os << suffix);
  }

protected:
  // Starting time
  std::chrono::high_resolution_clock::time_point tstart;

  // Stopping time
  std::chrono::high_resolution_clock::time_point tstop;

  // Number of runs
  std::size_t runs;

  // Threshold
  std::size_t threshold;
};

/**
 * @brief
 * Print Timer to string
 */
std::ostream&
operator<<(std::ostream& os, const Timer& timer)
{
  return timer.print(os);
}

/**
 * @brief
 * High-resolution performance counter
 */
template<std::size_t FLOPS_args, std::size_t MEMOPS_args>
struct PerfCounter : public Timer
{
public:
  /**
   * @brief
   * Constructor
   */
  PerfCounter(std::array<std::size_t, FLOPS_args> flops = { 1 },
              std::array<std::size_t, FLOPS_args> flops_size = { 1 },
              std::array<std::size_t, MEMOPS_args> memops = { 1 },
              std::array<std::size_t, MEMOPS_args> memops_size = { 1 },
              std::size_t runs = 1,
              std::size_t threshold = 1)
    : Timer(runs, threshold)
    , flops(flops)
    , flops_size(flops_size)
    , memops(memops)
    , memops_size(memops_size)
  {}

  /**
   * @brief
   * Set array with floating-point operation numbers
   */
  void setFlops(std::array<std::size_t, FLOPS_args>&& flops)
  {
    this->flops = flops;
  }

  /**
   * @brief
   * Get reference to array with floating-point operation numbers
   */
  std::array<std::size_t, FLOPS_args>& getFlops() const { return &flops; }

  /**
   * @brief
   * Set array with lengths of data arrays for floating-point operations
   */
  void setFlopsSize(std::array<std::size_t, FLOPS_args>&& flops_size)
  {
    this->flops_size = flops_size;
  }

  /**
   * @brief
   * Get reference to array with length of data arrays for floating-point
   * operations
   */
  std::array<std::size_t, FLOPS_args>& getFlopsSize() const
  {
    return &flops_size;
  }

  /**
   * @brief
   * Set arrays with floating-point operation numbers and length of data arrays
   */
  void setFlopsAll(std::array<std::size_t, FLOPS_args>&& flops,
                   std::array<std::size_t, FLOPS_args>&& flops_size)
  {
    this->flops = flops;
    this->flops_size = flops_size;
  }

  /**
   * @brief
   * Set array with memory-transfer operation numbers
   */
  void setMemops(std::array<std::size_t, MEMOPS_args>&& memops)
  {
    this->memops = memops;
  }

  /**
   * @brief
   * Get reference to array with memory-transfer operation numbers
   */
  std::array<std::size_t, MEMOPS_args>& getMemops() const { return &memops; }

  /**
   * @brief
   * Set array with lengths of data arrays for memory-transfers
   */
  void setMemopsSize(std::array<std::size_t, MEMOPS_args>&& memops_size)
  {
    this->memops_size = memops_size;
  }

  /**
   * @brief
   * Get reference to array with length of data arrays for memory-transfer
   */
  std::array<std::size_t, MEMOPS_args>& getMemopsSize() const
  {
    return &memops_size;
  }

  /**
   * @brief
   * Set arrays with memory-transfer operation numbers and lengths of data
   * arrays
   */
  void setMemopsAll(std::array<std::size_t, MEMOPS_args>&& memops,
                    std::array<std::size_t, MEMOPS_args>&& memops_size)
  {
    this->memops = memops;
    this->memops_size = memops_size;
  }

  /**
   * @brief
   * Print information about the performance counter
   */
  void info()
  {
    std::cout << setw() << "FLOPs" << setw() << "Length"
              << " | " << setw() << "MEMOPs" << setw() << "Length" << std::endl;
    for (auto i = 0; i < std::max(FLOPS_args, MEMOPS_args); i++) {
      if (FLOPS_args <= i)
        std::cout << setw() << 0 << setw() << 0;
      else
        std::cout << setw() << flops[i] << setw() << flops_size[i];
      std::cout << " | ";
      if (MEMOPS_args <= i)
        std::cout << setw() << 0 << setw() << 0;
      else
        std::cout << setw() << memops[i] << setw() << memops_size[i];
      std::cout << std::endl;
    }
  }

  /**
   * @brief
   * Print elapsed time, compute and memory performance
   */
  std::ostream& print(std::ostream& os,
                      const std::string& prefix = "",
                      const std::string& suffix = "") const
  {
    if (std::chrono::duration_cast<std::chrono::seconds>(tstop - tstart)
          .count() > threshold) {
      os << prefix << "Elapsed time:          "
         << std::chrono::duration_cast<std::chrono::seconds>(tstop - tstart)
              .count()
         << " sec" << suffix << prefix << "Computing performance: "
         << 1e-6 * totalFlops() /
              (double)std::chrono::duration_cast<std::chrono::seconds>(tstop -
                                                                       tstart)
                .count()
         << " MFLOPS" << suffix << prefix << "Memory performance:    "
         << 1e-6 * totalMemops() /
              (double)std::chrono::duration_cast<std::chrono::seconds>(tstop -
                                                                       tstart)
                .count()
         << " MB/s" << suffix;
    } else if (std::chrono::duration_cast<std::chrono::milliseconds>(tstop -
                                                                     tstart)
                 .count() > threshold) {
      os << prefix << "Elapsed time:          "
         << std::chrono::duration_cast<std::chrono::milliseconds>(tstop -
                                                                  tstart)
              .count()
         << " millisec" << suffix << prefix << "Computing performance: "
         << 1e-3 * totalFlops() /
              (double)std::chrono::duration_cast<std::chrono::milliseconds>(
                tstop - tstart)
                .count()
         << " MFLOPS" << suffix << prefix << "Memory performance:    "
         << 1e-3 * totalMemops() /
              (double)std::chrono::duration_cast<std::chrono::milliseconds>(
                tstop - tstart)
                .count()
         << " MB/s" << suffix;
    } else if (std::chrono::duration_cast<std::chrono::microseconds>(tstop -
                                                                     tstart)
                 .count() > threshold) {
      os << prefix << "Elapsed time:          "
         << std::chrono::duration_cast<std::chrono::microseconds>(tstop -
                                                                  tstart)
              .count()
         << " microsec" << suffix << prefix << "Computing performance: "
         << totalFlops() /
              (double)std::chrono::duration_cast<std::chrono::microseconds>(
                tstop - tstart)
                .count()
         << " MFLOPS" << suffix << prefix << "Memory performance:    "
         << totalMemops() /
              (double)std::chrono::duration_cast<std::chrono::microseconds>(
                tstop - tstart)
                .count()
         << " MB/s" << suffix;
    } else {
      os << prefix << "Elapsed time:          "
         << std::chrono::duration_cast<std::chrono::nanoseconds>(tstop - tstart)
              .count()
         << " nanosec" << suffix << prefix << "Computing performance: "
         << 1e+3 * totalFlops() /
              (double)std::chrono::duration_cast<std::chrono::nanoseconds>(
                tstop - tstart)
                .count()
         << " MFLOPS" << suffix << prefix << "Memory performance:    "
         << 1e+3 * totalMemops() /
              (double)std::chrono::duration_cast<std::chrono::nanoseconds>(
                tstop - tstart)
                .count()
         << " MB/s" << suffix;
    }
    return os;
  }

private:
  // Calculate tab width for info()
  inline auto setw() -> decltype(std::setw(1))
  {
    long unsigned int width = 7;
    for (auto i = 0; i < FLOPS_args; i++)
      width = std::max(width,
                       std::max(std::to_string(flops[i]).length(),
                                std::to_string(flops_size[i]).length()) +
                         1);
    for (auto i = 0; i < MEMOPS_args; i++)
      width = std::max(width,
                       std::max(std::to_string(memops[i]).length(),
                                std::to_string(memops_size[i]).length()) +
                         1);
    return std::setw(width);
  }

  // Calculate total number of FLOPs
  inline std::size_t totalFlops() const
  {
    std::size_t flops = 0;
    for (auto i = 0; i < FLOPS_args; i++)
      flops += this->flops[i] * this->flops_size[i];
    return flops * runs;
  }

  // Calculate total size of memory-transfer (in MBits)
  inline std::size_t totalMemops() const
  {
    std::size_t memops = 0;
    for (auto i = 0; i < MEMOPS_args; i++)
      memops += this->memops[i] * this->memops_size[i];
    return memops * runs;
  }

  // Array with floating-point operation numbers
  std::array<std::size_t, FLOPS_args> flops;

  // Array with lengths of data arrays for floating-point operations
  std::array<std::size_t, FLOPS_args> flops_size;

  // Array with memory-transfer operation numbers
  std::array<std::size_t, MEMOPS_args> memops;

  // Array with lengths of data arrays for memory-transfer operations
  std::array<std::size_t, MEMOPS_args> memops_size;
};

/**
 * @brief
 * Print Timer to string
 */
template<std::size_t FLOPS_args, std::size_t MEMOPS_args>
std::ostream&
operator<<(std::ostream& os,
           const PerfCounter<FLOPS_args, MEMOPS_args>& perfcounter)
{
  return perfcounter.print(os);
}

/**
 * @brief
 * Abstract base class of UnitTest fixtures
 */
template<typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct FixtureBase
{
public:
  /**
   * @brief
   * Fill host vector uniformly
   */
  virtual void fill_vector_h(vector_h& vec, const real_t val) = 0;

  /**
   * @brief
   * Fill device vector uniformly
   */
  virtual void fill_vector_d(vector_d& vec, const real_t val) = 0;

  /**
   * @brief
   * Copy host vector to device vector
   */
  virtual void copy_vector_h2d(const vector_h& vec_h, vector_d& vec_d) = 0;

  /**
   * @brief
   * Copy device vector to host vector
   */
  virtual void copy_vector_d2h(const vector_d& vec_d, vector_h& vec_h) = 0;

  /**
   * @brief
   * Fill host matrix uniformly
   */
  virtual void fill_matrix_h(matrix_h& mat, const real_t val) = 0;

  /**
   * @brief
   * Fill device matrix uniformly
   */
  virtual void fill_matrix_d(matrix_d& mat, const real_t val) = 0;

  /**
   * @brief
   * Copy host matrix to device matrix
   */
  virtual void copy_matrix_h2d(const matrix_h& mat_h, matrix_d& mat_d) = 0;

  /**
   * @brief
   * Copy device matrix to host matrix
   */
  virtual void copy_matrix_d2h(const matrix_d& mat_d, matrix_h& mat_h) = 0;

  /**
   * @brief
   * Check get_make_constant_impl selector
   */
  template<typename Expr>
  bool check_get_make_constant_impl(Expr&& expr) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_make_temp_impl selector
   */
  template<typename Expr>
  bool check_get_make_temp_impl(Expr&& expr) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_make_explicit_temp_impl selector
   */
  template<typename Temp, typename Expr>
  bool check_get_make_explicit_temp_impl(Expr&& expr) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_tag_impl selector
   */
  template<typename Expr>
  bool check_get_tag_impl(Expr&& expr) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_mul_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_mul_impl(A&& a, B&& b) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_div_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_div_impl(A&& a, B&& b) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_pow_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_pow_impl(A&& a, B&& b) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_abs_impl selector
   */
  template<typename A>
  bool check_get_elem_abs_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_acos_impl selector
   */
  template<typename A>
  bool check_get_elem_acos_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_acosh_impl selector
   */
  template<typename A>
  bool check_get_elem_acosh_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_asin_impl selector
   */
  template<typename A>
  bool check_get_elem_asin_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_asinh_impl selector
   */
  template<typename A>
  bool check_get_elem_asinh_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_atan_impl selector
   */
  template<typename A>
  bool check_get_elem_atan_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_atanh_impl selector
   */
  template<typename A>
  bool check_get_elem_atanh_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_ceil_impl selector
   */
  template<typename A>
  bool check_get_elem_ceil_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_conj_impl selector
   */
  template<typename A>
  bool check_get_elem_conj_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_cos_impl selector
   */
  template<typename A>
  bool check_get_elem_cos_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_cosh_impl selector
   */
  template<typename A>
  bool check_get_elem_cosh_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_erf_impl selector
   */
  template<typename A>
  bool check_get_elem_erf_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_erfc_impl selector
   */
  template<typename A>
  bool check_get_elem_erfc_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_exp_impl selector
   */
  template<typename A>
  bool check_get_elem_exp_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_exp10_impl selector
   */
  template<typename A>
  bool check_get_elem_exp10_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_exp2_impl selector
   */
  template<typename A>
  bool check_get_elem_exp2_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_fabs_impl selector
   */
  template<typename A>
  bool check_get_elem_fabs_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_floor_impl selector
   */
  template<typename A>
  bool check_get_elem_floor_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_imag_impl selector
   */
  template<typename A>
  bool check_get_elem_imag_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_log_impl selector
   */
  template<typename A>
  bool check_get_elem_log_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_log10_impl selector
   */
  template<typename A>
  bool check_get_elem_log10_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_log2_impl selector
   */
  template<typename A>
  bool check_get_elem_log2_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_real_impl selector
   */
  template<typename A>
  bool check_get_elem_real_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_round_impl selector
   */
  template<typename A>
  bool check_get_elem_round_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_rsqrt_impl selector
   */
  template<typename A>
  bool check_get_elem_rsqrt_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_sign_impl selector
   */
  template<typename A>
  bool check_get_elem_sign_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_sin_impl selector
   */
  template<typename A>
  bool check_get_elem_sin_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_sinh_impl selector
   */
  template<typename A>
  bool check_get_elem_sinh_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_sqrt_impl selector
   */
  template<typename A>
  bool check_get_elem_sqrt_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_tan_impl selector
   */
  template<typename A>
  bool check_get_elem_tan_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_tanh_impl selector
   */
  template<typename A>
  bool check_get_elem_tanh_impl(A&& a) const
  {
    return false;
  }

  /**
   * @brief
   * Check get_elem_trunc_impl selector
   */
  template<typename A>
  bool check_get_elem_trunc_impl(A&& a) const
  {
    return false;
  }
};

#include "detail/armadillo.hpp"
#include "detail/arrayfire.hpp"
#include "detail/blaze.hpp"
#include "detail/cmtl4.hpp"
#include "detail/eigen.hpp"
#include "detail/itpp.hpp"
#include "detail/mtl4.hpp"
#include "detail/ublas.hpp"
#include "detail/vexcl.hpp"
#include "detail/viennacl.hpp"

#endif // FDBB_UNITTESTS_HPP
