/** @file test_riemann_var2d_riemann_inv1.hpp
 *
 *  @brief UnitTests++ 2D Riemann variables: riemann_inv1 test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 2d: riemann_inv1
 */
TEST_FIXTURE(FDBB_RIEMANN_FIXTURE_2D, var2d_riemann_inv1)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::
    Variables<eos, 2, fdbb::fluids::EnumForm::Riemann_invariants>
      variables;

  fill_vector_d(w_1, type_real{ 2.0 });
  fill_vector_d(w_2, type_real{ 1.0 });
  fill_vector_d(w_3, type_real{ 2.0 });
  fill_vector_d(w_4, type_real{ 16.0 });

  TEST_INIT((0),
            (FDBB_RIEMANN_FIXTURE_2D::len),
            (2 * sizeof(type_real)),
            (FDBB_RIEMANN_FIXTURE_2D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check first Riemann invariant
      fdbb::tag<0>(result_d) = variables::w_1(fdbb::tag<1>(w_1),
                                              fdbb::tag<2>(w_2),
                                              fdbb::tag<3>(w_3),
                                              fdbb::tag<4>(w_4));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, 2.0);
  CHECK_ARRAY_CLOSE(
    result_h, dummy_h, FDBB_RIEMANN_FIXTURE_2D::len, type_real{ 1e-5 });
}
