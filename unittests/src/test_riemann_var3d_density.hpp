/** @file test_riemann_var3d_density.hpp
 *
 *  @brief UnitTests++ 3D Riemann variables: density test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 3d: density
 */
TEST_FIXTURE(FDBB_RIEMANN_FIXTURE_3D, var3d_w_1)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::
    Variables<eos, 3, fdbb::fluids::EnumForm::Riemann_invariants>
      variables;

  fill_vector_d(w_1, type_real{ 2.0 });
  fill_vector_d(w_2, type_real{ 1.0 });
  fill_vector_d(w_3, type_real{ 2.0 });
  fill_vector_d(w_4, type_real{ 3.0 });
  fill_vector_d(w_5, type_real{ 16.0 });

  TEST_INIT((0),
            (FDBB_RIEMANN_FIXTURE_3D::len),
            (2 * sizeof(type_real)),
            (FDBB_RIEMANN_FIXTURE_3D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check w_1
      fdbb::tag<0>(result_d) = variables::rho(fdbb::tag<1>(w_1),
                                              fdbb::tag<2>(w_2),
                                              fdbb::tag<3>(w_3),
                                              fdbb::tag<4>(w_4),
                                              fdbb::tag<5>(w_5));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 2.05629988 });
  CHECK_ARRAY_CLOSE(
    result_h, dummy_h, FDBB_RIEMANN_FIXTURE_3D::len, type_real{ 1e-5 });
}
