/** @file test_primitive_var3d_momentum_mag_pow2.hpp
 *
 *  @brief UnitTests++ 3D primitive variables: momentum_mag_pow2 test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 3d: momentum_mag_pow2
 */
TEST_FIXTURE(FDBB_PRIMITIVE_FIXTURE_3D, var3d_momentum_mag_pow2)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::primitive>
    variables;

  fill_vector_d(density, type_real{ 2.0 });
  fill_vector_d(velocity_x, type_real{ 1.0 });
  fill_vector_d(velocity_y, type_real{ 2.0 });
  fill_vector_d(velocity_z, type_real{ 3.0 });
  fill_vector_d(pressure, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_PRIMITIVE_FIXTURE_3D::len),
            (2 * sizeof(type_real)),
            (FDBB_PRIMITIVE_FIXTURE_3D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check mag2(velocity)
      fdbb::tag<0>(result_d) = variables::rhov_mag2(fdbb::tag<1>(density),
                                                    fdbb::tag<2>(velocity_x),
                                                    fdbb::tag<3>(velocity_y),
                                                    fdbb::tag<4>(velocity_z),
                                                    fdbb::tag<5>(pressure));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 56.0 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_PRIMITIVE_FIXTURE_3D::len);
}
