/** @file unittests/src/detail/armadillo.hpp
 *
 *  @brief Implementation details for Armadillo library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef DETAIL_ARMADILLO_HPP
#define DETAIL_ARMADILLO_HPP

#ifdef FDBB_BACKEND_ARMADILLO

#include <armadillo>

#ifdef UNITTESTS_WITH_CACHE
using namespace fdbb::cache;
#define CACHE_EXPR(tag, expr) CacheExpr<tag, expr>
#elif defined(UNITTESTS_WITH_CACHE2)
using namespace fdbb::cache2;
#define CACHE_EXPR(tag, expr) CacheExpr<tag, expr>
#else
#define CACHE_EXPR(tag, expr) expr
#endif

/**
 * @brief
 * Base class of UnitTest fixture implementing the virtual
 * methods from the abstract base class FixtureBase
 */
template<typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct FixtureBaseArmadillo
  : public FixtureBase<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  /**
   * @brief
   * Fill host vector uniformly
   */
  void fill_vector_h(vector_h& vec, const real_t val) { vec.fill(val); }

  template<std::size_t Tag>
  void fill_vector_h(CacheExpr<Tag, vector_h>& vec, const real_t val)
  {
    vec.get().fill(val);
  }

  template<std::size_t Tag>
  void fill_vector_h(CacheExprView<Tag, vector_h>& vec, const real_t val)
  {
    vec.get().fill(val);
  }

  /**
   * @brief
   * Fill device vector uniformly
   */
  void fill_vector_d(vector_d& vec, const real_t val) { vec.fill(val); }

  template<std::size_t Tag>
  void fill_vector_d(CacheExpr<Tag, vector_d>& vec, const real_t val)
  {
    vec.get().fill(val);
  }

  template<std::size_t Tag>
  void fill_vector_d(CacheExprView<Tag, vector_d>& vec, const real_t val)
  {
    vec.get().fill(val);
  }

  /**
   * @brief
   * Copy host vector to device vector
   */
  void copy_vector_h2d(const vector_h& vec_h, vector_d& vec_d)
  {
    if (&vec_h != &vec_d)
      vec_d = vec_h;
  }

  template<std::size_t Tag_h>
  void copy_vector_h2d(const CacheExpr<Tag_h, vector_h>& vec_h, vector_d& vec_d)
  {
    if (&(vec_h.get()) != &vec_d)
      vec_d = vec_h.get();
  }

  template<std::size_t Tag_h>
  void copy_vector_h2d(const CacheExprView<Tag_h, vector_h>& vec_h,
                       vector_d& vec_d)
  {
    if (&(vec_h.get()) != &vec_d)
      vec_d = vec_h.get();
  }

  template<std::size_t Tag_d>
  void copy_vector_h2d(const vector_h& vec_h, CacheExpr<Tag_d, vector_d>& vec_d)
  {
    if (&vec_h != &(vec_d.get()))
      vec_d.get() = vec_h;
  }

  template<std::size_t Tag_d>
  void copy_vector_h2d(const vector_h& vec_h,
                       CacheExprView<Tag_d, vector_d>& vec_d)
  {
    if (&vec_h != &(vec_d.get()))
      vec_d.get() = vec_h;
  }

  template<std::size_t Tag_h, std::size_t Tag_d>
  void copy_vector_h2d(const CacheExpr<Tag_h, vector_h>& vec_h,
                       CacheExpr<Tag_d, vector_d>& vec_d)
  {
    if (&(vec_h.get()) != &(vec_d.get()))
      vec_d.get() = vec_h.get();
  }

  template<std::size_t Tag_h, std::size_t Tag_d>
  void copy_vector_h2d(const CacheExpr<Tag_h, vector_h>& vec_h,
                       CacheExprView<Tag_d, vector_d>& vec_d)
  {
    if (&(vec_h.get()) != &(vec_d.get()))
      vec_d.get() = vec_h.get();
  }

  template<std::size_t Tag_h, std::size_t Tag_d>
  void copy_vector_h2d(const CacheExprView<Tag_h, vector_h>& vec_h,
                       CacheExpr<Tag_d, vector_d>& vec_d)
  {
    if (&(vec_h.get()) != &(vec_d.get()))
      vec_d.get() = vec_h.get();
  }

  template<std::size_t Tag_h, std::size_t Tag_d>
  void copy_vector_h2d(const CacheExprView<Tag_h, vector_h>& vec_h,
                       CacheExprView<Tag_d, vector_d>& vec_d)
  {
    if (&(vec_h.get()) != &(vec_d.get()))
      vec_d.get() = vec_h.get();
  }

  /**
   * @brief
   * Copy device vector to host vector
   */
  void copy_vector_d2h(const vector_d& vec_d, vector_h& vec_h)
  {
    if (&vec_d != &vec_h)
      vec_h = vec_d;
  }

  template<std::size_t Tag_d>
  void copy_vector_d2h(const CacheExpr<Tag_d, vector_d>& vec_d, vector_h& vec_h)
  {
    if (&(vec_d.get()) != &vec_h)
      vec_h = vec_d.get();
  }

  template<std::size_t Tag_d>
  void copy_vector_d2h(const CacheExprView<Tag_d, vector_d>& vec_d,
                       vector_h& vec_h)
  {
    if (&(vec_d.get()) != &vec_h)
      vec_h = vec_d.get();
  }

  template<std::size_t Tag_h>
  void copy_vector_d2h(const vector_d& vec_d, CacheExpr<Tag_h, vector_h>& vec_h)
  {
    if (&vec_d != &(vec_h.get()))
      vec_h.get() = vec_d;
  }

  template<std::size_t Tag_h>
  void copy_vector_d2h(const vector_d& vec_d,
                       CacheExprView<Tag_h, vector_h>& vec_h)
  {
    if (&vec_d != &(vec_h.get()))
      vec_h.get() = vec_d;
  }

  template<std::size_t Tag_d, std::size_t Tag_h>
  void copy_vector_d2h(const CacheExpr<Tag_d, vector_d>& vec_d,
                       CacheExpr<Tag_h, vector_h>& vec_h)
  {
    if (&(vec_d.get()) != &(vec_h.get()))
      vec_h.get() = vec_d.get();
  }

  template<std::size_t Tag_d, std::size_t Tag_h>
  void copy_vector_d2h(const CacheExpr<Tag_d, vector_d>& vec_d,
                       CacheExprView<Tag_h, vector_h>& vec_h)
  {
    if (&(vec_d.get()) != &(vec_h.get()))
      vec_h.get() = vec_d.get();
  }

  template<std::size_t Tag_d, std::size_t Tag_h>
  void copy_vector_d2h(const CacheExprView<Tag_d, vector_d>& vec_d,
                       CacheExpr<Tag_h, vector_h>& vec_h)
  {
    if (&(vec_d.get()) != &(vec_h.get()))
      vec_h.get() = vec_d.get();
  }

  template<std::size_t Tag_d, std::size_t Tag_h>
  void copy_vector_d2h(const CacheExprView<Tag_d, vector_d>& vec_d,
                       CacheExprView<Tag_h, vector_h>& vec_h)
  {
    if (&(vec_d.get()) != &(vec_h.get()))
      vec_h.get() = vec_d.get();
  }

  /**
   * @brief
   * Fill host matrix uniformly
   */
  void fill_matrix_h(matrix_h& mat, const real_t val) { mat.fill(val); }

  /**
   * @brief
   * Fill device matrix uniformly
   */
  void fill_matrix_d(matrix_d& mat, const real_t val) { mat.fill(val); }

  /**
   * @brief
   * Copy host matrix to device matrix
   */
  void copy_matrix_h2d(const matrix_h& mat_h, matrix_d& mat_d)
  {
    if (&mat_h != &mat_d)
      mat_d = mat_h;
  }

  /**
   * @brief
   * Copy device matrix to host matrix
   */
  void copy_matrix_d2h(const matrix_d& mat_d, matrix_h& mat_h)
  {
    if (&mat_d != &mat_h)
      mat_h = mat_d;
  }

  /**
   * @brief
   * Check get_make_constant_impl indicator
   */
  template<typename Expr>
  bool check_get_make_constant_impl(Expr&& expr) const
  {
    std::cout << "get_make_constant_impl(" << typeid(expr).name() << ") : "
              << fdbb::backend::detail::get_make_constant_impl<Expr>::value
              << std::endl;

    return (fdbb::backend::detail::get_make_constant_impl<Expr>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_make_temp_impl indicator
   */
  template<typename Expr>
  bool check_get_make_temp_impl(Expr&& expr) const
  {
    std::cout << "get_make_temp_impl(" << typeid(expr).name() << ") : "
              << fdbb::backend::detail::get_make_temp_impl<Expr>::value
              << std::endl;

    if (fdbb::backend::is_type_of<Expr, fdbb::EnumETL::ARMADILLO>::value)
      return (fdbb::backend::detail::get_make_temp_impl<Expr>::value ==
              fdbb::EnumETL::ARMADILLO);
    else
      return (fdbb::backend::detail::get_make_temp_impl<Expr>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_make_explicit_temp_impl indicator
   */
  template<typename Temp, typename Expr>
  bool check_get_make_explicit_temp_impl(Expr&& expr) const
  {
    std::cout
      << "get_make_explicit_temp_impl(" << typeid(expr).name() << ") : "
      << fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::value
      << std::endl;

    return (
      fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::value ==
      fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_tag_impl indicator
   */
  template<typename Expr>
  bool check_get_tag_impl(Expr&& expr) const
  {
    std::cout << "get_tag_impl(" << typeid(expr).name()
              << ") : " << fdbb::backend::detail::get_tag_impl<Expr>::value
              << std::endl;

    return (fdbb::backend::detail::get_tag_impl<Expr>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_mul_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_mul_impl(A&& a, B&& b) const
  {
    std::cout << "get_elem_mul_impl(" << typeid(a).name() << ","
              << typeid(b).name()
              << ") : " << fdbb::backend::detail::get_elem_mul_impl<A, B>::value
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::ARMADILLO>::value ||
        fdbb::backend::is_type_of<B, fdbb::EnumETL::ARMADILLO>::value)
      return (fdbb::backend::detail::get_elem_mul_impl<A, B>::value ==
              fdbb::EnumETL::ARMADILLO);
    else
      return (fdbb::backend::detail::get_elem_mul_impl<A, B>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_div_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_div_impl(A&& a, B&& b) const
  {
    std::cout << "get_elem_div_impl(" << typeid(a).name() << ","
              << typeid(b).name()
              << ") : " << fdbb::backend::detail::get_elem_div_impl<A, B>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_div_impl<A, B>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_pow_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_pow_impl(A&& a, B&& b) const
  {
    std::cout << "get_elem_pow_impl(" << typeid(a).name() << ","
              << typeid(b).name()
              << ") : " << fdbb::backend::detail::get_elem_pow_impl<A, B>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_pow_impl<A, B>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_abs_impl selector
   */
  template<typename A>
  bool check_get_elem_abs_impl(A&& a) const
  {
    std::cout << "get_elem_abs_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_abs_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_abs_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_acos_impl selector
   */
  template<typename A>
  bool check_get_elem_acos_impl(A&& a) const
  {
    std::cout << "get_elem_acos_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_acos_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_acos_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_acosh_impl selector
   */
  template<typename A>
  bool check_get_elem_acosh_impl(A&& a) const
  {
    std::cout << "get_elem_acosh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_acosh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_acosh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_asin_impl selector
   */
  template<typename A>
  bool check_get_elem_asin_impl(A&& a) const
  {
    std::cout << "get_elem_asin_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_asin_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_asin_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_asinh_impl selector
   */
  template<typename A>
  bool check_get_elem_asinh_impl(A&& a) const
  {
    std::cout << "get_elem_asinh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_asinh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_asinh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_atan_impl selector
   */
  template<typename A>
  bool check_get_elem_atan_impl(A&& a) const
  {
    std::cout << "get_elem_atan_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_atan_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_atan_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_atanh_impl selector
   */
  template<typename A>
  bool check_get_elem_atanh_impl(A&& a) const
  {
    std::cout << "get_elem_atanh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_atanh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_atanh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_ceil_impl selector
   */
  template<typename A>
  bool check_get_elem_ceil_impl(A&& a) const
  {
    std::cout << "get_elem_ceil_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_ceil_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_ceil_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_conj_impl selector
   */
  template<typename A>
  bool check_get_elem_conj_impl(A&& a) const
  {
    std::cout << "get_elem_conj_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_conj_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_conj_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_cos_impl selector
   */
  template<typename A>
  bool check_get_elem_cos_impl(A&& a) const
  {
    std::cout << "get_elem_cos_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_cos_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_cos_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_cosh_impl selector
   */
  template<typename A>
  bool check_get_elem_cosh_impl(A&& a) const
  {
    std::cout << "get_elem_cosh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_cosh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_cosh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_erf_impl selector
   */
  template<typename A>
  bool check_get_elem_erf_impl(A&& a) const
  {
    std::cout << "get_elem_erf_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_erf_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_erf_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_erfc_impl selector
   */
  template<typename A>
  bool check_get_elem_erfc_impl(A&& a) const
  {
    std::cout << "get_elem_erfc_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_erfc_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_erfc_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_exp_impl selector
   */
  template<typename A>
  bool check_get_elem_exp_impl(A&& a) const
  {
    std::cout << "get_elem_exp_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_exp_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_exp_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_exp10_impl selector
   */
  template<typename A>
  bool check_get_elem_exp10_impl(A&& a) const
  {
    std::cout << "get_elem_exp10_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_exp10_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_exp10_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_exp2_impl selector
   */
  template<typename A>
  bool check_get_elem_exp2_impl(A&& a) const
  {
    std::cout << "get_elem_exp2_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_exp2_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_exp2_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_fabs_impl selector
   */
  template<typename A>
  bool check_get_elem_fabs_impl(A&& a) const
  {
    std::cout << "get_elem_fabs_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_fabs_impl<A>::value
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::ARMADILLO>::value)
      return (fdbb::backend::detail::get_elem_fabs_impl<A>::value ==
              fdbb::EnumETL::ARMADILLO);
    else
      return (fdbb::backend::detail::get_elem_fabs_impl<A>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_floor_impl selector
   */
  template<typename A>
  bool check_get_elem_floor_impl(A&& a) const
  {
    std::cout << "get_elem_floor_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_floor_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_floor_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_imag_impl selector
   */
  template<typename A>
  bool check_get_elem_imag_impl(A&& a) const
  {
    std::cout << "get_elem_imag_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_imag_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_imag_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_log_impl selector
   */
  template<typename A>
  bool check_get_elem_log_impl(A&& a) const
  {
    std::cout << "get_elem_log_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_log_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_log_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_log10_impl selector
   */
  template<typename A>
  bool check_get_elem_log10_impl(A&& a) const
  {
    std::cout << "get_elem_log10_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_log10_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_log10_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_log2_impl selector
   */
  template<typename A>
  bool check_get_elem_log2_impl(A&& a) const
  {
    std::cout << "get_elem_log2_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_log2_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_log2_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_real_impl selector
   */
  template<typename A>
  bool check_get_elem_real_impl(A&& a) const
  {
    std::cout << "get_elem_real_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_real_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_real_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_round_impl selector
   */
  template<typename A>
  bool check_get_elem_round_impl(A&& a) const
  {
    std::cout << "get_elem_round_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_round_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_round_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_rsqrt_impl selector
   */
  template<typename A>
  bool check_get_elem_rsqrt_impl(A&& a) const
  {
    std::cout << "get_elem_rsqrt_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_rsqrt_impl<A>::value
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::ARMADILLO>::value)
      return (fdbb::backend::detail::get_elem_rsqrt_impl<A>::value ==
              fdbb::EnumETL::ARMADILLO);
    else
      return (fdbb::backend::detail::get_elem_rsqrt_impl<A>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sign_impl selector
   */
  template<typename A>
  bool check_get_elem_sign_impl(A&& a) const
  {
    std::cout << "get_elem_sign_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sign_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sign_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sin_impl selector
   */
  template<typename A>
  bool check_get_elem_sin_impl(A&& a) const
  {
    std::cout << "get_elem_sin_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sin_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sin_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sinh_impl selector
   */
  template<typename A>
  bool check_get_elem_sinh_impl(A&& a) const
  {
    std::cout << "get_elem_sinh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sinh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sinh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sqrt_impl selector
   */
  template<typename A>
  bool check_get_elem_sqrt_impl(A&& a) const
  {
    std::cout << "get_elem_sqrt_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sqrt_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sqrt_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_tan_impl selector
   */
  template<typename A>
  bool check_get_elem_tan_impl(A&& a) const
  {
    std::cout << "get_elem_tan_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_tan_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_tan_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_tanh_impl selector
   */
  template<typename A>
  bool check_get_elem_tanh_impl(A&& a) const
  {
    std::cout << "get_elem_tanh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_tanh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_tanh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_trunc_impl selector
   */
  template<typename A>
  bool check_get_elem_trunc_impl(A&& a) const
  {
    std::cout << "get_elem_trunc_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_trunc_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_trunc_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check result type
   */
  template<typename A>
  bool check_result_type(A&& a) const
  {
    std::cout << "result_type(" << typeid(a).name() << ") : "
              << typeid(typename fdbb::backend::result_type<A>::type).name()
              << std::endl;

    if (a.is_colvec())
      return (std::is_same<typename fdbb::backend::result_type<A>::type,
                           arma::Col<typename fdbb::utils::remove_all<
                             A>::type::elem_type>>::value);
    else if (a.is_rowvec())
      return (std::is_same<typename fdbb::backend::result_type<A>::type,
                           arma::Row<typename fdbb::utils::remove_all<
                             A>::type::elem_type>>::value);
    else if (arma::is_arma_cube_type<
               typename fdbb::utils::remove_all<A>::type>::value)
      return (std::is_same<typename fdbb::backend::result_type<A>::type,
                           arma::Cube<typename fdbb::utils::remove_all<
                             A>::type::elem_type>>::value);
    else if (arma::is_arma_sparse_type<
               typename fdbb::utils::remove_all<A>::type>::value)
      return (std::is_same<typename fdbb::backend::result_type<A>::type,
                           arma::SpMat<typename fdbb::utils::remove_all<
                             A>::type::elem_type>>::value);
    else if (arma::is_arma_type<
               typename fdbb::utils::remove_all<A>::type>::value)
      return (std::is_same<typename fdbb::backend::result_type<A>::type,
                           arma::Mat<typename fdbb::utils::remove_all<
                             A>::type::elem_type>>::value);
    else
      return false;
  }

  /**
   * @brief
   * Check value type
   */
  template<typename A>
  bool check_value_type(A&& a) const
  {
    std::cout << "value_type(" << typeid(a).name() << ") : "
              << typeid(typename fdbb::backend::value_type<A>::type).name()
              << std::endl;

    if (arma::is_arma_type<typename fdbb::utils::remove_all<A>::type>::value)
      return (std::is_same<
              typename fdbb::backend::value_type<A>::type,
              typename fdbb::utils::remove_all<A>::type::elem_type>::value);
    else
      return (
        std::is_same<typename fdbb::backend::value_type<A>::type, void>::value);
  }
};

/**
 * @brief
 * Armadillo Fixture
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct FixtureArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  FixtureArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Conservative Fixture in 1d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct ConservativeFixture1dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) density;
  CACHE_EXPR(3, vector_d) momentum_x;
  CACHE_EXPR(4, vector_d) energy;
  CACHE_EXPR(5, vector_d) normal_x;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  ConservativeFixture1dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , density(vector_d(size))
    , momentum_x(vector_d(size))
    , energy(vector_d(size))
    , normal_x(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Conservative Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct ConservativeFixture2dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) density;
  CACHE_EXPR(3, vector_d) momentum_x;
  CACHE_EXPR(4, vector_d) momentum_y;
  CACHE_EXPR(5, vector_d) energy;
  CACHE_EXPR(6, vector_d) normal_x;
  CACHE_EXPR(7, vector_d) normal_y;
  CACHE_EXPR(8, vector_d) tangential_1_x;
  CACHE_EXPR(9, vector_d) tangential_1_y;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  ConservativeFixture2dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , density(vector_d(size))
    , momentum_x(vector_d(size))
    , momentum_y(vector_d(size))
    , energy(vector_d(size))
    , normal_x(vector_d(size))
    , normal_y(vector_d(size))
    , tangential_1_x(vector_d(size))
    , tangential_1_y(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Conservative Fixture in 3d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct ConservativeFixture3dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) density;
  CACHE_EXPR(3, vector_d) momentum_x;
  CACHE_EXPR(4, vector_d) momentum_y;
  CACHE_EXPR(5, vector_d) momentum_z;
  CACHE_EXPR(6, vector_d) energy;
  CACHE_EXPR(7, vector_d) normal_x;
  CACHE_EXPR(8, vector_d) normal_y;
  CACHE_EXPR(9, vector_d) normal_z;
  CACHE_EXPR(10, vector_d) tangential_1_x;
  CACHE_EXPR(11, vector_d) tangential_1_y;
  CACHE_EXPR(12, vector_d) tangential_1_z;
  CACHE_EXPR(13, vector_d) tangential_2_x;
  CACHE_EXPR(14, vector_d) tangential_2_y;
  CACHE_EXPR(15, vector_d) tangential_2_z;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  ConservativeFixture3dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , density(vector_d(size))
    , momentum_x(vector_d(size))
    , momentum_y(vector_d(size))
    , momentum_z(vector_d(size))
    , energy(vector_d(size))
    , normal_x(vector_d(size))
    , normal_y(vector_d(size))
    , normal_z(vector_d(size))
    , tangential_1_x(vector_d(size))
    , tangential_1_y(vector_d(size))
    , tangential_1_z(vector_d(size))
    , tangential_2_x(vector_d(size))
    , tangential_2_y(vector_d(size))
    , tangential_2_z(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Primitive Fixture in 1d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct PrimitiveFixture1dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) density;
  CACHE_EXPR(3, vector_d) velocity_x;
  CACHE_EXPR(4, vector_d) pressure;
  CACHE_EXPR(5, vector_d) normal_x;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  PrimitiveFixture1dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , density(vector_d(size))
    , velocity_x(vector_d(size))
    , pressure(vector_d(size))
    , normal_x(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Primitive Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct PrimitiveFixture2dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) density;
  CACHE_EXPR(3, vector_d) velocity_x;
  CACHE_EXPR(4, vector_d) velocity_y;
  CACHE_EXPR(5, vector_d) pressure;
  CACHE_EXPR(6, vector_d) normal_x;
  CACHE_EXPR(7, vector_d) normal_y;
  CACHE_EXPR(8, vector_d) tangential_1_x;
  CACHE_EXPR(9, vector_d) tangential_1_y;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  PrimitiveFixture2dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , density(vector_d(size))
    , velocity_x(vector_d(size))
    , velocity_y(vector_d(size))
    , pressure(vector_d(size))
    , normal_x(vector_d(size))
    , normal_y(vector_d(size))
    , tangential_1_x(vector_d(size))
    , tangential_1_y(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Primitive Fixture in 3d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct PrimitiveFixture3dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) density;
  CACHE_EXPR(3, vector_d) velocity_x;
  CACHE_EXPR(4, vector_d) velocity_y;
  CACHE_EXPR(5, vector_d) velocity_z;
  CACHE_EXPR(6, vector_d) pressure;
  CACHE_EXPR(7, vector_d) normal_x;
  CACHE_EXPR(8, vector_d) normal_y;
  CACHE_EXPR(9, vector_d) normal_z;
  CACHE_EXPR(10, vector_d) tangential_1_x;
  CACHE_EXPR(11, vector_d) tangential_1_y;
  CACHE_EXPR(12, vector_d) tangential_1_z;
  CACHE_EXPR(13, vector_d) tangential_2_x;
  CACHE_EXPR(14, vector_d) tangential_2_y;
  CACHE_EXPR(15, vector_d) tangential_2_z;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  PrimitiveFixture3dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , density(vector_d(size))
    , velocity_x(vector_d(size))
    , velocity_y(vector_d(size))
    , velocity_z(vector_d(size))
    , pressure(vector_d(size))
    , normal_x(vector_d(size))
    , normal_y(vector_d(size))
    , normal_z(vector_d(size))
    , tangential_1_x(vector_d(size))
    , tangential_1_y(vector_d(size))
    , tangential_1_z(vector_d(size))
    , tangential_2_x(vector_d(size))
    , tangential_2_y(vector_d(size))
    , tangential_2_z(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Riemann Fixture in 1d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct RiemannFixture1dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) w_1;
  CACHE_EXPR(3, vector_d) w_2;
  CACHE_EXPR(4, vector_d) w_3;
  CACHE_EXPR(5, vector_d) normal_x;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  RiemannFixture1dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , w_1(vector_d(size))
    , w_2(vector_d(size))
    , w_3(vector_d(size))
    , normal_x(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Riemann Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct RiemannFixture2dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(1, vector_d) dummy_d;

  CACHE_EXPR(2, vector_d) w_1;
  CACHE_EXPR(3, vector_d) w_2;
  CACHE_EXPR(4, vector_d) w_3;
  CACHE_EXPR(5, vector_d) w_4;
  CACHE_EXPR(6, vector_d) normal_x;
  CACHE_EXPR(7, vector_d) normal_y;
  CACHE_EXPR(8, vector_d) tangential_1_x;
  CACHE_EXPR(9, vector_d) tangential_1_y;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  RiemannFixture2dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , w_1(vector_d(size))
    , w_2(vector_d(size))
    , w_3(vector_d(size))
    , w_4(vector_d(size))
    , normal_x(vector_d(size))
    , normal_y(vector_d(size))
    , tangential_1_x(vector_d(size))
    , tangential_1_y(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo Riemann Fixture in 3d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct RiemannFixture3dArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(1, vector_d) result_d;
  CACHE_EXPR(2, vector_d) dummy_d;

  CACHE_EXPR(3, vector_d) w_1;
  CACHE_EXPR(4, vector_d) w_2;
  CACHE_EXPR(5, vector_d) w_3;
  CACHE_EXPR(6, vector_d) w_4;
  CACHE_EXPR(7, vector_d) w_5;
  CACHE_EXPR(8, vector_d) normal_x;
  CACHE_EXPR(9, vector_d) normal_y;
  CACHE_EXPR(10, vector_d) normal_z;
  CACHE_EXPR(11, vector_d) tangential_1_x;
  CACHE_EXPR(12, vector_d) tangential_1_y;
  CACHE_EXPR(13, vector_d) tangential_1_z;
  CACHE_EXPR(14, vector_d) tangential_2_x;
  CACHE_EXPR(15, vector_d) tangential_2_y;
  CACHE_EXPR(16, vector_d) tangential_2_z;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  RiemannFixture3dArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , w_1(vector_d(size))
    , w_2(vector_d(size))
    , w_3(vector_d(size))
    , w_4(vector_d(size))
    , w_5(vector_d(size))
    , normal_x(vector_d(size))
    , normal_y(vector_d(size))
    , normal_z(vector_d(size))
    , tangential_1_x(vector_d(size))
    , tangential_1_y(vector_d(size))
    , tangential_1_z(vector_d(size))
    , tangential_2_x(vector_d(size))
    , tangential_2_y(vector_d(size))
    , tangential_2_z(vector_d(size))
    , len(size)
  {}
};

/**
 * @brief
 * Armadillo BlockExpression Fixture
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct BlockExpressionFixtureArmadillo
  : public FixtureBaseArmadillo<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  CACHE_EXPR(0, vector_d) result_d;
  CACHE_EXPR(2, vector_d) dummy_d;

  CACHE_EXPR(3, vector_d) v0;
  CACHE_EXPR(4, vector_d) v1;
  CACHE_EXPR(5, vector_d) v2;
  CACHE_EXPR(6, vector_d) v3;
  CACHE_EXPR(7, vector_d) v4;
  CACHE_EXPR(8, vector_d) v5;

  // Armadillo does not need different device and host data members
  vector_h& result_h = result_d.get();
  vector_h& dummy_h = dummy_d.get();

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  BlockExpressionFixtureArmadillo()
    : result_d(vector_d(size))
    , dummy_d(vector_d(size))
    , v0(vector_d(size))
    , v1(vector_d(size))
    , v2(vector_d(size))
    , v3(vector_d(size))
    , v4(vector_d(size))
    , v5(vector_d(size))
    , len(size)
  {}
};

#endif // FDBB_BACKEND_ARMADILLO
#endif // DETAIL_ARMADILLO_HPP
