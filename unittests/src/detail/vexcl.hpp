/** @file unittests/src/detail/vexcl.hpp
 *
 *  @brief Implementation details for VexCL library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef DETAIL_VEXCL_HPP
#define DETAIL_VEXCL_HPP

#ifdef FDBB_BACKEND_VEXCL

#include <stdexcept>
#include <vector>
#include <vexcl/vexcl.hpp>

/**
 * @brief
 * Base class of UnitTest fixture implementing the virtual
 * methods from the abstract base class FixtureBase
 */
template<typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct FixtureBaseVexCL
  : public FixtureBase<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  /**
   * @brief
   * Fill host vector uniformly
   */
  void fill_vector_h(vector_h& vec, const real_t val)
  {
    std::fill(vec.begin(), vec.end(), val);
  }

  /**
   * @brief
   * Fill device vector uniformly
   */
  void fill_vector_d(vector_d& vec, const real_t val) { vec = val; }

  /**
   * @brief
   * Copy host vector to device vector
   */
  void copy_vector_h2d(const vector_h& vec_h, vector_d& vec_d)
  {
    vex::copy(vec_h, vec_d);
  }

  /**
   * @brief
   * Copy device vector to host vector
   */
  void copy_vector_d2h(const vector_d& vec_d, vector_h& vec_h)
  {
    vex::copy(vec_d, vec_h);
  }

  /**
   * @brief
   * Fill host matrix uniformly
   */
  void fill_matrix_h(matrix_h& mat, const real_t val)
  {
    throw std::invalid_argument("fill_matrix_h not implemented yet.");
  }

  /**
   * @brief
   * Fill device matrix uniformly
   */
  void fill_matrix_d(matrix_d& mat, const real_t val)
  {
    throw std::invalid_argument("fill_matrix_d not implemented yet.");
  }

  /**
   * @brief
   * Copy host matrix to device matrix
   */
  void copy_matrix_h2d(const matrix_h& mat_h, matrix_d& mat_d)
  {
    throw std::invalid_argument("copy_matrix_h2d not implemented yet.");
  }

  /**
   * @brief
   * Copy device matrix to host matrix
   */
  void copy_matrix_d2h(const matrix_d& mat_d, matrix_h& mat_h)
  {
    throw std::invalid_argument("copy_matrix_d2h not implemented yet.");
  }

  /**
   * @brief
   * Check get_make_constant_impl indicator
   */
  template<typename Expr>
  bool check_get_make_constant_impl(Expr&& expr) const
  {
    std::cout << "get_make_constant_impl(" << typeid(expr).name() << ") : "
              << fdbb::backend::detail::get_make_constant_impl<Expr>::value
              << std::endl;

    if (fdbb::backend::is_type_of<Expr, fdbb::EnumETL::VEXCL>::value)
      return (fdbb::backend::detail::get_make_constant_impl<Expr>::value ==
              fdbb::EnumETL::VEXCL);
    else
      return (fdbb::backend::detail::get_make_constant_impl<Expr>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_make_explicit_temp_impl indicator
   */
  template<typename Temp, typename Expr>
  bool check_get_make_explicit_temp_impl(Expr&& expr) const
  {
    std::cout
      << "get_make_explicit_temp_impl(" << typeid(expr).name() << ") : "
      << fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::value
      << std::endl;

    return (
      fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::value ==
      fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_make_temp_impl indicator
   */
  template<typename Expr>
  bool check_get_make_temp_impl(Expr&& expr) const
  {
    std::cout << "get_make_temp_impl(" << typeid(expr).name() << ") : "
              << fdbb::backend::detail::get_make_temp_impl<Expr>::value
              << std::endl;

    if (std::is_same<typename std::enable_if<
                       boost::proto::matches<
                         typename boost::proto::result_of::as_expr<
                           typename fdbb::utils::remove_all<Expr>::type>::type,
                         vex::vector_expr_grammar>::value,
                       vex::temporary<
                         typename vex::detail::return_type<
                           typename fdbb::utils::remove_all<Expr>::type>::type,
                         0,
                         typename boost::proto::result_of::as_child<
                           const typename fdbb::utils::remove_all<Expr>::type,
                           vex::vector_domain>::type> const>::type,
                     typename std::enable_if<
                       boost::proto::matches<
                         typename boost::proto::result_of::as_expr<
                           typename fdbb::utils::remove_all<Expr>::type>::type,
                         vex::vector_expr_grammar>::value,
                       vex::temporary<
                         typename vex::detail::return_type<
                           typename fdbb::utils::remove_all<Expr>::type>::type,
                         0,
                         typename boost::proto::result_of::as_child<
                           const typename fdbb::utils::remove_all<Expr>::type,
                           vex::vector_domain>::type> const>::type>::value &&
        !std::is_arithmetic<
          typename fdbb::utils::remove_all<Expr>::type>::value)
      return (fdbb::backend::detail::get_make_temp_impl<Expr>::value ==
              fdbb::EnumETL::VEXCL);
    else
      return (fdbb::backend::detail::get_make_temp_impl<Expr>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_tag_impl indicator
   */
  template<typename Expr>
  bool check_get_tag_impl(Expr&& expr) const
  {
    std::cout << "get_tag_impl(" << typeid(expr).name()
              << ") : " << fdbb::backend::detail::get_tag_impl<Expr>::value
              << std::endl;

    if (std::is_same<vex::tagged_terminal<
                       0,
                       decltype(boost::proto::as_child<vex::vector_domain>(
                         std::declval<Expr>()))>,
                     vex::tagged_terminal<
                       0,
                       decltype(boost::proto::as_child<vex::vector_domain>(
                         std::declval<Expr>()))>>::value &&
        !std::is_arithmetic<Expr>::value)
      return (fdbb::backend::detail::get_tag_impl<Expr>::value ==
              fdbb::EnumETL::VEXCL);
    else
      return (fdbb::backend::detail::get_tag_impl<Expr>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_mul_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_mul_impl(A&& a, B&& b) const
  {
    std::cout << "get_elem_mul_impl(" << typeid(a).name() << ","
              << typeid(b).name()
              << ") : " << fdbb::backend::detail::get_elem_mul_impl<A, B>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_mul_impl<A, B>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_div_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_div_impl(A&& a, B&& b) const
  {
    std::cout << "get_elem_div_impl(" << typeid(a).name() << ","
              << typeid(b).name()
              << ") : " << fdbb::backend::detail::get_elem_div_impl<A, B>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_div_impl<A, B>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_pow_impl selector
   */
  template<typename A, typename B>
  bool check_get_elem_pow_impl(A&& a, B&& b) const
  {
    std::cout << "get_elem_pow_impl(" << typeid(a).name() << ","
              << typeid(b).name()
              << ") : " << fdbb::backend::detail::get_elem_pow_impl<A, B>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_pow_impl<A, B>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_abs_impl selector
   */
  template<typename A>
  bool check_get_elem_abs_impl(A&& a) const
  {
    std::cout << "get_elem_abs_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_abs_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_abs_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_acos_impl selector
   */
  template<typename A>
  bool check_get_elem_acos_impl(A&& a) const
  {
    std::cout << "get_elem_acos_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_acos_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_acos_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_acosh_impl selector
   */
  template<typename A>
  bool check_get_elem_acosh_impl(A&& a) const
  {
    std::cout << "get_elem_acosh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_acosh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_acosh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_asin_impl selector
   */
  template<typename A>
  bool check_get_elem_asin_impl(A&& a) const
  {
    std::cout << "get_elem_asin_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_asin_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_asin_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_asinh_impl selector
   */
  template<typename A>
  bool check_get_elem_asinh_impl(A&& a) const
  {
    std::cout << "get_elem_asinh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_asinh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_asinh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_atan_impl selector
   */
  template<typename A>
  bool check_get_elem_atan_impl(A&& a) const
  {
    std::cout << "get_elem_atan_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_atan_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_atan_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_atanh_impl selector
   */
  template<typename A>
  bool check_get_elem_atanh_impl(A&& a) const
  {
    std::cout << "get_elem_atanh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_atanh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_atanh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_ceil_impl selector
   */
  template<typename A>
  bool check_get_elem_ceil_impl(A&& a) const
  {
    std::cout << "get_elem_ceil_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_ceil_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_ceil_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_conj_impl selector
   */
  template<typename A>
  bool check_get_elem_conj_impl(A&& a) const
  {
    std::cout << "get_elem_conj_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_conj_impl<A>::value
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::VEXCL>::value)
      return (fdbb::backend::detail::get_elem_conj_impl<A>::value ==
              fdbb::EnumETL::VEXCL);
    else
      return (fdbb::backend::detail::get_elem_conj_impl<A>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_cos_impl selector
   */
  template<typename A>
  bool check_get_elem_cos_impl(A&& a) const
  {
    std::cout << "get_elem_cos_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_cos_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_cos_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_cosh_impl selector
   */
  template<typename A>
  bool check_get_elem_cosh_impl(A&& a) const
  {
    std::cout << "get_elem_cosh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_cosh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_cosh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_erf_impl selector
   */
  template<typename A>
  bool check_get_elem_erf_impl(A&& a) const
  {
    std::cout << "get_elem_erf_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_erf_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_erf_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_erfc_impl selector
   */
  template<typename A>
  bool check_get_elem_erfc_impl(A&& a) const
  {
    std::cout << "get_elem_erfc_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_erfc_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_erfc_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_exp_impl selector
   */
  template<typename A>
  bool check_get_elem_exp_impl(A&& a) const
  {
    std::cout << "get_elem_exp_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_exp_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_exp_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_exp10_impl selector
   */
  template<typename A>
  bool check_get_elem_exp10_impl(A&& a) const
  {
    std::cout << "get_elem_exp10_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_exp10_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_exp10_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_exp2_impl selector
   */
  template<typename A>
  bool check_get_elem_exp2_impl(A&& a) const
  {
    std::cout << "get_elem_exp2_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_exp2_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_exp2_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_fabs_impl selector
   */
  template<typename A>
  bool check_get_elem_fabs_impl(A&& a) const
  {
    std::cout << "get_elem_fabs_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_fabs_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_fabs_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_floor_impl selector
   */
  template<typename A>
  bool check_get_elem_floor_impl(A&& a) const
  {
    std::cout << "get_elem_floor_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_floor_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_floor_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_imag_impl selector
   */
  template<typename A>
  bool check_get_elem_imag_impl(A&& a) const
  {
    std::cout << "get_elem_imag_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_imag_impl<A>::value
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::VEXCL>::value)
      return (fdbb::backend::detail::get_elem_imag_impl<A>::value ==
              fdbb::EnumETL::VEXCL);
    else
      return (fdbb::backend::detail::get_elem_imag_impl<A>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_log_impl selector
   */
  template<typename A>
  bool check_get_elem_log_impl(A&& a) const
  {
    std::cout << "get_elem_log_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_log_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_log_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_log10_impl selector
   */
  template<typename A>
  bool check_get_elem_log10_impl(A&& a) const
  {
    std::cout << "get_elem_log10_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_log10_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_log10_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_log2_impl selector
   */
  template<typename A>
  bool check_get_elem_log2_impl(A&& a) const
  {
    std::cout << "get_elem_log2_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_log2_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_log2_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_real_impl selector
   */
  template<typename A>
  bool check_get_elem_real_impl(A&& a) const
  {
    std::cout << "get_elem_real_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_real_impl<A>::value
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::VEXCL>::value)
      return (fdbb::backend::detail::get_elem_real_impl<A>::value ==
              fdbb::EnumETL::VEXCL);
    else
      return (fdbb::backend::detail::get_elem_real_impl<A>::value ==
              fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_round_impl selector
   */
  template<typename A>
  bool check_get_elem_round_impl(A&& a) const
  {
    std::cout << "get_elem_round_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_round_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_round_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_rsqrt_impl selector
   */
  template<typename A>
  bool check_get_elem_rsqrt_impl(A&& a) const
  {
    std::cout << "get_elem_rsqrt_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_rsqrt_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_rsqrt_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sign_impl selector
   */
  template<typename A>
  bool check_get_elem_sign_impl(A&& a) const
  {
    std::cout << "get_elem_sign_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sign_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sign_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sin_impl selector
   */
  template<typename A>
  bool check_get_elem_sin_impl(A&& a) const
  {
    std::cout << "get_elem_sin_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sin_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sin_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sinh_impl selector
   */
  template<typename A>
  bool check_get_elem_sinh_impl(A&& a) const
  {
    std::cout << "get_elem_sinh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sinh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sinh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_sqrt_impl selector
   */
  template<typename A>
  bool check_get_elem_sqrt_impl(A&& a) const
  {
    std::cout << "get_elem_sqrt_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_sqrt_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_sqrt_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_tan_impl selector
   */
  template<typename A>
  bool check_get_elem_tan_impl(A&& a) const
  {
    std::cout << "get_elem_tan_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_tan_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_tan_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_tanh_impl selector
   */
  template<typename A>
  bool check_get_elem_tanh_impl(A&& a) const
  {
    std::cout << "get_elem_tanh_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_tanh_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_tanh_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check get_elem_trunc_impl selector
   */
  template<typename A>
  bool check_get_elem_trunc_impl(A&& a) const
  {
    std::cout << "get_elem_trunc_impl(" << typeid(a).name()
              << ") : " << fdbb::backend::detail::get_elem_trunc_impl<A>::value
              << std::endl;

    return (fdbb::backend::detail::get_elem_trunc_impl<A>::value ==
            fdbb::EnumETL::GENERIC);
  }

  /**
   * @brief
   * Check result type
   */
  template<typename A>
  bool check_result_type(A&& a) const
  {
    std::cout << "result_type(" << typeid(a).name() << ") : "
              << typeid(typename fdbb::backend::result_type<A>::type).name()
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::VEXCL>::value)
      return (std::is_same<
              typename fdbb::backend::result_type<A>::type,
              typename boost::proto::result_of::as_expr<
                typename fdbb::utils::remove_all<A>::type>::type>::value);
    else
      return (std::is_same<typename fdbb::backend::result_type<A>::type,
                           void>::value);
  }

  /**
   * @brief
   * Check value type
   */
  template<typename A>
  bool check_value_type(A&& a) const
  {
    std::cout << "value_type(" << typeid(a).name() << ") : "
              << typeid(typename fdbb::backend::value_type<A>::type).name()
              << std::endl;

    if (fdbb::backend::is_type_of<A, fdbb::EnumETL::VEXCL>::value)
      return (std::is_same<
              typename fdbb::backend::value_type<A>::type,
              typename fdbb::utils::remove_all<A>::type::value_type>::value);
    else
      return (
        std::is_same<typename fdbb::backend::value_type<A>::type, void>::value);
  }
};

/**
 * @brief
 * VexCL Fixture
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct FixtureVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  FixtureVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Conservative Fixture in 1d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct ConservativeFixture1dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d density;
  vector_d momentum_x;
  vector_d energy;
  vector_d normal_x;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  ConservativeFixture1dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , density(size)
    , momentum_x(size)
    , energy(size)
    , normal_x(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Conservative Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct ConservativeFixture2dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d density;
  vector_d momentum_x;
  vector_d momentum_y;
  vector_d energy;
  vector_d normal_x;
  vector_d normal_y;
  vector_d tangential_1_x;
  vector_d tangential_1_y;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  ConservativeFixture2dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , density(size)
    , momentum_x(size)
    , momentum_y(size)
    , energy(size)
    , normal_x(size)
    , normal_y(size)
    , tangential_1_x(size)
    , tangential_1_y(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Conservative Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct ConservativeFixture3dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d density;
  vector_d momentum_x;
  vector_d momentum_y;
  vector_d momentum_z;
  vector_d energy;
  vector_d normal_x;
  vector_d normal_y;
  vector_d normal_z;
  vector_d tangential_1_x;
  vector_d tangential_1_y;
  vector_d tangential_1_z;
  vector_d tangential_2_x;
  vector_d tangential_2_y;
  vector_d tangential_2_z;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  ConservativeFixture3dVexCL()
    : result_d(size)
    , dummy_d(size)
    , energy(size)
    , result_h(size)
    , dummy_h(size)
    , density(size)
    , momentum_x(size)
    , momentum_y(size)
    , momentum_z(size)
    , normal_x(size)
    , normal_y(size)
    , normal_z(size)
    , tangential_1_x(size)
    , tangential_1_y(size)
    , tangential_1_z(size)
    , tangential_2_x(size)
    , tangential_2_y(size)
    , tangential_2_z(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Primitive Fixture in 1d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct PrimitiveFixture1dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d density;
  vector_d velocity_x;
  vector_d pressure;
  vector_d normal_x;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  PrimitiveFixture1dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , density(size)
    , velocity_x(size)
    , pressure(size)
    , normal_x(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Primitive Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct PrimitiveFixture2dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d density;
  vector_d velocity_x;
  vector_d velocity_y;
  vector_d pressure;
  vector_d normal_x;
  vector_d normal_y;
  vector_d tangential_1_x;
  vector_d tangential_1_y;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  PrimitiveFixture2dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , density(size)
    , velocity_x(size)
    , velocity_y(size)
    , pressure(size)
    , normal_x(size)
    , normal_y(size)
    , tangential_1_x(size)
    , tangential_1_y(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Primitive Fixture in 3d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct PrimitiveFixture3dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d density;
  vector_d velocity_x;
  vector_d velocity_y;
  vector_d velocity_z;
  vector_d pressure;
  vector_d normal_x;
  vector_d normal_y;
  vector_d normal_z;
  vector_d tangential_1_x;
  vector_d tangential_1_y;
  vector_d tangential_1_z;
  vector_d tangential_2_x;
  vector_d tangential_2_y;
  vector_d tangential_2_z;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  PrimitiveFixture3dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , density(size)
    , velocity_x(size)
    , velocity_y(size)
    , velocity_z(size)
    , pressure(size)
    , normal_x(size)
    , normal_y(size)
    , normal_z(size)
    , tangential_1_x(size)
    , tangential_1_y(size)
    , tangential_1_z(size)
    , tangential_2_x(size)
    , tangential_2_y(size)
    , tangential_2_z(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Riemann Fixture in 1d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct RiemannFixture1dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d w_1;
  vector_d w_2;
  vector_d w_3;
  vector_d normal_x;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  RiemannFixture1dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , w_1(size)
    , w_2(size)
    , w_3(size)
    , normal_x(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Riemann Fixture in 2d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct RiemannFixture2dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d w_1;
  vector_d w_2;
  vector_d w_3;
  vector_d w_4;
  vector_d normal_x;
  vector_d normal_y;
  vector_d tangential_1_x;
  vector_d tangential_1_y;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  RiemannFixture2dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , w_1(size)
    , w_2(size)
    , w_3(size)
    , w_4(size)
    , normal_x(size)
    , normal_y(size)
    , tangential_1_x(size)
    , tangential_1_y(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL Riemann Fixture in 3d
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct RiemannFixture3dVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d w_1;
  vector_d w_2;
  vector_d w_3;
  vector_d w_4;
  vector_d w_5;
  vector_d normal_x;
  vector_d normal_y;
  vector_d normal_z;
  vector_d tangential_1_x;
  vector_d tangential_1_y;
  vector_d tangential_1_z;
  vector_d tangential_2_x;
  vector_d tangential_2_y;
  vector_d tangential_2_z;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  RiemannFixture3dVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , w_1(size)
    , w_2(size)
    , w_3(size)
    , w_4(size)
    , w_5(size)
    , normal_x(size)
    , normal_y(size)
    , normal_z(size)
    , tangential_1_x(size)
    , tangential_1_y(size)
    , tangential_1_z(size)
    , tangential_2_x(size)
    , tangential_2_y(size)
    , tangential_2_z(size)
    , len(size)
  {}
};

/**
 * @brief
 * VexCL BlockExpression Fixture
 */
template<std::size_t size,
         typename real_t,
         typename vector_d,
         typename matrix_d,
         typename vector_h = vector_d,
         typename matrix_h = matrix_d>
struct BlockExpressionFixtureVexCL
  : public FixtureBaseVexCL<real_t, vector_d, matrix_d, vector_h, matrix_h>
{
public:
  // Typedefs
  typedef real_t type_real;
  typedef vector_d type_vector_d;
  typedef matrix_d type_matrix_d;
  typedef vector_h type_vector_h;
  typedef matrix_h type_matrix_h;

  // Constants
  const std::size_t len;

  // Data members
  vector_d result_d;
  vector_d dummy_d;
  vector_h result_h;
  vector_h dummy_h;

  vector_d v0, v1, v2, v3, v4, v5;

  /**
   * @brief
   * Constructor: size is passed as class template parameter
   */
  BlockExpressionFixtureVexCL()
    : result_d(size)
    , dummy_d(size)
    , result_h(size)
    , dummy_h(size)
    , v0(size)
    , v1(size)
    , v2(size)
    , v3(size)
    , v4(size)
    , v5(size)
    , len(size)
  {}
};

#endif // FDBB_BACKEND_VEXCL
#endif // DETAIL_VEXCL_HPP
