/** @file test_copy.hpp
 *
 *  @brief UnitTests++ copy test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test copying of vectors between host and device (if any)
 */
TEST_FIXTURE(FDBB_FIXTURE, copy)
{
  TEST_INIT(
    (1), (FDBB_FIXTURE::len), (4 * sizeof(type_real)), (FDBB_FIXTURE::len));

  fill_vector_h(result_h, type_real{ 3.141 });

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      copy_vector_h2d(result_h, result_d);

      // Multiply by 2.0 (just to be sure that back and forth copy has ben
      // performed)
      fdbb::tag<0>(result_d) = CONSTANT(2.0, result_d) * fdbb::tag<0>(result_d);
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 6.282 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_FIXTURE::len);
}
