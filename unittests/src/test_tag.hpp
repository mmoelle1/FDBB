/** @file test_tag.hpp
 *
 *  @brief UnitTests++ tag test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test tagging of terminal expressions
 */
TEST_FIXTURE(FDBB_FIXTURE, tag)
{
  TEST_INIT(
    (1), (FDBB_FIXTURE::len), (3 * sizeof(type_real)), (FDBB_FIXTURE::len));

  TEST_START();

  for (auto i = 0; i < TEST_RUNS(); i++) {
    fill_vector_d(result_d, type_real{ 3.141 });

    // Multiply by 2.0 (just to be sure that back and forth copy has ben
    // performed)
    fdbb::tag<0>(result_d) = CONSTANT(2.0, result_d) * fdbb::tag<0>(result_d);
  }

  TEST_STOP();
  TEST_REPORT();

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 6.282 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_FIXTURE::len);
}
