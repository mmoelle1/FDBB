/** @file test_op_elem_abs.hpp
 *
 *  @brief UnitTests++ elem_abs test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test arithmetic operation: element-wise absolute value
 */
TEST_FIXTURE(FDBB_FIXTURE, op_elem_abs)
{
  TEST_INIT(
    (1), (FDBB_FIXTURE::len), (3 * sizeof(type_real)), (FDBB_FIXTURE::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{ -2.0 });

      // Absolute value: abs(-2.0) = 2.0
      fdbb::tag<0>(result_d) = fdbb::elem_abs(fdbb::tag<0>(result_d));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, std::abs(type_real{ -2.0 }));
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_FIXTURE::len);
}
