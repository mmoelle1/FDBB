/** @file test_op_elem_expression.hpp
 *
 *  @brief UnitTests++ element-wise expression test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test arithmetic operation: expression
 */
TEST_FIXTURE(FDBB_FIXTURE, op_expression)
{
  TEST_INIT(
    (4), (FDBB_FIXTURE::len), (3 * sizeof(type_real)), (FDBB_FIXTURE::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{ 2.0 });

      // Computation: (2.0+2.0) / (2.0 * (2.0+2.0)) = 0.5
      fdbb::tag<0>(result_d) = fdbb::elem_div(
        fdbb::tag<0>(result_d) + fdbb::tag<0>(result_d),
        fdbb::elem_mul(fdbb::tag<0>(result_d),
                       fdbb::tag<0>(result_d) + fdbb::tag<0>(result_d)));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 0.5 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_FIXTURE::len);
}
