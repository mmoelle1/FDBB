/** @file test_blockexpr_expression_imag.hpp
 *
 *  @brief UnitTests++ block expression test for imag() function
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

TEST_FIXTURE(FDBB_BLOCKEXPRESSION_FIXTURE, blockexpr_expression_imag1)
{
  if (fdbb::utils::is_complex<type_real>::value) {
    fill_vector_d(v0, type_real{ 0.0 });
    fill_vector_d(v1, type_real{ 1.0 });
    fill_vector_d(v2, type_real{ 2.0 });
    fill_vector_d(v3, type_real{ 3.0 });
    fill_vector_d(v4, type_real{ 4.0 });
    fill_vector_d(v5, type_real{ 5.0 });

    TEST_INIT((0),
              (FDBB_BLOCKEXPRESSION_FIXTURE::len),
              (6 * sizeof(type_real)),
              (FDBB_BLOCKEXPRESSIONFIXTURE::len));

    try {
      TEST_START();

      // Create block matrix from sequence of scalar vectors (parameter pack)
      fdbb::BlockMatrix<type_vector_d, 2, 3> A{ v0, v1, v2, v3, v4, v5 };

      // Create block expression
      auto Expr = fdbb::elem_imag(A);

      // Create block matrix from block expression
      fdbb::BlockMatrix<type_vector_d, 2, 3> B{ Expr };

      TEST_STOP();
      TEST_REPORT();

      // Check block matrix B
      copy_vector_d2h(fdbb::utils::get<0>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<1>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<2>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<3>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<4>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<5>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

    } catch (const std::exception& e) {
      std::cerr << e.what() << std::endl;
      return;
    }
  } else {
    return;
  }
}

TEST_FIXTURE(FDBB_BLOCKEXPRESSION_FIXTURE, blockexpr_expression_imag2)
{
  if (fdbb::utils::is_complex<type_real>::value) {
    fill_vector_d(v0, type_real{ 0.0 });
    fill_vector_d(v1, type_real{ 1.0 });
    fill_vector_d(v2, type_real{ 2.0 });
    fill_vector_d(v3, type_real{ 3.0 });
    fill_vector_d(v4, type_real{ 4.0 });
    fill_vector_d(v5, type_real{ 5.0 });

    TEST_INIT((0),
              (FDBB_BLOCKEXPRESSION_FIXTURE::len),
              (6 * sizeof(type_real)),
              (FDBB_BLOCKEXPRESSIONFIXTURE::len));

    try {
      TEST_START();

      // Create block matrix view from sequence of scalar vectors (parameter
      // pack)
      fdbb::BlockMatrixView<type_vector_d, 2, 3> A{ v0, v1, v2, v3, v4, v5 };

      // Create block expression
      auto Expr = fdbb::elem_imag(A);

      // Create block matrix from block expression
      fdbb::BlockMatrix<type_vector_d, 2, 3> B{ Expr };

      TEST_STOP();
      TEST_REPORT();

      // Check block matrix B
      copy_vector_d2h(fdbb::utils::get<0>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<1>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<2>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<3>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<4>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

      copy_vector_d2h(fdbb::utils::get<5>(B), result_h);
      fill_vector_h(dummy_h, type_real{ 0.0 });
      CHECK_ARRAY_CLOSE(result_h,
                        dummy_h,
                        FDBB_BLOCKEXPRESSION_FIXTURE::len,
                        type_real{ 1e-5 });

    } catch (const std::exception& e) {
      std::cerr << e.what() << std::endl;
      return;
    }
  } else {
    return;
  }
}
