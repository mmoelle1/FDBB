/** @file test_riemann_var2d_velocity_y.hpp
 *
 *  @brief UnitTests++ 2D Riemann variables: velocity_y test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 2d: velocity_y
 */
TEST_FIXTURE(FDBB_RIEMANN_FIXTURE_2D, var2d_w_3)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::
    Variables<eos, 2, fdbb::fluids::EnumForm::Riemann_invariants>
      variables;

  fill_vector_d(w_1, type_real{ 2.0 });
  fill_vector_d(w_2, type_real{ 1.0 });
  fill_vector_d(w_3, type_real{ 2.0 });
  fill_vector_d(w_4, type_real{ 16.0 });
  fill_vector_d(normal_x, type_real{ -2.0 });
  fill_vector_d(normal_y, type_real{ 0.0 });
  fill_vector_d(tangential_1_x, type_real{ 0.0 });
  fill_vector_d(tangential_1_y, type_real{ -4.0 });

  TEST_INIT((0),
            (FDBB_RIEMANN_FIXTURE_2D::len),
            (2 * sizeof(type_real)),
            (FDBB_RIEMANN_FIXTURE_2D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check w_3
      fdbb::tag<0>(result_d) = variables::v<1>(fdbb::tag<1>(normal_x),
                                               fdbb::tag<2>(normal_y),
                                               fdbb::tag<3>(tangential_1_x),
                                               fdbb::tag<4>(tangential_1_y),
                                               fdbb::tag<5>(w_1),
                                               fdbb::tag<6>(w_2),
                                               fdbb::tag<7>(w_3),
                                               fdbb::tag<8>(w_4));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ -2.0 });
  CHECK_ARRAY_CLOSE(
    result_h, dummy_h, FDBB_RIEMANN_FIXTURE_2D::len, type_real{ 1e-5 });
}
