/** @file test_value_type.hpp
 *
 *  @brief UnitTests++ value_type test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test value_type trait
 */
TEST_FIXTURE(FDBB_FIXTURE, value_type)
{
  CHECK(check_value_type(result_d));
}
