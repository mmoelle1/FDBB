/** @file test_blockexpr_row_vector.hpp
 *
 *  @brief UnitTests++ block row-vector test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test creation of block row-vector
*/

TEST_FIXTURE(FDBB_BLOCKEXPRESSION_FIXTURE, blockexpr_row_vector)
{
  fill_vector_d(v0, type_real{ 0.0 });
  fill_vector_d(v1, type_real{ 1.0 });
  fill_vector_d(v2, type_real{ 2.0 });
  fill_vector_d(v3, type_real{ 3.0 });
  fill_vector_d(v4, type_real{ 4.0 });
  fill_vector_d(v5, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_BLOCKEXPRESSION_FIXTURE::len),
            (6 * sizeof(type_real)),
            (FDBB_BLOCKEXPRESSIONFIXTURE::len));

  try {
    TEST_START();

    // Create block row-vector from sequence of scalar vectors
    fdbb::BlockRowVector<type_vector_d, 6> A{ v0, v1, v2, v3, v4, v5 };

    // Create block row-vector from block row-vector
    fdbb::BlockRowVector<type_vector_d, 6> B{ A };

    // Create block row-vector from temporal block row-vector
    fdbb::BlockRowVector<type_vector_d, 6> C{
      fdbb::BlockRowVector<type_vector_d, 6>{ A }
    };

    // Create block column-vector from block row-vector
    fdbb::BlockColVector<type_vector_d, 6> D{ A.transpose() };

    // Create block column-vector from temporal block row-vector
    fdbb::BlockRowVector<type_vector_d, 6> E{
      fdbb::BlockColVector<type_vector_d, 6>{ A.transpose() }.transpose()
    };
    // Create block row-vector view from sequence of scalar vectors
    fdbb::BlockRowVectorView<type_vector_d, 6> A1{ v0, v1, v2, v3, v4, v5 };

    // Create block row-vector view from block row-vector
    fdbb::BlockRowVectorView<type_vector_d, 6> B1{ A };

    // Create block row-vector view from block row-vector view
    fdbb::BlockRowVectorView<type_vector_d, 6> C1{ B1 };

    // Create block row-vector view from temporal block row-vector view
    fdbb::BlockRowVectorView<type_vector_d, 6> D1{
      fdbb::BlockRowVectorView<type_vector_d, 6>{ A1 }
    };

    // Change content of scalar source vectors
    fill_vector_d(v0, type_real{ 5.0 });
    fill_vector_d(v1, type_real{ 4.0 });
    fill_vector_d(v2, type_real{ 3.0 });
    fill_vector_d(v3, type_real{ 2.0 });
    fill_vector_d(v4, type_real{ 1.0 });
    fill_vector_d(v5, type_real{ 0.0 });

    TEST_STOP();
    TEST_REPORT();

    // Check block row-vector A
    copy_vector_d2h(fdbb::utils::get<0>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector B
    copy_vector_d2h(fdbb::utils::get<0>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector C
    copy_vector_d2h(fdbb::utils::get<0>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector D
    copy_vector_d2h(fdbb::utils::get<0>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector E
    copy_vector_d2h(fdbb::utils::get<0>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector A1
    copy_vector_d2h(fdbb::utils::get<0>(A1), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(A1), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(A1), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(A1), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(A1), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(A1), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector B1
    copy_vector_d2h(fdbb::utils::get<0>(B1), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(B1), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(B1), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(B1), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(B1), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(B1), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector C1
    copy_vector_d2h(fdbb::utils::get<0>(C1), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(C1), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(C1), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(C1), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(C1), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(C1), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector D1
    copy_vector_d2h(fdbb::utils::get<0>(D1), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<1>(D1), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<2>(D1), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<3>(D1), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<4>(D1), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::utils::get<5>(D1), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
