/** @file test_riemann_var3d_velocity_y.hpp
 *
 *  @brief UnitTests++ 3D Riemann variables: Velocity_y
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 3d: Velocity_y
 */
TEST_FIXTURE(FDBB_RIEMANN_FIXTURE_3D, var3d_w_3)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::
    Variables<eos, 3, fdbb::fluids::EnumForm::Riemann_invariants>
      variables;

  fill_vector_d(w_1, type_real{ 2.0 });
  fill_vector_d(w_2, type_real{ 1.0 });
  fill_vector_d(w_3, type_real{ 2.0 });
  fill_vector_d(w_4, type_real{ 3.0 });
  fill_vector_d(w_5, type_real{ 16.0 });
  fill_vector_d(normal_x, type_real{ -2.0 });
  fill_vector_d(normal_y, type_real{ 0.0 });
  fill_vector_d(normal_z, type_real{ 0.0 });
  fill_vector_d(tangential_1_x, type_real{ 0.0 });
  fill_vector_d(tangential_1_y, type_real{ -4.0 });
  fill_vector_d(tangential_1_z, type_real{ 0.0 });
  fill_vector_d(tangential_2_x, type_real{ 0.0 });
  fill_vector_d(tangential_2_y, type_real{ 0.0 });
  fill_vector_d(tangential_2_z, type_real{ -6.0 });

  TEST_INIT((0),
            (FDBB_RIEMANN_FIXTURE_3D::len),
            (2 * sizeof(type_real)),
            (FDBB_RIEMANN_FIXTURE_3D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check w_3
      fdbb::tag<0>(result_d) = variables::v<1>(fdbb::tag<1>(normal_x),
                                               fdbb::tag<2>(normal_y),
                                               fdbb::tag<3>(normal_z),
                                               fdbb::tag<4>(tangential_1_x),
                                               fdbb::tag<5>(tangential_1_y),
                                               fdbb::tag<6>(tangential_1_z),
                                               fdbb::tag<7>(tangential_2_x),
                                               fdbb::tag<8>(tangential_2_y),
                                               fdbb::tag<9>(tangential_2_z),
                                               fdbb::tag<10>(w_1),
                                               fdbb::tag<11>(w_2),
                                               fdbb::tag<12>(w_3),
                                               fdbb::tag<13>(w_4),
                                               fdbb::tag<14>(w_5));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ -2.0 });
  CHECK_ARRAY_CLOSE(
    result_h, dummy_h, FDBB_RIEMANN_FIXTURE_3D::len, type_real{ 1e-5 });
}
