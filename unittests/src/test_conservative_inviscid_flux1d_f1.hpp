/** @file test_conservative_inviscid_flux1d_f1.hpp
 *
 *  @brief UnitTests++ 1D conservative inviscid fluxes: test of
 *                 component f_1 of flux F in x-direction
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test inviscid fluxes in 1d: component f_1 of flux F in x-direction
 */
TEST_FIXTURE(FDBB_CONSERVATIVE_FIXTURE_1D, inviscid_flux1d_f1)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::conservative>
    variables;

  // Fluxes
  typedef fdbb::fluids::Fluxes<variables, 1> fluxes;

  fill_vector_d(density, type_real{ 2.0 });
  fill_vector_d(momentum_x, type_real{ 1.0 });
  fill_vector_d(energy, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_CONSERVATIVE_FIXTURE_1D::len),
            (2 * sizeof(type_real)),
            (FDBB_CONSERVATIVE_FIXTURE_1D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check component f_1 of flux F in x-direction
      fdbb::tag<0>(result_d) = fluxes::inviscid<fdbb::fluids::EnumCoord::x, 1>(
        fdbb::tag<1>(density), fdbb::tag<2>(momentum_x), fdbb::tag<3>(energy));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 2.4 });
  CHECK_ARRAY_CLOSE(
    result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_1D::len, type_real{ 1e-5 });
}
