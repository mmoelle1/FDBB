/** @file test_primitive_var1d_velocity_dir.hpp
 *
 *  @brief UnitTests++ 1D primitive variables: velocity_dir test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 1d: velocity_dir
 */
TEST_FIXTURE(FDBB_PRIMITIVE_FIXTURE_1D, var1d_velocity_dir)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::primitive>
    variables;

  fill_vector_d(density, type_real{ 2.0 });
  fill_vector_d(velocity_x, type_real{ 1.0 });
  fill_vector_d(pressure, type_real{ 5.0 });
  fill_vector_d(normal_x, type_real{ -2.0 });

  TEST_INIT((0),
            (FDBB_PRIMITIVE_FIXTURE_1D::len),
            (2 * sizeof(type_real)),
            (FDBB_PRIMITIVE_FIXTURE_1D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check velocity_dir
      fdbb::tag<0>(result_d) = variables::v_dir(fdbb::tag<1>(normal_x),
                                                fdbb::tag<2>(density),
                                                fdbb::tag<3>(velocity_x),
                                                fdbb::tag<4>(pressure));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ -1.0 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_PRIMITIVE_FIXTURE_1D::len);
}
