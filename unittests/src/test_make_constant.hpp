/** @file test_make_constant.hpp
 *
 *  @brief UnitTests++ make_constant test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test creation of constant
 */

TEST_FIXTURE(FDBB_FIXTURE, make_constant)
{
  TEST_INIT(
    (1), (FDBB_FIXTURE::len), (5 * sizeof(type_real)), (FDBB_FIXTURE::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{ 3.141 });

      // Multiply by 2.0
      result_d = CONSTANT(2.0, result_d) * result_d;
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 6.282 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_FIXTURE::len);
}
