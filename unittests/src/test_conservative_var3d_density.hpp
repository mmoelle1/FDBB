/** @file test_conservative_var3d_density.hpp
 *
 *  @brief UnitTests++ 3D conservative variables: density test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test variables in 3d: density
 */
TEST_FIXTURE(FDBB_CONSERVATIVE_FIXTURE_3D, var3d_density)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::conservative>
    variables;

  fill_vector_d(density, type_real{ 2.0 });
  fill_vector_d(momentum_x, type_real{ 1.0 });
  fill_vector_d(momentum_y, type_real{ 2.0 });
  fill_vector_d(momentum_z, type_real{ 3.0 });
  fill_vector_d(energy, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_CONSERVATIVE_FIXTURE_3D::len),
            (2 * sizeof(type_real)),
            (FDBB_CONSERVATIVE_FIXTURE_3D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Check density
      fdbb::tag<0>(result_d) = variables::rho(fdbb::tag<1>(density),
                                              fdbb::tag<2>(momentum_x),
                                              fdbb::tag<3>(momentum_y),
                                              fdbb::tag<4>(momentum_z),
                                              fdbb::tag<5>(energy));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 2.0 });
  CHECK_ARRAY_EQUAL(result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len);
}
