/** @file test_blockexpr_expression_conservative_inviscid_flux3d.hpp
 *
 *  @brief UnitTests++ block expression conservative inviscid fluxes in 3D test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

template<typename E1, typename E2>
auto
subexpression(E1& e1, E2& e2) -> decltype(fdbb::elem_sin(e1 + e2))
{
  return fdbb::elem_sin(e1 + e2);
}

TEST_FIXTURE(FDBB_CONSERVATIVE_FIXTURE_3D,
             blockexpr_expression_conservative_inviscid_flux3d)
{
  // Equation of state
  typedef fdbb::fluids::EOSidealGas<type_real> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::conservative>
    variables;

  // Fluxes
  typedef fdbb::fluids::Fluxes<variables, 3> fluxes;

  fill_vector_d(density, type_real{ 2.0 });
  fill_vector_d(momentum_x, type_real{ 1.0 });
  fill_vector_d(momentum_y, type_real{ 2.0 });
  fill_vector_d(momentum_z, type_real{ 3.0 });
  fill_vector_d(energy, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_CONSERVATIVE_FIXTURE_3D::len),
            (6 * sizeof(type_real)),
            (FDBB_CONSERVATIVE_FIXTURE_3D::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      // Create state vector of conservative variables in 3D
      auto state = variables::conservative(fdbb::tag<0>(density),
                                           fdbb::tag<1>(momentum_x),
                                           fdbb::tag<2>(momentum_y),
                                           fdbb::tag<3>(momentum_z),
                                           fdbb::tag<4>(energy));

      // Create inviscid fluxes in 3D
      auto fluxes = fluxes::inviscid<>(fdbb::tag<0>(density),
                                       fdbb::tag<1>(momentum_x),
                                       fdbb::tag<2>(momentum_y),
                                       fdbb::tag<3>(momentum_z),
                                       fdbb::tag<4>(energy));

      // Create block matrix from block expression
      fdbb::BlockMatrix<type_vector_d, 5, 3> Fluxes{ std::move(fluxes) };
    }

    TEST_STOP();
    TEST_REPORT();

    // Create state vector of conservative variables in 3D
    auto state = variables::conservative(fdbb::tag<0>(density),
                                         fdbb::tag<1>(momentum_x),
                                         fdbb::tag<2>(momentum_y),
                                         fdbb::tag<3>(momentum_z),
                                         fdbb::tag<4>(energy));

    // Create inviscid fluxes in 3D
    auto fluxes = fluxes::inviscid<>(fdbb::tag<0>(density),
                                     fdbb::tag<1>(momentum_x),
                                     fdbb::tag<2>(momentum_y),
                                     fdbb::tag<3>(momentum_z),
                                     fdbb::tag<4>(energy));

    // Create block matrix from block expression
    fdbb::BlockMatrix<type_vector_d, 5, 3> Fluxes{ std::move(fluxes) };

    // Check f_0 of flux F in x-direction
    copy_vector_d2h(fdbb::utils::get<0>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check f_1 of flux F in x-direction
    copy_vector_d2h(fdbb::utils::get<3>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 1.1 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check f_2 of flux F in x-direction
    copy_vector_d2h(fdbb::utils::get<6>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check f_3 of flux F in x-direction
    copy_vector_d2h(fdbb::utils::get<9>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 1.5 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check f_4 of flux F in x-direction
    copy_vector_d2h(fdbb::utils::get<12>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 2.8 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check g_0 of flux G in y-direction
    copy_vector_d2h(fdbb::utils::get<1>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check g_1 of flux G in y-direction
    copy_vector_d2h(fdbb::utils::get<4>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check g_2 of flux G in y-direction
    copy_vector_d2h(fdbb::utils::get<7>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 2.6 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check g_3 of flux G in y-direction
    copy_vector_d2h(fdbb::utils::get<10>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check g_4 of flux G in y-direction
    copy_vector_d2h(fdbb::utils::get<13>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 5.6 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check h_0 of flux H in z-direction
    copy_vector_d2h(fdbb::utils::get<2>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check h_1 of flux H in z-direction
    copy_vector_d2h(fdbb::utils::get<5>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 1.5 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check h_2 of flux H in z-direction
    copy_vector_d2h(fdbb::utils::get<8>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check h_3 of flux H in z-direction
    copy_vector_d2h(fdbb::utils::get<11>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 5.1 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

    // Check h_4 of flux H in z-direction
    copy_vector_d2h(fdbb::utils::get<14>(Fluxes), result_h);
    fill_vector_h(dummy_h, type_real{ 8.4 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_CONSERVATIVE_FIXTURE_3D::len, type_real{ 1e-5 });

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
