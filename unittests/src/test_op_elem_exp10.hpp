/** @file test_op_elem_exp10.hpp
 *
 *  @brief UnitTests++ elem_exp10 test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test arithmetic operation: element-wise base-10 exponential function value
 */
TEST_FIXTURE(FDBB_FIXTURE, op_elem_exp10)
{
  TEST_INIT(
    (1), (FDBB_FIXTURE::len), (3 * sizeof(type_real)), (FDBB_FIXTURE::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{ 0.3 });

      // Base-10 exponential function value: exp10(0.3) = 1.995262314968879
      fdbb::tag<0>(result_d) = fdbb::elem_exp10(fdbb::tag<0>(result_d));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{ 1.995262314968879 });
  CHECK_ARRAY_CLOSE(result_h, dummy_h, FDBB_FIXTURE::len, type_real{ 1e-5 });
}
