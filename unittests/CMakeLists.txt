########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

if (CMAKE_VERSION VERSION_LESS 3.2)
  set(UPDATE_DISCONNECTED_IF_AVAILABLE "")
else()
  set(UPDATE_DISCONNECTED_IF_AVAILABLE "")#"UPDATE_DISCONNECTED 1")
endif()

#
# Option list
#

# Configure device
set(FDBB_BUILD_UNITTESTS_DEVICES "CPU" CACHE STRING "Build unittests for selected devices")
set_property(CACHE FDBB_BUILD_UNITTESTS_DEVICES PROPERTY STRINGS ALL CPU GPU0 GPU1 GPU2 GPU3 GPU4 GPU5 GPU6 GPU7)

# Configure mode
option(FDBB_BUILD_UNITTESTS_PERFMODE "Build unittests in performance measurement" OFF)

# Configure precision
set(FDBB_BUILD_UNITTESTS_PRECISIONS "ALL" CACHE STRING "Build unittests for selected precisions")
set_property(CACHE FDBB_BUILD_UNITTESTS_PRECISIONS PROPERTY STRINGS ALL float double)

# Configure runs
set(FDBB_BUILD_UNITTESTS_RUNS "1" CACHE STRING "Build unittests for specified number of runs")

# Configure sizes
set(FDBB_BUILD_UNITTESTS_SIZES "10" CACHE STRING "Build unittests for specified size")

# Configure tests
set(FDBB_BUILD_UNITTESTS_TESTS "ALL" CACHE STRING "Build unittests for specified tests")
set_property(CACHE FDBB_BUILD_UNITTESTS_TESTS PROPERTY STRINGS ALL)

# List of tests to be included
if (NOT DEFINED FDBB_UNITTESTS_TESTS_INCLUDE)
  set(FDBB_UNITTESTS_TESTS_INCLUDE "test_*")
endif()

# List of tests to be excluded
if (NOT DEFINED FDBB_UNITTESTS_TESTS_EXCLUDE)
  set(FDBB_UNITTESTS_TESTS_EXCLUDE "")
endif()

# Generate list of included header files
foreach (LIST_ITEM ${FDBB_UNITTESTS_TESTS_INCLUDE})
  # Remove suffix if present
  string(REPLACE ".hpp"  "" LIST_ITEM ${LIST_ITEM})
  # Collect all header files matching the given pattern
  file(GLOB HEADERS RELATIVE
    ${CMAKE_CURRENT_SOURCE_DIR}/src/
    ${CMAKE_CURRENT_SOURCE_DIR}/src/${LIST_ITEM}.hpp)
  if (NOT HEADERS STREQUAL "")
    list(APPEND FDBB_UNITTESTS_HEADERS ${HEADERS})
  endif()
endforeach()

# Generate list of excluded header files
foreach (LIST_ITEM ${FDBB_UNITTESTS_TESTS_EXCLUDE})
  # Remove suffix if present
  string(REPLACE ".hpp"  "" LIST_ITEM ${LIST_ITEM})
  # Collect all header files matching the given pattern
  file(GLOB HEADERS RELATIVE
    ${CMAKE_CURRENT_SOURCE_DIR}/src/
    ${CMAKE_CURRENT_SOURCE_DIR}/src/${LIST_ITEM}.hpp)
  if (NOT HEADERS STREQUAL "")
    list(REMOVE_ITEM FDBB_UNITTESTS_HEADERS ${HEADERS})
  endif()
endforeach()

# Extract test names from header file names
foreach(TEST_HEADER ${FDBB_UNITTESTS_HEADERS})
  string(REPLACE "test_" "" TEST_NAME ${TEST_HEADER})
  string(REPLACE ".hpp"  "" TEST_NAME ${TEST_NAME})
  set_property(CACHE FDBB_BUILD_UNITTESTS_TESTS APPEND PROPERTY STRINGS ${TEST_NAME})
endforeach()

option(FDBB_BUILD_UNITTESTS_ARMADILLO "Build Armadillo unittests" OFF)
option(FDBB_BUILD_UNITTESTS_ARRAYFIRE "Build ArrayFire unittests" OFF)
option(FDBB_BUILD_UNITTESTS_BLAZE     "Build Blaze unittests"     OFF)
option(FDBB_BUILD_UNITTESTS_CMTL4     "Build CMTL4 unittests"     OFF)
option(FDBB_BUILD_UNITTESTS_EIGEN     "Build Eigen unittests"     OFF)
option(FDBB_BUILD_UNITTESTS_ITPP      "Build IT++ unittests"      OFF)
option(FDBB_BUILD_UNITTESTS_MTL4      "Build MTL4 unittests"      OFF)
option(FDBB_BUILD_UNITTESTS_UBLAS     "Build uBLAS unittests"     OFF)
option(FDBB_BUILD_UNITTESTS_VEXCL     "Build VexCL unittests"     OFF)
option(FDBB_BUILD_UNITTESTS_VIENNACL  "Build ViennaCL unittests"  OFF)

#
# Summary
#
message("")
message("UnitTests:")
message("FDBB_BUILD_UNITTESTS_DEVICES.......: ${FDBB_BUILD_UNITTESTS_DEVICES}")
message("FDBB_BUILD_UNITTESTS_PERFMODE......: ${FDBB_BUILD_UNITTESTS_PERFMODE}")
message("FDBB_BUILD_UNITTESTS_PRECISIONS....: ${FDBB_BUILD_UNITTESTS_PRECISIONS}")
message("FDBB_BUILD_UNITTESTS_SIZES.........: ${FDBB_BUILD_UNITTESTS_SIZES}")
message("FDBB_BUILD_UNITTESTS_RUNS..........: ${FDBB_BUILD_UNITTESTS_RUNS}")
message("FDBB_BUILD_UNITTESTS_TESTS.........: ${FDBB_BUILD_UNITTESTS_TESTS}")
message("")
message("FDBB_BUILD_UNITTESTS_ARMADILLO.....: ${FDBB_BUILD_UNITTESTS_ARMADILLO}")
message("FDBB_BUILD_UNITTESTS_ARRAYFIRE.....: ${FDBB_BUILD_UNITTESTS_ARRAYFIRE}")
message("FDBB_BUILD_UNITTESTS_BLAZE.........: ${FDBB_BUILD_UNITTESTS_BLAZE}")
message("FDBB_BUILD_UNITTESTS_CMTL4.........: ${FDBB_BUILD_UNITTESTS_CMTL4}")
message("FDBB_BUILD_UNITTESTS_EIGEN.........: ${FDBB_BUILD_UNITTESTS_EIGEN}")
message("FDBB_BUILD_UNITTESTS_ITPP..........: ${FDBB_BUILD_UNITTESTS_ITPP}")
message("FDBB_BUILD_UNITTESTS_MTL4..........: ${FDBB_BUILD_UNITTESTS_MTL4}")
message("FDBB_BUILD_UNITTESTS_UBLAS.........: ${FDBB_BUILD_UNITTESTS_UBLAS}")
message("FDBB_BUILD_UNITTESTS_VEXCL.........: ${FDBB_BUILD_UNITTESTS_VEXCL}")
message("FDBB_BUILD_UNITTESTS_VIENNACL......: ${FDBB_BUILD_UNITTESTS_VIENNACL}")
message("")

########################################################################
# Device handling
########################################################################
if(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "ALL")
  add_definitions(-DSUPPORT_DEVICES_ALL)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "CPU")
  add_definitions(-DSUPPORT_DEVICES_CPU)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU0")
  add_definitions(-DSUPPORT_DEVICES_GPU=0)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU1")
  add_definitions(-DSUPPORT_DEVICES_GPU=1)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU2")
  add_definitions(-DSUPPORT_DEVICES_GPU=2)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU3")
  add_definitions(-DSUPPORT_DEVICES_GPU=3)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU4")
  add_definitions(-DSUPPORT_DEVICES_GPU=4)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU5")
  add_definitions(-DSUPPORT_DEVICES_GPU=5)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU6")
  add_definitions(-DSUPPORT_DEVICES_GPU=6)
elseif(FDBB_BUILD_UNITTESTS_DEVICES STREQUAL "GPU7")
  add_definitions(-DSUPPORT_DEVICES_GPU=7)
endif()

########################################################################
# Generate test suites and tests
########################################################################

# Define mode: timer/performance counter
if(FDBB_BUILD_UNITTESTS_PERFMODE)
  add_definitions(-DENABLE_PERFMODE)
endif()

# Define list of precisions: ALL = float;double
if(FDBB_BUILD_UNITTESTS_PRECISIONS STREQUAL "ALL")
  set(FDBB_BUILD_UNITTESTS_PRECISIONS "float;double")
endif()

# Check for admissible values
unset(FDBB_UNITTESTS_PRECISIONS)
foreach(PREC ${FDBB_BUILD_UNITTESTS_PRECISIONS})    
  if( ${PREC} STREQUAL "float" OR ${PREC} STREQUAL "std::float")
    list(APPEND FDBB_UNITTESTS_PRECISIONS "FLOAT")
  elseif(${PREC} STREQUAL "double" OR ${PREC} STREQUAL "std::double")
    list(APPEND FDBB_UNITTESTS_PRECISIONS "DOUBLE")
  elseif(${PREC} STREQUAL "complex<float>" OR ${PREC} STREQUAL "std::complex<float>")
    list(APPEND FDBB_UNITTESTS_PRECISIONS "COMPLEX_FLOAT")
  elseif(${PREC} STREQUAL "complex<double>" OR ${PREC} STREQUAL "std::complex<double>")
    list(APPEND FDBB_UNITTESTS_PRECISIONS "COMPLEX_DOUBLE")
  else()
    message(FATAL_ERROR "Unsupported precision: ${PREC}")
  endif()
endforeach()

# Define list of sizes
set(FDBB_UNITTESTS_SIZES ${FDBB_BUILD_UNITTESTS_SIZES})

# Define list of runs
list(LENGTH FDBB_BUILD_UNITTESTS_RUNS len_runs)
if(${len_runs} EQUAL "1")
  # Use same number of runs for all tests
  unset(FDBB_UNITTESTS_RUNS)
  foreach(SIZE ${FDBB_UNITTESTS_SIZES})
    list(APPEND FDBB_UNITTESTS_RUNS ${FDBB_BUILD_UNITTESTS_RUNS})
  endforeach()
else()
  # Use a different number of runs for each test
  list(LENGTH FDBB_BUILD_UNITTESTS_SIZES len_sizes)
  if(NOT ${len_runs} EQUAL ${len_sizes})
    message(FATAL_ERROR "Mismatch between number of runs and sizes.")
  endif()
  set(FDBB_UNITTESTS_RUNS ${FDBB_BUILD_UNITTESTS_RUNS})
endif()

# Define list of tests
if(NOT FDBB_BUILD_UNITTESTS_TESTS STREQUAL "ALL")
  unset(FDBB_UNITTESTS_HEADERS)  
  foreach(TEST ${FDBB_BUILD_UNITTESTS_TESTS})
    list(APPEND FDBB_UNITTESTS_HEADERS "test_${TEST}.hpp")
  endforeach()
endif()

########################################################################
# UnitTest++
########################################################################
include(UnitTest++)

########################################################################
# Add src directory
########################################################################
include_directories(src)
file (GLOB_RECURSE FDBB_HEADERS
  ${PROJECT_SOURCE_DIR}/src/*.h
  ${PROJECT_SOURCE_DIR}/src/*.hpp
  ${PROJECT_SOURCE_DIR}/src/detail/*.hpp)
source_group("Headers" FILES ${FDBB_HEADERS})

########################################################################
# Add subdirectories
########################################################################

# Armadillo tests
if(FDBB_BUILD_UNITTESTS_ARMADILLO)
  add_subdirectory(Armadillo)
endif()

# ArrayFire tests
if(FDBB_BUILD_UNITTESTS_ARRAYFIRE)
  add_subdirectory(ArrayFire)
endif()

# Blaze
if(FDBB_BUILD_UNITTESTS_BLAZE)
  add_subdirectory(Blaze)
endif()

# CMTL4
if(FDBB_BUILD_UNITTESTS_CMTL4)
  add_subdirectory(CMTL4)
endif()

# Eigen
if(FDBB_BUILD_UNITTESTS_EIGEN)
  add_subdirectory(Eigen)
endif()

# IT++
if(FDBB_BUILD_UNITTESTS_ITPP)
  add_subdirectory(IT++)
endif()

# MTL4
if(FDBB_BUILD_UNITTESTS_MTL4)
  add_subdirectory(MTL4)
endif()

# uBLAS
if(FDBB_BUILD_UNITTESTS_UBLAS)
  add_subdirectory(uBLAS)
endif()

# VexCL
if(FDBB_BUILD_UNITTESTS_VEXCL)
  add_subdirectory(VexCL)
endif()

# ViennaCL
if(FDBB_BUILD_UNITTESTS_VIENNACL)
  add_subdirectory(ViennaCL)
endif()
