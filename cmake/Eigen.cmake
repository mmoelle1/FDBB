########################################################################
# Eigen.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Eigen
########################################################################

if (FDBB_BUILTIN_EIGEN)

  if (FDBB_BUILTIN_EIGEN MATCHES "3.2.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.0/eigen-3.2.0.tar.bz2
      URL_MD5           e2ae4a01ba621cfc449aba835c0a83af
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.1/eigen-3.2.1.tar.bz2
      URL_MD5           aea2c4839c6f5c80036d65847e00a813
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.2/eigen-3.2.2.tar.bz2
      URL_MD5           21bc9b11a27bf7e235910bbf1d77b7cf
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.3")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.3/eigen-3.2.3.tar.bz2
      URL_MD5           f90ffcaed57779a98fe906dd5ff6ae12
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.4")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.4/eigen-3.2.4.tar.bz2
      URL_MD5           1a2f7e607f9c34a69fefbafeca08ef7b
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.5")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.5/eigen-3.2.5.tar.bz2
      URL_MD5           d28d0225f8067a2f61069e654dddd57a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.6")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.6/eigen-3.2.6.tar.bz2
      URL_MD5           790049e434647854ff38e621f20645f7
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.7")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.7/eigen-3.2.7.tar.bz2
      URL_MD5           3d99db5afffd2e14eca54497c0f2e57c
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.8")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.8/eigen-3.2.8.tar.bz2
      URL_MD5           44c51536f5abc8de8a5e733819e5fc2c
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.9")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.9/eigen-3.2.9.tar.bz2
      URL_MD5           4b318f82e8028af2d8346237f6a1bcfd
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.2.10")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.2.10/eigen-3.2.10.tar.bz2
      URL_MD5           619329f3b03c9640f56150d4f40c9fdb
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.0/eigen-3.3.0.tar.bz2
      URL_MD5           345d7c7cae856b13e092c96705419138
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.1/eigen-3.3.1.tar.bz2
      URL_MD5           2971b8f96be55a8569c110a5077069a6
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.2/eigen-3.3.2.tar.bz2
      URL_MD5           05b29f29231257b9ad0975ba05c0b92a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.3")
  
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.3/eigen-3.3.3.tar.bz2
      URL_MD5           f5e7aebb881770d0836c42a288c869f0
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.4")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.4/eigen-3.3.4.tar.bz2
      URL_MD5           1896a1f682e6cdcffd91b6e5ba8286a2
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.5")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.5/eigen-3.3.5.tar.bz2
      URL_MD5           305eed19157335016bde9b4d57ee3d35
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.6")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.6/eigen-3.3.6.tar.bz2
      URL_MD5           2de216975d12a8947e5d91bd66f499b1
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.7")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.7/eigen-3.3.7.tar.bz2
      URL_MD5           77a2c934eaf35943c43ee600a83b72df
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.8")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.8/eigen-3.3.8.tar.bz2
      URL_MD5           432ef01499d514f4606343276afa0ec3
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3.9")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen                       
      URL               https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.tar.bz2
      URL_MD5           2d5a8dac126c4937fd94d5d10fcd7dd1
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  else()
    message(FATAL_ERROR, "Invalid Eigen version.")
  endif()
  
  # Add include directory
  include_directories("${Eigen_SOURCE_DIR}")

elseif (FDBB_BUILTIN_EIGEN MATCHES "latest")
  
  include(DownloadProject)
  download_project(
    PROJ              Eigen
    GIT_REPOSITORY    
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
    
  # Add include directory
  include_directories("${Eigen_SOURCE_DIR}")
  
else()

  # Add include directory
  if(EIGEN_INCLUDE_PATH)
    include_directories(${EIGEN_INCLUDE_PATH})
  else()
    message(WARNING "Variable EIGEN_INCLUDE_PATH is not defined. FDBB might be unable to find Eigen include files.")
  endif()
  
  # Add libraries
  if(EIGEN_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${EIGEN_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${EIGEN_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable EIGEN_LIBRARIES must point to the Eigen libraries.")
  endif()
  
endif()

# Enable Eigen support
add_definitions(-DFDBB_BACKEND_EIGEN)
