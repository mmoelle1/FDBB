########################################################################
# MTL4.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

# MTL4 depends on the Boost library
find_package(Boost COMPONENTS QUIET REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})
set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
########################################################################
# MTL4
########################################################################

if (FDBB_BUILTIN_MTL4)
  
  include(DownloadProject)
  download_project(
    PROJ              MTL4
    SVN_REPOSITORY    https://svn.simunova.com/svn/mtl4/trunk
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/MTL4
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Find MTL
  find_package(MTL REQUIRED HINTS ${MTL4_SOURCE_DIR})
  
  # Add MTL definitions
  add_definitions(-${MTL_CXX_DEFINITIONS})
  
  # Enable OpenMP support
  if(FDBB_WITH_OMP)
    add_definitions(-DENABLE_OPENMP)
  endif()
  
  # Add include directory
  include_directories("${MTL4_SOURCE_DIR}")

else()

  # Add include directory
  if(MTL4_INCLUDE_PATH)
    include_directories(${MTL4_INCLUDE_PATH})
  else()
    message(WARNING "Variable MTL4_INCLUDE_PATH is not defined. FDBB might be unable to find MTL4 include files.")
  endif()
  
  # Add libraries
  if(MTL4_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${MTL4_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${MTL4_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable MTL4_LIBRARIES must point to the MTL4 libraries.")
  endif()
  
endif()

# Enable MTL4 support
add_definitions(-DFDBB_BACKEND_MTL4)
