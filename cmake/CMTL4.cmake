########################################################################
# CMTL4.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

# CMTL4 depends on the Boost library
find_package(Boost COMPONENTS QUIET REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})
set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
########################################################################
# CMTL4
########################################################################

if (FDBB_BUILTIN_CMTL4)
  
  include(DownloadProject)
  download_project(
    PROJ              CMTL4
    SVN_REPOSITORY    https://svn.simunova.com/svn/mtl4/branches/cuda
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/CMTL4
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Find MTL
  find_package(MTL REQUIRED HINTS ${CMTL4_SOURCE_DIR})
  
  # Add CMTL4 definitions
  add_definitions(${MTL_CXX_DEFINITIONS})
  
  # Add include directory
  include_directories("${CMTL4_SOURCE_DIR}")

else()

  # Add include directory
  if(CMTL4_INCLUDE_PATH)
    include_directories(${CMTL4_INCLUDE_PATH})
  else()
    message(WARNING "Variable CMTL4_INCLUDE_PATH is not defined. FDBB might be unable to find CMTL4 include files.")
  endif()

  # Add libraries
  if(CMTL4_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${CMTL4_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${CMTL4_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable CMTL4_LIBRARIES must point to the CMTL4 libraries.")
  endif()
  
endif()

# Enable CMTL4 support
add_definitions(-DFDBB_BACKEND_CMTL4)
