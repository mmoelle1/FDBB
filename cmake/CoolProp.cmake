########################################################################
# CoolProp.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# CoolProp
########################################################################

if (FDBB_BUILTIN_COOLPROP)
  
  include(DownloadProject)
  download_project(
    PROJ              CoolProp
    GIT_REPOSITORY    https://github.com/CoolProp/CoolProp.git
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/CoolProp
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process CoolProp project
  if (NOT TARGET CoolProp)
    
    # Detect static/shared library
    #if(BUILD_SHARED_LIBS)
    #  set(COOLPROP_SHARED_LIBRARY true)
    #else()
      set(COOLPROP_STATIC_LIBRARY true)
    #endif()

    # Detect build type
    if(CMAKE_BUILD_TYPE STREQUAL "Release" OR CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
      set(COOLPROP_RELEASE ON)
    else()
      set(COOLPROP_DEBUG   ON)
    endif()
    
    add_subdirectory(${CoolProp_SOURCE_DIR} ${CoolProp_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${CoolProp_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   CoolProp)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES CoolProp)

else()

  # Add include directory
  if(COOLPROP_INCLUDE_PATH)
    include_directories(${COOLPROP_INCLUDE_PATH})
  else()
    message(WARNING "Variable COOLPROP_INCLUDE_PATH is not defined. FDBB might be unable to find CoolProp include files.")
  endif()

  # Add libraries
  if(COOLPROP_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${COOLPROP_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${COOLPROP_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable COOLPROP_LIBRARIES must point to the CoolProp libraries.")
  endif()
  
endif()

# Enable CoolProp support
add_definitions(-DFDBB_WITH_COOLPROP)
