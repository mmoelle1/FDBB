########################################################################
# VexCL.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# VexCL
########################################################################

if (FDBB_BUILTIN_VEXCL)

  if (FDBB_BUILTIN_VEXCL MATCHES "1.4.0")
    
    include(DownloadProject)
    download_project(
      PROJ              VexCL
      URL               https://github.com/ddemidov/vexcl/archive/1.4.0.tar.gz
      URL_MD5           2703e51b83dad608dc1abe51abef6869
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/VexCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_VEXCL MATCHES "1.4.1")
    
    include(DownloadProject)
    download_project(
      PROJ              VexCL
      URL               https://github.com/ddemidov/vexcl/archive/1.4.1.tar.gz
      URL_MD5           0469acddbee87e85f61ac04d952900f0
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/VexCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_VEXCL MATCHES "1.4.2")
    
    include(DownloadProject)
    download_project(
      PROJ              VexCL
      URL               https://github.com/ddemidov/vexcl/archive/1.4.2.tar.gz
      URL_MD5           40203a5e42b1e1f5161820f4d275930b
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/VexCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_VEXCL MATCHES "latest")
    
    include(DownloadProject)
    download_project(
      PROJ              VexCL
      GIT_REPOSITORY    https://github.com/ddemidov/vexcl.git
      GIT_TAG           master
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/VexCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  else()
    message(FATAL_ERROR, "Invalid VexCL version.")
  endif()

  # VexCL depends on the Boost library
  find_package(Boost COMPONENTS
    date_time
    filesystem
    system
    thread
    unit_test_framework
    program_options QUIET REQUIRED)
  include_directories(${Boost_INCLUDE_DIRS})
  set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES ${Boost_LIBRARIES})
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${Boost_LIBRARIES})
  
  # Set VexCL backend
  add_definitions(-DVEXCL_SHOW_COPIES)
  if(FDBB_WITH_CUDA)
    add_definitions(-DVEXCL_BACKEND_CUDA)
    include_directories(${CUDA_INCLUDE_DIRS})
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES cuda)
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES cuda)
  elseif(FDBB_WITH_OCL)
    add_definitions(-DVEXCL_BACKEND_OPENCL)
    include_directories(${OpenCL_INCLUDE_DIRS})
  else()
    if (UNIX AND NOT APPLE)
      list(APPEND FDBB_C_TARGET_LINK_LIBRARIES dl)
      list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES dl)
    endif()
    add_definitions(-DVEXCL_BACKEND_JIT -DVEXCL_JIT_COMPILER="${CMAKE_CXX_COMPILER}" -DVEXCL_JIT_COMPILER_OPTIONS="$<$<CXX_COMPILER_ID:GNU>:-fPIC -Wno-ignored-attributes> -shared ${CMAKE_CXX_FLAGS} $<$<CONFIG:Debug>:${CMAKE_CXX_FLAGS_DEBUG}>$<$<CONFIG:Release>:${CMAKE_CXX_FLAGS_RELEASE}>$<$<CONFIG:RelWithDebInfo>:${CMAKE_CXX_FLAGS_RELWITHDEBINFO}> -I${Boost_INCLUDE_DIRS}")
  endif()

  # Supress excessive warnings from GNU compiler
  if (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    add_definitions(-Wno-ignored-attributes)
  endif()
  
  # Add include directory
  include_directories("${VexCL_SOURCE_DIR}")

else()

  # Add include directory
  if(VEXCL_INCLUDE_PATH)
    include_directories(${VEXCL_INCLUDE_PATH})
  else()
    message(WARNING "Variable VEXCL_INCLUDE_PATH is not defined. FDBB might be unable to find VexCL include files.")
  endif()
  
  # Add libraries
  if(VEXCL_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${VEXCL_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${VEXCL_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable VEXCL_LIBRARIES must point to the VexCL libraries.")
  endif()
  
endif()

# Enable VexCL support
add_definitions(-DFDBB_BACKEND_VEXCL)
