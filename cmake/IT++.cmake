########################################################################
# IT++.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# IT++
########################################################################

if (FDBB_BUILTIN_ITPP)
  
  include(DownloadProject)
  download_project(
    PROJ              IT++
    GIT_REPOSITORY    http://git.code.sf.net/p/itpp/git
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/IT++
    PATCH_COMMAND     patch -p0 -N -d ${CMAKE_BINARY_DIR}/external/IT++/IT++-src < ${CMAKE_SOURCE_DIR}/patches/itpp_cmake.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process ITpp project
  if ((NOT TARGET itpp)        AND
      (NOT TARGET itpp_debug)  AND
      (NOT TARGET itpp_static))
    if (BUILD_SHARED_LIBS)
      set(ITPP_SHARED_LIB ON CACHE BOOL "Build IT++ shared library")
    else()
      set(ITPP_SHARED_LIB OFF CACHE BOOL "Build IT++ static library")
    endif()
    set(HTML_DOCS OFF CACHE BOOL "Build IT++ without documentation")
    add_subdirectory(${IT++_SOURCE_DIR} ${IT++_BINARY_DIR})
  endif()
    
  # Add include directory
  include_directories(${IT++_SOURCE_DIR} ${IT++_BINARY_DIR})
  
  # Add library
  if (ITPP_SHARED_LIBS)
    if ((CMAKE_BUILD_TYPE STREQUAL Release) OR (NOT CMAKE_BUILD_TYPE))
      list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   itpp)
      list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES itpp)
    else()
      list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   itpp_debug)
      list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES itpp_debug)
    endif()
  else()
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   itpp_static)
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES itpp_static)
  endif()

else()

  # Add include directory
  if(ITPP_INCLUDE_PATH)
    include_directories(${ITPP_INCLUDE_PATH})
  else()
    message(WARNING "Variable ITPP_INCLUDE_PATH is not defined. FDBB might be unable to find IT++ include files.")
  endif()

  # Add libraries
  if(ITPP_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${ITPP_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${ITPP_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable ITPP_LIBRARIES must point to the IT++ libraries.")
  endif()
  
endif()

# Enable IT++ support
add_definitions(-DFDBB_BACKEND_ITPP)
