########################################################################
# uBLAS.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

if (FDBB_BUILTIN_UBLAS)

  if (FDBB_BUILTIN_UBLAS MATCHES "1.69.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.69.0/source/boost_1_69_0.tar.gz
      URL_MD5           b50944c0c13f81ce2c006802a1186f5a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.70.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.70.0/source/boost_1_70_0.tar.gz
      URL_MD5           fea771fe8176828fabf9c09242ee8c26
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.71.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.71.0/source/boost_1_71_0.tar.gz
      URL_MD5           5f521b41b79bf8616582c4a8a2c10177
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.72.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.72.0/source/boost_1_72_0.tar.gz
      URL_MD5           e2b0b1eac302880461bcbef097171758
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.73.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.73.0/source/boost_1_73_0.tar.gz
      URL_MD5           4036cd27ef7548b8d29c30ea10956196
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.74.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.74.0/source/boost_1_74_0.tar.gz
      URL_MD5           3c8fb92ce08b9ad5a5f0b35731ac2c8e
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.75.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.75.0/source/boost_1_75_0.tar.gz
      URL_MD5           38813f6feb40387dfe90160debd71251
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_UBLAS MATCHES "1.76.0")
    
    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      URL               https://boostorg.jfrog.io/artifactory/main/release/1.76.0/source/boost_1_76_0.tar.gz
      URL_MD5           e425bf1f1d8c36a3cd464884e74f007a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_UBLAS MATCHES "latest")

    include(DownloadProject)
    download_project(
      PROJ              uBLAS
      GIT_REPOSITORY    https://github.com/boostorg/boost.git
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/uBLAS
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/uBLAS/uBLAS-src < ${CMAKE_SOURCE_DIR}/patches/uBLAS.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid uBLAS version.")
  endif()

  # Add include directory
  include_directories("${uBLAS_SOURCE_DIR}")
  
else()
  
  # Find system-wide BOOST installation
  find_package(Boost COMPONENTS QUIET REQUIRED)
    
  # Add include directory
  include_directories(${Boost_INCLUDE_DIRS})
  set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
endif()

# Enable uBLAS support
add_definitions(-DFDBB_BACKEND_UBLAS)
