########################################################################
# Armadillo.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Armadillo
########################################################################

if (FDBB_BUILTIN_ARMADILLO)

  if (FDBB_BUILTIN_ARMADILLO MATCHES "7.800.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/7.800.x/armadillo-code-7.800.x.tar.gz
      URL_MD5           e86f6370d32fd36646971c89864e1647
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "7.960.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/7.960.x/armadillo-code-7.960.x.tar.gz
      URL_MD5           7bf41ca837236dcf3d6cbeef3e476baf
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "8.200.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/8.200.x/armadillo-code-8.200.x.tar.gz
      URL_MD5           5b7d3238b4dae87d7972c4131e13c078
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "8.300.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/8.300.x/armadillo-code-8.300.x.tar.gz
      URL_MD5           84b64346a75924fe9e77e25cf42e147d
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "8.400.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/8.400.x/armadillo-code-8.400.x.tar.gz
      URL_MD5           124526e8c13a41df48bfb80490c2cd19
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "8.500.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/8.500.x/armadillo-code-8.500.x.tar.gz
      URL_MD5           5ad600e0958e71ac71c0af32c4357995
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "8.600.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/8.600.x/armadillo-code-8.600.x.tar.gz
      URL_MD5           7630a5f0dd79302e700a4027fac839c1
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.100.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.100.x/armadillo-code-9.100.x.tar.gz
      URL_MD5           90a9430edbecda9bf5831c8cfd25fc87
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.200.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.200.x/armadillo-code-9.200.x.tar.gz
      URL_MD5           d099804d12816639a0f24bb932d7f72e
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.300.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.300.x/armadillo-code-9.300.x.tar.gz
      URL_MD5           35075375ad0bb59e3ed42936113cd224
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.400.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.400.x/armadillo-code-9.400.x.tar.gz
      URL_MD5           b43252e75f1ddd4e41e0856febf5f15a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.500.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.500.x/armadillo-code-9.500.x.tar.gz
      URL_MD5           d63fbca5679145a74b82d07b5444fc2a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.600.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.600.x/armadillo-code-9.600.x.tar.gz
      URL_MD5           e8eca7deb9556e9e275b809b708e736a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.700.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.700.x/armadillo-code-9.700.x.tar.gz
      URL_MD5           bf0195f3320803c471983a1eca21230e
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.800.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.800.x/armadillo-code-9.800.x.tar.gz
      URL_MD5           69d86659dc12fe343ee7ba269d44d9ab
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.850.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.850.x/armadillo-code-9.850.x.tar.gz
      URL_MD5           5988cf8b89b3ecacae93312ed1371458
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.860.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.860.x/armadillo-code-9.860.x.tar.gz
      URL_MD5           293d2ecaed776c829732978d280c9d14
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "9.900.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/9.900.x/armadillo-code-9.900.x.tar.gz
      URL_MD5           65782cd3945c1037753619d64e65926a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "10.1.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/10.1.x/armadillo-code-10.1.x.tar.gz
      URL_MD5           8b82fdd3e8fff883cb81c29348de71d5
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "10.2.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/10.2.x/armadillo-code-10.2.x.tar.gz
      URL_MD5           28a0dd88b1f4b6d99d22ed127f738cba
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "10.3.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/10.3.x/armadillo-code-10.3.x.tar.gz
      URL_MD5           988e004bb27bee3dc9037b20f1ab309b
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "10.4.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/10.4.x/armadillo-code-10.4.x.tar.gz
      URL_MD5           32bf83c0a71dad601a6f1bc36e70746f
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "10.5.x")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               https://gitlab.com/conradsnicta/armadillo-code/-/archive/10.5.x/armadillo-code-10.5.x.tar.gz
      URL_MD5           596355effd5eeae2d7437a05094fc833
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_ARMADILLO MATCHES "latest")

    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      GIT_REPOSITORY    https://gitlab.com/conradsnicta/armadillo-code.git
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid Armadillo version.")
  endif()
    
  # Process Armadillo project
  if (NOT TARGET armadillo)
    add_subdirectory(${Armadillo_SOURCE_DIR} ${Armadillo_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${Armadillo_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   armadillo)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES armadillo)

elseif (FDBB_BUILTIN_ARMADILLO MATCHES "latest")
  
  include(DownloadProject)
  download_project(
    PROJ              Armadillo
    GIT_REPOSITORY    https://github.com/conradsnicta/armadillo-code.git
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process Armadillo project
  if (NOT TARGET armadillo)
    add_subdirectory(${Armadillo_SOURCE_DIR} ${Armadillo_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${Armadillo_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   armadillo)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES armadillo)
  
else()
  
  # Add include directory
  if(ARMADILLO_INCLUDE_PATH)
    include_directories(${ARMADILLO_INCLUDE_PATH})
  else()
    message(WARNING "Variable ARMADILLO_INCLUDE_PATH is not defined. FDBB might be unable to find Armadillo include files.")
  endif()

  # Add libraries
  if(ARMADILLO_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${ARMADILLO_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${ARMADILLO_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable ARMADILLO_LIBRARIES must point to the Armadillo libraries.")
  endif()
  
endif()

# Enable Armadillo support
add_definitions(-DFDBB_BACKEND_ARMADILLO)
