########################################################################
# add_executables.cmake
#
# Author: Matthias Moller
# Copyright (C) 2014 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

#
# CMake macro: add executables for each source file
#
# Remark: The source files must be given with relative paths
#
macro(add_executables SRC)
  
  # Loop over enabled languages
  foreach(lang C CXX)

    # Determine file extensions
    if(${lang} STREQUAL C)
      set(SUFFIX "\\.c$")
    elseif(${lang} STREQUAL CXX)
      set(SUFFIX "\\.(cc|cpp|cxx|cu)$")
    else()
      message(ERROR "Language ${lang} not recognized")
    endif()
    
    # Loop over source files
    foreach(src ${SRC})
      
      # Filter files according to file suffix
      string(REGEX MATCH "[^/|\\].*${SUFFIX}" src ${src})
      
      if(src)       
        # Remove file suffix     
        string(REGEX REPLACE ${SUFFIX} "" name ${src})
        
        # Add executable. Depending on whether CUDA support is enabled or
        # not the source is passed to the NVCC or the C/C++ compiler
        if (CUDA_FOUND)
          cuda_add_executable( ${name} ${src} ${FDBB_HEADERS})
        else()
          add_executable( ${name} ${src} ${FDBB_HEADERS})
        endif()

        # Set C++ standard
        if(${FDBB_CXX_STANDARD} MATCHES "11")
          # Enable C++11 support.
          set_property(TARGET ${name} PROPERTY CXX_STANDARD 11)
          set_property(TARGET ${name} PROPERTY CXX_STANDARD_REQUIRED_ON)
          
          # Workaround for Intel which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++11")
          endif()

          # Workaround for PGI which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PGI")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++11")
          endif()
          
          # Workaround for OracleStudio which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "SunPro")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++11")
          endif()

        elseif(${FDBB_CXX_STANDARD} MATCHES "14")
          # Enable C++14 support
          set_property(TARGET ${name} PROPERTY CXX_STANDARD 14)
          set_property(TARGET ${name} PROPERTY CXX_STANDARD_REQUIRED_ON)
          
          # Workaround for Intel which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++14")
          endif()

          # Workaround for PGI which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PGI")
            message(FATAL_ERROR "PGI compiler does not support C++14 standard yet")
          endif()

          # Workaround for OracleStudio which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "SunPro")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++14")
          endif()

        elseif(${FDBB_CXX_STANDARD} MATCHES "17")
          # Enable C++17 support
          set_property(TARGET ${name} PROPERTY CXX_STANDARD 17)
          set_property(TARGET ${name} PROPERTY CXX_STANDARD_REQUIRED_ON)
          
          # Workaround for Intel which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++17")
          endif()

          # Workaround for PGI which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PGI")
            message(FATAL_ERROR "PGI compiler does not support C++17 standard yet")
          endif()

          # Workaround for OracleStudio which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "SunPro")
            message(FATAL_ERROR "OracleStudio compiler does not support C++17 standard yet")
          endif()

        elseif(${FDBB_CXX_STANDARD} MATCHES "20")
          # Enable C++20 support
          set_property(TARGET ${name} PROPERTY CXX_STANDARD 20)
          set_property(TARGET ${name} PROPERTY CXX_STANDARD_REQUIRED_ON)
          
          # Workaround for Intel which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
            set_source_files_properties(${src} PROPERTIES COMPILE_FLAGS "-std=c++20")
          endif()

          # Workaround for PGI which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "PGI")
            message(FATAL_ERROR "PGI compiler does not support C++17 standard yet")
          endif()

          # Workaround for OracleStudio which does not yet support CXX_STANDARD
          if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "SunPro")
            message(FATAL_ERROR "OracleStudio compiler does not support C++17 standard yet")
          endif()
          
        else()
          message(FATAL_ERROR "Unsupported C++ standard")
        endif()
        
        # Set install rule
        string(REGEX REPLACE "^/.*/" "" appdir ${CMAKE_CURRENT_LIST_DIR})
        install(TARGETS ${name} DESTINATION ${appdir}/bin)

        # Set library dependencies
        if(FDBB_${lang}_TARGET_DEPENDENCIES)
          add_dependencies(${name} ${FDBB_${lang}_TARGET_DEPENDENCIES})
        endif()
        
        # Link to libraries
        if(FDBB_${lang}_TARGET_LINK_LIBRARIES)
          target_link_libraries(${name} ${FDBB_${lang}_TARGET_LINK_LIBRARIES})
        endif()
        
      endif()
      
    endforeach()
  endforeach()
endmacro()
