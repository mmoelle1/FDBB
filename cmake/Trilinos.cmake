########################################################################
# Trilinos.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Trilinos
########################################################################

if (FDBB_BUILTIN_TRILINOS)
  
  include(DownloadProject)
  download_project(
    PROJ              Trilinos
    URL               http://trilinos.csbsju.edu/download/files/trilinos-12.6.3-Source.tar.bz2
    URL_MD5           d94e31193559b334fd41d05eb22f9285
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Trilinos
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process Trilinos project with Epetra support enabled
  if (NOT TARGET epetra)
    set(Trilinos_ENABLE_Fortran OFF)
    set(Trilinos_ENABLE_Epetra  ON)
    set(Trilinos_ENABLE_Tpetra  ON)
    add_subdirectory(${Trilinos_SOURCE_DIR} ${Trilinos_BINARY_DIR})
  endif()
  
  # Add include directory
  message("${Trilinos_BINARY_DIR}/packages/epetra")
  include_directories("${Trilinos_BINARY_DIR}")
  include_directories("${Trilinos_BINARY_DIR}/packages/epetra/src")
  include_directories("${Trilinos_SOURCE_DIR}/packages/epetra/src")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   epetra)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES epetra)

else()

  # Add include directory
  if(TRILINOS_INCLUDE_PATH)
    include_directories(${TRILINOS_INCLUDE_PATH})
  else()
    message(WARNING "Variable TRILINOS_INCLUDE_PATH is not defined. FDBB might be unable to find Trilinos include files.")
  endif()
  
  # Add libraries
  if(TRILINOS_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${TRILINOS_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${TRILINOS_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable TRILINOS_LIBRARIES must point to the Trilinos libraries.")
  endif()
  
endif()

# Enable Trilinos with Epetra support
add_definitions(-DFDBB_BACKEND_EPETRA)
