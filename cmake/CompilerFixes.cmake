########################################################################
# CompilerFixes.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Apple Clang/Clang++
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "AppleClang")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
endif()

########################################################################
# Clang/Clang++
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
endif()

########################################################################
# GNU GCC/G++
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  # GNU C++ compiler needs "-fext-numeric-literals" to properly compile Boost library
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fext-numeric-literals")
endif()

########################################################################
# Intel ICC/ICPC
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "Intel")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
  # Intel C++ compiler below version 15.x does not support gcc 4.9
  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS 15.0)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -I/usr/include/c++/4.8 -I/usr/include/x86_64-linux-gnu/c++/4.8 -I/usr/lib/gcc/x86_64-linux-gnu/4.8/include")
  endif()
endif()

########################################################################
# PGI
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "PGI")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "PGI")
  # PGI C++ compiler brings its own Lapack library which needs
  # additional run-time libraries to be linked to the executable
  set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lpgftnrtl -lrt")
endif()

########################################################################
# PathScale
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "PathScale")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "PathScale")
endif()

########################################################################
# Sun/Oracle Studio
########################################################################

if(CMAKE_C_COMPILER_ID STREQUAL "SunPro")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "SunPro")
endif()
