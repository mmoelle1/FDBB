########################################################################
# add_tests.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

#
# CMake macro: add tests for each source file
#
# Remark: The source files must be given with relative paths

macro(add_tests SRC)

  # Loop over enabled languages
  foreach(lang C CXX)

    # Determine file extensions
    if(${lang} STREQUAL C)
      set(SUFFIX "\\.c$")
    elseif(${lang} STREQUAL CXX)
      set(SUFFIX "\\.(cc|cpp|cxx|cu)$")
    else()
      message(ERROR "Language ${lang} not recognized")
    endif()
    
    # Loop over source files
    foreach(src ${SRC})
      
      # Filter files according to file suffix
      string(REGEX MATCH "[^/|\\].*${SUFFIX}" src ${src})
      
      if(src)       
        # Remove file suffix     
        string(REGEX REPLACE ${SUFFIX} "" name ${src})
        
        # Add test.
        add_test( ${name} ${name} )
      endif()
      
    endforeach()
  endforeach()
endmacro()
