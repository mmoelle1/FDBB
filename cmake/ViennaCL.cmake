########################################################################
# VienaCL.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

# ViennaCL depends on the Boost library
find_package(Boost COMPONENTS QUIET REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})
set(LIBS ${LIBS} ${Boost_LIBRARIES})

########################################################################
# ViennaCL
########################################################################

if (FDBB_BUILTIN_VIENNACL)

  if (FDBB_BUILTIN_VIENNACL MATCHES "1.7.0")

    include(DownloadProject)
    download_project(
      PROJ              ViennaCL
      URL               https://github.com/viennacl/viennacl-dev/archive/release-1.7.0.tar.gz
      URL_MD5           310e8a2e130f2109496f35bd81980ce7
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/ViennaCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_VIENNACL MATCHES "1.7.1")
    
    include(DownloadProject)
    download_project(
      PROJ              ViennaCL
      URL               https://github.com/viennacl/viennacl-dev/archive/release-1.7.1.tar.gz
      URL_MD5           9b3882bc08189b84eda57549d17e6469
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/ViennaCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_VIENNACL MATCHES "latest")
    
    include(DownloadProject)
    download_project(
      PROJ              ViennaCL
      GIT_REPOSITORY    https://github.com/viennacl/viennacl-dev.git
      GIT_TAG           master
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/ViennaCL
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid ViennaCL version.")
  endif()
  
  # Add include directory
  include_directories("${ViennaCL_SOURCE_DIR}")
  
else()

  # Add include directory
  if(VIENNACL_INCLUDE_PATH)
    include_directories(${VIENNACL_INCLUDE_PATH})
  else()
    message(WARNING "Variable VIENNACL_INCLUDE_PATH is not defined. FDBB might be unable to find ViennaCL include files.")
  endif()
  
  # Add libraries
  if(VIENNACL_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${VIENNACL_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${VIENNACL_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable VIENNACL_LIBRARIES must point to the ViennaCL libraries.")
  endif()
  
endif()

# Set ViennaCL backend
if(FDBB_WITH_CUDA)
  add_definitions(-DVIENNACL_WITH_CUDA)
elseif(FDBB_WITH_OCL)
  add_definitions(-DVIENNACL_WITH_OPENCL)
elseif(FDBB_WITH_OMP)
  add_definitions(-DVIENNACL_WITH_OPENMP)
endif()

# Enable ViennaCL support
add_definitions(-DFDBB_BACKEND_VIENNACL)
