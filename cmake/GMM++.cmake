########################################################################
# GMM++.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# GMM++
########################################################################

if (FDBB_BUILTIN_GMM)

  if (FDBB_BUILTIN_GMM MATCHES "5.1")
  
    include(DownloadProject)
    download_project(
      PROJ              GMM++
      URL               http://download.gna.org/getfem/stable/gmm-5.1.tar.gz
      #  URL_MD5           
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/GMM++
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid GMM version.")
  endif()
  
  # Process GMM++ project
  if (NOT TARGET GMM++)
    add_subdirectory(${GMM++_SOURCE_DIR} ${GMM++_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${GMM++_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   GMM)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES GMM)

else()

  # Add include directory
  if(GMM_INCLUDE_PATH)
    include_directories(${GMM_INCLUDE_PATH})
  else()
    message(WARNING "Variable GMM_INCLUDE_PATH is not defined. FDBB might be unable to find GMM include files.")
  endif()
  
  # Add libraries
  if(GMM_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${GMM_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${GMM_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable GMM_LIBRARIES must point to the GMM libraries.")
  endif()
  
endif()

# Enable GMM++ support
add_definitions(-DFDBB_BACKEND_GMM)
