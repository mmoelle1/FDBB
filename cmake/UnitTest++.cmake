########################################################################
# UnitTest++.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# UnitTest
########################################################################

if (FDBB_BUILTIN_UNITTESTS)
  
  include(DownloadProject)
  download_project(
    PROJ              unittest
    GIT_REPOSITORY    https://github.com/unittest-cpp/unittest-cpp
    GIT_TAG           master
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/UnitTest++
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/UnitTest++/unittest-src < ${CMAKE_SOURCE_DIR}/patches/UnitTest++_cmake.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process UnitTest++ project
  if (NOT TARGET UnitTest++)
    add_subdirectory(${unittest_SOURCE_DIR} ${unittest_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${unittest_SOURCE_DIR}")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   UnitTest++)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES UnitTest++)

else()

  # Add include directory
  if(UNITTESTS_INCLUDE_PATH)
    include_directories(${UNITTESTS_INCLUDE_PATH})
  else()
    message(WARNING "Variable UNITTESTS_INCLUDE_PATH is not defined. FDBB might be unable to find UnitTests++ include files.")
  endif()

  # Add libraries
  if(UNITTESTS_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${UNITTESTS_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${UNITTESTS_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable UNITTESTS_LIBRARIES must point to the UnitTests++ libraries.")
  endif()
  
endif()
