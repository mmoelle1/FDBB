########################################################################
# Blaze.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

# Blaze requires C++14-support enabled
if(NOT ${FDBB_CXX_STANDARD} MATCHES "14")
  message(FATAL_ERROR "Blaze must be compiled with C++14-support enabled")
endif()
  
########################################################################
# Blaze
########################################################################

if (FDBB_BUILTIN_BLAZE)

  if (FDBB_BUILTIN_BLAZE MATCHES "3.0")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.0.tar.gz
      URL_MD5           0c4cefb0be7b5a27ed8a377941be1ab1
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.1")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.1.tar.gz
      URL_MD5           2938e015f0d274e8d62ee5c4c0c1e9f3
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.2")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.2.tar.gz
      URL_MD5           47bd4a4f1b6292f5a6f71ed9d5287480
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.3")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.3.tar.gz
      URL_MD5           78c3a4ae75366dd354265dbae467c84a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.4")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.4.tar.gz
      URL_MD5           e41e6d86b720c580374af688b35e1124
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.5")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.5.tar.gz
      URL_MD5           3ba946fd96ae62885a1463a9017685f4
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.6")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.6.tar.gz
      URL_MD5           059a8aed50991bed9ec72115dd208e8e
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.7")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.7.tar.gz
      URL_MD5           5e85f8b881bb1ba30ffc589942d7363d
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (FDBB_BUILTIN_BLAZE MATCHES "3.8")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      URL               https://bitbucket.org/blaze-lib/blaze/downloads/blaze-3.8.tar.gz
      URL_MD5           786ce9421ef3d9dfa719bba0660fea73
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (FDBB_BUILTIN_BLAZE MATCHES "latest")

    # Download Blaze
    include(DownloadProject)
    download_project(
      PROJ              Blaze
      GIT_REPOSITORY    https://bitbucket.org/blaze-lib/blaze.git
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid Blaze version.")
  endif()
  
  # Blaze depends on the Boost library
  find_package(Boost COMPONENTS
    system
    thread
    QUIET REQUIRED)
  include_directories(${Boost_INCLUDE_DIRS})
  set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
  # Blaze depends on Threads
  set(THREADS_PREFER_PTHREAD_FLAG ON)
  find_package(Threads QUIET REQUIRED)
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${CMAKE_THREAD_LIBS_INIT})
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
    
  # Add include directory
  include_directories("${Blaze_SOURCE_DIR}")

else()

  # Add include directory
  if(BLAZE_INCLUDE_PATH)
    include_directories(${BLAZE_INCLUDE_PATH})
  else()
    message(WARNING "Variable BLAZE_INCLUDE_PATH is not defined. FDBB might be unable to find Blaze include files.")
  endif()
  
endif()

# Enable Blaze support
add_definitions(-DFDBB_BACKEND_BLAZE)

# Enable OpenMP support
if(NOT OPENMP_FOUND)
  add_definitions(-DBLAZE_USE_CPP_THREADS)
endif()
