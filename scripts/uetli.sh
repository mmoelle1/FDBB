#!/bin/sh

echo "\n\n=================================\nPrepare UETLI library for release\n=================================\n\n"

# Copy relevant files from top level directory
rsync -avz CMakeLists.txt Makefile LICENSE.txt .gitlab-ci.yml uetli/

# Replace FDBB by UETLI
perl -pi -e 's/FDBB/UETLI/g' uetli/CMakeLists.txt
perl -pi -e 's/FDBB/UETLI/g' uetli/Makefile
perl -pi -e 's/FDBB/UETLI/g' uetli/.gitlab-ci.yml
perl -pi -e 's/fdbb/uetli/g' uetli/CMakeLists.txt
perl -pi -e 's/fdbb/uetli/g' uetli/Makefile
perl -pi -e 's/fdbb/uetli/g' uetli/.gitlab-ci.yml

# Remove CoolProp dependence
perl -i -0pe 's/#*\n# CoolProp\n#*\nif\(UETLI_WITH_COOLPROP\)\n.*include\(CoolProp\)\nendif\(\)\n//' uetli/CMakeLists.txt
perl -pi -e 's/.*COOLPROP.*//' uetli/CMakeLists.txt


# Copy relevant files from cmake directory
rsync -avze --include='*.cmake' --exclude='.DS_Store' --exclude='CoolProp.cmake' cmake/ uetli/cmake/

# Replace FDBB by UETLI
find uetli/cmake -type f -exec perl -pi -e 's/FDBB/UETLI/g' {} \;


# Copy relevant files from doc directory
rsync -avz doc/Doxyfile.in doc/Doxygen.bib uetli/doc/

# Replace FDBB by UETLI
find uetli/doc -type f -exec perl -pi -e 's/FDBB/UETLI/g' {} \;
find uetli/doc -type f -exec perl -pi -e 's/fdbb/uetli/g' {} \;

# Replace FDBB by UETLI
perl -pi -e 's/FDBB: Fluid Dynamics Building Blocks/UETLI: Universal Expression Template Library Interface/g' uetli/doc/Doxyfile.in


# Copy relevant files from patches directory
rsync -avze --include='*.patch' patches/ uetli/patches/


# Copy relevant files from script directory
rsync -avz scripts/convert.pl scripts/release.sh scripts/tex2md.pl uetli/scripts/

# Replace FDBB by UETLI
find uetli/scripts -type f -exec perl -pi -e 's/FDBB/UETLI/g' {} \;
find uetli/scripts -type f -exec perl -pi -e 's/fdbb/uetli/g' {} \;

# Remove tests dependence
perl -pi -e 's/ tests//g' uetli/scripts/release.sh

# Copy relevant files from include directory
rsync -avze --include='*.hpp' --exclude='.DS_Store' --exclude='*Conservative*' --exclude='*Primitive*' --exclude='*Riemann*' --exclude='*EOS*' --exclude='*Types*' --exclude='*Enums*' --exclude='*Traits*' --exclude='fdbb.h' fdbb/ uetli/uetli/

# Replace FDBB by UETLI
find uetli/uetli -type f -exec perl -pi -e 's/FDBB/UETLI/g' {} \;
find uetli/uetli -type f -exec perl -pi -e 's/fdbb/uetli/g' {} \;


# Copy relevant files from unittests directory
rsync -avze --include='*.hpp' --exclude='.DS_Store' --exclude='*conservative*' --exclude='*primitive*' --exclude='*riemann*' unittests/ uetli/unittests

# Replace FDBB by UETLI
find uetli/unittests -type f -exec perl -pi -e 's/FDBB/UETLI/g' {} \;
find uetli/unittests -type f -exec perl -pi -e 's/fdbb/uetli/g' {} \;
