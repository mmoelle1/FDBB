#!/bin/sh

echo "stages:"
echo "  - build"
echo ""
echo "#"
echo "# Create Doxygen documentation"
echo "#"
echo "pages:"
echo "  stage: build"
echo "  image: registry.gitlab.com/mmoelle1/docker-clang:4.0"
echo "  script: "
echo "    - mkdir -p build "
echo "    - cd build "
echo "    - CC=clang-4.0 CXX=clang++-4.0 cmake .. -DFDBB_BUILD_TESTS=OFF -DFDBB_BUILD_UNITTESTS=OFF"
echo "    - make doc"
echo "    - mv doc/html ../public"
echo "  artifacts:"
echo "    paths:"
echo "    - public"
echo "  only:"
echo "  - master"
echo ""

ETL="armadillo arrayfire blaze eigen itpp mtl4 ublas vexcl viennacl"

COMPILER="gcc4.9"
#"clang3.8 clang3.9 clang4.0 clang5.0 gcc4.9 gcc5 gcc6 gcc7 oraclestudio pgi"

for etl in ${ETL};
do

    echo "#"
    echo "# $(echo ${etl} | tr /a-z/ /A-Z/)"
    echo "#"
    
    for compiler in ${COMPILER};
    do

        case ${compiler} in
            clang3.8)
                cc=clang
                cxx=clang++
                image=docker-clang:3.8
                ;;
            clang3.9)
                cc=clang-3.9
                cxx=clang++-3.9
                image=docker-clang:3.9
                ;;
            clang4.0)
                cc=clang-4.0
                cxx=clang++-4.0
                image=docker-clang:4.0
                ;;
            clang5.0)
                cc=clang-5.0
                cxx=clang++-5.0
                image=docker-clang:5.0
                ;;
            gcc4.9)
                cc=gcc-4.9
                cxx=g++-4.9
                image=docker-gcc:4.9
                ;;
            gcc5)
                cc=gcc-5
                cxx=g++-5
                image=docker-gcc:5
                ;;
            gcc6)
                cc=gcc-6
                cxx=g++-6
                image=docker-gcc:6
                ;;

            gcc7)
                cc=gcc-7
                cxx=g++-7
                image=docker-gcc:7
                ;;

            oraclestudio)
                cc=suncc
                cxx=sunCC
                image=docker-oraclestudio:latest
                ;;
            pgi)
                cc=pgcc
                cxx=pgc++
                image=docker-pgi:latest
                ;;
            *)
                cc=""
                cxx=""
                image=""
                ;;
                  
        esac
        
        for std in 11 14;
        do

            enable="yes"
            enable_test="yes"
            cmake_extra=""
            cmake_options=""
            
            case ${etl}-c++${std}-${compiler} in
                
                *-*-oraclestudio)
                    enable="no"
                    ;;

                *-*-pgi)
                    enable="no"
                    ;;

                armadillo-*-*)
                    enable_test="no"
                    ;;

                blaze-c++11-*)
                    enable="no"
                    ;;

                blaze-*-gcc*)
                    #cmake_extra="CFLAGS='--param ggc-min-expand=0 --param ggc-min-heapsize=32768' CXXFLAGS='--param ggc-min-expand=0 --param ggc-min-heapsize=32768'"
                    ;;

                eigen-*-gcc*)
                    #cmake_extra="CFLAGS='--param ggc-min-expand=0 --param ggc-min-heapsize=32768' CXXFLAGS='--param ggc-min-expand=0 --param ggc-min-heapsize=32768'"
                    ;;
                
                mtl4-*-*)
                    enable_test="no"
                    ;;

                ublas-*-*)
                    enable_test="no"
                    ;;

                vexcl-*-*)
                    cmake_options="-DFDBB_WITH_OCL=ON"
                    ;;
                
                viennacl-*-*)
                    cmake_options="-DFDBB_WITH_OCL=ON"
                    enable_test="no"
                    ;;
            esac

            if [ ${enable} = "yes" ]; then
                echo "${etl}-c++${std}-${compiler}:"
                echo "  stage: build"
                echo "  image: registry.gitlab.com/mmoelle1/${image}"
                echo "  script:"
                echo "    - mkdir -p build"
                echo "    - cd build"
                echo "    - CC=${cc} CXX=${cxx} ${cmake_extra} cmake .. -DFDBB_CXX_STANDARD=${std} -DFDBB_BUILD_UNITTESTS=ON -DFDBB_BUILD_UNITTESTS_$(echo ${etl} | tr /a-z/ /A-Z/)=ON" ${cmake_options}
                echo "    - make"

                if [ ${enable_test} = "yes" ]; then
                    echo "    - make test"
                fi
                echo ""
            fi

        done 
    done 
done
