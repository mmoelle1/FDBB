#!/bin/sh

#
# Test #1
#

# Armadillo
grep '\[suite1\.test1a\.d\] memory consumption'  armadillo_double_test1.run | awk '{print $(NF-1)}' > ./armadillo_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' armadillo_double_test1.run | awk '{print $(NF-1)}' > ./armadillo_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  armadillo_double_test1.run | awk '{print $(NF-1)}' > ./armadillo_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  armadillo_double_test1.run | awk '{print $(NF-1)}' > ./armadillo_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' armadillo_double_test1.run | awk '{print $(NF-1)}' > ./armadillo_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  armadillo_double_test1.run | awk '{print $(NF-1)}' > ./armadillo_double_test1e_memory.dat

# ArrayFire
grep '\[suite1\.test1a\.d\] memory consumption'  arrayfire_double_test1.run | awk '{print $(NF-1)}' > ./arrayfire_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' arrayfire_double_test1.run | awk '{print $(NF-1)}' > ./arrayfire_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  arrayfire_double_test1.run | awk '{print $(NF-1)}' > ./arrayfire_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  arrayfire_double_test1.run | awk '{print $(NF-1)}' > ./arrayfire_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' arrayfire_double_test1.run | awk '{print $(NF-1)}' > ./arrayfire_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  arrayfire_double_test1.run | awk '{print $(NF-1)}' > ./arrayfire_double_test1e_memory.dat

# Blaze
grep '\[suite1\.test1a\.d\] memory consumption'  blaze_double_test1.run | awk '{print $(NF-1)}' > ./blaze_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' blaze_double_test1.run | awk '{print $(NF-1)}' > ./blaze_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  blaze_double_test1.run | awk '{print $(NF-1)}' > ./blaze_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  blaze_double_test1.run | awk '{print $(NF-1)}' > ./blaze_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' blaze_double_test1.run | awk '{print $(NF-1)}' > ./blaze_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  blaze_double_test1.run | awk '{print $(NF-1)}' > ./blaze_double_test1e_memory.dat

# Blitz++
grep '\[suite1\.test1a\.d\] memory consumption'  blitz++_double_test1.run | awk '{print $(NF-1)}' > ./blitz++_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' blitz++_double_test1.run | awk '{print $(NF-1)}' > ./blitz++_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  blitz++_double_test1.run | awk '{print $(NF-1)}' > ./blitz++_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  blitz++_double_test1.run | awk '{print $(NF-1)}' > ./blitz++_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' blitz++_double_test1.run | awk '{print $(NF-1)}' > ./blitz++_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  blitz++_double_test1.run | awk '{print $(NF-1)}' > ./blitz++_double_test1e_memory.dat

# Eigen
grep '\[suite1\.test1a\.d\] memory consumption'  eigen_double_test1.run | awk '{print $(NF-1)}' > ./eigen_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' eigen_double_test1.run | awk '{print $(NF-1)}' > ./eigen_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  eigen_double_test1.run | awk '{print $(NF-1)}' > ./eigen_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  eigen_double_test1.run | awk '{print $(NF-1)}' > ./eigen_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' eigen_double_test1.run | awk '{print $(NF-1)}' > ./eigen_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  eigen_double_test1.run | awk '{print $(NF-1)}' > ./eigen_double_test1e_memory.dat

# IT++
grep '\[suite1\.test1a\.d\] memory consumption'  it++_double_test1.run | awk '{print $(NF-1)}' > ./it++_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' it++_double_test1.run | awk '{print $(NF-1)}' > ./it++_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  it++_double_test1.run | awk '{print $(NF-1)}' > ./it++_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  it++_double_test1.run | awk '{print $(NF-1)}' > ./it++_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' it++_double_test1.run | awk '{print $(NF-1)}' > ./it++_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  it++_double_test1.run | awk '{print $(NF-1)}' > ./it++_double_test1e_memory.dat

# uBLAS
grep '\[suite1\.test1a\.d\] memory consumption'  ublas_double_test1.run | awk '{print $(NF-1)}' > ./ublas_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' ublas_double_test1.run | awk '{print $(NF-1)}' > ./ublas_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  ublas_double_test1.run | awk '{print $(NF-1)}' > ./ublas_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  ublas_double_test1.run | awk '{print $(NF-1)}' > ./ublas_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' ublas_double_test1.run | awk '{print $(NF-1)}' > ./ublas_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  ublas_double_test1.run | awk '{print $(NF-1)}' > ./ublas_double_test1e_memory.dat

# VexCL
grep '\[suite1\.test1a\.d\] memory consumption'  vexcl_double_test1.run | awk '{print $(NF-1)}' > ./vexcl_double_test1a_bytes.dat
grep '\[suite1\.test1a\.d\] compute performance' vexcl_double_test1.run | awk '{print $(NF-1)}' > ./vexcl_double_test1a_compute.dat
grep '\[suite1\.test1a\.d\] memory performance'  vexcl_double_test1.run | awk '{print $(NF-1)}' > ./vexcl_double_test1a_memory.dat

grep '\[suite1\.test1e\.d\] memory consumption'  vexcl_double_test1.run | awk '{print $(NF-1)}' > ./vexcl_double_test1e_bytes.dat
grep '\[suite1\.test1e\.d\] compute performance' vexcl_double_test1.run | awk '{print $(NF-1)}' > ./vexcl_double_test1e_compute.dat
grep '\[suite1\.test1e\.d\] memory performance'  vexcl_double_test1.run | awk '{print $(NF-1)}' > ./vexcl_double_test1e_memory.dat

# ViennaCL
#grep '\[suite1\.test1a\.d\] memory consumption'  viennacl_double_test1.run | awk '{print $(NF-1)}' > ./viennacl_double_test1a_bytes.dat
#grep '\[suite1\.test1a\.d\] compute performance' viennacl_double_test1.run | awk '{print $(NF-1)}' > ./viennacl_double_test1a_compute.dat
#grep '\[suite1\.test1a\.d\] memory performance'  viennacl_double_test1.run | awk '{print $(NF-1)}' > ./viennacl_double_test1a_memory.dat

#grep '\[suite1\.test1e\.d\] memory consumption'  viennacl_double_test1.run | awk '{print $(NF-1)}' > ./viennacl_double_test1e_bytes.dat
#grep '\[suite1\.test1e\.d\] compute performance' viennacl_double_test1.run | awk '{print $(NF-1)}' > ./viennacl_double_test1e_compute.dat
#grep '\[suite1\.test1e\.d\] memory performance'  viennacl_double_test1.run | awk '{print $(NF-1)}' > ./viennacl_double_test1e_memory.dat


#
# Test #2
#

# Armadillo
grep '\[suite1\.test2a\.d\] memory consumption'  armadillo_double_test2.run | awk '{print $(NF-1)}' > ./armadillo_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' armadillo_double_test2.run | awk '{print $(NF-1)}' > ./armadillo_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  armadillo_double_test2.run | awk '{print $(NF-1)}' > ./armadillo_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  armadillo_double_test2.run | awk '{print $(NF-1)}' > ./armadillo_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' armadillo_double_test2.run | awk '{print $(NF-1)}' > ./armadillo_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  armadillo_double_test2.run | awk '{print $(NF-1)}' > ./armadillo_double_test2e_memory.dat

# ArrayFire
grep '\[suite1\.test2a\.d\] memory consumption'  arrayfire_double_test2.run | awk '{print $(NF-1)}' > ./arrayfire_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' arrayfire_double_test2.run | awk '{print $(NF-1)}' > ./arrayfire_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  arrayfire_double_test2.run | awk '{print $(NF-1)}' > ./arrayfire_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  arrayfire_double_test2.run | awk '{print $(NF-1)}' > ./arrayfire_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' arrayfire_double_test2.run | awk '{print $(NF-1)}' > ./arrayfire_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  arrayfire_double_test2.run | awk '{print $(NF-1)}' > ./arrayfire_double_test2e_memory.dat

# Blaze
grep '\[suite1\.test2a\.d\] memory consumption'  blaze_double_test2.run | awk '{print $(NF-1)}' > ./blaze_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' blaze_double_test2.run | awk '{print $(NF-1)}' > ./blaze_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  blaze_double_test2.run | awk '{print $(NF-1)}' > ./blaze_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  blaze_double_test2.run | awk '{print $(NF-1)}' > ./blaze_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' blaze_double_test2.run | awk '{print $(NF-1)}' > ./blaze_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  blaze_double_test2.run | awk '{print $(NF-1)}' > ./blaze_double_test2e_memory.dat

# Blitz++
grep '\[suite1\.test2a\.d\] memory consumption'  blitz++_double_test2.run | awk '{print $(NF-1)}' > ./blitz++_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' blitz++_double_test2.run | awk '{print $(NF-1)}' > ./blitz++_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  blitz++_double_test2.run | awk '{print $(NF-1)}' > ./blitz++_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  blitz++_double_test2.run | awk '{print $(NF-1)}' > ./blitz++_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' blitz++_double_test2.run | awk '{print $(NF-1)}' > ./blitz++_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  blitz++_double_test2.run | awk '{print $(NF-1)}' > ./blitz++_double_test2e_memory.dat

# Eigen
grep '\[suite1\.test2a\.d\] memory consumption'  eigen_double_test2.run | awk '{print $(NF-1)}' > ./eigen_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' eigen_double_test2.run | awk '{print $(NF-1)}' > ./eigen_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  eigen_double_test2.run | awk '{print $(NF-1)}' > ./eigen_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  eigen_double_test2.run | awk '{print $(NF-1)}' > ./eigen_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' eigen_double_test2.run | awk '{print $(NF-1)}' > ./eigen_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  eigen_double_test2.run | awk '{print $(NF-1)}' > ./eigen_double_test2e_memory.dat

# IT++
grep '\[suite1\.test2a\.d\] memory consumption'  it++_double_test2.run | awk '{print $(NF-1)}' > ./it++_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' it++_double_test2.run | awk '{print $(NF-1)}' > ./it++_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  it++_double_test2.run | awk '{print $(NF-1)}' > ./it++_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  it++_double_test2.run | awk '{print $(NF-1)}' > ./it++_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' it++_double_test2.run | awk '{print $(NF-1)}' > ./it++_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  it++_double_test2.run | awk '{print $(NF-1)}' > ./it++_double_test2e_memory.dat

# uBLAS
grep '\[suite1\.test2a\.d\] memory consumption'  ublas_double_test2.run | awk '{print $(NF-1)}' > ./ublas_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' ublas_double_test2.run | awk '{print $(NF-1)}' > ./ublas_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  ublas_double_test2.run | awk '{print $(NF-1)}' > ./ublas_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  ublas_double_test2.run | awk '{print $(NF-1)}' > ./ublas_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' ublas_double_test2.run | awk '{print $(NF-1)}' > ./ublas_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  ublas_double_test2.run | awk '{print $(NF-1)}' > ./ublas_double_test2e_memory.dat

# VexCL
grep '\[suite1\.test2a\.d\] memory consumption'  vexcl_double_test2.run | awk '{print $(NF-1)}' > ./vexcl_double_test2a_bytes.dat
grep '\[suite1\.test2a\.d\] compute performance' vexcl_double_test2.run | awk '{print $(NF-1)}' > ./vexcl_double_test2a_compute.dat
grep '\[suite1\.test2a\.d\] memory performance'  vexcl_double_test2.run | awk '{print $(NF-1)}' > ./vexcl_double_test2a_memory.dat

grep '\[suite1\.test2e\.d\] memory consumption'  vexcl_double_test2.run | awk '{print $(NF-1)}' > ./vexcl_double_test2e_bytes.dat
grep '\[suite1\.test2e\.d\] compute performance' vexcl_double_test2.run | awk '{print $(NF-1)}' > ./vexcl_double_test2e_compute.dat
grep '\[suite1\.test2e\.d\] memory performance'  vexcl_double_test2.run | awk '{print $(NF-1)}' > ./vexcl_double_test2e_memory.dat

# ViennaCL
#grep '\[suite1\.test2a\.d\] memory consumption'  viennacl_double_test2.run | awk '{print $(NF-1)}' > ./viennacl_double_test2a_bytes.dat
#grep '\[suite1\.test2a\.d\] compute performance' viennacl_double_test2.run | awk '{print $(NF-1)}' > ./viennacl_double_test2a_compute.dat
#grep '\[suite1\.test2a\.d\] memory performance'  viennacl_double_test2.run | awk '{print $(NF-1)}' > ./viennacl_double_test2a_memory.dat

#grep '\[suite1\.test2e\.d\] memory consumption'  viennacl_double_test2.run | awk '{print $(NF-1)}' > ./viennacl_double_test2e_bytes.dat
#grep '\[suite1\.test2e\.d\] compute performance' viennacl_double_test2.run | awk '{print $(NF-1)}' > ./viennacl_double_test2e_compute.dat
#grep '\[suite1\.test2e\.d\] memory performance'  viennacl_double_test2.run | awk '{print $(NF-1)}' > ./viennacl_double_test2e_memory.dat
