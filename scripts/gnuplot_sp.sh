# GNUPlot script
#set terminal postscript eps enhanced color font 'Helvetica,16'
set terminal epslatex color colortext
set key outside
set mytics 10
set grid x ytics mytics
set format x "%.0te%T"
set format y "%.0te%T"
#set xtics rotate by 45# offset -0.8,-1.8

# Line definitions
set style line 1  lc rgb "#00A6D6" lw 4 lt 1 pt 0 ps 1.5
set style line 2  lc rgb "#00A6D6" lw 4 lt 1 pt 5 ps 1.5
set style line 3  lc rgb "#6EBBD5" lw 4 lt 1 pt 0 ps 1.5
set style line 4  lc rgb "#6EBBD5" lw 4 lt 1 pt 7 ps 1.5
set style line 5  lc rgb "#1D1C73" lw 4 lt 1 pt 0 ps 1.5
set style line 6  lc rgb "#1D1C73" lw 4 lt 1 pt 9 ps 1.5
set style line 7  lc rgb "#E64616" lw 4 lt 1 pt 0 ps 1.5
set style line 8  lc rgb "#E64616" lw 4 lt 1 pt 11 ps 1.5
set style line 9  lc rgb "#FFC400" lw 4 lt 1 pt 0 ps 1.5
set style line 10 lc rgb "#FFC400" lw 4 lt 1 pt 13 ps 1.5
set style line 11 lc rgb "#008891" lw 4 lt 1 pt 0 ps 1.5
set style line 12 lc rgb "#008891" lw 4 lt 1 pt 15 ps 1.5
set style line 13 lc rgb "#A5CA1A" lw 4 lt 1 pt 0 ps 1.5
set style line 14 lc rgb "#A5CA1A" lw 4 lt 1 pt 6 ps 1.5
set style line 15 lc rgb "#6D177F" lw 4 lt 1 pt 0 ps 1.5
set style line 16 lc rgb "#6D177F" lw 4 lt 1 pt 4 ps 1.5

# Compute performance
set output 'test1_compute_sp.tex'

set title "Single precision performance"
set xlabel "Problem size [bytes]"
set logscale x
set ylabel "Performance [Mflops]"
set logscale y

plot "< paste armadillo_float_test1a_bytes.dat armadillo_float_test1a_compute.dat" title 'Armadillo vanilla' with linespoints ls 1, \
     "< paste armadillo_float_test1e_bytes.dat armadillo_float_test1e_compute.dat" title 'fdbb' with points ls 2, \
     "< paste arrayfire_float_test1a_bytes.dat arrayfire_float_test1e_compute.dat" title 'ArrayFire vanilla' with linespoints ls 3, \
     "< paste arrayfire_float_test1e_bytes.dat arrayfire_float_test1e_compute.dat" title 'fdbb' with points ls 4, \
     "< paste blaze_float_test1a_bytes.dat blaze_float_test1e_compute.dat"         title 'Blaze vanilla' with linespoints ls 5, \
     "< paste blaze_float_test1e_bytes.dat blaze_float_test1e_compute.dat"         title 'fdbb' with points ls 6, \
     "< paste blitz++_float_test1a_bytes.dat blitz++_float_test1e_compute.dat"     title 'Blitz++ vanilla' with linespoints ls 7, \
     "< paste blitz++_float_test1e_bytes.dat blitz++_float_test1e_compute.dat"     title 'fdbb' with points ls 8, \
     "< paste eigen_float_test1a_bytes.dat eigen_float_test1e_compute.dat"         title 'Eigen vanilla' with linespoints ls 9, \
     "< paste eigen_float_test1e_bytes.dat eigen_float_test1e_compute.dat"         title 'fdbb' with points ls 10, \
     "< paste it++_float_test1a_bytes.dat it++_float_test1e_compute.dat"           title 'IT++ vanilla' with linespoints ls 11, \
     "< paste it++_float_test1e_bytes.dat it++_float_test1e_compute.dat"           title 'fdbb' with points ls 12, \
     "< paste ublas_float_test1a_bytes.dat ublas_float_test1e_compute.dat"         title 'uBLAS vanilla' with linespoints ls 13, \
     "< paste ublas_float_test1e_bytes.dat ublas_float_test1e_compute.dat"         title 'fdbb' with points ls 14, \
     "< paste vexcl_float_test1a_bytes.dat vexcl_float_test1e_compute.dat"         title 'VexCL vanilla' with linespoints ls 15, \
     "< paste vexcl_float_test1e_bytes.dat vexcl_float_test1e_compute.dat"         title 'fdbb' with points ls 16

# Memory performance
set output 'test1_memory_sp.tex'

set title "Single precision performance"
set xlabel "Problem size [bytes]"
set logscale x
set ylabel "Bandwidth [MB/s]"
set logscale y

plot "< paste armadillo_float_test1a_bytes.dat armadillo_float_test1a_memory.dat" title 'Armadillo vanilla' with linespoints ls 1, \
     "< paste armadillo_float_test1e_bytes.dat armadillo_float_test1e_memory.dat" title 'fdbb' with points ls 2, \
     "< paste arrayfire_float_test1a_bytes.dat arrayfire_float_test1e_memory.dat" title 'ArrayFire vanilla' with linespoints ls 3, \
     "< paste arrayfire_float_test1e_bytes.dat arrayfire_float_test1e_memory.dat" title 'fdbb' with points ls 4, \
     "< paste blaze_float_test1a_bytes.dat blaze_float_test1e_memory.dat"         title 'Blaze vanilla' with linespoints ls 5, \
     "< paste blaze_float_test1e_bytes.dat blaze_float_test1e_memory.dat"         title 'fdbb' with points ls 6, \
     "< paste blitz++_float_test1a_bytes.dat blitz++_float_test1e_memory.dat"     title 'Blitz++ vanilla' with linespoints ls 7, \
     "< paste blitz++_float_test1e_bytes.dat blitz++_float_test1e_memory.dat"     title 'fdbb' with points ls 8, \
     "< paste eigen_float_test1a_bytes.dat eigen_float_test1e_memory.dat"         title 'Eigen vanilla' with linespoints ls 9, \
     "< paste eigen_float_test1e_bytes.dat eigen_float_test1e_memory.dat"         title 'fdbb' with points ls 10, \
     "< paste it++_float_test1a_bytes.dat it++_float_test1e_memory.dat"           title 'IT++ vanilla' with linespoints ls 11, \
     "< paste it++_float_test1e_bytes.dat it++_float_test1e_memory.dat"           title 'fdbb' with points ls 12, \
     "< paste ublas_float_test1a_bytes.dat ublas_float_test1e_memory.dat"         title 'uBLAS vanilla' with linespoints ls 13, \
     "< paste ublas_float_test1e_bytes.dat ublas_float_test1e_memory.dat"         title 'fdbb' with points ls 14, \
     "< paste vexcl_float_test1a_bytes.dat vexcl_float_test1e_memory.dat"         title 'VexCL vanilla' with linespoints ls 15, \
     "< paste vexcl_float_test1e_bytes.dat vexcl_float_test1e_memory.dat"         title 'fdbb' with points ls 16

# Line definitions
set style line 1  lc rgb "#00A6D6" lw 2 lt 1
set style line 2  lc rgb "#00A6D6" lw 5 lt 1
set style line 3  lc rgb "#6EBBD5" lw 2 lt 1
set style line 4  lc rgb "#6EBBD5" lw 5 lt 1
set style line 5  lc rgb "#1D1C73" lw 2 lt 1
set style line 6  lc rgb "#1D1C73" lw 5 lt 1
set style line 7  lc rgb "#E64616" lw 2 lt 1
set style line 8  lc rgb "#E64616" lw 5 lt 1
set style line 9  lc rgb "#FFC400" lw 2 lt 1
set style line 10 lc rgb "#FFC400" lw 5 lt 1
set style line 11 lc rgb "#008891" lw 2 lt 1
set style line 12 lc rgb "#008891" lw 5 lt 1
set style line 13 lc rgb "#A5CA1A" lw 2 lt 1
set style line 14 lc rgb "#A5CA1A" lw 5 lt 1
set style line 15 lc rgb "#6D177F" lw 2 lt 1
set style line 16 lc rgb "#6D177F" lw 5 lt 1

# Compute performance
set output 'test2_compute_sp.tex'

set title "Single precision performance"
set xlabel "Problem size [bytes]"
set logscale x
set ylabel "Performance [Mflops]"
set logscale y

plot "< paste armadillo_float_test1a_bytes.dat armadillo_float_test1a_compute.dat" title 'Armadillo 7 flop' with lines ls 1, \
     "< paste armadillo_float_test2a_bytes.dat armadillo_float_test2a_compute.dat" title '11 flop' with lines ls 2, \
     "< paste arrayfire_float_test1a_bytes.dat arrayfire_float_test1a_compute.dat" title 'ArrayFire 7 flop' with lines ls 3, \
     "< paste arrayfire_float_test2a_bytes.dat arrayfire_float_test2a_compute.dat" title '11 flop' with lines ls 4, \
     "< paste blaze_float_test1a_bytes.dat blaze_float_test1a_compute.dat"         title 'Blaze 7 flop' with lines ls 5, \
     "< paste blaze_float_test2a_bytes.dat blaze_float_test2a_compute.dat"         title '11 flop' with lines ls 6, \
     "< paste blitz++_float_test1a_bytes.dat blitz++_float_test1a_compute.dat"     title 'Blitz++ 7 flop' with lines ls 7, \
     "< paste blitz++_float_test2a_bytes.dat blitz++_float_test2a_compute.dat"     title '11 flop' with lines ls 8, \
     "< paste eigen_float_test1a_bytes.dat eigen_float_test1a_compute.dat"         title 'Eigen 7 flop' with lines ls 9, \
     "< paste eigen_float_test2a_bytes.dat eigen_float_test2a_compute.dat"         title '11 flop' with lines ls 10, \
     "< paste it++_float_test1a_bytes.dat it++_float_test1a_compute.dat"           title 'IT++ 7 flop' with lines ls 11, \
     "< paste it++_float_test2a_bytes.dat it++_float_test2a_compute.dat"           title '11 flop' with lines ls 12, \
     "< paste ublas_float_test1a_bytes.dat ublas_float_test1a_compute.dat"         title 'uBLAS 7 flop' with lines ls 13, \
     "< paste ublas_float_test2a_bytes.dat ublas_float_test2a_compute.dat"         title '11 flop' with lines ls 14, \
     "< paste vexcl_float_test1a_bytes.dat vexcl_float_test1a_compute.dat"         title 'VexCL 7 flop' with lines ls 15, \
     "< paste vexcl_float_test2a_bytes.dat vexcl_float_test2a_compute.dat"         title '11 flop' with lines ls 16

# Memory performance
set output 'test2_memory_sp.tex'

set title "Single precision performance"
set xlabel "Problem size [bytes]"
set logscale x
set ylabel "Bandwidth [MB/s]"
set logscale y

plot "< paste armadillo_float_test1a_bytes.dat armadillo_float_test1a_memory.dat" title 'Armadillo 7 flop' with lines ls 1, \
     "< paste armadillo_float_test2a_bytes.dat armadillo_float_test2a_memory.dat" title '11 flop' with lines ls 2, \
     "< paste arrayfire_float_test1a_bytes.dat arrayfire_float_test1a_memory.dat" title 'ArrayFire 7 flop' with lines ls 3, \
     "< paste arrayfire_float_test2a_bytes.dat arrayfire_float_test2a_memory.dat" title '11 flop' with lines ls 4, \
     "< paste blaze_float_test1a_bytes.dat blaze_float_test1a_memory.dat"         title 'Blaze 7 flop' with lines ls 5, \
     "< paste blaze_float_test2a_bytes.dat blaze_float_test2a_memory.dat"         title '11 flop' with lines ls 6, \
     "< paste blitz++_float_test1a_bytes.dat blitz++_float_test1a_memory.dat"     title 'Blitz++ 7 flop' with lines ls 7, \
     "< paste blitz++_float_test2a_bytes.dat blitz++_float_test2a_memory.dat"     title '11 flop' with lines ls 8, \
     "< paste eigen_float_test1a_bytes.dat eigen_float_test1a_memory.dat"         title 'Eigen 7 flop' with lines ls 9, \
     "< paste eigen_float_test2a_bytes.dat eigen_float_test2a_memory.dat"         title '11 flop' with lines ls 10, \
     "< paste it++_float_test1a_bytes.dat it++_float_test1a_memory.dat"           title 'IT++ 7 flop' with lines ls 11, \
     "< paste it++_float_test2a_bytes.dat it++_float_test2a_memory.dat"           title '11 flop' with lines ls 12, \
     "< paste ublas_float_test1a_bytes.dat ublas_float_test1a_memory.dat"         title 'uBLAS 7 flop' with lines ls 13, \
     "< paste ublas_float_test2a_bytes.dat ublas_float_test2a_memory.dat"         title '11 flop' with lines ls 14, \
     "< paste vexcl_float_test1a_bytes.dat vexcl_float_test1a_memory.dat"         title 'VexCL 7 flop' with lines ls 15, \
     "< paste vexcl_float_test2a_bytes.dat vexcl_float_test2a_memory.dat"         title '11 flop' with lines ls 16
