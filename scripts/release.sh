#!/bin/sh
          
echo "\n\n================================\nPrepare FDBB library for release\n================================\n\n"

# Update version number
echo "[1.] Update version number to \c"
echo "$(date '+%Y.%m')-$(git rev-list --count master) ...\c"
sed -i '' "s/^set(FDBB_MAJOR_VERSION.*)$/set(FDBB_MAJOR_VERSION $(date '+%Y')\)/g" CMakeLists.txt
sed -i '' "s/^set(FDBB_MINOR_VERSION.*)$/set(FDBB_MINOR_VERSION $(date '+%m')\)/g" CMakeLists.txt
sed -i '' "s/^set(FDBB_REVISION_VERSION.*)$/set(FDBB_REVISION_VERSION $(git rev-list --count master)\)/g" CMakeLists.txt
echo "done!"

# Find all C++ source code files and apply clang-format to them
echo "[2.] Apply clang-format ... \c"
find fdbb tests unittests -type f \( -iname \*.hpp -o -iname \*.cxx \) \
     -exec clang-format -i {} \; 
echo "done!"

# Find all C++ source code files and apply C++-style comments
echo "[3.] Apply C++-style comments ... \c"
find fdbb tests unittests -type f \( -iname \*.hpp -o -iname \*.cxx \) \
     -exec sh -c 'tmpfile=`mktemp`; mv "{}" "${tmpfile}"; perl scripts/convert.pl < "${tmpfile}" > "{}"; rm -f "${tmpfile}"' \;
echo "done!"

# Generate top-level README.md file
echo "[4.] Generating top-level README.md file ... \c"
perl -p scripts/tex2md.pl < doc/README.md > README.md
echo "done!"
