#!/bin/sh

#
# Test #1
#

# Armadillo
grep '\[suite1\.test1a\.f\] memory consumption'  armadillo_float_test1.run | awk '{print $(NF-1)}' > ./armadillo_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' armadillo_float_test1.run | awk '{print $(NF-1)}' > ./armadillo_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  armadillo_float_test1.run | awk '{print $(NF-1)}' > ./armadillo_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  armadillo_float_test1.run | awk '{print $(NF-1)}' > ./armadillo_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' armadillo_float_test1.run | awk '{print $(NF-1)}' > ./armadillo_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  armadillo_float_test1.run | awk '{print $(NF-1)}' > ./armadillo_float_test1e_memory.dat

# ArrayFire
grep '\[suite1\.test1a\.f\] memory consumption'  arrayfire_float_test1.run | awk '{print $(NF-1)}' > ./arrayfire_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' arrayfire_float_test1.run | awk '{print $(NF-1)}' > ./arrayfire_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  arrayfire_float_test1.run | awk '{print $(NF-1)}' > ./arrayfire_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  arrayfire_float_test1.run | awk '{print $(NF-1)}' > ./arrayfire_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' arrayfire_float_test1.run | awk '{print $(NF-1)}' > ./arrayfire_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  arrayfire_float_test1.run | awk '{print $(NF-1)}' > ./arrayfire_float_test1e_memory.dat

# Blaze
grep '\[suite1\.test1a\.f\] memory consumption'  blaze_float_test1.run | awk '{print $(NF-1)}' > ./blaze_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' blaze_float_test1.run | awk '{print $(NF-1)}' > ./blaze_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  blaze_float_test1.run | awk '{print $(NF-1)}' > ./blaze_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  blaze_float_test1.run | awk '{print $(NF-1)}' > ./blaze_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' blaze_float_test1.run | awk '{print $(NF-1)}' > ./blaze_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  blaze_float_test1.run | awk '{print $(NF-1)}' > ./blaze_float_test1e_memory.dat

# Blitz++
grep '\[suite1\.test1a\.f\] memory consumption'  blitz++_float_test1.run | awk '{print $(NF-1)}' > ./blitz++_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' blitz++_float_test1.run | awk '{print $(NF-1)}' > ./blitz++_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  blitz++_float_test1.run | awk '{print $(NF-1)}' > ./blitz++_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  blitz++_float_test1.run | awk '{print $(NF-1)}' > ./blitz++_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' blitz++_float_test1.run | awk '{print $(NF-1)}' > ./blitz++_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  blitz++_float_test1.run | awk '{print $(NF-1)}' > ./blitz++_float_test1e_memory.dat

# Eigen
grep '\[suite1\.test1a\.f\] memory consumption'  eigen_float_test1.run | awk '{print $(NF-1)}' > ./eigen_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' eigen_float_test1.run | awk '{print $(NF-1)}' > ./eigen_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  eigen_float_test1.run | awk '{print $(NF-1)}' > ./eigen_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  eigen_float_test1.run | awk '{print $(NF-1)}' > ./eigen_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' eigen_float_test1.run | awk '{print $(NF-1)}' > ./eigen_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  eigen_float_test1.run | awk '{print $(NF-1)}' > ./eigen_float_test1e_memory.dat

# IT++
grep '\[suite1\.test1a\.f\] memory consumption'  it++_float_test1.run | awk '{print $(NF-1)}' > ./it++_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' it++_float_test1.run | awk '{print $(NF-1)}' > ./it++_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  it++_float_test1.run | awk '{print $(NF-1)}' > ./it++_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  it++_float_test1.run | awk '{print $(NF-1)}' > ./it++_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' it++_float_test1.run | awk '{print $(NF-1)}' > ./it++_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  it++_float_test1.run | awk '{print $(NF-1)}' > ./it++_float_test1e_memory.dat

# uBLAS
grep '\[suite1\.test1a\.f\] memory consumption'  ublas_float_test1.run | awk '{print $(NF-1)}' > ./ublas_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' ublas_float_test1.run | awk '{print $(NF-1)}' > ./ublas_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  ublas_float_test1.run | awk '{print $(NF-1)}' > ./ublas_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  ublas_float_test1.run | awk '{print $(NF-1)}' > ./ublas_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' ublas_float_test1.run | awk '{print $(NF-1)}' > ./ublas_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  ublas_float_test1.run | awk '{print $(NF-1)}' > ./ublas_float_test1e_memory.dat

# VexCL
grep '\[suite1\.test1a\.f\] memory consumption'  vexcl_float_test1.run | awk '{print $(NF-1)}' > ./vexcl_float_test1a_bytes.dat
grep '\[suite1\.test1a\.f\] compute performance' vexcl_float_test1.run | awk '{print $(NF-1)}' > ./vexcl_float_test1a_compute.dat
grep '\[suite1\.test1a\.f\] memory performance'  vexcl_float_test1.run | awk '{print $(NF-1)}' > ./vexcl_float_test1a_memory.dat

grep '\[suite1\.test1e\.f\] memory consumption'  vexcl_float_test1.run | awk '{print $(NF-1)}' > ./vexcl_float_test1e_bytes.dat
grep '\[suite1\.test1e\.f\] compute performance' vexcl_float_test1.run | awk '{print $(NF-1)}' > ./vexcl_float_test1e_compute.dat
grep '\[suite1\.test1e\.f\] memory performance'  vexcl_float_test1.run | awk '{print $(NF-1)}' > ./vexcl_float_test1e_memory.dat

# ViennaCL
#grep '\[suite1\.test1a\.f\] memory consumption'  viennacl_float_test1.run | awk '{print $(NF-1)}' > ./viennacl_float_test1a_bytes.dat
#grep '\[suite1\.test1a\.f\] compute performance' viennacl_float_test1.run | awk '{print $(NF-1)}' > ./viennacl_float_test1a_compute.dat
#grep '\[suite1\.test1a\.f\] memory performance'  viennacl_float_test1.run | awk '{print $(NF-1)}' > ./viennacl_float_test1a_memory.dat

#grep '\[suite1\.test1e\.f\] memory consumption'  viennacl_float_test1.run | awk '{print $(NF-1)}' > ./viennacl_float_test1e_bytes.dat
#grep '\[suite1\.test1e\.f\] compute performance' viennacl_float_test1.run | awk '{print $(NF-1)}' > ./viennacl_float_test1e_compute.dat
#grep '\[suite1\.test1e\.f\] memory performance'  viennacl_float_test1.run | awk '{print $(NF-1)}' > ./viennacl_float_test1e_memory.dat


#
# Test #2
#

# Armadillo
grep '\[suite1\.test2a\.f\] memory consumption'  armadillo_float_test2.run | awk '{print $(NF-1)}' > ./armadillo_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' armadillo_float_test2.run | awk '{print $(NF-1)}' > ./armadillo_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  armadillo_float_test2.run | awk '{print $(NF-1)}' > ./armadillo_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  armadillo_float_test2.run | awk '{print $(NF-1)}' > ./armadillo_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' armadillo_float_test2.run | awk '{print $(NF-1)}' > ./armadillo_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  armadillo_float_test2.run | awk '{print $(NF-1)}' > ./armadillo_float_test2e_memory.dat

# ArrayFire
grep '\[suite1\.test2a\.f\] memory consumption'  arrayfire_float_test2.run | awk '{print $(NF-1)}' > ./arrayfire_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' arrayfire_float_test2.run | awk '{print $(NF-1)}' > ./arrayfire_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  arrayfire_float_test2.run | awk '{print $(NF-1)}' > ./arrayfire_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  arrayfire_float_test2.run | awk '{print $(NF-1)}' > ./arrayfire_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' arrayfire_float_test2.run | awk '{print $(NF-1)}' > ./arrayfire_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  arrayfire_float_test2.run | awk '{print $(NF-1)}' > ./arrayfire_float_test2e_memory.dat

# Blaze
grep '\[suite1\.test2a\.f\] memory consumption'  blaze_float_test2.run | awk '{print $(NF-1)}' > ./blaze_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' blaze_float_test2.run | awk '{print $(NF-1)}' > ./blaze_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  blaze_float_test2.run | awk '{print $(NF-1)}' > ./blaze_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  blaze_float_test2.run | awk '{print $(NF-1)}' > ./blaze_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' blaze_float_test2.run | awk '{print $(NF-1)}' > ./blaze_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  blaze_float_test2.run | awk '{print $(NF-1)}' > ./blaze_float_test2e_memory.dat

# Blitz++
grep '\[suite1\.test2a\.f\] memory consumption'  blitz++_float_test2.run | awk '{print $(NF-1)}' > ./blitz++_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' blitz++_float_test2.run | awk '{print $(NF-1)}' > ./blitz++_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  blitz++_float_test2.run | awk '{print $(NF-1)}' > ./blitz++_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  blitz++_float_test2.run | awk '{print $(NF-1)}' > ./blitz++_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' blitz++_float_test2.run | awk '{print $(NF-1)}' > ./blitz++_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  blitz++_float_test2.run | awk '{print $(NF-1)}' > ./blitz++_float_test2e_memory.dat

# Eigen
grep '\[suite1\.test2a\.f\] memory consumption'  eigen_float_test2.run | awk '{print $(NF-1)}' > ./eigen_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' eigen_float_test2.run | awk '{print $(NF-1)}' > ./eigen_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  eigen_float_test2.run | awk '{print $(NF-1)}' > ./eigen_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  eigen_float_test2.run | awk '{print $(NF-1)}' > ./eigen_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' eigen_float_test2.run | awk '{print $(NF-1)}' > ./eigen_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  eigen_float_test2.run | awk '{print $(NF-1)}' > ./eigen_float_test2e_memory.dat

# IT++
grep '\[suite1\.test2a\.f\] memory consumption'  it++_float_test2.run | awk '{print $(NF-1)}' > ./it++_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' it++_float_test2.run | awk '{print $(NF-1)}' > ./it++_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  it++_float_test2.run | awk '{print $(NF-1)}' > ./it++_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  it++_float_test2.run | awk '{print $(NF-1)}' > ./it++_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' it++_float_test2.run | awk '{print $(NF-1)}' > ./it++_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  it++_float_test2.run | awk '{print $(NF-1)}' > ./it++_float_test2e_memory.dat

# uBLAS
grep '\[suite1\.test2a\.f\] memory consumption'  ublas_float_test2.run | awk '{print $(NF-1)}' > ./ublas_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' ublas_float_test2.run | awk '{print $(NF-1)}' > ./ublas_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  ublas_float_test2.run | awk '{print $(NF-1)}' > ./ublas_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  ublas_float_test2.run | awk '{print $(NF-1)}' > ./ublas_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' ublas_float_test2.run | awk '{print $(NF-1)}' > ./ublas_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  ublas_float_test2.run | awk '{print $(NF-1)}' > ./ublas_float_test2e_memory.dat

# VexCL
grep '\[suite1\.test2a\.f\] memory consumption'  vexcl_float_test2.run | awk '{print $(NF-1)}' > ./vexcl_float_test2a_bytes.dat
grep '\[suite1\.test2a\.f\] compute performance' vexcl_float_test2.run | awk '{print $(NF-1)}' > ./vexcl_float_test2a_compute.dat
grep '\[suite1\.test2a\.f\] memory performance'  vexcl_float_test2.run | awk '{print $(NF-1)}' > ./vexcl_float_test2a_memory.dat

grep '\[suite1\.test2e\.f\] memory consumption'  vexcl_float_test2.run | awk '{print $(NF-1)}' > ./vexcl_float_test2e_bytes.dat
grep '\[suite1\.test2e\.f\] compute performance' vexcl_float_test2.run | awk '{print $(NF-1)}' > ./vexcl_float_test2e_compute.dat
grep '\[suite1\.test2e\.f\] memory performance'  vexcl_float_test2.run | awk '{print $(NF-1)}' > ./vexcl_float_test2e_memory.dat

# ViennaCL
#grep '\[suite1\.test2a\.f\] memory consumption'  viennacl_float_test2.run | awk '{print $(NF-1)}' > ./viennacl_float_test2a_bytes.dat
#grep '\[suite1\.test2a\.f\] compute performance' viennacl_float_test2.run | awk '{print $(NF-1)}' > ./viennacl_float_test2a_compute.dat
#grep '\[suite1\.test2a\.f\] memory performance'  viennacl_float_test2.run | awk '{print $(NF-1)}' > ./viennacl_float_test2a_memory.dat

#grep '\[suite1\.test2e\.f\] memory consumption'  viennacl_float_test2.run | awk '{print $(NF-1)}' > ./viennacl_float_test2e_bytes.dat
#grep '\[suite1\.test2e\.f\] compute performance' viennacl_float_test2.run | awk '{print $(NF-1)}' > ./viennacl_float_test2e_compute.dat
#grep '\[suite1\.test2e\.f\] memory performance'  viennacl_float_test2.run | awk '{print $(NF-1)}' > ./viennacl_float_test2e_memory.dat
