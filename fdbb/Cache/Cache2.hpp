/** @file fdbb/Cache/Cache2.hpp
 *
 *  @brief Caching mechanism (version 2)
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CACHE_CACHE2_HPP
#define FDBB_CACHE_CACHE2_HPP

#include <memory>

#include <Cache/ForwardDeclarations.hpp>
#include <Core/Backend.hpp>
#include <Core/Constant.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

/** @namespace fdbb::cache2
 *
 *  @brief
 *  The \ref fdbb::cache2 namespace, containing the lightweight
 *  caching functionality of the FDBB library
 *
 *  The \ref fdbb::cache2 namespace contains a lightweight
 *  implementation of a caching strategy for the FDBB library. In
 *  contrast to the caching mechanism implemented in the \ref
 *  fdbb::cache namespace, this implementation does not store
 *  individual objects for each operator of the expression tree but
 *  fuses all subexpressions into a single object.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace cache2 {

/** @brief
    A cache object storing an object of type Expr including ownership
    of that type

    A cache object stores an object of type Expr and has ownership of
    that type. The type must be created externally and passed to the
    cache object via its move constructor.
 */
template<std::size_t Tag, typename Expr>
struct CacheExpr : public CacheExprBase
{
private:
  /// @brief Self type
  using self_type = CacheExpr<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::utils::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  CacheExpr() = default;

  /// @brief Constructor (copy from expression)
  explicit CacheExpr(const Expr& expr)
    : expr(expr)
  {}

  /// @brief Constructor (move from expression)
  explicit CacheExpr(Expr&& expr)
    : expr(expr)
  {}

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<
             std::is_base_of<CacheExprBase, T>::value>::type>
  explicit CacheExpr(const T& obj)
    : expr(obj.get())
  {}

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<
             std::is_base_of<CacheExprBase, T>::value>::type>
  explicit CacheExpr(T&& obj)
    : expr(obj.get())
  {}

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expression data
  const expr_type& get() { return expr; }

  /// @brief Returns pointer to raw expression data
  expr_type* operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const { os << "expr<" << Tag << ">"; }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, CacheExpr<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
    A view on a cache object storing a reference to an object of type
    Expr excluding ownership of that object

    A view on a cache object stores a reference to an object of type
    Expr but does not have ownership of that type. The object is
    managed externally. The view just serves as a lughtweight hull.
 */
template<std::size_t Tag, typename Expr>
struct CacheExprView : public CacheExprBase
{
private:
  /// @brief Self type
  using self_type = CacheExprView<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::utils::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  const expr_type* expr;

public:
  /// @brief Default constructor
  CacheExprView() = default;

  /// @brief Constructor (wrap expression)
  explicit CacheExprView(const Expr& expr)
    : expr(&expr)
  {}

  /// @brief Constructor (copy from other expression view)
  template<std::size_t TagOther>
  explicit CacheExprView(const CacheExprView<TagOther, Expr>& other)
    : expr(&other.get())
  {}

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return *expr; }

  /// @brief Returns pointer to raw expression data
  constexpr expr_type* operator->() const { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const { os << "view<" << Tag << ">"; }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, CacheExprView<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
    Binary operator+ between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE2>::type>
auto
operator+(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '+',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a.get() + b.get())>(std::move(a.get() +
                                                               b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag,
                     '+',
                     fdbb::utils::remove_all<B>::type::tag),
                   decltype(a.get() + b.get())>(std::move(a.get() + b.get()));
}

/** @brief
    Binary operator+ between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE2,
           std::is_arithmetic<A>::value>::type>
auto
operator+(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          725795011,
                          '+',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a + b.get())>(std::move(a + b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     725795011, '+', fdbb::utils::remove_all<B>::type::tag),
                   decltype(a + b.get())>(std::move(a + b.get()));
}

/** @brief
    Binary operator+ between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE2,
           std::is_arithmetic<B>::value>::type>
auto
operator+(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '+',
                          169422329),
                        decltype(a.get() + b)>(std::move(a.get() + b)))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag, '+', 169422329),
                   decltype(a.get() + b)>(std::move(a.get() + b));
}

/** @brief
    Binary operator- between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE2>::type>
auto
operator-(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '-',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a.get() - b.get())>(std::move(a.get() -
                                                               b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag,
                     '-',
                     fdbb::utils::remove_all<B>::type::tag),
                   decltype(a.get() - b.get())>(std::move(a.get() - b.get()));
}

/** @brief
    Binary operator- between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE2,
           std::is_arithmetic<A>::value>::type>
auto
operator-(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          574849605,
                          '-',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a - b.get())>(std::move(a - b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     574849605, '-', fdbb::utils::remove_all<B>::type::tag),
                   decltype(a - b.get())>(std::move(a - b.get()));
}

/** @brief
    Binary operator- between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE2,
           std::is_arithmetic<B>::value>::type>
auto
operator-(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '-',
                          681648267),
                        decltype(a.get() - b)>(std::move(a.get() - b)))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag, '-', 681648267),
                   decltype(a.get() - b)>(std::move(a.get() - b));
}

/** @brief
    Binary operator* between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE2>::type>
auto
operator*(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '*',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a.get() * b.get())>(std::move(a.get() *
                                                               b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag,
                     '*',
                     fdbb::utils::remove_all<B>::type::tag),
                   decltype(a.get() * b.get())>(std::move(a.get() * b.get()));
}

/** @brief
    Binary operator* between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE2,
           std::is_arithmetic<A>::value>::type>
auto
operator*(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          839176472,
                          '*',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a * b.get())>(std::move(a * b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     839176472, '*', fdbb::utils::remove_all<B>::type::tag),
                   decltype(a * b.get())>(std::move(a * b.get()));
}

/** @brief
    Binary operator* between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE2,
           std::is_arithmetic<B>::value>::type>
auto
operator*(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '*',
                          689934539),
                        decltype(a.get() * b)>(std::move(a.get() * b)))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag, '*', 689934539),
                   decltype(a.get() * b)>(std::move(a.get() * b));
}

/** @brief
    Binary operator/ between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE2>::type>
auto
operator/(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '/',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a.get() / b.get())>(std::move(a.get() /
                                                               b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag,
                     '/',
                     fdbb::utils::remove_all<B>::type::tag),
                   decltype(a.get() / b.get())>(std::move(a.get() / b.get()));
}

/** @brief
    Binary operator/ between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE2,
           std::is_arithmetic<A>::value>::type>
auto
operator/(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          351934064,
                          '/',
                          fdbb::utils::remove_all<B>::type::tag),
                        decltype(a / b.get())>(std::move(a / b.get())))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     351934064, '/', fdbb::utils::remove_all<B>::type::tag),
                   decltype(a / b.get())>(std::move(a / b.get()));
}

/** @brief
    Binary operator/ between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE2,
           std::is_arithmetic<B>::value>::type>
auto
operator/(A&& a, B&& b) noexcept
  -> decltype(CacheExpr<fdbb::utils::hash<std::size_t>(
                          fdbb::utils::remove_all<A>::type::tag,
                          '/',
                          253548959),
                        decltype(a.get() / b)>(std::move(a.get() / b)))
{
  return CacheExpr<fdbb::utils::hash<std::size_t>(
                     fdbb::utils::remove_all<A>::type::tag, '/', 253548959),
                   decltype(a.get() / b)>(std::move(a.get() / b));
}

} // namespace cache2

} // namespace fdbb

#endif // FDBB_CACHE_CACHE2_HPP
