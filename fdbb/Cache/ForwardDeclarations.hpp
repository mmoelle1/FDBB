/** @file fdbb/Cache/ForwardDeclarations.hpp
 *
 *  @brief Forward declarations of the caching mechanism
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CACHE_FORWARD_DECLARATIONS_HPP
#define FDBB_CACHE_FORWARD_DECLARATIONS_HPP

#include <iostream>

namespace fdbb {

// Forward declarations of caching mechanism
namespace cache {

/** @brief
    Base class of all cached objects
 */
struct CacheExprBase
{};

/** @brief
    Cache object (forward declaration)
 */
template<std::size_t Tag, typename Expr>
struct CacheExpr;

/** @brief
    View on cache object (forward declaration)
 */
template<std::size_t Tag, typename Expr>
struct CacheExprView;

#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  template<std::size_t Tag, typename Expr>                                     \
  struct CacheExprUnaryOp_elem_##OPNAME;

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template<std::size_t Tag, typename A, typename B>                            \
  struct CacheExprBinaryOp_elem_##OPNAME;

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

#define FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(OPNAME)                       \
  template<std::size_t Tag, typename A, typename B>                            \
  struct CacheExprBinaryOp_##OPNAME;

FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(add)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(sub)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(div)

#undef FDBB_GENERATE_BINARY_OPERATION_OVERLOADS

} // namespace cache

// Forward declarations of cache-2 mechanism
namespace cache2 {

/** @brief
    Base class of all cache-2 objects
 */
struct CacheExprBase
{};

/** @brief
    Cache-2 object (forward declaration)
 */
template<std::size_t Tag, typename Expr>
struct CacheExpr;

/** @brief
    View on cache-2 object (forward declaration)
 */
template<std::size_t Tag, typename Expr>
struct CacheExprView;

} // namespace cache2

} // namespace fdbb

#endif // FDBB_CACHE_FORWARD_DECLARATIONS_HPP
