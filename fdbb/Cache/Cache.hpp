/** @file fdbb/Cache/Cache.hpp
 *
 *  @brief Caching mechanism
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CACHE_CACHE_HPP
#define FDBB_CACHE_CACHE_HPP

#include <memory>

#include <Cache/ForwardDeclarations.hpp>
#include <Core/Backend.hpp>
#include <Core/Constant.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

/** @namespace fdbb::cache
 *
 *  @brief
 *  The \ref fdbb::cache namespace, containing the standard caching
 *  functionality of the FDBB library
 *
 *  The \ref fdbb::cache namespace contains an implementation of a
 *  caching strategy for the FDBB library. The caching strategy is
 *  implemented as expression template wrapper library. A lightweight
 *  object is created for each operator of the expression tree storing
 *  references to the argument(s). In a later implementation,
 *  mechanisms to optimize the expression tree to reduce redundant
 *  computation of terms might be included.
 */
namespace cache {

/** @brief
    A cache object storing an object of type Expr including ownership
    of that type

    A cache object stores an object of type Expr and has ownership of
    that type. The type must be created externally and passed to the
    cache object via its move constructor.
 */
template<std::size_t Tag, typename Expr>
struct CacheExpr : public CacheExprBase
{
private:
  /// @brief Self type
  using self_type = CacheExpr<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::utils::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  CacheExpr() = default;

  /// @brief Constructor (copy from expression)
  explicit CacheExpr(const Expr& expr)
    : expr(expr)
  {}

  /// @brief Constructor (move from expression)
  explicit CacheExpr(Expr&& expr)
    : expr(expr)
  {}

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<std::is_base_of<
             CacheExprBase,
             typename fdbb::utils::remove_all<T>::type>::value>::type>
  explicit CacheExpr(const T& obj)
    : expr(obj.get())
  {}

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<std::is_base_of<
             CacheExprBase,
             typename fdbb::utils::remove_all<T>::type>::value>::type>
  explicit CacheExpr(T&& obj)
    : expr(obj.get())
  {}

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expression data
  expr_type& get() { return expr; }

  /// @brief Returns pointer to raw expression data
  expr_type* operator->() { return expr; }

  /// @brief Prints expression
  void print(std::ostream& os) const
  {
    if (std::is_arithmetic<Expr>::value)
      os << "<" << Tag << ">E(" << expr << ")";
    else
      os << "<" << Tag << ">E(" << typeid(expr).name() << ")";
  }

  /// @brief Pretty prints expression
  void pretty_print(std::ostream& os) const
  {
    if (std::is_arithmetic<Expr>::value)
      os << "E(" << expr << ")";
    else
      os << "E(" << typeid(expr).name() << ")";
  }

  /// @brief Prints details of expression
  template<typename Expr_ = Expr>
  void print_details(
    std::ostream& os,
    typename std::enable_if<
      std::is_base_of<CacheExprBase,
                      typename fdbb::utils::remove_all<Expr_>::type>::value,
      std::size_t>::type ident = 0) const
  {
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "CacheExpr\n"
       << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "        tag = " << tag << "\n"
       << std::string(ident, '|') << "| "
       << "  expr_type = " << typeid(expr_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "result_type = " << typeid(result_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "       expr @ " << &expr << "\n"
       << std::string(ident, '|') << "| "
       << " >> expr =\n";
    expr.print_details(os, ident + 1);
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n";
  }

  /// @brief Prints details of expression
  template<typename Expr_ = Expr>
  void print_details(
    std::ostream& os,
    typename std::enable_if<
      !std::is_base_of<CacheExprBase,
                       typename fdbb::utils::remove_all<Expr_>::type>::value,
      std::size_t>::type ident = 0) const
  {
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "CacheExpr\n"
       << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "        tag = " << tag << "\n"
       << std::string(ident, '|') << "| "
       << "  expr_type = " << typeid(expr_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "result_type = " << typeid(result_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "       expr @ " << &expr << "\n"
       << std::string(ident, '|') << "| "
       << " >> expr =\n"
       << std::string(ident, '|') << "  " << expr << "\n"
       << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n";
  }

  void eval() { expr.eval(); }

  /// @brief Assign (evaluate from cache object)
  template<typename T>
  auto operator=(const T& obj) -> typename std::enable_if<
    std::is_base_of<CacheExprBase,
                    typename fdbb::utils::remove_all<T>::type>::value,
    self_type>::type&
  {
    if ((void*)&expr != (void*)&(obj.get()))
      expr = obj.get();
    return *this;
  }

  /// @brief Assign (evaluate from non-cache object)
  template<typename T>
  auto operator=(const T& obj) -> typename std::enable_if<
    !std::is_base_of<CacheExprBase,
                     typename fdbb::utils::remove_all<T>::type>::value,
    self_type>::type&
  {
    if ((void*)&expr != (void*)&obj)
      expr = obj;
    return *this;
  }

  /// @brief Assign (evaluate from cache object)
  template<typename T>
  auto operator=(T&& obj) -> typename std::enable_if<
    std::is_base_of<CacheExprBase,
                    typename fdbb::utils::remove_all<T>::type>::value,
    self_type>::type&
  {
    if ((void*)&expr != (void*)&(obj.get()))
      expr = obj.get();
    return *this;
  }

  /// @brief Assign (evaluate from non-cache object)
  template<typename T>
  auto operator=(T&& obj) -> typename std::enable_if<
    !std::is_base_of<CacheExprBase,
                     typename fdbb::utils::remove_all<T>::type>::value,
    self_type>::type&
  {
    if ((void*)&expr != (void*)&obj)
      expr = obj;
    return *this;
  }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, CacheExpr<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
    A view on a cache object storing a reference to an object of type
    Expr excluding ownership of that object

    A view on a cache object stores a reference to an object of type
    Expr but does not have ownership of that type. The object is
    managed externally. The view just serves as a lughtweight hull.
 */
template<std::size_t Tag, typename Expr>
struct CacheExprView : public CacheExprBase
{
private:
  /// @brief Self type
  using self_type = CacheExprView<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::utils::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  expr_type* const expr;

public:
  /// @brief Default constructor
  CacheExprView() = default;

  /// @brief Constructor (wrap expression)
  explicit CacheExprView(const Expr& expr)
    : expr(&expr)
  {}

  /// @brief Constructor (copy from other expression view)
  template<std::size_t TagOther>
  explicit CacheExprView(const CacheExprView<TagOther, Expr>& other)
    : expr(&other.get())
  {}

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return *expr; }

  /// @brief Returns reference to raw expression data
  expr_type& get() { return *expr; }

  /// @brief Returns constant pointer to raw expression data
  constexpr expr_type* operator->() const { return expr; }

  /// @brief Prints expression
  void print(std::ostream& os) const
  {
    if (std::is_base_of<CacheExprBase, expr_type>::value)
      os << "<" << Tag << ">V(" << *expr << ")";
    else
      os << "<" << Tag << ">V[" << typeid(expr).name() << "]";
  }

  /// @brief Pretty prints expression
  void pretty_print(std::ostream& os) const
  {
    if (std::is_base_of<CacheExprBase, expr_type>::value)
      os << "V(" << *expr << ")";
    else
      os << "V[" << typeid(expr).name() << "]";
  }

  /// @brief Prints details of expression
  void print_details(std::ostream& os, std::size_t ident = 0) const
  {
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "CacheExprView\n"
       << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "        tag = " << tag << "\n"
       << std::string(ident, '|') << "| "
       << "  expr_type = " << typeid(expr_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "result_type = " << typeid(result_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "       expr @ " << expr << "\n"
       << std::string(ident, '|') << "| "
       << " >> expr =\n";
    expr->print_details(os, ident + 1);
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n";
  }

  void eval() { expr->eval(); }

  /// @brief Assign (evaluate from cache object)
  template<typename T>
  auto operator=(const T& obj) ->
    typename std::enable_if<std::is_base_of<CacheExprBase, T>::value,
                            self_type>::type&
  {
    if ((void*)expr != (void*)&(obj.get()))
      *expr = obj.get();
    return *this;
  }

  /// @brief Assign (evaluate from non-cache object)
  template<typename T>
  auto operator=(const T& obj) ->
    typename std::enable_if<!std::is_base_of<CacheExprBase, T>::value,
                            self_type>::type&
  {
    if ((void*)expr != (void*)&obj)
      *expr = obj;
    return *this;
  }

  /// @brief Assign (evaluate from cache object)
  template<typename T>
  auto operator=(T&& obj) ->
    typename std::enable_if<std::is_base_of<CacheExprBase, T>::value,
                            self_type>::type&
  {
    if ((void*)expr != (void*)&(obj.get()))
      *expr = obj.get();
    return *this;
  }

  /// @brief Assign (evaluate from non-cache object)
  template<typename T>
  auto operator=(T&& obj) ->
    typename std::enable_if<!std::is_base_of<CacheExprBase, T>::value,
                            self_type>::type&
  {
    if ((void*)expr != (void*)&obj)
      *expr = obj;
    return *this;
  }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, CacheExprView<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
    Element-wise unitary operators for a cache object
 */
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  template<std::size_t Tag, typename Expr>                                     \
  struct CacheExprUnaryOp_elem_##OPNAME : public CacheExprBase                 \
  {                                                                            \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = CacheExprUnaryOp_elem_##OPNAME<Tag, Expr>;               \
                                                                               \
    /** @brief Expression type */                                              \
    using expr_type = typename fdbb::utils::remove_all<Expr>::type;            \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type =                                                        \
      decltype(fdbb::elem_##OPNAME(std::declval<expr_type>().get()));          \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief Raw expression data */                                          \
    expr_type expr;                                                            \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    CacheExprUnaryOp_elem_##OPNAME() = default;                                \
                                                                               \
    /** @brief Constructor */                                                  \
    CacheExprUnaryOp_elem_##OPNAME(Expr&& expr)                                \
      : expr(expr)                                                             \
      , result(fdbb::elem_##OPNAME(expr.get()))                                \
    {}                                                                         \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type& get() const { return result; }                          \
                                                                               \
    /** @brief Returns reference to evaluated expression */                    \
    result_type& get() { return result; }                                      \
                                                                               \
    /** @brief Returns pointer to evaluated expression */                      \
    result_type& operator->() { return result; }                               \
                                                                               \
    /** @brief Prints expression */                                            \
    void print(std::ostream& os) const                                         \
    {                                                                          \
      os << "<" << Tag << ">" << #OPNAME << "(" << expr << ")";                \
    }                                                                          \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void pretty_print(std::ostream& os) const                                  \
    {                                                                          \
      os << #OPNAME << "(";                                                    \
      expr.pretty_print(os);                                                   \
      os << ")";                                                               \
    }                                                                          \
                                                                               \
    /** @brief Prints details of expression **/                                \
    void print_details(std::ostream& os, std::size_t ident = 0) const          \
    {                                                                          \
      os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n"                                                               \
         << std::string(ident, '|') << "| "                                    \
         << "CacheExprUnaryOp_elem_" << #OPNAME << "\n"                        \
         << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n"                                                               \
         << std::string(ident, '|') << "| "                                    \
         << "        tag = " << tag << "\n"                                    \
         << std::string(ident, '|') << "| "                                    \
         << "  expr_type = " << typeid(expr_type).name() << "\n"               \
         << std::string(ident, '|') << "| "                                    \
         << "result_type = " << typeid(result_type).name() << "\n"             \
         << std::string(ident, '|') << "| "                                    \
         << "       expr @ " << &expr << "\n"                                  \
         << std::string(ident, '|') << "| "                                    \
         << " >> expr =\n";                                                    \
      expr.print_details(os, ident + 1);                                       \
      os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n";                                                              \
    }                                                                          \
                                                                               \
    void eval()                                                                \
    {                                                                          \
      expr.eval();                                                             \
      result.eval();                                                           \
    }                                                                          \
  };                                                                           \
                                                                               \
  template<std::size_t Tag, typename Expr>                                     \
  std::ostream& operator<<(std::ostream& os,                                   \
                           CacheExprUnaryOp_elem_##OPNAME<Tag, Expr> expr)     \
  {                                                                            \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Element-wise binary operators between two cache objects
 */
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template<std::size_t Tag, typename A, typename B>                            \
  struct CacheExprBinaryOp_elem_##OPNAME : public CacheExprBase                \
  {                                                                            \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = CacheExprBinaryOp_elem_##OPNAME<Tag, A, B>;              \
                                                                               \
    /** @brief First expression type */                                        \
    using a_type = typename fdbb::utils::remove_all<A>::type;                  \
                                                                               \
    /** @brief Second expression type */                                       \
    using b_type = typename fdbb::utils::remove_all<B>::type;                  \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type =                                                        \
      decltype(fdbb::elem_##OPNAME(std::declval<a_type>().get(),               \
                                   std::declval<b_type>().get()));             \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief First raw expression data */                                    \
    a_type a;                                                                  \
                                                                               \
    /** @brief Second raw expression data */                                   \
    b_type b;                                                                  \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    CacheExprBinaryOp_elem_##OPNAME() = default;                               \
                                                                               \
    /** @brief Constructor */                                                  \
    CacheExprBinaryOp_elem_##OPNAME(A&& a, B&& b)                              \
      : a(a)                                                                   \
      , b(b)                                                                   \
      , result(fdbb::elem_##OPNAME(a.get(), b.get()))                          \
    {}                                                                         \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type& get() const { return result; }                          \
                                                                               \
    /** @brief Returns pointer to evaluated expression */                      \
    result_type& operator->() { return result; }                               \
                                                                               \
    /** @brief Prints expression */                                            \
    void print(std::ostream& os) const                                         \
    {                                                                          \
      os << "<" << Tag << ">(" << a << "." << #OPNAME << "." << b << ")";      \
    }                                                                          \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void pretty_print(std::ostream& os) const                                  \
    {                                                                          \
      os << "(";                                                               \
      a.pretty_print(os);                                                      \
      os << "." << #OPNAME << ".";                                             \
      b.pretty_print(os);                                                      \
      os << ")";                                                               \
    }                                                                          \
                                                                               \
    /** @brief Prints details of expression **/                                \
    void print_details(std::ostream& os, std::size_t ident = 0) const          \
    {                                                                          \
      os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n"                                                               \
         << std::string(ident, '|') << "| "                                    \
         << "CacheExprBinaryOp_elem_" << #OPNAME << "\n"                       \
         << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n"                                                               \
         << std::string(ident, '|') << "| "                                    \
         << "        tag = " << tag << "\n"                                    \
         << std::string(ident, '|') << "| "                                    \
         << "      a_type = " << typeid(a_type).name() << "\n"                 \
         << std::string(ident, '|') << "| "                                    \
         << "      b_type = " << typeid(b_type).name() << "\n"                 \
         << std::string(ident, '|') << "| "                                    \
         << "result_type = " << typeid(result_type).name() << "\n"             \
         << std::string(ident, '|') << "| "                                    \
         << "          a @ " << &a << "\n"                                     \
         << std::string(ident, '|') << "| "                                    \
         << "          b @ " << &b << "\n"                                     \
         << std::string(ident, '|') << "| "                                    \
         << " >> a =\n";                                                       \
      a.print_details(os, ident);                                              \
      os << std::string(ident, '|') << "| "                                    \
         << " >> b =\n";                                                       \
      b.print_details(os, ident);                                              \
      os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n";                                                              \
    }                                                                          \
                                                                               \
    void eval()                                                                \
    {                                                                          \
      a.eval();                                                                \
      b.eval();                                                                \
      result.eval();                                                           \
    }                                                                          \
  };                                                                           \
                                                                               \
  template<std::size_t Tag, typename A, typename B>                            \
  std::ostream& operator<<(std::ostream& os,                                   \
                           CacheExprBinaryOp_elem_##OPNAME<Tag, A, B> expr)    \
  {                                                                            \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Binary operators between two cache objects
 */
#define FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(OPNAME, OP)                   \
  template<std::size_t Tag, typename A, typename B>                            \
  struct CacheExprBinaryOp_##OPNAME : public CacheExprBase                     \
  {                                                                            \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = CacheExprBinaryOp_##OPNAME<Tag, A, B>;                   \
                                                                               \
    /** @brief First expression type */                                        \
    using a_type = typename fdbb::utils::remove_all<A>::type;                  \
                                                                               \
    /** @brief Second expression type */                                       \
    using b_type = typename fdbb::utils::remove_all<B>::type;                  \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type =                                                        \
      decltype(std::declval<a_type>().get() OP std::declval<b_type>().get());  \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief First raw expression data */                                    \
    a_type a;                                                                  \
                                                                               \
    /** @brief Second raw expression data */                                   \
    b_type b;                                                                  \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    CacheExprBinaryOp_##OPNAME() = default;                                    \
                                                                               \
    /** @brief Constructor */                                                  \
    CacheExprBinaryOp_##OPNAME(A&& a, B&& b)                                   \
      : a(a)                                                                   \
      , b(b)                                                                   \
      , result(a.get() OP b.get())                                             \
    {}                                                                         \
                                                                               \
    /** @brief Prints expression */                                            \
    void print(std::ostream& os) const                                         \
    {                                                                          \
      os << "<" << Tag << ">(" << a << #OP << b << ")";                        \
    }                                                                          \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void pretty_print(std::ostream& os) const                                  \
    {                                                                          \
      os << "(";                                                               \
      a.pretty_print(os);                                                      \
      os << #OP;                                                               \
      b.pretty_print(os);                                                      \
      os << ")";                                                               \
    }                                                                          \
                                                                               \
    /** @brief Prints details of expression **/                                \
    void print_details(std::ostream& os, std::size_t ident = 0) const          \
    {                                                                          \
      os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n"                                                               \
         << std::string(ident, '|') << "| "                                    \
         << "CacheExprBinaryOp_" << #OPNAME << "\n"                            \
         << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n"                                                               \
         << std::string(ident, '|') << "| "                                    \
         << "        tag = " << tag << "\n"                                    \
         << std::string(ident, '|') << "| "                                    \
         << "     a_type = " << typeid(a_type).name() << "\n"                  \
         << std::string(ident, '|') << "| "                                    \
         << "     b_type = " << typeid(b_type).name() << "\n"                  \
         << std::string(ident, '|') << "| "                                    \
         << "result_type = " << typeid(result_type).name() << "\n"             \
         << std::string(ident, '|') << "| "                                    \
         << "          a @ " << &a << "\n"                                     \
         << std::string(ident, '|') << "| "                                    \
         << "          b @ " << &b << "\n"                                     \
         << std::string(ident, '|') << "| "                                    \
         << " >> a =\n";                                                       \
      a.print_details(os, ident + 1);                                          \
      os << std::string(ident, '|') << "| "                                    \
         << " >> b =\n";                                                       \
      b.print_details(os, ident + 1);                                          \
      os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')    \
         << "\n";                                                              \
    }                                                                          \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type& get() const { return result; }                          \
                                                                               \
    /** @brief Returns reference to evaluated expression */                    \
    result_type& get() { return result; }                                      \
  };                                                                           \
                                                                               \
  template<std::size_t Tag, typename A, typename B>                            \
  std::ostream& operator<<(std::ostream& os,                                   \
                           CacheExprBinaryOp_##OPNAME<Tag, A, B> expr)         \
  {                                                                            \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(add, +)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(sub, -)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(mul, *)

#undef FDBB_GENERATE_BINARY_OPERATION_OVERLOADS

/** @brief
    Binary operator/ for cache object
 */
template<std::size_t Tag, typename A, typename B>
struct CacheExprBinaryOp_div : public CacheExprBase
{
private:
  /// @brief Self type
  using self_type = CacheExprBinaryOp_div<Tag, A, B>;

  /// @brief First expression type
  using a_type = typename fdbb::utils::remove_all<A>::type;

  /// @brief Second expression type
  using b_type = typename fdbb::utils::remove_all<B>::type;

public:
  /// @brief Result type
  using result_type =
    decltype(std::declval<a_type>().get() / std::declval<b_type>().get());

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief First raw expression data
  a_type a;

  /// @brief Second raw expression data
  b_type b;

  /// @brief Result expression data
  result_type result;

public:
  /// @brief Default constructor
  CacheExprBinaryOp_div() = default;

  /// @brief Constructor
  CacheExprBinaryOp_div(A&& a, B&& b)
    : a(a)
    , b(b)
    , result(a.get() / b.get())
  {}

  /// @brief Prints expression
  void print(std::ostream& os) const
  {
    os << "<" << Tag << ">(" << a << "/" << b << ")";
  }

  /// @brief Pretty prints expression
  void pretty_print(std::ostream& os) const
  {
    os << "(";
    a.pretty_print(os);
    os << "/";
    b.pretty_print(os);
    os << ")";
  }

  /// @brief Prints details of expression
  void print_details(std::ostream& os, std::size_t ident = 0) const
  {
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "CacheExprBinaryOp_div\n"
       << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n"
       << std::string(ident, '|') << "| "
       << "        tag = " << tag << "\n"
       << std::string(ident, '|') << "| "
       << "     a_type = " << typeid(a_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "     b_type = " << typeid(b_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "result_type = " << typeid(result_type).name() << "\n"
       << std::string(ident, '|') << "| "
       << "          a @ " << &a << "\n"
       << std::string(ident, '|') << "| "
       << "          b @ " << &b << "\n"
       << std::string(ident, '|') << "| "
       << " >> a =\n";
    a.print_details(os, ident + 1);
    os << std::string(ident, '|') << "| "
       << " >> b =\n";
    b.print_details(os, ident + 1);
    os << std::string(ident, '|') << "+-" << std::string(80 - ident, '-')
       << "\n";
  }

  /// @brief Returns evaluated expression
  const result_type& get() const { return result; }
};

template<std::size_t Tag, typename A, typename B>
std::ostream&
operator<<(std::ostream& os, CacheExprBinaryOp_div<Tag, A, B> expr)
{
  expr.print(os);
  return os;
}

/** @brief
    Binary operator+ between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE>::type>
auto
operator+(A&& a, B&& b) noexcept -> CacheExprBinaryOp_add<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '+',
                                 fdbb::utils::remove_all<B>::type::tag),
  A,
  B>
{
  return CacheExprBinaryOp_add<fdbb::utils::hash<std::size_t>(
                                 fdbb::utils::remove_all<A>::type::tag,
                                 '+',
                                 fdbb::utils::remove_all<B>::type::tag),
                               A,
                               B>(std::forward<A>(a), std::forward<B>(b));
}

/** @brief
    Binary operator+ between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE,
           std::is_arithmetic<A>::value>::type>
auto
operator+(A&& a, B&& b) noexcept -> CacheExprBinaryOp_add<
  fdbb::utils::hash<std::size_t>(725795011,
                                 '+',
                                 fdbb::utils::remove_all<B>::type::tag),
  CacheExpr<725795011, A>,
  B>
{
  return CacheExprBinaryOp_add<
    fdbb::utils::hash<std::size_t>(
      725795011, '+', fdbb::utils::remove_all<B>::type::tag),
    CacheExpr<725795011, A>,
    B>(CacheExpr<725795011, A>(A(a)), std::forward<B>(b));
}

/** @brief
    Binary operator+ between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE,
           std::is_arithmetic<B>::value>::type>
auto
operator+(A&& a, B&& b) noexcept -> CacheExprBinaryOp_add<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '+',
                                 169422329),
  A,
  CacheExpr<169422329, B>>
{
  return CacheExprBinaryOp_add<
    fdbb::utils::hash<std::size_t>(
      fdbb::utils::remove_all<A>::type::tag, '+', 169422329),
    A,
    CacheExpr<169422329, B>>(std::forward<A>(a), CacheExpr<169422329, B>(B(b)));
}

/** @brief
    Binary operator- between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE>::type>
auto
operator-(A&& a, B&& b) noexcept -> CacheExprBinaryOp_sub<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '-',
                                 fdbb::utils::remove_all<B>::type::tag),
  A,
  B>
{
  return CacheExprBinaryOp_sub<fdbb::utils::hash<std::size_t>(
                                 fdbb::utils::remove_all<A>::type::tag,
                                 '-',
                                 fdbb::utils::remove_all<B>::type::tag),
                               A,
                               B>(std::forward<A>(a), std::forward<B>(b));
}

/** @brief
    Binary operator- between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE,
           std::is_arithmetic<A>::value>::type>
auto
operator-(A&& a, B&& b) noexcept -> CacheExprBinaryOp_sub<
  fdbb::utils::hash<std::size_t>(574849605,
                                 '-',
                                 fdbb::utils::remove_all<B>::type::tag),
  CacheExpr<574849605, A>,
  B>
{
  return CacheExprBinaryOp_sub<
    fdbb::utils::hash<std::size_t>(
      574849605, '-', fdbb::utils::remove_all<B>::type::tag),
    CacheExpr<574849605, A>,
    B>(CacheExpr<574849605, A>(A(a)), std::forward<B>(b));
}

/** @brief
    Binary operator- between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE,
           std::is_arithmetic<B>::value>::type>
auto
operator-(A&& a, B&& b) noexcept -> CacheExprBinaryOp_sub<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '-',
                                 681648267),
  A,
  CacheExpr<681648267, B>>
{
  return CacheExprBinaryOp_sub<
    fdbb::utils::hash<std::size_t>(
      fdbb::utils::remove_all<A>::type::tag, '-', 681648267),
    A,
    CacheExpr<681648267, B>>(std::forward<A>(a), CacheExpr<681648267, B>(B(b)));
}

/** @brief
    Binary operator* between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE>::type>
auto
operator*(A&& a, B&& b) noexcept -> CacheExprBinaryOp_mul<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '*',
                                 fdbb::utils::remove_all<B>::type::tag),
  A,
  B>
{
  return CacheExprBinaryOp_mul<fdbb::utils::hash<std::size_t>(
                                 fdbb::utils::remove_all<A>::type::tag,
                                 '*',
                                 fdbb::utils::remove_all<B>::type::tag),
                               A,
                               B>(std::forward<A>(a), std::forward<B>(b));
}

/** @brief
    Binary operator* between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE,
           std::is_arithmetic<A>::value>::type>
auto
operator*(A&& a, B&& b) noexcept -> CacheExprBinaryOp_mul<
  fdbb::utils::hash<std::size_t>(839176472,
                                 '*',
                                 fdbb::utils::remove_all<B>::type::tag),
  CacheExpr<839176472, A>,
  B>
{
  return CacheExprBinaryOp_mul<
    fdbb::utils::hash<std::size_t>(
      839176472, '*', fdbb::utils::remove_all<B>::type::tag),
    CacheExpr<839176472, A>,
    B>(CacheExpr<839176472, A>(A(a)), std::forward<B>(b));
}

/** @brief
    Binary operator* between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE,
           std::is_arithmetic<B>::value>::type>
auto
operator*(A&& a, B&& b) noexcept -> CacheExprBinaryOp_mul<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '*',
                                 689934539),
  A,
  CacheExpr<689934539, B>>
{
  return CacheExprBinaryOp_mul<
    fdbb::utils::hash<std::size_t>(
      fdbb::utils::remove_all<A>::type::tag, '*', 689934539),
    A,
    CacheExpr<689934539, B>>(std::forward<A>(a), CacheExpr<689934539, B>(B(b)));
}

/** @brief
    Binary operator/ between two cache objects
 */
template<
  typename A,
  typename B,
  typename = typename fdbb::enable_if_all_type_of<A, B, EnumETL::CACHE>::type>
auto
operator/(A&& a, B&& b) noexcept -> CacheExprBinaryOp_div<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '/',
                                 fdbb::utils::remove_all<B>::type::tag),
  A,
  B>
{
  return CacheExprBinaryOp_div<fdbb::utils::hash<std::size_t>(
                                 fdbb::utils::remove_all<A>::type::tag,
                                 '/',
                                 fdbb::utils::remove_all<B>::type::tag),
                               A,
                               B>(std::forward<A>(a), std::forward<B>(b));
}

/** @brief
    Binary operator/ between an object of arithmetic type and a cache object
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           B,
           EnumETL::CACHE,
           std::is_arithmetic<A>::value>::type>
auto
operator/(A&& a, B&& b) noexcept -> CacheExprBinaryOp_div<
  fdbb::utils::hash<std::size_t>(351934064,
                                 '/',
                                 fdbb::utils::remove_all<B>::type::tag),
  CacheExpr<351934064, A>,
  B>
{
  return CacheExprBinaryOp_div<
    fdbb::utils::hash<std::size_t>(
      351934064, '/', fdbb::utils::remove_all<B>::type::tag),
    CacheExpr<351934064, A>,
    B>(CacheExpr<351934064, A>(A(a)), std::forward<B>(b));
}

/** @brief
    Binary operator/ between a cache object and an object of arithmetic type
 */
template<typename A,
         typename B,
         typename = typename fdbb::enable_if_type_of_and_cond<
           A,
           EnumETL::CACHE,
           std::is_arithmetic<B>::value>::type>
auto
operator/(A&& a, B&& b) noexcept -> CacheExprBinaryOp_div<
  fdbb::utils::hash<std::size_t>(fdbb::utils::remove_all<A>::type::tag,
                                 '/',
                                 253548959),
  A,
  CacheExpr<253548959, B>>
{
  return CacheExprBinaryOp_div<
    fdbb::utils::hash<std::size_t>(
      fdbb::utils::remove_all<A>::type::tag, '/', 253548959),
    A,
    CacheExpr<253548959, B>>(std::forward<A>(a), CacheExpr<253548959, B>(B(b)));
}

} // namespace cache

} // namespace fdbb

#endif // FDBB_CACHE_CACHE_HPP
