/** @file Core/Backend.hpp
 *
 *  @brief Compatibility backend functionality
 *
 *  This file implements a compatibility layer between the
 *  non-standardized expression template libraries and the naming
 *  convention using in FDBB. It also implements extra functionality
 *  not available in (some of) the expression template libraries.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CORE_BACKEND_HPP
#define FDBB_CORE_BACKEND_HPP

#include <Backend/ForwardDeclarations.hpp>
#include <Core/Config.hpp>

namespace fdbb {

/** @brief
    If `T` is a type of the expression template library `Expr`, then
    `fdbb::enable_if_type_of` has a public member typedef `type`,
    equal to `T`; otherwise there is no public member typedef `type`.

    @tparam   T      The type to check against the given expression type
    @tparam   Expr   The expression type
 */
template<typename T, EnumETL Expr, typename C = void>
using enable_if_type_of =
  std::enable_if<fdbb::backend::is_type_of<T, Expr>::value, C>;

/** @brief
    If `T` is a type of the expression template library `Expr` and
    condition `cond` is `true`, then `fdbb::enable_if_type_of` has a
    public member typedef `type`, equal to `T`; otherwise there is no
    public member typedef `type`.

    @tparam   T      The type to check against the given expression type
    @tparam   Expr   The expression type
    @tparam   Cond   The condition consider additionally
 */
template<typename T, EnumETL Expr, bool Cond, typename C = void>
using enable_if_type_of_and_cond =
  std::enable_if<fdbb::backend::is_type_of<T, Expr>::value && Cond, C>;

/** @brief
    If `T` is a type of the expression template library `Expr` or
    condition `cond` is `true`, then `fdbb::enable_if_type_of` has a
    public member typedef `type`, equal to `T`; otherwise there is no
    public member typedef `type`.

    @tparam   T      The type to check against the given expression type
    @tparam   Expr   The expression type
    @tparam   Cond   The condition consider additionally
 */
template<typename T, EnumETL Expr, bool Cond, typename C = void>
using enable_if_type_of_or_cond =
  std::enable_if<fdbb::backend::is_type_of<T, Expr>::value || Cond, C>;

/** @brief
    If `T1` and `T2` are the same types of the expression template
    library `Expr`, then `fdbb::enable_if_type_of` has a public member
    typedef `type`, equal to `T1`; otherwise there is no public member
    typedef `type`.

    @tparam   T1     The first type to check against the given expression type
    @tparam   T2     The second type to check against the given expression type
    @tparam   Expr   The expression type
 */
template<typename T1, typename T2, EnumETL Expr, typename C = void>
using enable_if_all_type_of =
  std::enable_if<fdbb::backend::is_type_of<T1, Expr>::value &&
                   fdbb::backend::is_type_of<T2, Expr>::value,
                 C>;

/** @brief
    If `T1` and `T2` are the same types of the expression template
    library `Expr` and condition `cond` is `true`, then
    `fdbb::enable_if_type_of` has a public member typedef `type`,
    equal to `T1`; otherwise there is no public member typedef `type`.

    @tparam   T1     The first type to check against the given expression type
    @tparam   T2     The second type to check against the given expression type
    @tparam   Expr   The expression type
    @tparam   Cond   The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, typename C = void>
using enable_if_all_type_of_and_cond =
  std::enable_if<(fdbb::backend::is_type_of<T1, Expr>::value &&
                  fdbb::backend::is_type_of<T2, Expr>::value) &&
                   Cond,
                 C>;

/** @brief
    If `T1` and `T2` are the same types of the expression template
    library `Expr` or condition `cond` is `true`, then
    `fdbb::enable_if_type_of` has a public member typedef `type`,
    equal to `T1`; otherwise there is no public member typedef `type`.

    @tparam   T1     The first type to check against the given expression type
    @tparam   T2     The second type to check against the given expression type
    @tparam   Expr   The expression type
    @tparam   Cond   The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, typename C = void>
using enable_if_all_type_of_or_cond =
  std::enable_if<(fdbb::backend::is_type_of<T1, Expr>::value &&
                  fdbb::backend::is_type_of<T2, Expr>::value) ||
                   Cond,
                 C>;

/** @brief
    If `T1` or `T2` are the same types of the expression template
    library `Expr`, then `fdbb::enable_if_type_of` has a public member
    typedef `type`, equal to `T1` or `T2`; otherwise there is no
    public member typedef `type`.

    @tparam   T1     The first type to check against the given expression type
    @tparam   T2     The second type to check against the given expression type
    @tparam   Expr   The expression type
 */
template<typename T1, typename T2, EnumETL Expr, typename C = void>
using enable_if_any_type_of =
  std::enable_if<(fdbb::backend::is_type_of<T1, Expr>::value ||
                  fdbb::backend::is_type_of<T2, Expr>::value),
                 C>;

/** @brief
    If `T1` or `T2` are the same types of the expression template
    library `Expr` and condition `cond` is `true`, then
    `fdbb::enable_if_type_of` has a public member typedef `type`,
    equal to `T1` or `T2`; otherwise there is no public member typedef
    `type`.

    @tparam   T1     The first type to check against the given expression type
    @tparam   T2     The second type to check against the given expression type
    @tparam   Expr   The expression type
    @tparam   Cond   The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, typename C = void>
using enable_if_any_type_of_and_cond =
  std::enable_if<(fdbb::backend::is_type_of<T1, Expr>::value ||
                  fdbb::backend::is_type_of<T2, Expr>::value) &&
                   Cond,
                 C>;

/** @brief
    If `T1` or `T2` are the same types of the expression template
    library `Expr` or condition `cond` is `true`, then
    `fdbb::enable_if_type_of` has a public member typedef `type`,
    equal to `T1` or `T2`; otherwise there is no member typedef
    `type`.

    @tparam   T1     The first type to check against the given expression type
    @tparam   T2     The second type to check against the given expression type
    @tparam   Expr   The expression type
    @tparam   Cond   The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, typename C = void>
using enable_if_any_type_of_or_cond =
  std::enable_if<(fdbb::backend::is_type_of<T1, Expr>::value ||
                  fdbb::backend::is_type_of<T2, Expr>::value) ||
                   Cond,
                 C>;

} // namespace fdbb

// Include backend implementations
#include <Backend/armadillo.hpp>
#include <Backend/arrayfire.hpp>
#include <Backend/blaze.hpp>
#include <Backend/eigen.hpp>
#include <Backend/itpp.hpp>
#include <Backend/mtl4.hpp>
#include <Backend/ublas.hpp>
#include <Backend/vexcl.hpp>
#include <Backend/viennacl.hpp>

// Include cache-backend implementation:
//
// Make sure that the cache implementation is included after all
// other implementations have been included so that calls to methods
// from the underlying backends get dispatched correctly.
#include <Backend/cache.hpp>

// Include block expression implementation:
//
// Make sure that the block expression implementation is included
// after all other implementations have been included so that calls
// to methods from the underlying backends get dispatched correctly.
#include <Backend/blockexpression.hpp>

namespace fdbb {

/** @brief
    Creates constant that complies with the value-type of the expression.

    @note
    If no specialized implementation is available then this
    implementation casts the given value to the scalar value-type of
    the expression and returns it as const.

    @tparam      TC      The typeconstant (stores constant encoded in template
    parameter)

    @tparam      T       The constant type
    @tparam      Expr    The expression type

    @param[in]   value   The constant value
    @param[in]   expr    The expression

    @return              The constant
 */
template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant(const T value,
                                                Expr&& expr) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::backend::detail::make_constant_impl<
              TC,
              T,
              Expr,
              fdbb::backend::detail::get_make_constant_impl<Expr>::value>::
                eval(value, std::forward<Expr>(expr)))
#endif
{
  return fdbb::backend::detail::make_constant_impl<
    TC,
    T,
    Expr,
    fdbb::backend::detail::get_make_constant_impl<Expr>::value>::
    eval(value, std::forward<Expr>(expr));
}

/** @brief
    Creates temporary expression that may be reused in a vector
    expression. The type of the resulting temporary variable must
    be explicitly specified as a template parameter.

    @note
    This functions requires T!=Expr. If T==Expr the
    make_temp<Tag,Expr>(Expr&& expr) function gets activated

    @tparam      Tag    The unique tag identifier
    @tparam      T      The temporal type
    @tparam      Expr   The expression type

    @param[in]   expr   The expression

    @return             The tagged expression
 */
template<std::size_t Tag, typename Temp, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept
#if !defined(DOXYGEN)
  -> typename std::enable_if<
    !std::is_same<Temp, Expr>::value,
    decltype(fdbb::backend::detail::make_explicit_temp_impl<
             Tag,
             Temp,
             Expr,
             fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::
               value>::eval(std::forward<Expr>(expr)))>::type
#endif
{
  return fdbb::backend::detail::make_explicit_temp_impl<
    Tag,
    Temp,
    Expr,
    fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::value>::
    eval(std::forward<Expr>(expr));
}

/** @brief
    Creates temporary expression that may be reused in a vector
    expression. The type of the resulting temporary variable is
    automatically deduced from the expression.

    @note
    If no specialized implementation is available then this
    implementation performs perfect forwarding of the argument to the
    caller and no temporary is created at all.

    @tparam      Tag    The unique tag identifier
    @tparam      Expr   The expression type

    @param[in]   expr   The expression

    @return             The temporary expression
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::backend::detail::make_temp_impl<
              Tag,
              Expr,
              fdbb::backend::detail::get_make_temp_impl<Expr>::value>::
                eval(std::forward<Expr>(expr)))
#endif
{
  return fdbb::backend::detail::make_temp_impl<
    Tag,
    Expr,
    fdbb::backend::detail::get_make_temp_impl<Expr>::value>::
    eval(std::forward<Expr>(expr));
}

/** @brief
    Tags terminal with a unique (in a single expression) tag.

    @note
    If no specialized implementation is available then this
    implementation performs perfect forarding of the argument to the
    caller and no tag is created at all.

    @tparam      Tag    The unique tag identifier
    @tparam      Expr   The expression type

    @param[in]   expr   The expression

    @return             The tagged terminal
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag(Expr&& expr) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::backend::detail::tag_impl<
              Tag,
              Expr,
              fdbb::backend::detail::get_tag_impl<Expr>::value>::
                eval(std::forward<Expr>(expr)))
#endif
{
  return fdbb::backend::detail::
    tag_impl<Tag, Expr, fdbb::backend::detail::get_tag_impl<Expr>::value>::eval(
      std::forward<Expr>(expr));
}

/** @brief
    Helper macro for generating element-wise binary operations
 */
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief
      Element-wise \a OPNAME function

      @tparam    A The first expression type
      @tparam    B The second expression type

      @param[in] a The first expression
      @param[in] b The second expression

      @return      The element-wise evaluation of OPNAME(a,b)
   */                                                                          \
  template<typename A, typename B>                                             \
  auto constexpr elem_##OPNAME(A&& a, B&& b) noexcept                          \
    ->decltype(fdbb::backend::detail::elem_##OPNAME##_impl<                    \
               A,                                                              \
               B,                                                              \
               fdbb::backend::detail::get_elem_##OPNAME##_impl<A, B>::value>:: \
                 eval(std::forward<A>(a), std::forward<B>(b)))                 \
  {                                                                            \
    return fdbb::backend::detail::elem_##OPNAME##_impl<                        \
      A,                                                                       \
      B,                                                                       \
      fdbb::backend::detail::get_elem_##OPNAME##_impl<A, B>::value>::          \
      eval(std::forward<A>(a), std::forward<B>(b));                            \
  }

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Helper macro for generating element-wise unary operations
 */
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Element-wise \a OPNAME function

      @tparam      A   The expression type

      @param[in]   a   The expression

      @return          The element-wise evaluation of OPNAME(a)
   */                                                                          \
  template<typename A>                                                         \
  auto constexpr elem_##OPNAME(A&& a) noexcept                                 \
    ->decltype(fdbb::backend::detail::elem_##OPNAME##_impl<                    \
               A,                                                              \
               fdbb::backend::detail::get_elem_##OPNAME##_impl<A>::value>::    \
                 eval(std::forward<A>(a)))                                     \
  {                                                                            \
    return fdbb::backend::detail::elem_##OPNAME##_impl<                        \
      A,                                                                       \
      fdbb::backend::detail::get_elem_##OPNAME##_impl<A>::value>::             \
      eval(std::forward<A>(a));                                                \
  }

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace fdbb

#endif // FDBB_CORE_BACKEND_HPP
