/** @file fdbb/Core/Utils.hpp
 *
 *  @brief Utilities
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CORE_UTILS_HPP
#define FDBB_CORE_UTILS_HPP

#include <complex>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>

#include <Core/Config.hpp>

namespace fdbb {

/** @namespace fdbb::utils
 *
 *  @brief
 *  The \ref fdbb::utils namespace, containing extra utilities of the
 *  FDBB library
 *
 *  The \ref fdbb::utils namespace contains some extra utilities that
 *  are used by the FDBB library. Like the functionality in the \ref
 *  fdbb namespace, the extra utilities have a stable API that does
 *  not change over future releases of the FDBB library.
 */
namespace utils {

// The FDBB library provides its own fdbb::utils::get<I>(Type object)
// functionality extending the std::get<I>(std::tuple) functionality
//
// Note that the C++ standard 17.6.4.2.1p1 applies stating that adding
// a declaration of your own function template overload is undefined
// behaviour. We therefore import std::get into the fdbb::utils
// namespace and add declarations of our won function template
// overloads to fdbb::utils::get<I>(Type object).
using std::get;

/** @namespace fdbb::utils::detail
 *
 *  @brief
 *  The \ref fdbb::utils::detail namespace, containing implementation
 *  details of the extra utilities of the FDBB library
 *
 *  The \ref fdbb::utils::detail namespace contains the implementation
 *  details of the extra utilities that are used by the FDBB
 *  library. The functionality and API is subject to change between
 *  different releases without notice. For this reason, end-users are
 *  advised to not make explicit use of functionality from the \ref
 *  fdbb::utils::detail namespace.
 */

namespace detail {} // namespace detail

/** @brief
    Compile-time djb2-hashing of variadic parameter pack

    The djb2 hashing algorithm by Dan Bernstein is described at
    http://www.cse.yorku.ca/~oz/hash.html

    @tparam    hast_t The hash type
    @tparam    T      The parameter type

    @param[in] hash   The initial hash value
    @param[in] t      The parameter

    @return           The hash value
 */
template<typename hash_t, typename T>
FDBB_INLINE hash_t constexpr djb2(hash_t hash, T t)
{
  return static_cast<hash_t>(((hash << 5) + hash) + static_cast<hash_t>(t));
}

/** @brief
    Compile-time djb2-hashing of variadic parameter pack

    The djb2 hashing algorithm by Dan Bernstein is described at
    http://www.cse.yorku.ca/~oz/hash.html

    @tparam    hast_t The hash type
    @tparam    T      The parameter type
    @tparam    Ts...  The variadic parameter pack type

    @param[in] hash   The initial hash value
    @param[in] t      The parameter
    @param[in] ts     The variadic parameter pack

    @return           The hash value
 */
template<typename hash_t, typename T, typename... Ts>
FDBB_INLINE hash_t constexpr djb2(hash_t hash, T t, Ts... ts)
{
  return djb2(djb2(hash, t), ts...);
}

/** @brief
    Compile-time hashing of variadic parameter pack

    @tparam    hast_t The hash type
    @tparam    Ts...  The variadic parameter pack types

    @param[in] ts     The variadic parameter pack

    @return           The hash value
 */
template<typename hash_t, typename... Ts>
FDBB_INLINE hash_t constexpr hash(Ts... ts)
{
  return djb2<hash_t>(5381, ts...);
}

/** @brief
    Extracts the unqualified base type by removing any `const`,
    `volatile` and `reference`

    @tparam   T  The type from which to extract the unqualified base type
 */
template<typename T>
using remove_all =
  std::remove_cv<typename std::remove_pointer<typename std::remove_reference<
    typename std::remove_all_extents<T>::type>::type>::type>;

/**
 * @brief
 * Converts argument `source` from type `Source` to type `Dest`
 *
 * @tparam   Dest     The destination type
 * @tparam   Source   The source type
 */
template<typename Dest, typename Source>
Dest
to_type(Source source)
{
  return std::forward<Dest>(source);
}

/**
 * @brief
 * Converts argument `source` from type `Source` to type `Dest`
 *
 * @tparam   Dest     The destination type
 * @tparam   Source   The source type
 */
template<typename Dest, typename Source>
Dest
to_type(std::complex<Source> source)
{
  return (Dest)source.real();
}

/**
 * @brief
 * Checks if type `T` is a complex type
 *
 * @tparam   T   The type that is checked for being of complex type
 */
template<typename T>
struct is_complex : std::false_type
{};

/**
 * @brief
 * Checks if type `T` is a complex type
 *
 * @tparam   T   The type that is checked for being of complex type
 */
template<typename T>
struct is_complex<std::complex<T>> : std::true_type
{};

namespace detail {

/** @brief
    Compile-time for_each loop without return value (one parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    T0   The type of the 0th parameter
    @tparam    F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         typename T0,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_1_impl
{
  /** @brief
      Compile-time for_each loop evaluator without return value (one parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>) noexcept
  {
    f(fdbb::utils::get<I0>(t0), std::integral_constant<std::size_t, I0>());
    for_each_1_impl<I0 + Inc0, Inc0, T0, F>::eval(
      std::forward<T0>(t0),
      f,
      std::integral_constant<std::size_t, I0 + Inc0>(),
      std::integral_constant<std::size_t, Inc0>());
  }
};

/** @brief
    Compile-time for_each loop without return value (one parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    T0   The type of the 0th parameter
    @tparam    F    The functor to be applied
 */
template<std::size_t I0, std::size_t Inc0, typename T0, typename F>
struct for_each_1_impl<I0, Inc0, T0, F, true>
{
  /** @brief
      Compile-time for_each loop evaluator without return value (one parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>) noexcept
  {
    f(fdbb::utils::get<I0>(t0), std::integral_constant<std::size_t, I0>());
  }
};

/** @brief
    Compile-time for_each loop with return value (one parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    T0   The type of the 0th parameter
    @tparam    F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         typename T0,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_1_impl
{
  /** @brief
      Compile-time for_each loop evaluator with return value (one parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        std::integral_constant<std::size_t, I0>())),
      for_each_return_1_impl<I0 + Inc0, Inc0, T0, F>::eval(
        std::forward<T0>(t0),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>())))
#endif
  {
    return std::tuple_cat(
      std::make_tuple(
        f(fdbb::utils::get<I0>(t0), std::integral_constant<std::size_t, I0>())),
      for_each_return_1_impl<I0 + Inc0, Inc0, T0, F>::eval(
        std::forward<T0>(t0),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>()));
  }
};

/** @brief
    Compile-time for_each loop with return value (one parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    T0   The type of the 0th parameter
    @tparam    F    The functor to be applied
 */
template<std::size_t I0, std::size_t Inc0, typename T0, typename F>
struct for_each_return_1_impl<I0, Inc0, T0, F, true>
{
  /** @brief
      Compile-time for_each loop evaluator with return value (one parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::make_tuple(f(fdbb::utils::get<I0>(t0),
                                  std::integral_constant<std::size_t, I0>())))
#endif
  {
    return std::make_tuple(
      f(fdbb::utils::get<I0>(t0), std::integral_constant<std::size_t, I0>()));
  }
};

} // namespace detail

/** @brief
    Compile-time for_each loop without return value (one parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    T0   The type of the 0th parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] f    The functor
 */
template<std::size_t I0 = 0, std::size_t Inc0 = 1, typename T0, typename F>
FDBB_INLINE void
for_each(T0&& t0, F f) noexcept
{
  fdbb::utils::detail::for_each_1_impl<I0, Inc0, T0, F>::eval(
    std::forward<T0>(t0),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>());
}

/** @brief
    Compile-time for_each loop with return value (one parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    T0   The type of the 0th parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] f    The functor
 */
template<std::size_t I0 = 0, std::size_t Inc0 = 1, typename T0, typename F>
FDBB_INLINE auto constexpr for_each_return(T0&& t0, F f) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::utils::detail::for_each_return_1_impl<I0, Inc0, T0, F>::
                eval(std::forward<T0>(t0),
                     f,
                     std::integral_constant<std::size_t, I0>(),
                     std::integral_constant<std::size_t, Inc0>()))
#endif
{
  return fdbb::utils::detail::for_each_return_1_impl<I0, Inc0, T0, F>::eval(
    std::forward<T0>(t0),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>());
}

namespace detail {

/** @brief
    Compile-time for_each loop without return value (two parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1th parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1th parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_2_impl
{
  /** @brief
      Compile-time for_each loop evaluator without return value (two parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    T1&& t1,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>) noexcept
  {
    f(fdbb::utils::get<I0>(t0),
      fdbb::utils::get<I1>(t1),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>());
    for_each_2_impl<I0 + Inc0, Inc0, I1 + Inc1, Inc1, T0, T1, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      f,
      std::integral_constant<std::size_t, I0 + Inc0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1 + Inc1>(),
      std::integral_constant<std::size_t, Inc1>());
  }
};

/** @brief
    Compile-time for_each loop without return value (two parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1th parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1th parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F>
struct for_each_2_impl<I0, Inc0, I1, Inc1, T0, T1, F, true>
{
  /** @brief
      Compile-time for_each loop evaluator without return value (two parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    T1&& t1,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>) noexcept
  {
    f(fdbb::utils::get<I0>(t0),
      fdbb::utils::get<I1>(t1),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>());
  }
};

/** @brief
    Compile-time for_each loop with return value (two parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_2_impl
{
  /** @brief
      Compile-time for_each loop evaluator with return value (two parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    T1&& t1,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        fdbb::utils::get<I1>(t1),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>())),
      for_each_return_2_impl<I0 + Inc0, Inc0, I1 + Inc1, Inc1, T0, T1, F>::eval(
        std::forward<T0>(t0),
        std::forward<T1>(t1),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>(),
        std::integral_constant<std::size_t, I1 + Inc1>(),
        std::integral_constant<std::size_t, Inc1>())))
#endif
  {
    return std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        fdbb::utils::get<I1>(t1),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>())),
      for_each_return_2_impl<I0 + Inc0, Inc0, I1 + Inc1, Inc1, T0, T1, F>::eval(
        std::forward<T0>(t0),
        std::forward<T1>(t1),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>(),
        std::integral_constant<std::size_t, I1 + Inc1>(),
        std::integral_constant<std::size_t, Inc1>()));
  }
};

/** @brief
    Compile-time for_each loop with return value (two parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F>
struct for_each_return_2_impl<I0, Inc0, I1, Inc1, T0, T1, F, true>
{
  /** @brief
      Compile-time for_each loop evaluator with return value (two parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    T1&& t1,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::make_tuple(f(fdbb::utils::get<I0>(t0),
                                  fdbb::utils::get<I1>(t1),
                                  std::integral_constant<std::size_t, I0>(),
                                  std::integral_constant<std::size_t, I1>())))
#endif
  {
    return std::make_tuple(f(fdbb::utils::get<I0>(t0),
                             fdbb::utils::get<I1>(t1),
                             std::integral_constant<std::size_t, I0>(),
                             std::integral_constant<std::size_t, I1>()));
  }
};

} // namespace detail

/** @brief
    Compile-time for_each loop without return value (two parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    I1   The initial position of the 1st parameter
    @tparam    Inc1 The increment of the 1st parameter
    @tparam    T0   The type of the 0th parameter
    @tparam    T1   The type of the 1st parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] t1   The 1st parameter
    @param[in] f    The functor
 */
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         typename T0,
         typename T1,
         typename F>
FDBB_INLINE void
for_each(T0&& t0, T1&& t1, F f) noexcept
{
  fdbb::utils::detail::for_each_2_impl<I0, Inc0, I1, Inc1, T0, T1, F>::eval(
    std::forward<T0>(t0),
    std::forward<T1>(t1),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>(),
    std::integral_constant<std::size_t, I1>(),
    std::integral_constant<std::size_t, Inc1>());
}

/** @brief
    Compile-time for_each loop with return value (two parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    I1   The initial position of the 1st parameter
    @tparam    Inc1 The increment of the 1st parameter
    @tparam    T0   The type of the 0th parameter
    @tparam    T1   The type of the 1st parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] t1   The 1st parameter
    @param[in] f    The functor

    @return         The std::tuple with the result of the evaluation
 */
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         typename T0,
         typename T1,
         typename F>
FDBB_INLINE auto
for_each_return(T0&& t0, T1&& t1, F f) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::utils::detail::
                for_each_return_2_impl<I0, Inc0, I1, Inc1, T0, T1, F>::eval(
                  std::forward<T0>(t0),
                  std::forward<T1>(t1),
                  f,
                  std::integral_constant<std::size_t, I0>(),
                  std::integral_constant<std::size_t, Inc0>(),
                  std::integral_constant<std::size_t, I1>(),
                  std::integral_constant<std::size_t, Inc1>()))
#endif
{
  return fdbb::utils::detail::
    for_each_return_2_impl<I0, Inc0, I1, Inc1, T0, T1, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      f,
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, Inc1>());
}

namespace detail {

/** @brief
    Compile-time for_each loop without return value (three parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_3_impl
{
  /** @brief
      Compile-time for_each loop evaluator without return value (three parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>) noexcept
  {
    f(fdbb::utils::get<I0>(t0),
      fdbb::utils::get<I1>(t1),
      fdbb::utils::get<I2>(t2),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>());
    for_each_3_impl<I0 + Inc0,
                    Inc0,
                    I1 + Inc1,
                    Inc1,
                    I2 + Inc2,
                    Inc2,
                    T0,
                    T1,
                    T2,
                    F>::eval(std::forward<T0>(t0),
                             std::forward<T1>(t1),
                             std::forward<T2>(t2),
                             f,
                             std::integral_constant<std::size_t, I0 + Inc0>(),
                             std::integral_constant<std::size_t, Inc0>(),
                             std::integral_constant<std::size_t, I1 + Inc1>(),
                             std::integral_constant<std::size_t, Inc1>(),
                             std::integral_constant<std::size_t, I2 + Inc2>(),
                             std::integral_constant<std::size_t, Inc2>());
  }
};

/** @brief
    Compile-time for_each loop without return value (three parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F>
struct for_each_3_impl<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F, true>
{
  /** @brief
      Compile-time for_each loop evaluator without return value (three parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>) noexcept
  {
    f(fdbb::utils::get<I0>(t0),
      fdbb::utils::get<I1>(t1),
      fdbb::utils::get<I2>(t2),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>());
  }
};

/** @brief
    Compile-time for_each loop with return value (three parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_3_impl
{
  /** @brief
      Compile-time for_each loop evaluator with return value (three parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        fdbb::utils::get<I1>(t1),
                        fdbb::utils::get<I2>(t2),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>())),
      for_each_return_3_impl<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        T0,
        T1,
        T2,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>())))
#endif
  {
    return std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        fdbb::utils::get<I1>(t1),
                        fdbb::utils::get<I2>(t2),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>())),
      for_each_return_3_impl<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        T0,
        T1,
        T2,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>()));
  }
};

/** @brief
    Compile-time for_each loop with return value (three parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F>
struct for_each_return_3_impl<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F, true>
{
  /** @brief
      Compile-time for_each loop evaluator with return value (three parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::make_tuple(f(fdbb::utils::get<I0>(t0),
                                  fdbb::utils::get<I1>(t1),
                                  fdbb::utils::get<I2>(t2),
                                  std::integral_constant<std::size_t, I0>(),
                                  std::integral_constant<std::size_t, I1>(),
                                  std::integral_constant<std::size_t, I2>())))
#endif
  {
    return std::make_tuple(f(fdbb::utils::get<I0>(t0),
                             fdbb::utils::get<I1>(t1),
                             fdbb::utils::get<I2>(t2),
                             std::integral_constant<std::size_t, I0>(),
                             std::integral_constant<std::size_t, I1>(),
                             std::integral_constant<std::size_t, I2>()));
  }
};

} // namespace detail

/** @brief
    Compile-time for_each loop without return value (three parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    I1   The initial position of the 1st parameter
    @tparam    Inc1 The increment of the 1st parameter
    @tparam    I2   The initial position of the 2nd parameter
    @tparam    Inc2 The increment of the 2nd parameter
    @tparam    T0   The type of the 0th parameter
    @tparam    T1   The type of the 1st parameter
    @tparam    T2   The type of the 2nd parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] t1   The 1st parameter
    @param[in] t2   The 2nd parameter
    @param[in] f    The functor
 */
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename F>
FDBB_INLINE void
for_each(T0&& t0, T1&& t1, T2&& t2, F f) noexcept
{
  fdbb::utils::detail::
    for_each_3_impl<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      std::forward<T2>(t2),
      f,
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, Inc1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, Inc2>());
}

/** @brief
    Compile-time for_each loop with return value (three parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    I1   The initial position of the 1st parameter
    @tparam    Inc1 The increment of the 1st parameter
    @tparam    I2   The initial position of the 2nd parameter
    @tparam    Inc2 The increment of the 2nd parameter
    @tparam    T0   The type of the 0th parameter
    @tparam    T1   The type of the 1st parameter
    @tparam    T2   The type of the 2nd parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] t1   The 1st parameter
    @param[in] t2   The 2nd parameter
    @param[in] f    The functor

    @return         The std::tuple with the result of the evaluation
 */
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename F>
FDBB_INLINE auto
for_each_return(T0&& t0, T1&& t1, T2&& t2, F f) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::utils::detail::for_each_return_3_impl<
              I0,
              Inc0,
              I1,
              Inc1,
              I2,
              Inc2,
              T0,
              T1,
              T2,
              F>::eval(std::forward<T0>(t0),
                       std::forward<T1>(t1),
                       std::forward<T2>(t2),
                       f,
                       std::integral_constant<std::size_t, I0>(),
                       std::integral_constant<std::size_t, Inc0>(),
                       std::integral_constant<std::size_t, I1>(),
                       std::integral_constant<std::size_t, Inc1>(),
                       std::integral_constant<std::size_t, I2>(),
                       std::integral_constant<std::size_t, Inc2>()))
#endif
{
  return fdbb::utils::detail::
    for_each_return_3_impl<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      std::forward<T2>(t2),
      f,
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, Inc1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, Inc2>());
}

namespace detail {

/** @brief
    Compile-time for_each loop without return value (four parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam Inc3 The increment of the 3rd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam T3   The type of the 3rd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_4_impl
{
  /** @brief
      Compile-time for_each loop evaluator without return value (four parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] t3   The 3rd parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    T3&& t3,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>,
    std::integral_constant<std::size_t, I3>,
    std::integral_constant<std::size_t, Inc3>) noexcept
  {
    f(fdbb::utils::get<I0>(t0),
      fdbb::utils::get<I1>(t1),
      fdbb::utils::get<I2>(t2),
      fdbb::utils::get<I3>(t3),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, I3>());
    for_each_4_impl<I0 + Inc0,
                    Inc0,
                    I1 + Inc1,
                    Inc1,
                    I2 + Inc2,
                    Inc2,
                    I3 + Inc3,
                    Inc3,
                    T0,
                    T1,
                    T2,
                    T3,
                    F>::eval(std::forward<T0>(t0),
                             std::forward<T1>(t1),
                             std::forward<T2>(t2),
                             std::forward<T3>(t3),
                             f,
                             std::integral_constant<std::size_t, I0 + Inc0>(),
                             std::integral_constant<std::size_t, Inc0>(),
                             std::integral_constant<std::size_t, I1 + Inc1>(),
                             std::integral_constant<std::size_t, Inc1>(),
                             std::integral_constant<std::size_t, I2 + Inc2>(),
                             std::integral_constant<std::size_t, Inc2>(),
                             std::integral_constant<std::size_t, I3 + Inc3>(),
                             std::integral_constant<std::size_t, Inc3>());
  }
};

/** @brief
    Compile-time for_each loop without return value (four parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam Inc3 The increment of the 3rd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam T3   The type of the 3rd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
struct for_each_4_impl<I0,
                       Inc0,
                       I1,
                       Inc1,
                       I2,
                       Inc2,
                       I3,
                       Inc3,
                       T0,
                       T1,
                       T2,
                       T3,
                       F,
                       true>
{
  /** @brief
      Compile-time for_each loop evaluator without return value (four parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] t3   The 3rd parameter
      @param[in] f    The functor
   */
  static FDBB_INLINE void eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    T3&& t3,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>,
    std::integral_constant<std::size_t, I3>,
    std::integral_constant<std::size_t, Inc3>) noexcept
  {
    f(fdbb::utils::get<I0>(t0),
      fdbb::utils::get<I1>(t1),
      fdbb::utils::get<I2>(t2),
      fdbb::utils::get<I3>(t3),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, I3>());
  }
};

/** @brief
    Compile-time for_each loop with return value (four parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam Inc3 The increment of the 3rd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam T3   The type of the 3rd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_4_impl
{
  /** @brief
      Compile-time for_each loop evaluator with return value (four parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] t3   The 3rd parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    T3&& t3,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>,
    std::integral_constant<std::size_t, I3>,
    std::integral_constant<std::size_t, Inc3>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        fdbb::utils::get<I1>(t1),
                        fdbb::utils::get<I2>(t2),
                        fdbb::utils::get<I3>(t3),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>(),
                        std::integral_constant<std::size_t, I3>())),
      for_each_return_4_impl<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        I3 + Inc3,
        Inc3,
        T0,
        T1,
        T2,
        T3,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 std::forward<T3>(t3),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>(),
                 std::integral_constant<std::size_t, I3 + Inc3>(),
                 std::integral_constant<std::size_t, Inc3>())))
#endif
  {
    return std::tuple_cat(
      std::make_tuple(f(fdbb::utils::get<I0>(t0),
                        fdbb::utils::get<I1>(t1),
                        fdbb::utils::get<I2>(t2),
                        fdbb::utils::get<I3>(t3),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>(),
                        std::integral_constant<std::size_t, I3>())),
      for_each_return_4_impl<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        I3 + Inc3,
        Inc3,
        T0,
        T1,
        T2,
        T3,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 std::forward<T3>(t3),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>(),
                 std::integral_constant<std::size_t, I3 + Inc3>(),
                 std::integral_constant<std::size_t, Inc3>()));
  }
};

/** @brief
    Compile-time for_each loop with return value (four parameter version)

    @tparam Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam Inc1 The increment of the 1st parameter
    @tparam Inc2 The increment of the 2nd parameter
    @tparam Inc3 The increment of the 3rd parameter
    @tparam T0   The type of the 0th parameter
    @tparam T1   The type of the 1st parameter
    @tparam T2   The type of the 2nd parameter
    @tparam T3   The type of the 3rd parameter
    @tparam F    The functor to be applied
 */
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
struct for_each_return_4_impl<I0,
                              Inc0,
                              I1,
                              Inc1,
                              I2,
                              Inc2,
                              I3,
                              Inc3,
                              T0,
                              T1,
                              T2,
                              T3,
                              F,
                              true>
{
  /** @brief
      Compile-time for_each loop evaluator with return value (four parameter
      version)

      @param[in] t0   The 0th parameter
      @param[in] t1   The 1st parameter
      @param[in] t2   The 2nd parameter
      @param[in] t3   The 3rd parameter
      @param[in] f    The functor

      @return         The std::tuple with the result of the evaluation
   */
  static FDBB_INLINE auto constexpr eval(
    T0&& t0,
    T1&& t1,
    T2&& t2,
    T3&& t3,
    F f,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, Inc0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, Inc1>,
    std::integral_constant<std::size_t, I2>,
    std::integral_constant<std::size_t, Inc2>,
    std::integral_constant<std::size_t, I3>,
    std::integral_constant<std::size_t, Inc3>) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::make_tuple(f(fdbb::utils::get<I0>(t0),
                                  fdbb::utils::get<I1>(t1),
                                  fdbb::utils::get<I2>(t2),
                                  fdbb::utils::get<I3>(t3),
                                  std::integral_constant<std::size_t, I0>(),
                                  std::integral_constant<std::size_t, I1>(),
                                  std::integral_constant<std::size_t, I2>(),
                                  std::integral_constant<std::size_t, I3>())))
#endif
  {
    return std::make_tuple(f(fdbb::utils::get<I0>(t0),
                             fdbb::utils::get<I1>(t1),
                             fdbb::utils::get<I2>(t2),
                             fdbb::utils::get<I3>(t3),
                             std::integral_constant<std::size_t, I0>(),
                             std::integral_constant<std::size_t, I1>(),
                             std::integral_constant<std::size_t, I2>(),
                             std::integral_constant<std::size_t, I3>()));
  }
};

} // namespace detail

/** @brief
    Compile-time for_each loop without return value (four parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    I1   The initial position of the 1st parameter
    @tparam    Inc1 The increment of the 1st parameter
    @tparam    I2   The initial position of the 2nd parameter
    @tparam    Inc2 The increment of the 2nd parameter
    @tparam    I3   The initial position of the 3rd parameter
    @tparam    Inc3 The increment of the 3rd parameter
    @tparam    T0   The type of the 0th parameter
    @tparam    T1   The type of the 1st parameter
    @tparam    T2   The type of the 2nd parameter
    @tparam    T3   The type of the 3rd parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] t1   The 1st parameter
    @param[in] t2   The 2nd parameter
    @param[in] t3   The 3rd parameter
    @param[in] f    The functor
 */
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         std::size_t I3 = I0,
         std::size_t Inc3 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
FDBB_INLINE void
for_each(T0&& t0, T1&& t1, T2&& t2, T3&& t3, F f) noexcept
{
  fdbb::utils::detail::
    for_each_4_impl<I0, Inc0, I1, Inc1, I2, Inc2, I3, Inc3, T0, T1, T2, T3, F>::
      eval(std::forward<T0>(t0),
           std::forward<T1>(t1),
           std::forward<T2>(t2),
           std::forward<T3>(t3),
           f,
           std::integral_constant<std::size_t, I0>(),
           std::integral_constant<std::size_t, Inc0>(),
           std::integral_constant<std::size_t, I1>(),
           std::integral_constant<std::size_t, Inc1>(),
           std::integral_constant<std::size_t, I2>(),
           std::integral_constant<std::size_t, Inc2>(),
           std::integral_constant<std::size_t, I3>(),
           std::integral_constant<std::size_t, Inc3>());
}

/** @brief
    Compile-time for_each loop with return value (four parameter version)

    @tparam    I0   The initial position of the 0th parameter
    @tparam    Inc0 The increment of the 0th parameter (must be nonzero)
    @tparam    I1   The initial position of the 1st parameter
    @tparam    Inc1 The increment of the 1st parameter
    @tparam    I2   The initial position of the 2nd parameter
    @tparam    Inc2 The increment of the 2nd parameter
    @tparam    I3   The initial position of the 3rd parameter
    @tparam    Inc3 The increment of the 3rd parameter
    @tparam    T0   The type of the 0th parameter
    @tparam    T1   The type of the 1st parameter
    @tparam    T2   The type of the 2nd parameter
    @tparam    T3   The type of the 3rd parameter
    @tparam    F    The functor to be applied

    @param[in] t0   The 0th parameter
    @param[in] t1   The 1st parameter
    @param[in] t2   The 2nd parameter
    @param[in] t3   The 3rd parameter
    @param[in] f    The functor

    @return         The std::tuple with the result of the evaluation
 */
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         std::size_t I3 = I0,
         std::size_t Inc3 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
FDBB_INLINE auto
for_each_return(T0&& t0, T1&& t1, T2&& t2, T3&& t3, F f) noexcept
#if !defined(DOXYGEN)
  -> decltype(fdbb::utils::detail::for_each_return_4_impl<
              I0,
              Inc0,
              I1,
              Inc1,
              I2,
              Inc2,
              I3,
              Inc3,
              T0,
              T1,
              T2,
              T3,
              F>::eval(std::forward<T0>(t0),
                       std::forward<T1>(t1),
                       std::forward<T2>(t2),
                       std::forward<T3>(t3),
                       f,
                       std::integral_constant<std::size_t, I0>(),
                       std::integral_constant<std::size_t, Inc0>(),
                       std::integral_constant<std::size_t, I1>(),
                       std::integral_constant<std::size_t, Inc1>(),
                       std::integral_constant<std::size_t, I2>(),
                       std::integral_constant<std::size_t, Inc2>(),
                       std::integral_constant<std::size_t, I3>(),
                       std::integral_constant<std::size_t, Inc3>()))
#endif
{
  return fdbb::utils::detail::for_each_return_4_impl<
    I0,
    Inc0,
    I1,
    Inc1,
    I2,
    Inc2,
    I3,
    Inc3,
    T0,
    T1,
    T2,
    T3,
    F>::eval(std::forward<T0>(t0),
             std::forward<T1>(t1),
             std::forward<T2>(t2),
             std::forward<T3>(t3),
             f,
             std::integral_constant<std::size_t, I0>(),
             std::integral_constant<std::size_t, Inc0>(),
             std::integral_constant<std::size_t, I1>(),
             std::integral_constant<std::size_t, Inc1>(),
             std::integral_constant<std::size_t, I2>(),
             std::integral_constant<std::size_t, Inc2>(),
             std::integral_constant<std::size_t, I3>(),
             std::integral_constant<std::size_t, Inc3>());
}

/** @brief
    Prints content of the std::tuple as std::ostream
 */
template<size_t N>
struct print_tuple
{
  template<typename... T>
  static typename std::enable_if<(N < sizeof...(T))>::type print(
    std::ostream& os,
    const std::tuple<T...>& t)
  {
    char quote = (std::is_convertible<decltype(fdbb::utils::get<N>(t)),
                                      std::string>::value)
                   ? '"'
                   : 0;
    os << ", " << quote << fdbb::utils::get<N>(t) << quote;
    print_tuple<N + 1>::print(os, t);
  }

  template<typename... T>
  static typename std::enable_if<!(N < sizeof...(T))>::type print(
    std::ostream&,
    const std::tuple<T...>&)
  {}
};

} // namespace utils

/// @brief Prints content of the std::tuple as std::ostream
std::ostream&
operator<<(std::ostream& os, const std::tuple<>&)
{
  return os << "()";
}

/// @brief Prints content of the std::tuple as std::ostream
template<typename T0, typename... T>
std::ostream&
operator<<(std::ostream& os, const std::tuple<T0, T...>& t)
{
  char quote = (std::is_convertible<T0, std::string>::value) ? '"' : 0;
  os << '(' << quote << std::get<0>(t) << quote;
  fdbb::utils::print_tuple<1>::print(os, t);
  return os << ')';
}

namespace utils {

// C++14 backport: std::integer_sequence,
//                 std::index_sequence,
//                 std::make_integer_sequence,
//                 std::make_index_sequence,
//                 std::index_sequence_for,
//                 std::tie
#if __cplusplus < 201402L

/// @brief Class template integer_sequence (Backport from C++14)
template<typename _Tp, _Tp... _Idx>
struct integer_sequence
{
  using value_type = _Tp;
  static constexpr std::size_t size() { return sizeof...(_Idx); }
};

#if !defined(DOXYGEN)

// Stores a tuple of indices. Used by tuple and pair, and by bind() to
// extract the elements in a tuple.
template<std::size_t... _Indexes>
struct _Index_tuple
{};

// Concatenates two _Index_tuples.
template<typename _Itup1, typename _Itup2>
struct _Itup_cat;

template<std::size_t... _Ind1, std::size_t... _Ind2>
struct _Itup_cat<_Index_tuple<_Ind1...>, _Index_tuple<_Ind2...>>
{
  using __type = _Index_tuple<_Ind1..., (_Ind2 + sizeof...(_Ind1))...>;
};

// Builds an _Index_tuple<0, 1, 2, ..., _Num-1>.
template<std::size_t _Num>
struct _Build_index_tuple
  : _Itup_cat<typename _Build_index_tuple<_Num / 2>::__type,
              typename _Build_index_tuple<_Num - _Num / 2>::__type>
{};

template<>
struct _Build_index_tuple<1>
{
  using __type = _Index_tuple<0>;
};

template<>
struct _Build_index_tuple<0>
{
  using __type = _Index_tuple<>;
};

// Class template _Make_integer_sequence
template<typename _Tp,
         _Tp _Num,
         typename _ISeq = typename _Build_index_tuple<_Num>::__type>
struct _Make_integer_sequence;

// Class template _Make_integer_sequence
template<typename _Tp, _Tp _Num, std::size_t... _Idx>
struct _Make_integer_sequence<_Tp, _Num, _Index_tuple<_Idx...>>
{
  static_assert(_Num >= 0, "Cannot make integer sequence of negative length");

  using __type = fdbb::utils::integer_sequence<_Tp, static_cast<_Tp>(_Idx)...>;
};

#endif

/// @brief Alias template fdbb::utils::make_integer_sequence (Backport from
/// C++14)
template<typename _Tp, _Tp _Num>
using make_integer_sequence =
  typename fdbb::utils::_Make_integer_sequence<_Tp, _Num>::__type;

/// @brief Alias template fdbb::utils::index_sequence (Backport from C++14)
template<std::size_t... _Idx>
using index_sequence = fdbb::utils::integer_sequence<std::size_t, _Idx...>;

/// @brief Alias template fdbb::utils::make_index_sequence (Backport from C++14)
template<std::size_t _Num>
using make_index_sequence =
  fdbb::utils::make_integer_sequence<std::size_t, _Num>;

/// @brief Alias template fdbb::utils::index_sequence_for (Backport from C++14)
template<typename... _Types>
using index_sequence_for = fdbb::utils::make_index_sequence<sizeof...(_Types)>;

/// @brief Alias template fdbb::utils::tie (Backport from C++14)
template<typename... _Elements>
constexpr std::tuple<_Elements&...>
tie(_Elements&... __args) noexcept
{
  return std::tuple<_Elements&...>(__args...);
}

#else // C++14

/// @brief Alias template fdbb::utils::make_integer_sequence from
/// std::make_integer_sequence (C++14)
using std::integer_sequence;

/// @brief Alias temmplate fdbb::utils::index_sequence from
/// std::index_squence (C++14)
using std::index_sequence;

/// @brief Alias template fdbb::utils::make_integer_sequence from
/// std::make_integer_sequence (C++14)
using std::make_integer_sequence;

/// @brief Alias template fdbb::utils::index_sequence_for from
/// std::index_sequence_for (C++14)
using std::index_sequence_for;

/// @brief Alias template fdbb::utils::make_index_sequence from
/// std::make_index_sequence (C++14)
using std::make_index_sequence;

/// @brief Alias template fdbb::utils::tie from std::tie (C++14)
using std::tie;

#endif // C++14

/** @brief
    Returns content of integer_sequence as std::tuple
 */
template<typename _Tp, _Tp... _Idx>
auto constexpr make_tuple(integer_sequence<_Tp, _Idx...>) noexcept
#if !defined(DOXYGEN)
  -> decltype(std::make_tuple(_Idx...))
#endif
{
  return std::make_tuple(_Idx...);
}

namespace detail {

/// @brief Returns the sum of entries in the tuple
template<int I, typename T, typename Enable = void>
struct tuple_sum_impl;

// There is just the last term
template<int I, typename T>
struct tuple_sum_impl<
  I,
  T,
  typename std::enable_if<(
    I + 1 == std::tuple_size<typename std::decay<T>::type>::value)>::type>
{
  typedef typename std::decay<T>::type Tuple;

  typedef typename std::decay<
    decltype(std::declval<typename std::tuple_element<I, Tuple>::type>())>::type
    return_type;

  static const return_type& get(const T& t) { return fdbb::utils::get<I>(t); }
};

// There are more than one terms to go
template<int I, typename T>
struct tuple_sum_impl<
  I,
  T,
  typename std::enable_if<(
    I + 1 < std::tuple_size<typename std::decay<T>::type>::value)>::type>
{
  typedef typename std::decay<T>::type Tuple;

  typedef decltype(std::declval<typename std::tuple_element<I, Tuple>::type>() +
                   std::declval<
                     typename tuple_sum_impl<I + 1, T>::return_type>())
    return_type;

  static const return_type get(const T& t)
  {
    return fdbb::utils::get<I>(t) + tuple_sum_impl<I + 1, T>::get(t);
  }
};

} // namespace detail

/** @brief
    Returns the sum of entries in type T
 */
template<typename T>
FDBB_INLINE auto constexpr tuple_sum(const T& t)
#if !defined(DOXYGEN)
  -> typename fdbb::utils::detail::tuple_sum_impl<0, T>::return_type
#endif
{
  return fdbb::utils::detail::tuple_sum_impl<0, T>::get(t);
}

namespace detail {

/// @brief Returns the product of entries in the tuple
template<int I, typename T, typename Enable = void>
struct tuple_prod_impl;

// There is just the last term
template<int I, typename T>
struct tuple_prod_impl<
  I,
  T,
  typename std::enable_if<(
    I + 1 == std::tuple_size<typename std::decay<T>::type>::value)>::type>
{
  typedef typename std::decay<T>::type Tuple;

  typedef typename std::decay<
    decltype(std::declval<typename std::tuple_element<I, Tuple>::type>())>::type
    return_type;

  static const return_type& get(const T& t) { return fdbb::utils::get<I>(t); }
};

// There are more than one terms to go
template<int I, typename T>
struct tuple_prod_impl<
  I,
  T,
  typename std::enable_if<(
    I + 1 < std::tuple_size<typename std::decay<T>::type>::value)>::type>
{
  typedef typename std::decay<T>::type Tuple;

  typedef decltype(std::declval<typename std::tuple_element<I, Tuple>::type>() +
                   std::declval<
                     typename tuple_prod_impl<I + 1, T>::return_type>())
    return_type;

  static const return_type get(const T& t)
  {
    return fdbb::utils::get<I>(t) * tuple_prod_impl<I + 1, T>::get(t);
  }
};

} // namespace detail

/** @brief
    Returns the product of entries in type T
 */
template<typename T>
FDBB_INLINE auto constexpr tuple_prod(const T& t)
#if !defined(DOXYGEN)
  -> typename fdbb::utils::detail::tuple_prod_impl<0, T>::return_type
#endif
{
  return fdbb::utils::detail::tuple_prod_impl<0, T>::get(t);
}

// C++17 backport: std::apply
#if __cplusplus < 201703L

namespace detail {

// Backport of std::apply() from C++17
template<typename F, typename Tuple, std::size_t... I>
FDBB_INLINE constexpr auto
apply_impl(F&& f, Tuple&& t, index_sequence<I...>)
#if !defined(DOXYGEN)
  -> decltype(f(fdbb::utils::get<I>(std::forward<Tuple>(t))...))
#endif
{
  return f(fdbb::utils::get<I>(std::forward<Tuple>(t))...);
}

} // namespace detail

/** @brief
    Applies the callable object f to all arguments of the tuple (C++17 backport)
 */
template<typename F, typename Tuple>
FDBB_INLINE constexpr auto
apply(F&& f, Tuple&& t)
#if !defined(DOXYGEN)
  -> decltype(fdbb::utils::detail::apply_impl(
    std::forward<F>(f),
    std::forward<Tuple>(t),
    make_index_sequence<
      std::tuple_size<typename std::decay<Tuple>::type>::value>{}))
#endif
{
  return fdbb::utils::detail::apply_impl(
    std::forward<F>(f),
    std::forward<Tuple>(t),
    make_index_sequence<
      std::tuple_size<typename std::decay<Tuple>::type>::value>{});
}

#else // C++17

/// @brief Alias template fdbb::utils::apply() to std::apply() (C++17)
template<typename F, typename Tuple>
using apply = std::apply<T, Tuple>;

#endif // C++17

/**
 * @brief
 * Creates array-like std::tuple of `N` entries of type `T`
 *
 * @tparam   T   The type of all entries of the std::tuple
 * @tparam   N   The number of entries of the std::tuple
 */
template<class T, std::size_t N>
struct tuple_t
{
private:
  template<size_t, class T_>
  using T__ = T_;

  template<class T_, size_t... Is>
  static auto gen(fdbb::utils::index_sequence<Is...>)
#if __cplusplus <= 201103L
    -> decltype(std::tuple<T__<Is, T_>...>{})
#endif
  {
    return std::tuple<T__<Is, T_>...>{};
  }

  template<class T_, size_t N_>
  static auto gen()
#if __cplusplus <= 201103L
    -> decltype(gen<T_>(fdbb::utils::make_index_sequence<N_>{}))
#endif
  {
    return gen<T_>(fdbb::utils::make_index_sequence<N_>{});
  }

public:
  using type = decltype(gen<T, N>());
};

} // namespace utils

} // namespace fdbb

#endif // FDBB_CORE_UTILS_HPP
