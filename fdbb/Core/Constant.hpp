/** @file fdbb/Core/Constant.hpp
 *
 *  @brief Constants
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CORE_CONSTANT_HPP
#define FDBB_CORE_CONSTANT_HPP

#include <iomanip>
#include <iostream>
#include <sstream>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Utils.hpp>

namespace fdbb {
namespace utils {
namespace detail {

/** @brief
    Converts strings into types

    @note
    This is a simplified version of the 'typestring' header file
    developed by George Makrydakis <george@irrequietus.eu>.

    The header of the original 'typestring' header file reads as follows:

    @copyright Copyright (C) 2015, 2016 George Makrydakis
    <george@irrequietus.eu>

    The 'typestring' header is a single header C++ library for creating types
    to use as type parameters in template instantiations, repository available
    at https://github.com/irrequietus/typestring. Conceptually stemming from
    own implementation of the same thing (but in a more complicated manner to
    be revised) in 'clause': https://github.com/irrequietus/clause.

    File subject to the terms and conditions of the Mozilla Public
    License v 2.0. If a copy of the MPLv2 license text was not
    distributed with this file, you can obtain it at:
    http://mozilla.org/MPL/2.0/.
 */
template<int N, int M>
constexpr char
tygrab(char const (&c)[M]) noexcept
{
  return c[N < M ? N : M - 1];
}

} // namespace detail
} // namespace utils

#define TYPESTRING16(n, x)                                                     \
  fdbb::utils::detail::tygrab<0x##n##0>(x),                                    \
    fdbb::utils::detail::tygrab<0x##n##1>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##2>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##3>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##4>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##5>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##6>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##7>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##8>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##9>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##A>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##B>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##C>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##D>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##E>(x),                                  \
    fdbb::utils::detail::tygrab<0x##n##F>(x)

/** @brief
    Creates an expression-complient constant of value Value.
 */
#define CONSTANT(Value, Expr)                                                  \
  (fdbb::make_constant<                                                        \
    fdbb::Constant<decltype(Expr), TYPESTRING16(, #Value)>>(Value, Expr))

/** @brief
    Compile-time constant that decodes the given value as template parameter
 */
template<typename Expr, char... C>
struct Constant final
{
  // Scalar value type of the expression
  using value_type = typename fdbb::backend::value_type<Expr>::type;

  // Tag value (bdj2 hash of the constant value)
  static const std::size_t tag = fdbb::utils::hash<std::size_t>(C...);

  // Return constant value as string
  static std::string get()
  {
    std::string value;
    int unpack[]{ 0, (value += C, 0)... };
    static_cast<void>(unpack);

    std::ostringstream s;
    s << "( " << std::scientific << std::setprecision(16) << std::stold(value)
      << (std::is_same<value_type, float>::value         ? 'f'
          : std::is_same<value_type, long double>::value ? 'l'
                                                         : ' ')
      << " )";
    return s.str();
  }
};

} // namespace fdbb

#endif // FDBB_CORE_CONSTANT_HPP
