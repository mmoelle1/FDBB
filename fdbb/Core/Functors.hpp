/** @file fdbb/Core/Functors.hpp
 *
 *  @brief Functors
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CORE_FUNCTORS_HPP
#define FDBB_CORE_FUNCTORS_HPP

#include <array>
#include <iostream>
#include <tuple>
#include <type_traits>

#include <Backend/ForwardDeclarations.hpp>
#include <Core/Config.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

/** @namespace fdbb::functor
 *
 *  @brief
 *  The \ref fdbb::functor namespace, containing functors of the FDBB
 *  library
 *
 *  The \ref fdbb::functor namespace contains functors that are used
 *  by the FDBB library.
 */
namespace functor {

/// @brief Functor returns content of the paramter pack as a std::array
struct make_array
{
  template<class T, class... Ts>
  std::array<T, 1 + sizeof...(Ts)> operator()(const T& arg,
                                              const Ts&... args) const noexcept
  {
    return { arg, args... };
  }
};

/// @brief Functor returns content of the paramter pack as a std::tuple
struct make_tuple
{
  template<class T, class... Ts>
  std::tuple<T, Ts...> operator()(const T& arg,
                                  const Ts&... args) const noexcept
  {
    return fdbb::utils::tie(arg, args...);
  }
};

/// @brief Functor dereferences the object
struct deref
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(
    const T0* t0,
    std::integral_constant<std::size_t, I0>) const noexcept -> decltype(*t0)
  {
    return *t0;
  }
};

/// @brief Functor returns a pointer to the object
struct ref
{
  template<typename T0, std::size_t I0>
  constexpr T0* operator()(T0& t0, std::integral_constant<std::size_t, I0>)
    const noexcept
  {
    return (&t0);
  }

  template<typename T0, std::size_t I0>
  constexpr T0* operator()(T0* t0, std::integral_constant<std::size_t, I0>)
    const noexcept
  {
    return (t0);
  }
};

/// @brief Functor returns the object
struct get
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(T0& t0, std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(t0)
  {
    return t0;
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(*t0)
  {
    return *t0;
  }
};

/// @brief Functor returns the size of the object
struct size
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0& t0,
                            std::integral_constant<std::size_t, I0>)
    const noexcept -> typename std::enable_if<
      std::is_member_function_pointer<decltype(&T0::size)>::value,
      decltype(t0.size())>::type
  {
    return t0.size();
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0& t0,
                            std::integral_constant<std::size_t, I0>)
    const noexcept -> typename std::enable_if<
      std::is_member_function_pointer<decltype(&T0::elements)>::value,
      decltype(t0.elements())>::type
  {
    return t0.elements();
  }

#if defined(FDBB_BACKEND_MTL4)
  template<typename T0, std::size_t I0>
  constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(mtl::size(*t0))
  {
    return mtl::size(*t0);
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0& t0,
                            std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(mtl::size(t0))
  {
    return mtl::size(t0);
  }
#endif

  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0& t0,
                            std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(t0->size())
  {
    return t0->size();
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0& t0,
                            std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(t0->elements())
  {
    return t0->elements();
  }
};

/// @brief Functor assigns another object to the object
struct assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<!std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = t0;
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = *t0;
  }
};

/// @brief Functor move-assigns another object to the object
struct move_assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<!std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = std::move(t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = std::move(*t0);
  }
};

/// @brief Functor copy-constructs the object from another object
struct copy_construct
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<!std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = T1(t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = *t0;
  }
};

/// @brief Functor tags the terminals of the object one by one
template<std::size_t Tag, std::size_t TagIncr>
struct tag
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(fdbb::tag<Tag + I0 * TagIncr>(*t0))
  {
    return fdbb::tag<Tag + I0 * TagIncr>(*t0);
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0& t0,
                            std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(fdbb::tag<Tag + I0 * TagIncr>(t0))
  {
    return fdbb::tag<Tag + I0 * TagIncr>(t0);
  }
};

/// @brief Functor adds two objects
struct add
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) + (*t1))
  {
    return (*t0) + (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) + (*t1))
  {
    return (t0) + (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) + (t1))
  {
    return (*t0) + (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) + (t1))
  {
    return (t0) + (t1);
  }
};

/// @brief Functor adds two objects and assings the result to the first one
struct add_assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) += (*t1))
  {
    return (*t0) += (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) += (*t1))
  {
    return (t0) += (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) += (t1))
  {
    return (*t0) += (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) += (t1))
  {
    return (t0) += (t1);
  }
};

/// @brief Functor subtracts one object from the other
struct sub
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) - (*t1))
  {
    return (*t0) - (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) - (*t1))
  {
    return (t0) - (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) - (t1))
  {
    return (*t0) - (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) - (t1))
  {
    return (t0) - (t1);
  }
};

/// @brief Functor subtracts one object from the other and assings the result to
/// the first one
struct sub_assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) -= (*t1))
  {
    return (*t0) -= (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) -= (*t1))
  {
    return (t0) -= (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) -= (t1))
  {
    return (*t0) -= (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) -= (t1))
  {
    return (t0) -= (t1);
  }
};

/// @brief Functor subtracts one object from the other and multiplies
/// the result by minus one
///
/// In contrast to the functor fdbb::functor::sub this functor
/// computes the difference between a and b as -(b-a). This is helpful
/// when the functor is used inside fdbb::utils::for_each_return
/// method, which always uses the dimension of the first(!) argument
/// as loop counter. To compute the difference c-T between a constant
/// c and a type T that is derived from fdbb::BlockBase one has to
/// compute -(T-c), thereby using this functor.
struct minus_sub
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t1) - (*t0))
  {
    return (*t1) - (*t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t1) - (t0))
  {
    return (*t1) - (t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t1) - (*t0))
  {
    return (t1) - (*t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t1) - (t0))
  {
    return (t1) - (t0);
  }
};

/// @brief Functor multiplies two objects element-wise
struct elem_mul
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(
    T0* t0,
    T1* t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
#if __cplusplus <= 201103L
    -> decltype(fdbb::elem_mul((*t0), (*t1)))
#endif
  {
    return fdbb::elem_mul((*t0), (*t1));
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_mul(
      std::declval<const typename fdbb::utils::remove_all<T0>::type>(),
      (*t1)))
  {
    return fdbb::elem_mul((t0), (*t1));
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_mul(
      (*t0),
      std::declval<const typename fdbb::utils::remove_all<T1>::type>()))
  {
    return fdbb::elem_mul((*t0), (t1));
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_mul(
      std::declval<const typename fdbb::utils::remove_all<T0>::type>(),
      std::declval<const typename fdbb::utils::remove_all<T1>::type>()))
  {
    return fdbb::elem_mul((t0), (t1));
  }
};

/// @brief Functor divides one object by the other element-wise
struct elem_div
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_div((*t0), (*t1)))
  {
    return fdbb::elem_div((*t0), (*t1));
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_div(
      std::declval<const typename fdbb::utils::remove_all<T0>::type>(),
      (*t1)))
  {
    return fdbb::elem_div((t0), (*t1));
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_div(
      (*t0),
      std::declval<const typename fdbb::utils::remove_all<T1>::type>()))
  {
    return fdbb::elem_div((*t0), (t1));
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_div(
      std::declval<const typename fdbb::utils::remove_all<T0>::type>(),
      std::declval<const typename fdbb::utils::remove_all<T1>::type>()))
  {
    return fdbb::elem_div((t0), (t1));
  }
};

/// @brief Functor returns the power of a to b of two objects
struct elem_pow
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype(fdbb::elem_pow((t0), (t1)))
  {
    return fdbb::elem_pow((t0), (t1));
  }
};

/// @brief Functor multiplies one object by plus one
struct plus
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(*t0)
  {
    return *t0;
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(
    const T0& t0,
    std::integral_constant<std::size_t, I0>) const noexcept -> decltype(t0)
  {
    return t0;
  }
};

/// @brief Functor multiplies one object by minus one
struct minus
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>)
    const noexcept -> decltype(-(*t0))
  {
    return -(*t0);
  }

  template<typename T0, std::size_t I0>
  constexpr auto operator()(
    const T0& t0,
    std::integral_constant<std::size_t, I0>) const noexcept -> decltype(-(t0))
  {
    return -(t0);
  }
};

/// @brief Functor multiplies two objects
struct mul
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) * (*t1))
  {
    return (*t0) * (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) * (*t1))
  {
    return (t0) * (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) * (t1))
  {
    return (*t0) * (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) * (t1))
  {
    return (t0) * (t1);
  }
};

/// @brief Functor multiplies two objects and assigns the result to the first
/// one
struct mul_assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) *= (*t1))
  {
    return (*t0) *= (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) *= (*t1))
  {
    return (t0) *= (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) *= (t1))
  {
    return (*t0) *= (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) *= (t1))
  {
    return (t0) *= (t1);
  }
};

/// @brief Functor divides one objects by the other
struct div
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) / (*t1))
  {
    return (*t0) / (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) / (*t1))
  {
    return (t0) / (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) / (t1))
  {
    return (*t0) / (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) / (t1))
  {
    return (t0) / (t1);
  }
};

/// @brief Functor divides one objects by the other and assigns the result to
/// the first one
struct div_assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) /= (*t1))
  {
    return (*t0) /= (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            T1* t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) /= (*t1))
  {
    return (t0) /= (*t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0* t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((*t0) /= (t1))
  {
    return (*t0) /= (t1);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>)
    const noexcept -> decltype((t0) /= (t1))
  {
    return (t0) /= (t1);
  }
};

#if __cplusplus <= 201103L
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /**                                                                          \
     @brief Functor returns the element-wise \a OPNAME(a) function             \
     of the object \a a                                                        \
  */                                                                           \
  struct elem_##OPNAME                                                         \
  {                                                                            \
    template<typename T0, std::size_t I0>                                      \
    constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>) \
      const noexcept -> decltype(fdbb::elem_##OPNAME(*t0))                     \
    {                                                                          \
      return fdbb::elem_##OPNAME(*t0);                                         \
    }                                                                          \
                                                                               \
    template<typename T0, std::size_t I0>                                      \
    constexpr auto operator()(const T0& t0,                                    \
                              std::integral_constant<std::size_t, I0>)         \
      const noexcept -> decltype(fdbb::elem_##OPNAME(                          \
        std::declval<const typename fdbb::utils::remove_all<T0>::type>()))     \
    {                                                                          \
      return fdbb::elem_##OPNAME(t0);                                          \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /**                                                                          \
     @brief Functor returns the element-wise \a OPNAME(a) function             \
     of the object \a a                                                        \
  */                                                                           \
  struct elem_##OPNAME                                                         \
  {                                                                            \
    template<typename T0, std::size_t I0>                                      \
    constexpr auto operator()(T0* t0, std::integral_constant<std::size_t, I0>) \
      const noexcept                                                           \
    {                                                                          \
      return fdbb::elem_##OPNAME(*t0);                                         \
    }                                                                          \
                                                                               \
    template<typename T0, std::size_t I0>                                      \
    constexpr auto operator()(                                                 \
      const T0& t0,                                                            \
      std::integral_constant<std::size_t, I0>) const noexcept                  \
    {                                                                          \
      return fdbb::elem_##OPNAME(t0);                                          \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/// @brief Functor multiplies two objects (inner summation loop)
struct matmul_inner
{
  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            T2* t2,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>)
    const noexcept -> decltype((*t1) * (*t2))
  {
    return (*t1) * (*t2);
  }

  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(
    const T0& t0,
    const T1& t1,
    T2* t2,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, I2>) const noexcept
    -> decltype(std::declval<
                  const typename fdbb::utils::remove_all<T1>::type>() *
                (*t2))
  {
    return (t1) * (*t2);
  }

  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(
    const T0& t0,
    T1* t1,
    const T2& t2,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, I2>) const noexcept
    -> decltype((*t1) * std::declval<
                          const typename fdbb::utils::remove_all<T2>::type>())
  {
    return (*t1) * (t2);
  }

  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(
    const T0& t0,
    const T1& t1,
    const T2& t2,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, I2>) const noexcept
    -> decltype(std::declval<
                  const typename fdbb::utils::remove_all<T1>::type>() *
                std::declval<
                  const typename fdbb::utils::remove_all<T2>::type>())
  {
    return (t1) * (t2);
  }
};

/// @brief Functor multiplies two objects (outer loop)
struct matmul
{
  template<typename T0,
           typename T1,
           typename T2,
           typename T3,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2,
           std::size_t I3>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            const T2& t2,
                            const T3& t3,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>,
                            std::integral_constant<std::size_t, I3>)
    const noexcept -> decltype(fdbb::utils::tuple_sum(
      fdbb::utils::for_each_return<
        0,
        1,
        (I0 /
         (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1))) *
          (std::tuple_size<T2>::value / I1),
        1,
        I0 - (I0 / (std::tuple_size<T3>::value /
                    (std::tuple_size<T2>::value / I1))) *
               (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)),
        std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)>(
        fdbb::utils::make_tuple(
          fdbb::utils::make_index_sequence<std::tuple_size<T2>::value / I1>{}),
        t2,
        t3,
        matmul_inner())))
  {
    // Remark: indices are calculated as follows
    // constexpr auto row0 = I1;
    // constexpr auto col0 = std::tuple_size<T2>::value/I1;
    //
    // constexpr auto row1 = col0;
    // constexpr auto col1 = std::tuple_size<T3>::value/row1;
    //
    // constexpr auto irow = I0/col1;;
    // constexpr auto icol = I0 - irow * col1;

    return fdbb::utils::tuple_sum(
      fdbb::utils::for_each_return<
        0,
        1,
        (I0 /
         (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1))) *
          (std::tuple_size<T2>::value / I1),
        1,
        I0 - (I0 / (std::tuple_size<T3>::value /
                    (std::tuple_size<T2>::value / I1))) *
               (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)),
        std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)>(
        fdbb::utils::make_tuple(
          fdbb::utils::make_index_sequence<std::tuple_size<T2>::value / I1>{}),
        t2,
        t3,
        matmul_inner()));
  }
};

/// @brief Functor multiplies two objects (inner summation loop)
struct elem_matmul_inner
{
  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            T2* t2,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>)
    const noexcept -> decltype(fdbb::elem_mul(*t1, *t2))
  {
    return fdbb::elem_mul(*t1, *t2);
  }

  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            T2* t2,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>)
    const noexcept -> decltype(fdbb::elem_mul(
      std::declval<const typename fdbb::utils::remove_all<T1>::type>(),
      *t2))
  {
    return fdbb::elem_mul(t1, *t2);
  }

  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(const T0& t0,
                            T1* t1,
                            const T2& t2,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>)
    const noexcept -> decltype(fdbb::elem_mul(
      *t1,
      std::declval<const typename fdbb::utils::remove_all<T2>::type>()))
  {
    return fdbb::elem_mul(*t1, t2);
  }

  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            const T2& t2,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>)
    const noexcept -> decltype(fdbb::elem_mul(
      std::declval<const typename fdbb::utils::remove_all<T1>::type>(),
      std::declval<const typename fdbb::utils::remove_all<T2>::type>()))
  {
    return fdbb::elem_mul(t1, t2);
  }
};

/// @brief Functor multiplies two objects (outer loop)
struct elem_matmul
{
  template<typename T0,
           typename T1,
           typename T2,
           typename T3,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2,
           std::size_t I3>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            const T2& t2,
                            const T3& t3,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>,
                            std::integral_constant<std::size_t, I3>)
    const noexcept -> decltype(fdbb::utils::tuple_sum(
      fdbb::utils::for_each_return<
        0,
        1,
        (I0 /
         (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1))) *
          (std::tuple_size<T2>::value / I1),
        1,
        I0 - (I0 / (std::tuple_size<T3>::value /
                    (std::tuple_size<T2>::value / I1))) *
               (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)),
        std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)>(
        fdbb::utils::make_tuple(
          fdbb::utils::make_index_sequence<std::tuple_size<T2>::value / I1>{}),
        t2,
        t3,
        elem_matmul_inner())))
  {
    // Remark: indices are calculated as follows
    // constexpr auto row0 = I1;
    // constexpr auto col0 = std::tuple_size<T2>::value/I1;
    //
    // constexpr auto row1 = col0;
    // constexpr auto col1 = std::tuple_size<T3>::value/row1;
    //
    // constexpr auto irow = I0/col1;;
    // constexpr auto icol = I0 - irow * col1;

    return fdbb::utils::tuple_sum(
      fdbb::utils::for_each_return<
        0,
        1,
        (I0 /
         (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1))) *
          (std::tuple_size<T2>::value / I1),
        1,
        I0 - (I0 / (std::tuple_size<T3>::value /
                    (std::tuple_size<T2>::value / I1))) *
               (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)),
        std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)>(
        fdbb::utils::make_tuple(
          fdbb::utils::make_index_sequence<std::tuple_size<T2>::value / I1>{}),
        t2,
        t3,
        elem_matmul_inner()));
  }
};

/// @brief Functor returns the transpose of the object
struct transpose
{
  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(
    const T0& t0,
    const T1& t1,
    const T2& t2,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>,
    std::integral_constant<std::size_t, I2>) const noexcept
    -> decltype(std::get<((I0 < std::tuple_size<T2>::value - 1)
                            ? (I0 * I1) % (std::tuple_size<T2>::value - 1)
                            : I0)>(t2))
  {
    // The index of the transposed matrix is calculated as
    //
    // idx_trans = idx < rows*cols-1 ? (idx * cols)%(rows*cols-1) : idx

    return std::get<((I0 < std::tuple_size<T2>::value - 1)
                       ? (I0 * I1) % (std::tuple_size<T2>::value - 1)
                       : I0)>(t2);
  }
};

} // namespace functor

} // namespace fdbb

#endif // FDBB_CORE_FUNCTORS_HPP
