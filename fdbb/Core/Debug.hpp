/** @file fdbb/Core/Debug.hpp
 *
 *  @brief Debugging functionality
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CORE_DEBUG_HPP
#define FDBB_CORE_DEBUG_HPP

#include <iostream>
#include <typeinfo>

#ifndef NDEBUG
#define DBGMSG(msg)
#else
#define DBGMSG(msg)                                                            \
  {                                                                            \
    std::cerr << msg << std::endl;                                             \
  }
#endif

namespace fdbb {
/** @brief
    Print information about memory layout of given object

    @tparam Obj The object type
 */
template<typename Obj>
static void
static_meminfo(std::string name = "")
{
  if (!name.empty())
    std::cout << "Type             : " << name << std::endl;
  std::cout << "Type name        : "
            << typeid(decltype(std::declval<Obj>())).name() << std::endl;
  std::cout << "Reference        : " << std::is_reference<Obj>::value
            << std::endl;
  std::cout << "Lvalue-reference : " << std::is_lvalue_reference<Obj>::value
            << std::endl;
  std::cout << "Rvalue-reference : " << std::is_rvalue_reference<Obj>::value
            << std::endl;
  std::cout << "Const            : " << std::is_const<Obj>::value << std::endl;
  std::cout << "Volatile         : " << std::is_volatile<Obj>::value
            << std::endl;
}

/** @brief
    Print information about memory layout of given object

    @tparam Obj The object type
 */
template<typename Obj>
static Obj&&
meminfo(Obj&& obj, std::string name = "")
{
  if (!name.empty())
    std::cout << "Type             : " << name << std::endl;
  std::cout << "Type name        : " << typeid(obj).name() << std::endl;
  std::cout << "Memory address   : " << &obj << std::endl;
  std::cout << "Reference        : " << std::is_reference<decltype(obj)>::value
            << std::endl;
  std::cout << "Lvalue-reference : "
            << std::is_lvalue_reference<decltype(obj)>::value << std::endl;
  std::cout << "Rvalue-reference : "
            << std::is_rvalue_reference<decltype(obj)>::value << std::endl;
  std::cout << "Const            : " << std::is_const<decltype(obj)>::value
            << std::endl;
  std::cout << "Volatile         : " << std::is_volatile<decltype(obj)>::value
            << std::endl;

  return std::forward<Obj>(obj);
}

} // namespace fdbb

#endif // FDBB_CORE_DEBUG_HPP
