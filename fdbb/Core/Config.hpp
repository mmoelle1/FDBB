/** @file fdbb/Core/Config.hpp
 *
 *  @brief FDBB configuration
 *
 *  This file contains the global configuration settings of the Fluid
 *  Dynamics Building Block (FDBB) library.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CORE_CONFIG_HPP
#define FDBB_CORE_CONFIG_HPP

#include <cstdint>
#include <iostream>

namespace fdbb {

/** @enum EnumETL
 *
 *  @brief
 *  The fdbb::EnumETL enumeration type, listing all supported vector
 *  expression template libraries explicitly supported by the library.
 */
enum class EnumETL
{
  /// @brief Generic implementation
  GENERIC,

  /// @brief Implementation for caching mechanism
  CACHE,

  /// @brief Implementation for lightweight caching mechanism
  CACHE2,

  /// @brief Implementation for block expressions
  BLOCKEXPR,

#ifdef FDBB_BACKEND_ARMADILLO
  /// @brief Armadillo library
  /// http://arma.sourceforge.net
  ARMADILLO,
#endif

#ifdef FDBB_BACKEND_ARRAYFIRE
  /// @brief ArrayFire library
  /// http://arrayfire.com
  ARRAYFIRE,
#endif

#ifdef FDBB_BACKEND_BLAZE
  /// @brief Blaze library
  /// https://bitbucket.org/blaze-lib/blaze
  BLAZE,
#endif

#ifdef FDBB_BACKEND_EIGEN
  /// @brief Eigen library
  /// http://eigen.tuxfamily.org
  EIGEN,
  /// @brief Eigen library (array type)
  /// http://eigen.tuxfamily.org
  EIGEN_ARRAY,
  /// @brief Eigen library (matrix type)
  /// http://eigen.tuxfamily.org
  EIGEN_MATRIX,
  EIGEN_MATRIX_XTYPE,
  EIGEN_XTYPE_MATRIX,
  EIGEN_MATRIX_MATRIX,
#endif

#ifdef FDBB_BACKEND_ITPP
  /// @brief IT++ library
  /// http://itpp.sourceforge.net
  ITPP,
  /// @brief IT++ library (vector type)
  /// http://itpp.sourceforge.net
  ITPP_VECTOR,
  ITPP_VECTOR_SCALAR,
  /// @brief IT++ library (matrix type)
  /// http://itpp.sourceforge.net
  ITPP_MATRIX,
  ITPP_MATRIX_SCALAR,
#endif

#if defined(FDBB_BACKEND_MTL4) || defined(FDBB_BACKEND_CMTL4)
  /// @brief MTL4 library
  /// http://www.simunova.com/docs/mtl4/html/index.html
  MTL4,
  /// @brief MTL4 library (vector type)
  /// http://www.simunova.com/docs/mtl4/html/index.html
  MTL4_VECTOR,
  /// @brief MTL4 library (matrix type)
  /// http://www.simunova.com/docs/mtl4/html/index.html
  MTL4_MATRIX,
#endif

#ifdef FDBB_BACKEND_UBLAS
  /// @brief uBLAS library
  /// http://www.boost.org/doc/libs/1_60_0/libs/numeric/ublas/doc/
  UBLAS,
  UBLAS_SCALAR,
#endif

#ifdef FDBB_BACKEND_VEXCL
  /// @brief VexCL library
  /// https://github.com/ddemidov/vexcl
  VEXCL,
#endif

#ifdef FDBB_BACKEND_VIENNACL
  /// @brief ViennaCL library
  /// http://viennacl.sourceforge.net
  VIENNACL
#endif
};

/** @brief
    Print fdbb::EnumETL to output stream

    @param[in]   os    The output stream
    @param[in]   obj   The ETL enumerator

    @return            The output stream with printing of fdbb::EnumETL
 */
std::ostream&
operator<<(std::ostream& os, EnumETL obj)
{
  switch (obj) {
    case EnumETL::GENERIC:
      return os << "GENERIC";

    case EnumETL::CACHE:
      return os << "CACHE";

    case EnumETL::CACHE2:
      return os << "CACHE2";

    case EnumETL::BLOCKEXPR:
      return os << "BLOCKEXPR";

#ifdef FDBB_BACKEND_ARMADILLO
    case EnumETL::ARMADILLO:
      return os << "ARMADILLO";
#endif

#ifdef FDBB_BACKEND_ARRAYFIRE
    case EnumETL::ARRAYFIRE:
      return os << "ARRAYFIRE";
#endif

#ifdef FDBB_BACKEND_BLAZE
    case EnumETL::BLAZE:
      return os << "BLAZE";
#endif

#ifdef FDBB_BACKEND_EIGEN
    case EnumETL::EIGEN:
      return os << "EIGEN";
    case EnumETL::EIGEN_ARRAY:
      return os << "EIGEN_ARRAY";
    case EnumETL::EIGEN_MATRIX:
      return os << "EIGEN_MATRIX";
    case EnumETL::EIGEN_MATRIX_MATRIX:
      return os << "EIGEN_MATRIX_MATRIX";
    case EnumETL::EIGEN_MATRIX_XTYPE:
      return os << "EIGEN_MATRIX_XTYPE";
    case EnumETL::EIGEN_XTYPE_MATRIX:
      return os << "EIGEN_XTYPE_MATRIX";
#endif

#ifdef FDBB_BACKEND_ITPP
    case EnumETL::ITPP:
      return os << "ITPP";
    case EnumETL::ITPP_VECTOR:
      return os << "ITPP_VECTOR";
    case EnumETL::ITPP_VECTOR_SCALAR:
      return os << "ITPP_VECTOR_SCALAR";
    case EnumETL::ITPP_MATRIX:
      return os << "ITPP_MATRIX";
    case EnumETL::ITPP_MATRIX_SCALAR:
      return os << "ITPP_MATRIX_SCALAR";
#endif

#if defined(FDBB_BACKEND_MTL4) || defined(FDBB_BACKEND_CMTL4)
    case EnumETL::MTL4:
      return os << "MTL4";
    case EnumETL::MTL4_VECTOR:
      return os << "MTL4_VECTOR";
    case EnumETL::MTL4_MATRIX:
      return os << "MTL4_MATRIX";
#endif

#ifdef FDBB_BACKEND_UBLAS
    case EnumETL::UBLAS:
      return os << "UBLAS";
    case EnumETL::UBLAS_SCALAR:
      return os << "UBLAS_SCALAR";
#endif

#ifdef FDBB_BACKEND_VEXCL
    case EnumETL::VEXCL:
      return os << "VEXCL";
#endif

#ifdef FDBB_BACKEND_VIENNACL
    case EnumETL::VIENNACL:
      return os << "VIENNACL";
#endif
  }
  return os << static_cast<std::uint16_t>(obj);
}

/** @typedef INDEX_T
 *
 *  @brief
 *  The \ref INDEX_T typedef specifying the default type for indices
 */
#ifndef INDEX_T
#ifdef FDBB_INDEX_TYPE
#define INDEX_T FDBB_INDEX_TYPE
#else
#define INDEX_T std::size_t
#endif
#endif

/** @brief Macro for forcing inlining of code.

    The macro defines the attribute of the function such that it is
    directly inlined and not just an recomendation for the compiler.
 */
#ifdef FDBB_FORCE_INLINE
#if defined(_MSC_VER) || defined(__INTEL_COMPILER)
#define FDBB_INLINE __forceinline
#elif defined(__GNUC__)
#define FDBB_INLINE inline __attribute__((always_inline))
#else
#warning                                                                       \
  "Could not determine compiler for forced inline definitions. Using inline."
#define FDBB_INLINE inline
#endif
#else
#define FDBB_INLINE inline
#endif

/** @brief Macro for avoiding inlining of code.

    The macro defines the attribute of the function such that it is no
    longer considered for inlining.
 */
#ifdef FDBB_FORCE_INLINE
#define FDBB_NO_INLINE __attribute__((noinline))
#else
#define FDBB_NO_INLINE /* no avoiding of inline defined */
#endif

/** @brief Macro for shortening return-type deduction clause

    The macro provides a short-hand notation for functions with
    single-expression return type
 */
#if !defined(DOXYGEN)
#define FDBB_RETURN_WITH_DECLTYPE(expression)                                  \
  ->decltype(expression) { expression; }
#else
#define FDBB_RETURN_WITH_DECLTYPE(expression)                                  \
  {                                                                            \
    expression;                                                                \
  }
#endif

} // namespace fdbb

#endif // FDBB_CORE_CONFIG_HPP
