/** @file fdbb/Backend/blockexpression.hpp
 *
 *  @brief Implementation details for block expressions
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_BLOCK_EXPRESSIONS_HPP
#define BACKEND_BLOCK_EXPRESSIONS_HPP

#include <tuple>
#include <type_traits>

#include <BlockExpression/BlockExpression.hpp>
#include <BlockExpression/BlockMatrix.hpp>
#include <BlockExpression/BlockOperations.hpp>
#include <Core/Backend.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::BLOCKEXPR, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::BLOCKEXPR,
                  typename std::enable_if<std::is_base_of<
                    fdbb::BlockBase,
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (BLOCKEXPR type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::BLOCKEXPR>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::result_type;
};

/** @brief
    Scalar value type of the expression (BLOCKEXPR type)
 */
template<typename Expr>
struct value_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::BLOCKEXPR>::type>
{
  using type = typename fdbb::backend::value_type<
    typename fdbb::backend::result_type<Expr>::type>::type;
};

namespace detail {

/** @brief
    Selector for specialized BLOCKEXPR implementation of
    fdbb::detail::tag_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_tag_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{};

/** @brief
    Tags terminal with a unique (in a single expression) tag.

    By tagging terminals user guarantees that the terminals with same
    tags actually refer to the same data. VexCL is able to use this
    information in order to reduce number of kernel parameters and
    unnecessary global memory I/O operations.
 */
template<std::size_t Tag, typename Expr>
struct tag_impl<Tag, Expr, EnumETL::BLOCKEXPR>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::tag<Tag, 1>(std::forward<Expr>(expr)))
#endif
  {
    return fdbb::tag<Tag, 1>(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized BLOCKEXPR implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{};

/** @brief
    Element-wise multiplication of BLOCKEXPR types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::BLOCKEXPR>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    ->
    typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                            fdbb::utils::remove_all<A>::type::cols,
                            fdbb::utils::remove_all<A>::type::options,
                            decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                              a.get(),
                              b.get(),
                              fdbb::functor::elem_mul()))>::type
#endif
  {
#if __cplusplus <= 201103L
    return typename BlockExpr_type<
      fdbb::utils::remove_all<A>::type::rows,
      fdbb::utils::remove_all<A>::type::cols,
      fdbb::utils::remove_all<A>::type::options,
      decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
        a.get(), b.get(), fdbb::functor::elem_mul()))>::type{
      fdbb::utils::for_each_return<0, 1, 0, 1>(
        a.get(), b.get(), fdbb::functor::elem_mul())
    };
#else
    auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
      a.get(), b.get(), fdbb::functor::elem_mul());
    return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                   fdbb::utils::remove_all<A>::type::cols,
                                   fdbb::utils::remove_all<A>::type::options,
                                   decltype(ExprType)>::type{ ExprType };
#endif
  }
};

/** @brief
    Selector for specialized BLOCKEXPR implementation of
    fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{};

/** @brief
    Element-wise division of BLOCKEXPR types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::BLOCKEXPR>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    ->
    typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                            fdbb::utils::remove_all<A>::type::cols,
                            fdbb::utils::remove_all<A>::type::options,
                            decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                              a.get(),
                              b.get(),
                              fdbb::functor::elem_div()))>::type
#endif
  {
#if __cplusplus <= 201103L
    return typename BlockExpr_type<
      fdbb::utils::remove_all<A>::type::rows,
      fdbb::utils::remove_all<A>::type::cols,
      fdbb::utils::remove_all<A>::type::options,
      decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
        a.get(), b.get(), fdbb::functor::elem_div()))>::type{
      fdbb::utils::for_each_return<0, 1, 0, 1>(
        a.get(), b.get(), fdbb::functor::elem_div())
    };
#else
    auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
      a.get(), b.get(), fdbb::functor::elem_div());
    return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                   fdbb::utils::remove_all<A>::type::cols,
                                   fdbb::utils::remove_all<A>::type::options,
                                   decltype(ExprType)>::type{ ExprType };
#endif
  }
};

/** @brief
    Selector for specialized BLOCKEXPR implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of<A, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{};

/** @brief
    Element-wise pow-function of BLOCKEXPR types
    @{
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::BLOCKEXPR>
{
  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_arithmetic<typename fdbb::utils::remove_all<_B>::type>::value,
      typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                              fdbb::utils::remove_all<A>::type::cols,
                              fdbb::utils::remove_all<A>::type::options,
                              decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                                a.get(),
                                fdbb::utils::tie(b),
                                fdbb::functor::elem_pow()))>::type>::type
#endif
  {
#if __cplusplus <= 201103L
    return typename BlockExpr_type<
      fdbb::utils::remove_all<A>::type::rows,
      fdbb::utils::remove_all<A>::type::cols,
      fdbb::utils::remove_all<A>::type::options,
      decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
        a.get(), fdbb::utils::tie(b), fdbb::functor::elem_pow()))>::type{
      fdbb::utils::for_each_return<0, 1, 0, 0>(
        a.get(), fdbb::utils::tie(b), fdbb::functor::elem_pow())
    };
#else
    auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
      a.get(), fdbb::utils::tie(b), fdbb::functor::elem_pow());
    return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                   fdbb::utils::remove_all<A>::type::cols,
                                   fdbb::utils::remove_all<A>::type::options,
                                   decltype(ExprType)>::type{ ExprType };
#endif
  }

  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      !std::is_arithmetic<typename fdbb::utils::remove_all<_B>::type>::value,
      typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                              fdbb::utils::remove_all<A>::type::cols,
                              fdbb::utils::remove_all<A>::type::options,
                              decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                                a.get(),
                                b.get(),
                                fdbb::functor::elem_pow()))>::type>::type
#endif
  {
#if __cplusplus <= 201103L
    return typename BlockExpr_type<
      fdbb::utils::remove_all<A>::type::rows,
      fdbb::utils::remove_all<A>::type::cols,
      fdbb::utils::remove_all<A>::type::options,
      decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
        a.get(), b.get(), fdbb::functor::elem_pow()))>::type{
      fdbb::utils::for_each_return<0, 1, 0, 1>(
        a.get(), b.get(), fdbb::functor::elem_pow())
    };
#else
    auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
      a.get(), b.get(), fdbb::functor::elem_pow());
    return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                   fdbb::utils::remove_all<A>::type::cols,
                                   fdbb::utils::remove_all<A>::type::options,
                                   decltype(ExprType)>::type{ ExprType };
#endif
  }
};
/** @} */

#if __cplusplus <= 201103L
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized BLOCKEXPR implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
*/                                                                             \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::BLOCKEXPR>::type>             \
    : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>               \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of BLOCKEXPR type
  */                                                                           \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::BLOCKEXPR>                           \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept ->                  \
      typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,          \
                              fdbb::utils::remove_all<A>::type::cols,          \
                              fdbb::utils::remove_all<A>::type::options,       \
                              decltype(fdbb::utils::for_each_return<0, 1>(     \
                                a.get(),                                       \
                                fdbb::functor::elem_##OPNAME()))>::type        \
    {                                                                          \
      return                                                                   \
        typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,        \
                                fdbb::utils::remove_all<A>::type::cols,        \
                                fdbb::utils::remove_all<A>::type::options,     \
                                decltype(fdbb::utils::for_each_return<0, 1>(   \
                                  a.get(),                                     \
                                  fdbb::functor::elem_##OPNAME()))>::type{     \
          fdbb::utils::for_each_return<0, 1>(a.get(),                          \
                                             fdbb::functor::elem_##OPNAME())   \
        };                                                                     \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized BLOCKEXPR implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
*/                                                                             \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::BLOCKEXPR>::type>             \
    : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>               \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of BLOCKEXPR type
  */                                                                           \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::BLOCKEXPR>                           \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept ->                  \
      typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,          \
                              fdbb::utils::remove_all<A>::type::cols,          \
                              fdbb::utils::remove_all<A>::type::options,       \
                              decltype(fdbb::utils::for_each_return<0, 1>(     \
                                a.get(),                                       \
                                fdbb::functor::elem_##OPNAME()))>::type        \
    {                                                                          \
      auto ExprType = fdbb::utils::for_each_return<0, 1>(                      \
        a.get(), fdbb::functor::elem_##OPNAME());                              \
      return                                                                   \
        typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,        \
                                fdbb::utils::remove_all<A>::type::cols,        \
                                fdbb::utils::remove_all<A>::type::options,     \
                                decltype(ExprType)>::type{ ExprType };         \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // BACKEND_BLOCKEXPR_HPP
