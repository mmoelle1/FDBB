/** @file fdbb/Backend/vexcl.hpp
 *
 *  @brief Implementation details for VexCL library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_VEXCL_HPP
#define BACKEND_VEXCL_HPP

#ifdef FDBB_BACKEND_VEXCL

#include <type_traits>
#include <vexcl/vexcl.hpp>
#if defined(CL_VERSION_2_0) || defined(VEXCL_BACKEND_CUDA)
#include <vexcl/element_index.hpp>
#include <vexcl/function.hpp>
#include <vexcl/svm_vector.hpp>
#endif

#include <BlockExpression/ForwardDeclarations.hpp>
#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Constant.hpp>
#include <Core/Functors.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::VEXCL, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::VEXCL,
  typename std::enable_if<(vex::is_vector_expression<T>::value ||
                           vex::is_multivector_expression<T>::value) &&
                          !std::is_arithmetic<T>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (VexCL type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::VEXCL>::type>
{
  using type = typename boost::proto::result_of::as_expr<
    typename fdbb::utils::remove_all<Expr>::type>::type;
};

/** @brief
    Scalar value type of the expression (VexCL type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::VEXCL>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::value_type;
};

namespace detail {

/** @brief
    Selector for specialized VexCL implementation of
    fdbb::backend::detail::make_constant_impl<T,Expr>(const T value, Expr&&
    expr) function
 */
template<typename Expr>
struct get_make_constant_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, fdbb::EnumETL::VEXCL>::type>
  : public std::integral_constant<EnumETL, EnumETL::VEXCL>
{};

/** @brief
    Create VexCL constant
 */
template<typename TC, typename T, typename Expr>
struct make_constant_impl<TC, T, Expr, EnumETL::VEXCL>
{
  static FDBB_INLINE auto constexpr eval(const T value, Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::proto::as_expr<vex::vector_domain>(
      vex::user_constant<TC>()))
#endif
  {
    return boost::proto::as_expr<vex::vector_domain>(vex::user_constant<TC>());
  }
};

/** @brief
    Selector for specialized VexCL implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function

    @note
    Identification of VexCL types that can be converted into temporary
    is non-trivial. This implementation uses the same detection
    mechanism as used internally by VexCL.
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename std::enable_if<
    std::is_same<
      typename std::enable_if<
        boost::proto::matches<
          typename boost::proto::result_of::as_expr<
            typename fdbb::utils::remove_all<Expr>::type>::type,
          vex::vector_expr_grammar>::value,
        vex::temporary<typename vex::detail::return_type<
                         typename fdbb::utils::remove_all<Expr>::type>::type,
                       0,
                       typename boost::proto::result_of::as_child<
                         const typename fdbb::utils::remove_all<Expr>::type,
                         vex::vector_domain>::type> const>::type,
      typename std::enable_if<
        boost::proto::matches<
          typename boost::proto::result_of::as_expr<
            typename fdbb::utils::remove_all<Expr>::type>::type,
          vex::vector_expr_grammar>::value,
        vex::temporary<typename vex::detail::return_type<
                         typename fdbb::utils::remove_all<Expr>::type>::type,
                       0,
                       typename boost::proto::result_of::as_child<
                         const typename fdbb::utils::remove_all<Expr>::type,
                         vex::vector_domain>::type> const>::type>::value &&
    !std::is_arithmetic<typename fdbb::utils::remove_all<Expr>::type>::value>::
    type> : public std::integral_constant<EnumETL, EnumETL::VEXCL>
{};

/** @brief
    VexCL terminal type creation from expressions

    @note
    VexCL has a dedicated vex::make_temp<Tag,Expr>(Expr expr)
    implementation, which returns a dedicated temporal
    type. Therefore, this trait  does not make use of the
    fdbb::backend::result_type<Expr> trait to create a temporal.
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::VEXCL>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(vex::make_temp<Tag>(std::forward<Expr>(expr)))
#endif
  {
    return vex::make_temp<Tag>(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized VexCL implementation of
    fdbb::detail::tag_impl<Tag,Expr>(Expr&& expr) function

    @note
    Identification of VexCL types that can be tagged is
    non-trivial. This implementation uses the same detection mechanism
    as used internally by VexCL.
 */
template<typename Expr>
struct get_tag_impl<
  Expr,
  typename std::enable_if<
    std::is_same<
      vex::tagged_terminal<0,
                           decltype(boost::proto::as_child<vex::vector_domain>(
                             std::declval<Expr>()))>,
      vex::tagged_terminal<0,
                           decltype(boost::proto::as_child<vex::vector_domain>(
                             std::declval<Expr>()))>>::value &&
    !fdbb::backend::is_type_of<Expr, EnumETL::BLOCKEXPR>::value &&
    !std::is_arithmetic<Expr>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::VEXCL>
{};

/** @brief
    Tags terminal with a unique (in a single expression) tag.

    By tagging terminals user guarantees that the terminals with same
    tags actually refer to the same data. VexCL is able to use this
    information in order to reduce number of kernel parameters and
    unnecessary global memory I/O operations.
 */
template<std::size_t Tag, typename Expr>
struct tag_impl<Tag, Expr, EnumETL::VEXCL>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(vex::tag<Tag>(std::forward<Expr>(expr)))
#endif
  {
    return vex::tag<Tag>(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized VexCL implementation of
    fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::VEXCL>::type>
  : public std::integral_constant<EnumETL, EnumETL::VEXCL>
{};

/** @brief
    Element-wise \a conj(x) function for x being of of VexCL type
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::VEXCL>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized VexCL implementation of
    fdbb::elem_imag<A>(A&& a) function
 */
template<typename A>
struct get_elem_imag_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::VEXCL>::type>
  : public std::integral_constant<EnumETL, EnumETL::VEXCL>
{};

/** @brief
    Element-wise \a imag(x) function for x being of VexCL type
 */
template<typename A>
struct elem_imag_impl<A, EnumETL::VEXCL>
{
  static FDBB_INLINE auto // constexpr and noexcept not possible
  eval(A&& a)
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized VexCL implementation of
    fdbb::elem_real<A>(A&& a) function
 */
template<typename A>
struct get_elem_real_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::VEXCL>::type>
  : public std::integral_constant<EnumETL, EnumETL::VEXCL>
{};

/** @brief
    Element-wise \a real(x) function for x being of VexCL type
 */
template<typename A>
struct elem_real_impl<A, EnumETL::VEXCL>
{
  static FDBB_INLINE auto // constexpr and noexcept not possible
  eval(A&& a)
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return abs(std::forward<A>(a));
  }
};

} // namespace detail
} // namespace backend

namespace functor {

struct vexcl_resize
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  void operator()(T0* t0,
                  T1* t1,
                  std::integral_constant<std::size_t, I0>,
                  std::integral_constant<std::size_t, I1>) const noexcept
  {
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(*t0, prop);
    t1->resize(prop.queue, prop.size);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  void operator()(const T0& t0,
                  T1* t1,
                  std::integral_constant<std::size_t, I0>,
                  std::integral_constant<std::size_t, I1>) const noexcept
  {
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(t0, prop);
    t1->resize(prop.queue, prop.size);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  void operator()(T0* t0,
                  T1& t1,
                  std::integral_constant<std::size_t, I0>,
                  std::integral_constant<std::size_t, I1>) const noexcept
  {
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(*t0, prop);
    t1.resize(prop.queue, prop.size);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  void operator()(const T0& t0,
                  T1& t1,
                  std::integral_constant<std::size_t, I0>,
                  std::integral_constant<std::size_t, I1>) const noexcept
  {
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(t0, prop);
    t1.resize(prop.queue, prop.size);
  }
};

} // namespace functor

/** @brief
    A fixed-size block matrix

    This is a block matrix container for a fixed number of scalar
    matrices of type T. It supports all arithmetic operations
    like addition, subtraction, element-wise multiplication and
    division, and matrix-vector multiplication. Moreover, it can be
    transposed by returning a view of a block matrix.

    @note
    Block matrices are stored in a std::tuple. The template
    parameter \a _options specifies the storage order. The default
    storage order is row-major. Note that it is not possible to
    perform arithmetic operations between block expressions stored in
    different storage orders.
*/

template<typename T,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options>
struct BlockMatrix<vex::vector<T>, _rows, _cols, _options>
  : public fdbb::detail::BlockMatrix<vex::vector<T>, _rows, _cols, _options>
{
  /// Base class type
  using Base = detail::BlockMatrix<vex::vector<T>, _rows, _cols, _options>;

  /// @brief Default constructor
  BlockMatrix() {}

  /// @brief Constructor (copy arguments from parameter pack)
  template<typename Arg,
           typename... Args,
           typename = typename std::enable_if<
             !std::is_base_of<
               BlockBase,
               typename fdbb::utils::remove_all<typename std::tuple_element<
                 0,
                 std::template tuple<Arg, Args...>>::type>::type>::value &&
             _rows * _cols == 1 + sizeof...(Args)>::type>
  explicit BlockMatrix(Arg&& arg, Args&&... args) noexcept
  {
    fdbb::utils::for_each(fdbb::utils::tie(arg, args...),
                          Base::expr,
                          fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(arg, prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, fdbb::utils::tie(arg, args...), prop.queue, prop.part);
  }

  /// @brief Constructor (copy from expression passed as std::tuple)
  explicit BlockMatrix(const typename Base::expr_type& expr)
  {
    fdbb::utils::for_each(expr, Base::expr, fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(expr.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, expr, prop.queue, prop.part);
  }

  /// @brief Constructor (move from expression passed as std::tuple)
  explicit BlockMatrix(typename Base::expr_type&& expr)
  {
    fdbb::utils::for_each(expr, Base::expr, fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(expr.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, expr, prop.queue, prop.part);
  }

  /// @brief Constructor (copy from block expression)
  template<typename... Ts>
  BlockMatrix(BlockExpr<_rows, _cols, _options, Ts...>& other) noexcept
  {
    fdbb::utils::for_each(
      other.get(), Base::expr, fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(other.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, other.get(), prop.queue, prop.part);
  }

  /// @brief Constructor (move from block expression)
  template<typename... Ts>
  BlockMatrix(BlockExpr<_rows, _cols, _options, Ts...>&& other) noexcept
  {
    fdbb::utils::for_each(
      other.get(), Base::expr, fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(other.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, other.get(), prop.queue, prop.part);
  }

  /// @brief Constructor (copy from block matrix)
  BlockMatrix(
    const BlockMatrix<vex::vector<T>, _rows, _cols, _options>& other) noexcept
  {
    fdbb::utils::for_each(
      other.get(), Base::expr, fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(other.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, other.get(), prop.queue, prop.part);
  }

  /// @brief Constructor (move from block matrix)
  BlockMatrix(
    BlockMatrix<vex::vector<T>, _rows, _cols, _options>&& other) noexcept
    : Base{ std::move(other) }
  {}

  /// @brief Constructor (copy from block matrix view)
  BlockMatrix(const BlockMatrixView<vex::vector<T>, _rows, _cols, _options>&
                other) noexcept
  {
    fdbb::utils::for_each(
      other.get(), Base::expr, fdbb::functor::vexcl_resize());
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(*other.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr,
      fdbb::utils::for_each_return(other.get(), fdbb::functor::deref()),
      prop.queue,
      prop.part);
  }

  /// @brief Assignment operator (copy from block expression)
  template<typename... Ts>
  BlockMatrix<vex::vector<T>, _rows, _cols, _options>& operator=(
    const BlockExpr<_rows, _cols, _options, Ts...>& other) noexcept
  {
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(other.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, other.get(), prop.queue, prop.part);
    return *this;
  }

  /// @brief Assignment operator (move from block expression)
  template<typename... Ts>
  BlockMatrix<vex::vector<T>, _rows, _cols, _options>& operator=(
    BlockExpr<_rows, _cols, _options, Ts...>&& other) noexcept
  {
    vex::detail::get_expression_properties prop;
    vex::detail::extract_terminals()(other.template get<0>(), prop);
    vex::detail::assign_multiexpression<vex::assign::SET>(
      Base::expr, other.get(), prop.queue, prop.part);
    return *this;
  }

  /// @brief Assignment operator (copy from block matrix)
  BlockMatrix<vex::vector<T>, _rows, _cols, _options>& operator=(
    const BlockMatrix<vex::vector<T>, _rows, _cols, _options>& other) noexcept
  {
    if (&other != this)
      Base::expr = other.get();
    return *this;
  }

  /// @brief Assignment operator (move from block matrix)
  BlockMatrix<vex::vector<T>, _rows, _cols, _options>& operator=(
    BlockMatrix<vex::vector<T>, _rows, _cols, _options>&& other) noexcept
  {
    if (&other != this)
      Base::expr = std::move(other.get());
    return *this;
  }
};

} // namespace fdbb

namespace std {

/** @brief
    Spezialization of std::is_arithmetic for constants

    @tparam   Expr   The expression type of the constant
    @tparam   C...   The character encoding of the constant
*/

// template<>
template<typename Expr, char... C>
struct is_arithmetic<vex::vector_expression<boost::proto::exprns_::basic_expr<
  boost::proto::tagns_::tag::terminal,
  boost::proto::argsns_::term<vex::user_constant<fdbb::Constant<Expr, C...>>>,
  0>>> : public true_type
{};

} // namespace std

#endif // FDBB_BACKEND_VEXCL
#endif // BACKEND_VEXCL_HPP
