/** @file fdbb/Backend/eigen.hpp
 *
 *  @brief Implementation details for Eigen library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_EIGEN_HPP
#define BACKEND_EIGEN_HPP

#ifdef FDBB_BACKEND_EIGEN

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Constant.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::EIGEN, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN,
                  typename std::enable_if<std::is_base_of<
                    Eigen::EigenBase<typename fdbb::utils::remove_all<T>::type>,
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/// Detect Eigen base-type wrapped in G+Smo
template<typename T>
struct is_type_of<
  T,
  EnumETL::EIGEN,
  typename std::enable_if<std::is_base_of<
    Eigen::EigenBase<typename fdbb::utils::remove_all<T>::type::Base>,
    typename fdbb::utils::remove_all<T>::type::Base>::value>::type>
  : public std::true_type
{};

/** @brief
    If T is of type EnumETL::EIGEN_MATRIX, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::EIGEN_MATRIX,
  typename std::enable_if<std::is_base_of<
    Eigen::MatrixBase<typename fdbb::utils::remove_all<T>::type>,
    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/// Detect Eigen matrix-type wrapped in G+Smo
template<typename T>
struct is_type_of<
  T,
  EnumETL::EIGEN_MATRIX,
  typename std::enable_if<std::is_base_of<
    Eigen::MatrixBase<typename fdbb::utils::remove_all<T>::type::Base>,
    typename fdbb::utils::remove_all<T>::type::Base>::value>::type>
  : public std::true_type
{};

/** @brief
    If T is of type EnumETL::EIGEN_ARRAY, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN_ARRAY,
                  typename std::enable_if<std::is_base_of<
                    Eigen::ArrayBase<typename fdbb::utils::remove_all<T>::type>,
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/// Detect Eigen array-type wrapped in G+Smo
template<typename T>
struct is_type_of<
  T,
  EnumETL::EIGEN_ARRAY,
  typename std::enable_if<std::is_base_of<
    Eigen::ArrayBase<typename fdbb::utils::remove_all<T>::type::Base>,
    typename fdbb::utils::remove_all<T>::type::Base>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (Eigen type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::PlainObject;
};

/** @brief
    Scalar value type of the expression (Eigen type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::Scalar;
};

namespace detail {

/** @brief
    Selector for specialized Eigen implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN>
{};

/** @brief
    Eigen terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::EIGEN>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::backend::result_type<Expr>::type
#endif
  {
    using Temp = typename fdbb::backend::result_type<Expr>::type;
    return Temp(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise multiplication of Eigen::Matrix types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype((a.array() * b.array()).matrix())
#endif
  {
    return (a.array() * b.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise division of Eigen::Matrix types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype((a.array() / b.array()).matrix())
#endif
  {
    return (a.array() / b.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function, for the case
    that A and B are of type Eigen::Matrix
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX_MATRIX>
{};

/** @brief
    Element-wise pow-function of Eigen::Matrix types, for the case
    that A and B are of type Eigen::matrix
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::EIGEN_MATRIX_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(a.array(), b.array()).matrix())
#endif
  {
    return pow(a.array(), b.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function, for the case
    that A is of type Eigen::Matrix and B is not
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of_and_cond<
    A,
    EnumETL::EIGEN_MATRIX,
    !fdbb::backend::is_type_of<B, EnumETL::EIGEN_MATRIX>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX_XTYPE>
{};

/** @brief
    Element-wise pow-function of Eigen::Matrix types, for the case
    that A is of type Eigen::Matrix and B is not
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::EIGEN_MATRIX_XTYPE>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(a.array(), b).matrix())
#endif
  {
    return pow(a.array(), b).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function, for the case
    that B is of type Eigen::Matrix and A is not
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of_and_cond<
    B,
    EnumETL::EIGEN_MATRIX,
    !fdbb::backend::is_type_of<A, EnumETL::EIGEN_MATRIX>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_XTYPE_MATRIX>
{};

/** @brief
    Element-wise pow-function of Eigen::Matrix types, for the case
    that B is of type Eigen::Matrix and A is not
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::EIGEN_XTYPE_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(a, b.array()).matrix())
#endif
  {
    return pow(a, b.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a fabs(x) function for x being of Eigen::Array type

    @note
    Eigen does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a fabs(x) function for x being of Eigen::Matrix type

    @note
    Eigen does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(a.array()).matrix())
#endif
  {
    return abs(a.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a exp10(x) function for x being of Eigen::Array type
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(
      std::forward<A>(a) *
      CONSTANT(2.302585092994045684017991454684364207601101488628772976033, a)))
#endif
  {
    return exp(
      std::forward<A>(a) *
      CONSTANT(2.302585092994045684017991454684364207601101488628772976033, a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a exp10(x) function for x being of Eigen::Matrix type
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(
                  a.array() *
                  CONSTANT(
                    2.302585092994045684017991454684364207601101488628772976033,
                    a))
                  .matrix())
#endif
  {
    return exp(
             a.array() *
             CONSTANT(
               2.302585092994045684017991454684364207601101488628772976033, a))
      .matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_exp2<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a exp2(x) function for x being of Eigen::Array type
 */
template<typename A>
struct elem_exp2_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(
      std::forward<A>(a) *
      CONSTANT(0.693147180559945309417232121458176568075500134360255254120, a)))
#endif
  {
    return exp(
      std::forward<A>(a) *
      CONSTANT(0.693147180559945309417232121458176568075500134360255254120, a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_exp2<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a exp2(x) function for x being of Eigen::Matrix type
 */
template<typename A>
struct elem_exp2_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(
                  a.array() *
                  CONSTANT(
                    0.693147180559945309417232121458176568075500134360255254120,
                    a))
                  .matrix())
#endif
  {
    return exp(
             a.array() *
             CONSTANT(
               0.693147180559945309417232121458176568075500134360255254120, a))
      .matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a rsqrt(x) function for x being of Eigen::Array type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(std::forward<A>(a), CONSTANT(-0.5, a)))
#endif
  {
    return pow(std::forward<A>(a), CONSTANT(-0.5, a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a rsqrt(x) function for x being of Eigen::Matrix type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(a.array(), CONSTANT(-0.5, a)).matrix())
#endif
  {
    return pow(a.array(), CONSTANT(-0.5, a)).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_log2<A>(A&& a) function
 */
template<typename A>
struct get_elem_log2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a log2(x) function for x being of Eigen::Array type
 */
template<typename A>
struct elem_log2_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(log(std::forward<A>(a)) *
                1.442695040888963407359924681001892137426645954152985934135)
#endif
  {
    return log(std::forward<A>(a)) *
           1.442695040888963407359924681001892137426645954152985934135;
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_log2<A>(A&& a) function
 */
template<typename A>
struct get_elem_log2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a log2(x) function for x being of Eigen::Matrix type
 */
template<typename A>
struct elem_log2_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(log(a.array()).matrix() *
                1.442695040888963407359924681001892137426645954152985934135)
#endif
  {
    return log(a.array()).matrix() *
           1.442695040888963407359924681001892137426645954152985934135;
  }
};

/// Helper macro for generating element-wise unary operations
#if !defined(DOXYGEN)
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized Eigen::Matrix implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>          \
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>            \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of Eigen::Matrix types
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::EIGEN_MATRIX>                        \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(a.array().OPNAME().matrix())                                 \
    {                                                                          \
      return a.array().OPNAME().matrix();                                      \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized Eigen::Matrix implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>          \
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>            \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of Eigen::Matrix types
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::EIGEN_MATRIX>                        \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
    {                                                                          \
      return a.array().OPNAME().matrix();                                      \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a conj(x) function for x being of Eigen::Array type
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a conj(x) function for x being of Eigen::Matrix type
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(a.array()).matrix())
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return abs(a.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_imag<A>(A&& a) function
 */
template<typename A>
struct get_elem_imag_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a imag(x) function for x being of Eigen::Array types
 */
template<typename A>
struct elem_imag_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_imag<A>(A&& a) function
 */
template<typename A>
struct get_elem_imag_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a imag(x) function for x being of Eigen::Matrix types
 */
template<typename A>
struct elem_imag_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(a.array()).matrix())
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return abs(a.array()).matrix();
  }
};

/** @brief
    Selector for specialized Eigen::Array implementation of
    fdbb::elem_real<A>(A&& a) function
 */
template<typename A>
struct get_elem_real_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{};

/** @brief
    Element-wise \a real(x) function for x being of Eigen::Array types
 */
template<typename A>
struct elem_real_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Eigen::Matrix implementation of
    fdbb::elem_real<A>(A&& a) function
 */
template<typename A>
struct get_elem_real_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{};

/** @brief
    Element-wise \a real(x) function for x being of Eigen::Matrix types
 */
template<typename A>
struct elem_real_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(abs(a.array()).matrix())
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return abs(a.array()).matrix();
  }
};

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // FDBB_BACKEND_EIGEN
#endif // BACKEND_EIGEN_HPP
