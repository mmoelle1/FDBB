/** @file fdbb/Backend/arrayfire.hpp
 *
 *  @brief Implementation details for ArrayFire library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_ARRAYFIRE_HPP
#define BACKEND_ARRAYFIRE_HPP

#ifdef FDBB_BACKEND_ARRAYFIRE

#include <arrayfire.h>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Constant.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::ARRAYFIRE, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::ARRAYFIRE,
                  typename std::enable_if<std::is_base_of<
                    af::array,
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (ArrayFire type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::ARRAYFIRE>::type>
{
  using type = typename af::array;
};

/** @brief
    Scalar value type of the expression (ArrayFire type)
 */
template<typename Expr>
struct value_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::ARRAYFIRE>::type>
{
  using type = double;
};

namespace detail {

/** @brief
    Selector for specialized ArrayFire implementation of
    fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE>
{};

/** @brief
    Element-wise \a conj(x) function for x being of ArrayFire type
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::ARRAYFIRE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(af::conjg(std::forward<A>(a)))
#endif
  {
    return af::conjg(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized ArrayFire implementation of
    fdbb::elem_exp2<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE>
{};

/** @brief
    Element-wise \a exp2(x) function for x being of ArrayFire type
 */
template<typename A>
struct elem_exp2_impl<A, EnumETL::ARRAYFIRE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(
      std::forward<A>(a) *
      CONSTANT(0.693147180559945309417232121458176568075500134360255254120, a)))
#endif
  {
    return exp(
      std::forward<A>(a) *
      CONSTANT(0.693147180559945309417232121458176568075500134360255254120, a));
  }
};

/** @brief
    Selector for specialized ArrayFire implementation of
    fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE>
{};

/** @brief
    Element-wise \a exp10(x) function for x being of ArrayFire type
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::ARRAYFIRE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(
      std::forward<A>(a) *
      2.302585092994045684017991454684364207601101488628772976033))
#endif
  {
    return exp(std::forward<A>(a) *
               2.302585092994045684017991454684364207601101488628772976033);
  }
};

/** @brief
    Selector for specialized ArrayFire implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE>
{};

/** @brief
    Element-wise \a fabs(x) function for x being of ArrayFire type

    @note
    ArrayFire does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::ARRAYFIRE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized ArrayFire implementation of
    fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE>
{};

/** @brief
    Element-wise \a rsqrt(x) function for x being of ArrayFire type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::ARRAYFIRE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(std::forward<A>(a),
                    (typename fdbb::backend::value_type<A>::type)CONSTANT(-0.5,
                                                                          a)))
#endif
  {
    return pow(std::forward<A>(a), CONSTANT(CONSTANT(-0.5, a), a));
  }
};

/** @brief
    Selector for specialized ArrayFire implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_sign_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE>
{};

/** @brief
    Element-wise \a sign(x) function for x being of ArrayFire type

    @note
    ArrayFire's sign(x) function checks if the input is negative. That
    is, it returns 1 if the input data is negative and 0
    otherwise. This wrapper computes the mathematical sign(x).
 */
template<typename A>
struct elem_sign_impl<A, EnumETL::ARRAYFIRE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(-sign(a) + sign(-a))
#endif
  {
    return -sign(a) + sign(-a);
  }
};

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // FDBB_BACKEND_ARRAYFIRE
#endif // BACKEND_ARRAYFIRE_HPP
