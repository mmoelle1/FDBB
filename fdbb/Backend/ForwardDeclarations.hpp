/** @file fdbb/Backend/ForwardDeclarations.hpp
 *
 *  @brief Forward declarations of the compatibility backend
 *
 *  This file provides the forward declarations of the compatibility
 *  backend together with default implementations that are used if the
 *  particular backend does not provide a specialized implementation.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BACKEND_FORWARD_DECLARATIONS_HPP
#define FDBB_BACKEND_FORWARD_DECLARATIONS_HPP

#include <Core/Config.hpp>

namespace fdbb {

/** @namespace fdbb::backend
 *
 *  @brief
 *  The \ref fdbb::backend namespace, containing functionality of the
 *  compatibility backend of the FDBB library
 *
 *  The \ref fdbb::backend namespace contains all functionality of the
 *  compatibility backend provided by the FDBB library. Like the
 *  functionality in the \ref fdbb namespace, the compatibility
 *  backend has a stable API that does not change over future releases
 *  of the FDBB library.
 */
namespace backend {

/** @brief
    If `T` is a type of the expression template library `Expr`,
    provides the member constant value equal to `true`. Otherwise
    value is `false`.

    @tparam   T      The type to check against the given expression type
    @tparam   Expr   The expression type
 */
template<typename T, EnumETL Expr, typename = void>
struct is_type_of : std::false_type
{};

/** @brief
    Result type of the expression `Expr`

    @tparam   Expr   The expression type
 */
template<typename Expr, typename = void>
struct result_type;
//{
//  using type = void;
//};

/** @brief
    Result type of the expression `Expr` (arithmetic type)

    @tparam   Expr   The expression type
 */
template<typename Expr>
struct result_type<
  Expr,
  typename std::enable_if<std::is_arithmetic<Expr>::value>::type>
{
  using type = Expr;
};

/** @brief
    Scalar value type of the expression `Expr`

    @tparam   Expr   The expression type
 */
template<typename Expr, typename = void>
struct value_type;
//{
//  using type = void;
//};

/** @brief
    Scalar value type of the expression `Expr` (arithmetic type)

    @tparam   Expr   The expression type
 */
template<typename Expr>
struct value_type<
  Expr,
  typename std::enable_if<std::is_arithmetic<Expr>::value>::type>
{
  using type = Expr;
};

namespace detail {

/** @brief
    Selector for (specialized) implementation of
    `fdbb::backend::detail::make_constant_impl<T,Expr>(const T c, Expr&& expr)`
    function

    @tparam   Expr   The expression type
 */
template<typename Expr, typename = void>
struct get_make_constant_impl
  : public std::integral_constant<EnumETL, EnumETL::GENERIC>
{};

/** @brief
    Generic implementation of
    `fdbb::backend::detail::make_constant_impl<T,Expr>(const T c, Expr&& expr)`
    function

    @tparam      TC      The typeconstant (stores constant encoded in template
    parameter)
    @tparam      T       The constant type
    @tparam      Expr    The expression type

    @param[in]   value   The value of the constant
    @param[in]   expr    The expression

    @return              The constant
 */
template<typename TC, typename T, typename Expr, EnumETL = EnumETL::GENERIC>
struct make_constant_impl
{
  static FDBB_INLINE auto constexpr eval(const T value, Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(static_cast<typename fdbb::backend::value_type<Expr>::type>(
      value))
#endif
  {
    return static_cast<typename fdbb::backend::value_type<Expr>::type>(value);
  }
};

/** @brief
    Selector for (specialized) implementation of
    `fdbb::backend::detail::make_explicit_temp_impl<Tag,Temp,Expr>(Expr&& expr)`
    function

    @tparam   Temp   The temporary type
    @tparam   Expr   The expression type
 */
template<typename Temp, typename Expr, typename = void>
struct get_make_explicit_temp_impl
  : public std::integral_constant<EnumETL, EnumETL::GENERIC>
{};

/** @brief
    Generic implementation of
    `fdbb::backend::detail::make_explicit_temp_impl<Tag,Temp,Expr>(Expr&& expr)`
    function

    @tparam      Tag    The unique tag identifier
    @tparam      Temp   The temporary type
    @tparam      Expr   The expression type

    @param[in]   expr   The expression

    @return             The tagged temporary
 */
template<std::size_t Tag,
         typename Temp,
         typename Expr,
         EnumETL = EnumETL::GENERIC>
struct make_explicit_temp_impl
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(Temp(std::forward<Expr>(expr)))
#endif
  {
    return Temp(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for (specialized) implementation of
    `fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr)` function

    @tparam   Expr   The expression type
 */
template<typename Expr, typename = void>
struct get_make_temp_impl
  : public std::integral_constant<EnumETL, EnumETL::GENERIC>
{};

/** @brief
    Generic implementation of
    `fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr)` function

    @tparam      Tag    The unique tag identifier
    @tparam      Expr   The expression type

    @param[in]   expr   The expression

    @return             The tagged temporary
 */
template<std::size_t Tag, typename Expr, EnumETL = EnumETL::GENERIC>
struct make_temp_impl
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::forward<Expr>(expr))
#endif
  {
    return std::forward<Expr>(expr);
  }
};

/** @brief
    Selector for (specialized) implementation of
    `fdbb::backend::detail::tag_impl<Tag,Expr>(Expr&& expr)` function

    @tparam   Expr   The expression type
 */
template<typename Expr, typename = void>
struct get_tag_impl : public std::integral_constant<EnumETL, EnumETL::GENERIC>
{};

/** @brief
    Generic implementation of
    `fdbb::backend::detail::tag_impl<Tag,Expr>(Expr&& expr)` function

    @tparam      Tag    The unique tag identifier
    @tparam      Expr   The expression type

    @param[in]   expr   The expression

    @return             The tagged expression
 */
template<std::size_t Tag, typename Expr, EnumETL = EnumETL::GENERIC>
struct tag_impl
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::forward<Expr>(expr))
#endif
  {
    return std::forward<Expr>(expr);
  }
};

/** @brief
    Helper macro for generating selectors for specialialized
    implementations of element-wise binary operations; generic
    implementation follows below
 */
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                  \
  /** @def FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS                       \
                                                                               \ \
      @brief                                                                     \
      Selector for (specialized) implementation of                               \
      `fdbb::backend::detail::elem_##OPNAME##_impl<A,B>(A&& a, B&& b)`           \
     function                                                                    \
                                                                               \ \
      @tparam   A   The first expression type                                    \
      @tparam   B   The second expression type                                   \
  */                                                                             \
  template<typename A, typename B, typename = void>                              \
  struct get_elem_##OPNAME##_impl                                                \
    : public std::integral_constant<EnumETL, EnumETL::GENERIC>                   \
  {};

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Element-wise multiplication (perfect forwarding to *-operator)

    @tparam      A         The first expression type
    @tparam      B         The second expression type
    @tparam      EnumETL   The expression type enumerator

    @param[in]   a         The first expression
    @param[in]   b         The second expression

    @return                The element-wise product of a and b
 */
template<typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_mul_impl
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a) * std::forward<B>(b))
#endif
  {
    return std::forward<A>(a) * std::forward<B>(b);
  }
};

/** @brief
    Element-wise division (perfect forwarding to /-operator)

    @tparam      A         The first expression type
    @tparam      B         The second expression type
    @tparam      EnumETL   The expression type enumerator

    @param[in]   a         The first expression
    @param[in]   b         The second expression

    @return                The element-wise division of a by b
 */
template<typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_div_impl
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a) / std::forward<B>(b))
#endif
  {
    return std::forward<A>(a) / std::forward<B>(b);
  }
};

/** @brief
    Element-wise raise to power (perfect forwarding to \a pow-function)

    @tparam      A         The first expression type
    @tparam      B         The second expression type
    @tparam      EnumETL   The expression type enumerator

    @param[in]   a         The first expression
    @param[in]   b         The second expression

    @return                The element-wise raise of a to the power b
 */
template<typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_pow_impl
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return pow(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
    Helper macro for generating selectors for specialized
    implementations of element-wise unary operations and generic
    implementation
 */
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for (specialized) implementation of
      fdbb::backend::detail::elem_##OPNAME##_impl<A>(A&& a) function

      @tparam   A         The expression type
      @tparam   EnumETL   The expression type enumerator
   */                                                                          \
  template<typename A, typename = void>                                        \
  struct get_elem_##OPNAME##_impl                                              \
    : public std::integral_constant<EnumETL, EnumETL::GENERIC>                 \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(a) function (perfect forwarding to \a
      OPNAME-function)

      @tparam      A         The expression type
      @tparam      EnumETL   The expression type enumerator

      @param[in]   a         The expression

      @return                The element-wise evaluation of OPNAME(a)
   */                                                                          \
  template<typename A, EnumETL = EnumETL::GENERIC>                             \
  struct elem_##OPNAME##_impl                                                  \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(OPNAME(std::forward<A>(a)))                                  \
    {                                                                          \
      return OPNAME(std::forward<A>(a));                                       \
    }                                                                          \
  };

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace detail
} // namespace backend

/// Forward declaration
template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant(const T value,
                                                Expr&& expr) noexcept
  -> decltype(fdbb::backend::detail::make_constant_impl<
              TC,
              T,
              Expr,
              fdbb::backend::detail::get_make_constant_impl<Expr>::value>::
                eval(value, std::forward<Expr>(expr)));

/// Forward declaration
template<std::size_t Tag, typename Temp, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<
    !std::is_same<Temp, Expr>::value,
    decltype(fdbb::backend::detail::make_explicit_temp_impl<
             Tag,
             Temp,
             Expr,
             fdbb::backend::detail::get_make_explicit_temp_impl<Temp, Expr>::
               value>::eval(std::forward<Expr>(expr)))>::type;

/// Forward declaration
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept
  -> decltype(fdbb::backend::detail::make_temp_impl<
              Tag,
              Expr,
              fdbb::backend::detail::get_make_temp_impl<Expr>::value>::
                eval(std::forward<Expr>(expr)));

/// Forward declaration
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag(Expr&& expr) noexcept
  -> decltype(fdbb::backend::detail::tag_impl<
              Tag,
              Expr,
              fdbb::backend::detail::get_tag_impl<Expr>::value>::
                eval(std::forward<Expr>(expr)));

/// Forward declaration
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template<typename A, typename B>                                             \
  auto constexpr elem_##OPNAME(A&& a, B&& b) noexcept                          \
    ->decltype(fdbb::backend::detail::elem_##OPNAME##_impl<                    \
               A,                                                              \
               B,                                                              \
               fdbb::backend::detail::get_elem_##OPNAME##_impl<A, B>::value>:: \
                 eval(std::forward<A>(a), std::forward<B>(b)));

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Forward declaration
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  template<typename A>                                                         \
  auto constexpr elem_##OPNAME(A&& a) noexcept                                 \
    ->decltype(fdbb::backend::detail::elem_##OPNAME##_impl<                    \
               A,                                                              \
               fdbb::backend::detail::get_elem_##OPNAME##_impl<A>::value>::    \
                 eval(std::forward<A>(a)));

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace fdbb

#endif // FDBB_BACKEND_FORWARD_DECLARATIONS_HPP
