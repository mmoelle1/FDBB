/** @file fdbb/Backend/ublas.hpp
 *
 *  @brief Implementation details for uBLAS library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_UBLAS_HPP
#define BACKEND_UBLAS_HPP

#ifdef FDBB_BACKEND_UBLAS

#include <boost/numeric/ublas/expression_types.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Constant.hpp>
#include <Core/Utils.hpp>

/// @brief Extensions of the BOOST library namespace \ref boost by the
/// FDBB library
namespace boost {
/// @brief Extensions of the BOOST library namespace \ref
/// boost::numeric by the FDBB library
namespace numeric {
/// @brief Extensions of the BOOST library namespace \ref
/// boost::numeric::ublas by the FDBB library
namespace ublas {

/** @brief
    Deduces the temporary of the expression

    @tparam The type from which to deduce the temporary
 */
template<typename E>
struct temporary_traits
{
  using result_type = E;
  using value_type = E;
};

/** @brief
    Deduces the common type of two expressions

    @tparam The types from which to deduce the common type
 */
template<typename E1, typename E2>
struct common_traits
{
private:
  using type1 = temporary_traits<E1>;
  using type2 = temporary_traits<E2>;

public:
  using type = common_traits<type1, type2>;
};

/// @brief Non-terminal type: vector_unary
template<typename E, typename F>
struct temporary_traits<boost::numeric::ublas::vector_unary<E, F>>
{
  using result_type = typename temporary_traits<E>::result_type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: vector_scalar_unary
template<typename E, typename F>
struct temporary_traits<boost::numeric::ublas::vector_scalar_unary<E, F>>
{
  using result_type = typename temporary_traits<E>::result_type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: vector_binary
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::vector_binary<E1, E2, F>>
{
  using result_type =
    typename common_traits<typename temporary_traits<E1>::result_type,
                           typename temporary_traits<E2>::result_type>::type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: vector_binary_scalar1
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::vector_binary_scalar1<E1, E2, F>>
{
  using value_type = typename temporary_traits<E2>::value_type;
  using result_type = typename temporary_traits<E2>::result_type;
};

/// @brief Non-terminal type: vector_binary_scalar2
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::vector_binary_scalar2<E1, E2, F>>
{
  using value_type = typename temporary_traits<E1>::value_type;
  using result_type = typename temporary_traits<E1>::result_type;
};

/// @brief Non-terminal type: vector_scalar_binary
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::vector_scalar_binary<E1, E2, F>>
{
  using result_type =
    typename common_traits<typename temporary_traits<E1>::result_type,
                           typename temporary_traits<E2>::result_type>::type;
  using value_type = typename result_type::value_type;
};

/// @brief Terminal type: vector_reference
template<typename T>
struct temporary_traits<boost::numeric::ublas::vector_reference<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::vector<T>;
};

/// @brief Terminal type: vector
template<typename T>
struct temporary_traits<boost::numeric::ublas::vector<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::vector<T>;
};

/// @brief Terminal type: mapped_vector
template<typename T>
struct temporary_traits<boost::numeric::ublas::mapped_vector<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::mapped_vector<T>;
};

/// @brief Terminal type: compressed_vector
template<typename T>
struct temporary_traits<boost::numeric::ublas::compressed_vector<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::compressed_vector<T>;
};

/// @brief Terminal type: coordinate_vector
template<typename T>
struct temporary_traits<boost::numeric::ublas::coordinate_vector<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::coordinate_vector<T>;
};

/// @brief Terminal type: unit_vector
template<typename T>
struct temporary_traits<boost::numeric::ublas::unit_vector<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::unit_vector<T>;
};

/// @brief Terminal type: zero_vector
template<typename T>
struct temporary_traits<boost::numeric::ublas::zero_vector<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::zero_vector<T>;
};

/// @brief Terminal types: vector, vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::vector<T1>,
                     boost::numeric::ublas::vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: vector, compressed_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::vector<T1>,
                     boost::numeric::ublas::compressed_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: vector, coordinate_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::vector<T1>,
                     boost::numeric::ublas::coordinate_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: vector, mapped_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::vector<T1>,
                     boost::numeric::ublas::mapped_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_vector, vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_vector<T1>,
                     boost::numeric::ublas::vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_vector, compressed_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_vector<T1>,
                     boost::numeric::ublas::compressed_vector<T2>>
{
  using type = boost::numeric::ublas::compressed_vector<
    typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_vector, coordinate_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_vector<T1>,
                     boost::numeric::ublas::coordinate_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_vector, mapped_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_vector<T1>,
                     boost::numeric::ublas::mapped_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_vector, vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_vector<T1>,
                     boost::numeric::ublas::vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_vector, compressed_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_vector<T1>,
                     boost::numeric::ublas::compressed_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_vector, coordinate_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_vector<T1>,
                     boost::numeric::ublas::coordinate_vector<T2>>
{
  using type = boost::numeric::ublas::coordinate_vector<
    typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_vector, mapped_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_vector<T1>,
                     boost::numeric::ublas::mapped_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_vector, vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_vector<T1>,
                     boost::numeric::ublas::vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_vector, compressed_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_vector<T1>,
                     boost::numeric::ublas::compressed_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_vector, coordinate_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_vector<T1>,
                     boost::numeric::ublas::coordinate_vector<T2>>
{
  using type =
    boost::numeric::ublas::vector<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_vector, mapped_vector
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_vector<T1>,
                     boost::numeric::ublas::mapped_vector<T2>>
{
  using type = boost::numeric::ublas::mapped_vector<
    typename std::common_type<T1, T2>::type>;
};

/// @brief Non-terminal type: matrix_unary1
template<typename E, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_unary1<E, F>>
{
  using result_type = typename temporary_traits<E>::result_type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: matrix_unary2
template<typename E, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_unary2<E, F>>
{
  using result_type = typename temporary_traits<E>::result_type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: matrix_scalar_unary
template<typename E, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_scalar_unary<E, F>>
{
  using result_type = typename temporary_traits<E>::result_type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: matrix_binary
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_binary<E1, E2, F>>
{
  using result_type =
    typename common_traits<typename temporary_traits<E1>::result_type,
                           typename temporary_traits<E2>::result_type>::type;
  using value_type = typename result_type::value_type;
};

/// @brief Non-terminal type: matrix_binary_scalar1
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_binary_scalar1<E1, E2, F>>
{
  using value_type = typename temporary_traits<E2>::value_type;
  using result_type = typename temporary_traits<E2>::result_type;
};

/// @brief Non-terminal type: matrix_binary_scalar2
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_binary_scalar2<E1, E2, F>>
{
  using value_type = typename temporary_traits<E1>::value_type;
  using result_type = typename temporary_traits<E1>::result_type;
};

/// @brief Non-terminal type: matrix_vector_binary1
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_vector_binary1<E1, E2, F>>
{
  using value_type = typename temporary_traits<E2>::value_type;
  using result_type = typename temporary_traits<E2>::result_type;
};

/// @brief Non-terminal type: matrix_vector_binary2
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_vector_binary2<E1, E2, F>>
{
  using value_type = typename temporary_traits<E1>::value_type;
  using result_type = typename temporary_traits<E1>::result_type;
};

/// @brief Non-terminal type: matrix_matrix_binary
template<typename E1, typename E2, typename F>
struct temporary_traits<boost::numeric::ublas::matrix_matrix_binary<E1, E2, F>>
{
  using result_type =
    typename common_traits<typename temporary_traits<E1>::result_type,
                           typename temporary_traits<E2>::result_type>::type;
  using value_type = typename result_type::value_type;
};

/// @brief Terminal type: matrix_reference
template<typename T>
struct temporary_traits<boost::numeric::ublas::matrix_reference<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::matrix<T>;
};

/// @brief Terminal type: matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::matrix<T>;
};

/// @brief Terminal type: mapped_matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::mapped_matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::mapped_matrix<T>;
};

/// @brief Terminal type: compressed_matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::compressed_matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::compressed_matrix<T>;
};

/// @brief Terminal type: coordinate_matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::coordinate_matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::coordinate_matrix<T>;
};

/// @brief Terminal type: identity_matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::identity_matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::identity_matrix<T>;
};

/// @brief Terminal type: scalar_matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::scalar_matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::scalar_matrix<T>;
};

/// @brief Terminal type: zero_matrix
template<typename T>
struct temporary_traits<boost::numeric::ublas::zero_matrix<T>>
{
  using value_type = T;
  using result_type = boost::numeric::ublas::zero_matrix<T>;
};

/// @brief Terminal types: matrix, matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::matrix<T1>,
                     boost::numeric::ublas::matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: matrix, compressed_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::matrix<T1>,
                     boost::numeric::ublas::compressed_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: matrix, coordinate_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::matrix<T1>,
                     boost::numeric::ublas::coordinate_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: matrix, mapped_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::matrix<T1>,
                     boost::numeric::ublas::mapped_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_matrix, matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_matrix<T1>,
                     boost::numeric::ublas::matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_matrix, compressed_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_matrix<T1>,
                     boost::numeric::ublas::compressed_matrix<T2>>
{
  using type = boost::numeric::ublas::compressed_matrix<
    typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_matrix, coordinate_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_matrix<T1>,
                     boost::numeric::ublas::coordinate_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: compressed_matrix, mapped_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::compressed_matrix<T1>,
                     boost::numeric::ublas::mapped_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_matrix, matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_matrix<T1>,
                     boost::numeric::ublas::matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_matrix, compressed_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_matrix<T1>,
                     boost::numeric::ublas::compressed_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_matrix, coordinate_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_matrix<T1>,
                     boost::numeric::ublas::coordinate_matrix<T2>>
{
  using type = boost::numeric::ublas::coordinate_matrix<
    typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: coordinate_matrix, mapped_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::coordinate_matrix<T1>,
                     boost::numeric::ublas::mapped_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_matrix, matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_matrix<T1>,
                     boost::numeric::ublas::matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_matrix, compressed_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_matrix<T1>,
                     boost::numeric::ublas::compressed_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_matrix, coordinate_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_matrix<T1>,
                     boost::numeric::ublas::coordinate_matrix<T2>>
{
  using type =
    boost::numeric::ublas::matrix<typename std::common_type<T1, T2>::type>;
};

/// @brief Terminal types: mapped_matrix, mapped_matrix
template<typename T1, typename T2>
struct common_traits<boost::numeric::ublas::mapped_matrix<T1>,
                     boost::numeric::ublas::mapped_matrix<T2>>
{
  using type = boost::numeric::ublas::mapped_matrix<
    typename std::common_type<T1, T2>::type>;
};

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a vector expression
    and a functor implementing a unary operation and applies it to all
    components of the vector expression.

    (op v) [i] = op( v [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E>
FDBB_INLINE
  typename boost::numeric::ublas::vector_unary_traits<E, OP>::result_type
  apply_to_all(const boost::numeric::ublas::vector_expression<E>& e,
               const OP& op = OP())
{
  using expression_type =
    typename boost::numeric::ublas::vector_unary_traits<E, OP>::expression_type;
  return expression_type(e());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a matrix expression
    and a functor implementing a unary1 operation and applies it to all
    components of the vector expression.

    (op v) [i] = op( v [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E>
FDBB_INLINE
  typename boost::numeric::ublas::matrix_unary1_traits<E, OP>::result_type
  apply_to_all(const boost::numeric::ublas::matrix_expression<E>& e,
               const OP& op = OP())
{
  using expression_type =
    typename boost::numeric::ublas::matrix_unary1_traits<E,
                                                         OP>::expression_type;
  return expression_type(e());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a matrix expression
    and a functor implementing a unary2 operation and applies it to all
    components of the vector expression.

    (op v) [i] = op( v [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E>
FDBB_INLINE
  typename boost::numeric::ublas::matrix_unary2_traits<E, OP>::result_type
  apply_to_all(const boost::numeric::ublas::matrix_expression<E>& e,
               const OP& op = OP())
{
  using expression_type =
    typename boost::numeric::ublas::matrix_unary2_traits<E,
                                                         OP>::expression_type;
  return expression_type(e());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts two vector expressions
    and a functor implementing a binary operation and applies it to all
    components of the vector expression.

    (op v1 v2) [i] = op( v1 [i], v2 [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E1, class E2>
FDBB_INLINE
  typename boost::numeric::ublas::vector_binary_traits<E1, E2, OP>::result_type
  apply_to_all(const boost::numeric::ublas::vector_expression<E1>& e1,
               const boost::numeric::ublas::vector_expression<E2>& e2,
               const OP& op = OP())
{
  using expression_type = typename boost::numeric::ublas::
    vector_binary_traits<E1, E2, OP>::expression_type;
  return expression_type(e1(), e2());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts two matrix expressions
    and a functor implementing a binary operation and applies it to all
    components of the vector expression.

    (op v1 v2) [i] = op( v1 [i], v2 [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E1, class E2>
FDBB_INLINE
  typename boost::numeric::ublas::matrix_binary_traits<E1, E2, OP>::result_type
  apply_to_all(const boost::numeric::ublas::matrix_expression<E1>& e1,
               const boost::numeric::ublas::matrix_expression<E2>& e2,
               const OP& op = OP())
{
  using expression_type = typename boost::numeric::ublas::
    matrix_binary_traits<E1, E2, OP>::expression_type;
  return expression_type(e1(), e2());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a scalar and a vector
    expression and a functor implementing a binary operation and
    applies it to all components of the vector expression.

    (op t v) [i] = op( t , v [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class T1, class E2>
FDBB_INLINE typename boost::numeric::ublas::
  vector_binary_scalar1_traits<T1, const E2, OP>::result_type
  apply_to_all1(const T1& e1,
                const boost::numeric::ublas::vector_expression<E2>& e2,
                const OP& op = OP())
{
  using expression_type = typename boost::numeric::ublas::
    vector_binary_scalar1_traits<const T1, E2, OP>::expression_type;
  return expression_type(e1, e2());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a vector and a scalar
    expression and a functor implementing a binary operation and
    applies it to all components of the vector expression.

    (op v t) [i] = op( v [i], t )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E1, class T2>
FDBB_INLINE typename boost::numeric::ublas::
  vector_binary_scalar2_traits<E1, const T2, OP>::result_type
  apply_to_all2(const boost::numeric::ublas::vector_expression<E1>& e1,
                const T2& e2,
                const OP& op = OP())
{
  using expression_type = typename boost::numeric::ublas::
    vector_binary_scalar2_traits<E1, const T2, OP>::expression_type;
  return expression_type(e1(), e2);
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a scalar and a matrix
    expression and a functor implementing a binary operation and
    applies it to all components of the matrix expression.

    (op t v) [i] = op( t , v [i] )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class T1, class E2>
FDBB_INLINE typename boost::numeric::ublas::
  matrix_binary_scalar1_traits<T1, const E2, OP>::result_type
  apply_to_all1(const T1& e1,
                const boost::numeric::ublas::matrix_expression<E2>& e2,
                const OP& op = OP())
{
  using expression_type = typename boost::numeric::ublas::
    matrix_binary_scalar1_traits<const T1, E2, OP>::expression_type;
  return expression_type(e1, e2());
}

/** @brief
    uBLAS does not provide any element-wise operations except for
    multiplication and division. The following implementation realizes
    a generic apply-to-all function that accepts a matrix and a scalar
    expression and a functor implementing a binary operation and
    applies it to all components of the matrix expression.

    (op v t) [i] = op( v [i], t )

    @note
    This implementation is based on Example 1 "Vectorize" standard
    math functions from "How To Extend UBLAS"
    http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?Examples_-_How_To_Extend_UBLAS
 */
template<class OP, class E1, class T2>
FDBB_INLINE typename boost::numeric::ublas::
  matrix_binary_scalar2_traits<E1, const T2, OP>::result_type
  apply_to_all2(const boost::numeric::ublas::matrix_expression<E1>& e1,
                const T2& e2,
                const OP& op = OP())
{
  using expression_type = typename boost::numeric::ublas::
    matrix_binary_scalar2_traits<E1, const T2, OP>::expression_type;
  return expression_type(e1(), e2);
}

/// @brief Extensions of the BOOST library namespace \ref
/// boost::numeric::ublas::functor by the FDBB library
namespace functor {

/// Helper macro for generating element-wise unary operations
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
                                                                               \
  /** @brief Functor implementing the unary operation OPNAME(v) */             \
  template<class T>                                                            \
  class OPNAME                                                                 \
  {                                                                            \
  public:                                                                      \
    using value_type = T;                                                      \
    using result_type = T;                                                     \
    OPNAME() {}                                                                \
                                                                               \
    static result_type apply(const value_type& x) { return std::OPNAME(x); }   \
  };

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Functor implementing the binary operation pow(v,t)
 */
template<class T1, class T2>
class pow
{
public:
  using value_type = typename std::common_type<T1, T2>::type;
  using result_type = typename std::common_type<T1, T2>::type;
  pow() {}

  static result_type apply(const T1& x, const T2& y) { return std::pow(x, y); }
};

/** @brief
    Functor implementing the unary operation exp10(v)
 */
template<class T>
class exp10
{
public:
  using value_type = T;
  using result_type = T;
  exp10() {}

  static result_type apply(const value_type& x)
  {
    return std::exp(
      x * 2.302585092994045684017991454684364207601101488628772976033);
  }
};

/** @brief
    Functor implementing the unary operation rsqrt(v)
 */
template<class T>
class rsqrt
{
public:
  using value_type = T;
  using result_type = T;
  rsqrt() {}

  static result_type apply(const value_type& x) { return std::pow(x, -0.5); }
};

/** @brief
    Functor implementing the unary operation sign(v)
 */
template<class T>
class sign
{
public:
  using value_type = T;
  using result_type = T;
  sign() {}

  static result_type apply(const value_type& x)
  {
    return (x > 0.0 ? 1.0 : x < 0.0 ? -1.0 : 0.0);
  }
};

} // namespace functor
} // namespace ublas
} // namespace numeric
} // namespace boost

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::UBLAS, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::UBLAS,
  typename std::enable_if<std::is_same<
    typename fdbb::utils::remove_all<T>::type::expression_type,
    typename fdbb::utils::remove_all<T>::type::expression_type>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (uBLAS type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::UBLAS>::type>
{
  using type = typename boost::numeric::ublas::temporary_traits<
    typename fdbb::utils::remove_all<Expr>::type>::result_type;
};

/** @brief
    Scalar value type of the expression (uBLAS type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::UBLAS>::type>
{
  using type = typename boost::numeric::ublas::temporary_traits<
    typename fdbb::utils::remove_all<Expr>::type>::value_type;
};

namespace detail {

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::UBLAS>::type>
  : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    uBLAS type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::UBLAS>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::backend::result_type<Expr>::type
#endif
  {
    using Temp = typename fdbb::backend::result_type<Expr>::type;
    return Temp(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename std::enable_if<std::is_same<
    decltype(boost::numeric::ublas::element_prod(std::declval<A>(),
                                                 std::declval<B>())),
    decltype(boost::numeric::ublas::element_prod(std::declval<A>(),
                                                 std::declval<B>()))>::value>::
    type> : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    Element-wise multiplication of uBLAS types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::UBLAS>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::element_prod(std::forward<A>(a),
                                                    std::forward<B>(b)))
#endif
  {
    return boost::numeric::ublas::element_prod(std::forward<A>(a),
                                               std::forward<B>(b));
  }
};

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename std::enable_if<std::is_same<
    decltype(boost::numeric::ublas::element_div(std::declval<A>(),
                                                std::declval<B>())),
    decltype(boost::numeric::ublas::element_div(std::declval<A>(),
                                                std::declval<B>()))>::value>::
    type> : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    Element-wise division of uBLAS types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::UBLAS>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::element_div(std::forward<A>(a),
                                                   std::forward<B>(b)))
#endif
  {
    return boost::numeric::ublas::element_div(std::forward<A>(a),
                                              std::forward<B>(b));
  }
};

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_pow<A>(A&& a, B&& b) function for A
    being of uBLAS type and B being of arithmetic type
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of_and_cond<A,
                                            EnumETL::UBLAS,
                                            std::is_arithmetic<B>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::UBLAS_SCALAR>
{};

/** @brief
    Element-wise pow function for uBLAS types for A being of uBLAS
    type and B being of arithmetic type
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::UBLAS_SCALAR>
{
  // A is of uBLAS type, B is arithmetic type
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::pow(std::forward<A>(a),
                                           std::forward<B>(b)))
  //    -> decltype(
  //      boost::numeric::ublas::apply_to_all2<boost::numeric::ublas::functor::pow<
  //        typename fdbb::utils::remove_all<A>::type::value_type,
  //        typename fdbb::utils::remove_all<B>::type>>(a, b))
#endif
  {
    return boost::numeric::ublas::pow(std::forward<A>(a), std::forward<B>(b));
    // return boost::numeric::ublas::apply_to_all2<
    //   boost::numeric::ublas::functor::pow<
    //     typename fdbb::utils::remove_all<A>::type::value_type,
    //     typename fdbb::utils::remove_all<B>::type>>(a, b);
  }
};

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_pow<A>(A&& a, B&& b) function for A and B being of
    uBLAS
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::UBLAS>::type>
  : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    Element-wise pow function for uBLAS types for A and B being of
    uBLAS type
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::UBLAS>
{
  // A is of uBLAS type, B is arithmetic type
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::element_pow(std::forward<A>(a),
                                                   std::forward<B>(b)))
#endif
  {
    return boost::numeric::ublas::element_pow(std::forward<A>(a),
                                              std::forward<B>(b));
  }
};

/// Helper macro for generating element-wise unary operations
#if !defined(DOXYGEN)
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized uBLAS implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
 */                                                                            \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::UBLAS>::type>                 \
    : public std::integral_constant<EnumETL, EnumETL::UBLAS>                   \
  {};                                                                          \
                                                                               \
  /** @brief Element-wise \a OPNAME(x) function for x being of uBLAS types */  \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::UBLAS>                               \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(boost::numeric::ublas::apply_to_all<                         \
                  boost::numeric::ublas::functor::OPNAME<                      \
                    typename fdbb::utils::remove_all<A>::type::value_type>>(   \
        a))                                                                    \
    {                                                                          \
      return boost::numeric::ublas::apply_to_all<                              \
        boost::numeric::ublas::functor::OPNAME<                                \
          typename fdbb::utils::remove_all<A>::type::value_type>>(a);          \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
                                                                               \
  /** @brief
      Selector for specialized uBLAS implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::UBLAS>::type>                 \
    : public std::integral_constant<EnumETL, EnumETL::UBLAS>                   \
  {};                                                                          \
                                                                               \
  /** @brief Element-wise OPNAME(x) function for x being of uBLAS type */      \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::UBLAS>                               \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
    {                                                                          \
      return boost::numeric::ublas::apply_to_all<                              \
        boost::numeric::ublas::functor::OPNAME<                                \
          typename fdbb::utils::remove_all<A>::type::value_type>>(a);          \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::UBLAS>::type>
  : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    Element-wise \a exp10(x) function for x being of uBLAS type
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::UBLAS>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::apply_to_all<
                boost::numeric::ublas::functor::exp10<
                  typename fdbb::utils::remove_all<A>::type::value_type>>(a))
#endif
  {
    return boost::numeric::ublas::apply_to_all<
      boost::numeric::ublas::functor::exp10<
        typename fdbb::utils::remove_all<A>::type::value_type>>(a);
  }
};

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::UBLAS>::type>
  : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    Element-wise \a rsqrt(x) function for x being of uBLAS type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::UBLAS>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::apply_to_all<
                boost::numeric::ublas::functor::rsqrt<
                  typename fdbb::utils::remove_all<A>::type::value_type>>(a))
#endif
  {
    return boost::numeric::ublas::apply_to_all<
      boost::numeric::ublas::functor::rsqrt<
        typename fdbb::utils::remove_all<A>::type::value_type>>(a);
  }
};

/** @brief
    Selector for specialized uBLAS implementation of
    fdbb::elem_sign<A>(A&& a) function
 */
template<typename A>
struct get_elem_sign_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::UBLAS>::type>
  : public std::integral_constant<EnumETL, EnumETL::UBLAS>
{};

/** @brief
    Element-wise \a sign(x) function for x being of uBLAS type
 */
template<typename A>
struct elem_sign_impl<A, EnumETL::UBLAS>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(boost::numeric::ublas::apply_to_all<
                boost::numeric::ublas::functor::sign<
                  typename fdbb::utils::remove_all<A>::type::value_type>>(a))
#endif
  {
    return boost::numeric::ublas::apply_to_all<
      boost::numeric::ublas::functor::sign<
        typename fdbb::utils::remove_all<A>::type::value_type>>(a);
  }
};

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // FDBB_BACKEND_UBLAS
#endif // BACKEND_UBLAS_HPP
