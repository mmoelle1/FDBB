/** @file fdbb/Backend/armadillo.hpp
 *
 *  @brief Implementation details for Armadillo library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_ARMADILLO_HPP
#define BACKEND_ARMADILLO_HPP

#ifdef FDBB_BACKEND_ARMADILLO

#include <armadillo>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Constant.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::ARMADILLO, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::ARMADILLO,
  typename std::enable_if<
    arma::is_arma_type<typename fdbb::utils::remove_all<T>::type>::value ||
    arma::is_arma_cube_type<typename fdbb::utils::remove_all<T>::type>::value ||
    arma::is_arma_sparse_type<
      typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (dense column vector of Armadillo type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::ARMADILLO,
                     fdbb::utils::remove_all<Expr>::type::is_col>::type>
{
  using type =
    typename arma::Col<typename fdbb::utils::remove_all<Expr>::type::elem_type>;
};

/** @brief
    Result type of the expression (dense row vector of Armadillo type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::ARMADILLO,
                     fdbb::utils::remove_all<Expr>::type::is_row>::type>
{
  using type =
    typename arma::Row<typename fdbb::utils::remove_all<Expr>::type::elem_type>;
};

/** @brief
    Result type of the expression (dense matrix being of Armadillo type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of_and_cond<
    Expr,
    EnumETL::ARMADILLO,
    arma::is_arma_type<typename fdbb::utils::remove_all<Expr>::type>::value &&
      !(fdbb::utils::remove_all<Expr>::type::is_col ||
        fdbb::utils::remove_all<Expr>::type::is_row)>::type>
{
  using type =
    typename arma::Mat<typename fdbb::utils::remove_all<Expr>::type::elem_type>;
};

/** @brief
    Result type of the expression (sparse matrix being of Armadillo type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::ARMADILLO,
                     arma::is_arma_sparse_type<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type = typename arma::SpMat<
    typename fdbb::utils::remove_all<Expr>::type::elem_type>;
};

/** @brief
    Result type of the expression (2D matrix being of Armadillo type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::ARMADILLO,
                     arma::is_arma_cube_type<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type = typename arma::Cube<
    typename fdbb::utils::remove_all<Expr>::type::elem_type>;
};

/** @brief
    Scalar value type of the expression (Armadillo type)
 */
template<typename Expr>
struct value_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::ARMADILLO>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::elem_type;
};

namespace detail {

/** @brief
    Selector for specialized Armadillo implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::ARMADILLO>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARMADILLO>
{};

/** @brief
    Armadillo terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::ARMADILLO>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::backend::result_type<Expr>::type
#endif
  {
    using Temp = typename fdbb::backend::result_type<Expr>::type;
    return Temp(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized Armadillo implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::ARMADILLO>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARMADILLO>
{};

/** @brief
    Element-wise multiplication of Armadillo types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::ARMADILLO>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a) % std::forward<B>(b))
#endif
  {
    return std::forward<A>(a) % std::forward<B>(b);
  }
};

/** @brief
    Selector for specialized Armadillo implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARMADILLO>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARMADILLO>
{};

/** @brief
    Element-wise \a fabs(a) function for \a a being of Armadillo types

    @note
    Armadillo does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::ARMADILLO>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Armadillo implementation of
    fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ARMADILLO>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARMADILLO>
{};

/** @brief
    Element-wise \a rsqrt(a) function for \a a being of Armadillo types

    @note
    Armadillo does not provide rsqrt.
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::ARMADILLO>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(std::forward<A>(a), CONSTANT(-0.5, a)))
#endif
  {
    return pow(std::forward<A>(a), CONSTANT(-0.5, a));
  }
};

/** @brief
    Selector for specialized Armadillo implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function for \a a
    and \a b being of Armadillo type
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::ARMADILLO>::type>
  : public std::integral_constant<EnumETL, EnumETL::ARMADILLO>
{};

/** @brief
    Element-wise \a pow(a,b) function for \a a and \a b being of
    Armadillo types
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::ARMADILLO>
{
  static FDBB_INLINE A eval(A&& a, B&& b) noexcept
  {
    auto it_b = b.begin();
    for (auto it_a = a.begin(); it_a != a.end(); it_a++, it_b++)
      *it_a = std::pow(*it_a, *it_b);

    return a;
  }
};

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // FDBB_BACKEND_ARMADILLO
#endif // BACKEND_ARMADILLO_HPP
