/** @file fdbb/Backend/cache.hpp
 *
 *  @brief Implementation details for caching mechanism
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_CACHE_HPP
#define BACKEND_CACHE_HPP

#include <type_traits>

#include <Cache/Cache.hpp>
#include <Cache/Cache2.hpp>

namespace fdbb {

/// Forward declaration
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template<typename A, typename B>                                             \
  auto constexpr elem_##OPNAME(A&& a, B&& b) noexcept                          \
    ->decltype(fdbb::backend::detail::elem_##OPNAME##_impl<                    \
               A,                                                              \
               B,                                                              \
               fdbb::backend::detail::get_elem_##OPNAME##_impl<A, B>::value>:: \
                 eval(std::forward<A>(a), std::forward<B>(b)));

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Forward declaration
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  template<typename A>                                                         \
  auto constexpr elem_##OPNAME(A&& a) noexcept                                 \
    ->decltype(fdbb::backend::detail::elem_##OPNAME##_impl<                    \
               A,                                                              \
               fdbb::backend::detail::get_elem_##OPNAME##_impl<A>::value>::    \
                 eval(std::forward<A>(a)));

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

namespace backend {

/** @brief
    If T is of type EnumETL::CACHE, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::CACHE,
                  typename std::enable_if<std::is_base_of<
                    fdbb::cache::CacheExprBase,
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    If T is of type EnumETL::CACHE2, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::CACHE2,
                  typename std::enable_if<std::is_base_of<
                    fdbb::cache2::CacheExprBase,
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (CACHE type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::result_type;
};

/** @brief
    Result type of the expression (CACHE2 type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE2>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::result_type;
};

/** @brief
    Scalar value type of the expression (CACHE type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE>::type>
{
  using type = typename fdbb::backend::value_type<
    typename fdbb::backend::result_type<Expr>::type>::type;
};

/** @brief
    Scalar value type of the expression (CACHE2 type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE2>::type>
{
  using type = typename fdbb::backend::value_type<
    typename fdbb::backend::result_type<Expr>::type>::type;
};

namespace detail {

/** @brief
    Selector for specialized CACHE implementation of
    fdbb::backend::detail::make_constant_impl<T,Expr>(const T value, Expr&&
    expr) function
 */
template<typename Expr>
struct get_make_constant_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, fdbb::EnumETL::CACHE>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE>
{};

/** @brief
    Create CACHE constant
 */
template<typename TC, typename T, typename Expr>
struct make_constant_impl<TC, T, Expr, EnumETL::CACHE>
{
  static FDBB_INLINE auto constexpr eval(const T value, Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::cache::CacheExpr<TC::tag, T>
#endif
  {
    return fdbb::cache::CacheExpr<TC::tag, T>(T(value));
  }
};

/** @brief
    Selector for specialized CACHE implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE>
{};

/** @brief
    CACHE terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::CACHE>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_temp<Tag,
                                typename fdbb::backend::result_type<
                                  decltype(expr.get())>::type>(expr.get()))
#endif
  {
    return fdbb::make_temp<
      Tag,
      typename fdbb::backend::result_type<decltype(expr.get())>::type>(
      expr.get());
  }
};

/** @brief
    Selector for specialized CACHE2 implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE2>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE2>
{};

/** @brief
    CACHE2 terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::CACHE2>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_temp<Tag,
                                typename fdbb::backend::result_type<
                                  decltype(expr.get())>::type>(expr.get()))
#endif
  {
    return fdbb::make_temp<
      Tag,
      typename fdbb::backend::result_type<decltype(expr.get())>::type>(
      expr.get());
  }
};

/** @brief
    Selector for specialized CACHE implementation of
    fdbb::detail::tag_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_tag_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE>
{};

/** @brief
    Tags terminal with a unique (in a single expression) tag.

    By tagging terminals user guarantees that the terminals with same
    tags actually refer to the same data.
 */
template<std::size_t Tag, typename Expr>
struct tag_impl<Tag, Expr, EnumETL::CACHE>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::cache::CacheExprView<Tag, decltype(expr.get())>
#endif
  {
    return fdbb::cache::CacheExprView<Tag, decltype(expr.get())>(expr.get());
  }
};

/** @brief
    Selector for specialized CACHE2 implementation of
    fdbb::detail::tag_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_tag_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::CACHE2>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE2>
{};

/** @brief
    Tags terminal with a unique (in a single expression) tag.

    By tagging terminals user guarantees that the terminals with same
    tags actually refer to the same data.
 */
template<std::size_t Tag, typename Expr>
struct tag_impl<Tag, Expr, EnumETL::CACHE2>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::cache2::CacheExprView<Tag, decltype(expr.get())>
#endif
  {
    return fdbb::cache2::CacheExprView<Tag, decltype(expr.get())>(expr.get());
  }
};

/** @brief
    Selector for specialized CACHE implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::CACHE>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE>
{};

/** @brief
    Element-wise multiplication of CACHE types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::CACHE>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> fdbb::cache::CacheExprBinaryOp_elem_mul<
      fdbb::utils::hash<std::size_t>('m',
                                     'u',
                                     'l',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<B>::type::tag),
      A,
      B>
#endif
  {
    return fdbb::cache::CacheExprBinaryOp_elem_mul<
      fdbb::utils::hash<std::size_t>('m',
                                     'u',
                                     'l',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<B>::type::tag),
      A,
      B>(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
    Selector for specialized CACHE2 implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::CACHE2>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE2>
{};

/** @brief
    Element-wise multiplication of CACHE2 types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::CACHE2>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::cache2::CacheExpr<
                fdbb::utils::hash<std::size_t>(
                  'm',
                  'u',
                  'l',
                  fdbb::utils::remove_all<A>::type::tag,
                  fdbb::utils::remove_all<B>::type::tag),
                decltype(fdbb::elem_mul(a.get(), b.get()))>(
      std::move(fdbb::elem_mul(a.get(), b.get()))))
#endif
  {
    return fdbb::cache2::CacheExpr<fdbb::utils::hash<std::size_t>(
                                     'm',
                                     'u',
                                     'l',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<B>::type::tag),
                                   decltype(fdbb::elem_mul(a.get(), b.get()))>(
      std::move(fdbb::elem_mul(a.get(), b.get())));
  }
};

/** @brief
    Selector for specialized CACHE implementation of
    fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::CACHE>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE>
{};

/** @brief
    Element-wise division of CACHE types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::CACHE>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> fdbb::cache::CacheExprBinaryOp_elem_div<
      fdbb::utils::hash<std::size_t>('d',
                                     'i',
                                     'v',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<B>::type::tag),
      A,
      B>
#endif
  {
    return fdbb::cache::CacheExprBinaryOp_elem_div<
      fdbb::utils::hash<std::size_t>('d',
                                     'i',
                                     'v',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<B>::type::tag),
      A,
      B>(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
    Selector for specialized CACHE2 implementation of
    fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::CACHE2>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE2>
{};

/** @brief
    Element-wise division of CACHE2 types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::CACHE2>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::cache2::CacheExpr<
                fdbb::utils::hash<std::size_t>(
                  'd',
                  'i',
                  'v',
                  fdbb::utils::remove_all<A>::type::tag,
                  fdbb::utils::remove_all<B>::type::tag),
                decltype(fdbb::elem_div(a.get(), b.get()))>(
      std::move(fdbb::elem_div(a.get(), b.get()))))
#endif
  {
    return fdbb::cache2::CacheExpr<fdbb::utils::hash<std::size_t>(
                                     'd',
                                     'i',
                                     'v',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<B>::type::tag),
                                   decltype(fdbb::elem_div(a.get(), b.get()))>(
      std::move(fdbb::elem_div(a.get(), b.get())));
  }
};

/** @brief
    Selector for specialized CACHE implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of<A, EnumETL::CACHE>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE>
{};

/** @brief
    Element-wise pow-function of CACHE types
    @{
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::CACHE>
{
  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_arithmetic<_B>::value,
      fdbb::cache::CacheExprBinaryOp_elem_pow<
        fdbb::utils::hash<std::size_t>('p',
                                       'o',
                                       'w',
                                       fdbb::utils::remove_all<A>::type::tag,
                                       379248415),
        A,
        fdbb::cache::CacheExpr<379248415, _B>>>::type
#endif
  {
    return fdbb::cache::CacheExprBinaryOp_elem_pow<
      fdbb::utils::hash<std::size_t>(
        'p', 'o', 'w', fdbb::utils::remove_all<A>::type::tag, 379248415),
      A,
      fdbb::cache::CacheExpr<379248415, _B>>(
      std::forward<A>(a), fdbb::cache::CacheExpr<379248415, _B>(_B(b)));
  }

  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      !std::is_arithmetic<_B>::value,
      fdbb::cache::CacheExprBinaryOp_elem_pow<
        fdbb::utils::hash<std::size_t>('p',
                                       'o',
                                       'w',
                                       fdbb::utils::remove_all<A>::type::tag,
                                       fdbb::utils::remove_all<_B>::type::tag),
        A,
        _B>>::type
#endif
  {
    return fdbb::cache::CacheExprBinaryOp_elem_pow<
      fdbb::utils::hash<std::size_t>('p',
                                     'o',
                                     'w',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<_B>::type::tag),
      A,
      _B>(std::forward<A>(a), std::forward<_B>(b));
  }
};
/** @} */

/** @brief
    Selector for specialized CACHE2 implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of<A, EnumETL::CACHE2>::type>
  : public std::integral_constant<EnumETL, EnumETL::CACHE2>
{};

/** @brief
    Element-wise pow-function of CACHE2 types

    @{
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::CACHE2>
{
  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_arithmetic<_B>::value,
      decltype(fdbb::cache2::CacheExpr<fdbb::utils::hash<std::size_t>(
                                         'p',
                                         'o',
                                         'w',
                                         fdbb::utils::remove_all<A>::type::tag,
                                         621246465),
                                       decltype(fdbb::elem_pow(a.get(), b))>(
        std::move(fdbb::elem_pow(a.get(), b))))>::type
#endif
  {
    return fdbb::cache2::CacheExpr<
      fdbb::utils::hash<std::size_t>(
        'p', 'o', 'w', fdbb::utils::remove_all<A>::type::tag, 621246465),
      decltype(fdbb::elem_pow(a.get(), b))>(
      std::move(fdbb::elem_pow(a.get(), b)));
  }

  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      !std::is_arithmetic<_B>::value,
      decltype(fdbb::cache2::CacheExpr<
               fdbb::utils::hash<std::size_t>(
                 'p',
                 'o',
                 'w',
                 fdbb::utils::remove_all<A>::type::tag,
                 fdbb::utils::remove_all<_B>::type::tag),
               decltype(fdbb::elem_pow(a.get(), b.get()))>(
        std::move(fdbb::elem_pow(a.get(), b.get()))))>::type
#endif
  {
    return fdbb::cache2::CacheExpr<fdbb::utils::hash<std::size_t>(
                                     'p',
                                     'o',
                                     'w',
                                     fdbb::utils::remove_all<A>::type::tag,
                                     fdbb::utils::remove_all<_B>::type::tag),
                                   decltype(fdbb::elem_pow(a.get(), b.get()))>(
      std::move(fdbb::elem_pow(a.get(), b.get())));
  }
};
/** @} */

/** @brief
    Unitary operator cache object
 */
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME, ...)            \
  /** @brief
      Selector for specialized CACHE implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::CACHE>::type>                 \
    : public std::integral_constant<EnumETL, EnumETL::CACHE>                   \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of CACHE type
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::CACHE>                               \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(fdbb::cache::CacheExprUnaryOp_elem_##OPNAME<                 \
                  fdbb::utils::hash<std::size_t>(                              \
                    __VA_ARGS__,                                               \
                    fdbb::utils::remove_all<A>::type::tag),                    \
                  A>(std::forward<A>(a)))                                      \
    {                                                                          \
      return fdbb::cache::CacheExprUnaryOp_elem_##OPNAME<                      \
        fdbb::utils::hash<std::size_t>(__VA_ARGS__,                            \
                                       fdbb::utils::remove_all<A>::type::tag), \
        A>(std::forward<A>(a));                                                \
    }                                                                          \
  };                                                                           \
                                                                               \
  /** @brief
      Selector for specialized CACHE2 implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::CACHE2>::type>                \
    : public std::integral_constant<EnumETL, EnumETL::CACHE2>                  \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of CACHE2 type
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::CACHE2>                              \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(fdbb::cache2::CacheExpr<                                     \
                  fdbb::utils::hash<std::size_t>(                              \
                    __VA_ARGS__,                                               \
                    fdbb::utils::remove_all<A>::type::tag),                    \
                  decltype(fdbb::elem_##OPNAME(a.get()))>(                     \
        std::move(fdbb::elem_##OPNAME(a.get()))))                              \
    {                                                                          \
      return fdbb::cache2::CacheExpr<fdbb::utils::hash<std::size_t>(           \
                                       __VA_ARGS__,                            \
                                       fdbb::utils::remove_all<A>::type::tag), \
                                     decltype(fdbb::elem_##OPNAME(a.get()))>(  \
        std::move(fdbb::elem_##OPNAME(a.get())));                              \
    }                                                                          \
  };

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs, 'a', 'b', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos, 'a', 'c', 'o', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh, 'a', 'c', 'o', 's', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin, 'a', 's', 'i', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh, 'a', 's', 'i', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan, 'a', 't', 'a', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh, 'a', 't', 'a', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil, 'c', 'e', 'i', 'l')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj, 'c', 'o', 'n', 'j')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos, 'c', 'o', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh, 'c', 'o', 's', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf, 'e', 'r', 'f')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc, 'e', 'r', 'f', 'c')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp, 'e', 'x', 'p')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10, 'e', 'x', 'p', '1', '0')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2, 'e', 'x', 'p', '2')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs, 'f', 'a', 'b', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor, 'f', 'l', 'o', 'o', 'r')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag, 'i', 'm', 'a', 'g')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log, 'l', 'o', 'g')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10, 'l', 'o', 'g', '1', '0')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2, 'l', 'o', 'g', '2')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real, 'r', 'e', 'a', 'l')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round, 'r', 'o', 'u', 'n', 'd')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt, 'r', 's', 'q', 'r', 't')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign, 's', 'i', 'g', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin, 's', 'i', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh, 's', 'i', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt, 's', 'q', 'r', 't')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan, 't', 'a', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh, 't', 'a', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc, 't', 'r', 'u', 'n', 'c')

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // BACKEND_CACHE_HPP
