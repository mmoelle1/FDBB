/** @file fdbb/Backend/viennacl.hpp
 *
 *  @brief Implementation details for ViennaCL library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_VIENNACL_HPP
#define BACKEND_VIENNACL_HPP

#ifdef FDBB_BACKEND_VIENNACL

#include <type_traits>
#include <viennacl/matrix.hpp>
#include <viennacl/vector.hpp>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::VIENNACL, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::VIENNACL,
  typename std::enable_if<
    std::is_same<typename viennacl::result_of::cpu_value_type<
                   typename fdbb::utils::remove_all<T>::type>::type,
                 typename viennacl::result_of::cpu_value_type<
                   typename fdbb::utils::remove_all<T>::type>::type>::value &&
    !std::is_arithmetic<T>::value>::type> : public std::true_type
{};

/** @brief
    Result type of the expression (ViennaCL scalar type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::VIENNACL,
                     viennacl::is_any_scalar<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type =
    typename viennacl::scalar<typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL vector or vector expression type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of_and_cond<
    Expr,
    EnumETL::VIENNACL,
    viennacl::is_any_vector<
      typename fdbb::utils::remove_all<Expr>::type>::value ||
      viennacl::is_any_vector<
        decltype(viennacl::vector<
                 typename fdbb::backend::value_type<Expr>::type>(
          std::declval<typename fdbb::utils::remove_all<Expr>::type>()))>::
        value>::type>
{
  using type =
    typename viennacl::vector<typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL dense matrix or matrix expression
    type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of_and_cond<
    Expr,
    EnumETL::VIENNACL,
    viennacl::is_any_dense_matrix<
      typename fdbb::utils::remove_all<Expr>::type>::value ||
      viennacl::is_any_dense_matrix<
        decltype(viennacl::matrix<
                 typename fdbb::backend::value_type<Expr>::type>(
          std::declval<typename fdbb::utils::remove_all<Expr>::type>()))>::
        value>::type>
{
  using type =
    typename viennacl::matrix<typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL compressed matrix type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of_and_cond<
    Expr,
    EnumETL::VIENNACL,
    viennacl::is_compressed_matrix<
      typename fdbb::utils::remove_all<Expr>::type>::value>::type>
{
  using type = typename viennacl::compressed_matrix<
    typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL coordinate matrix type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of_and_cond<
    Expr,
    EnumETL::VIENNACL,
    viennacl::is_coordinate_matrix<
      typename fdbb::utils::remove_all<Expr>::type>::value>::type>
{
  using type = typename viennacl::coordinate_matrix<
    typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL ELL matrix type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::VIENNACL,
                     viennacl::is_ell_matrix<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type = typename viennacl::ell_matrix<
    typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL hybrid matrix type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::VIENNACL,
                     viennacl::is_hyb_matrix<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type = typename viennacl::hyb_matrix<
    typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (ViennaCL sliced ELL matrix type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of_and_cond<
    Expr,
    EnumETL::VIENNACL,
    viennacl::is_sliced_ell_matrix<
      typename fdbb::utils::remove_all<Expr>::type>::value>::type>
{
  using type = typename viennacl::sliced_ell_matrix<
    typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Scalar value type of the expression (ViennaCL type)
 */
template<typename Expr>
struct value_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::VIENNACL>::type>
{
  using type = typename viennacl::result_of::cpu_value_type<
    typename fdbb::utils::remove_all<Expr>::type>::type;
};

namespace detail {

/** @brief
    Selector for specialized ViennaCL implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::VIENNACL>::type>
  : public std::integral_constant<EnumETL, EnumETL::VIENNACL>
{};

/** @brief
    ViennaCL terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::VIENNACL>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::backend::result_type<Expr>::type
#endif
  {
    using Temp = typename fdbb::backend::result_type<Expr>::type;
    return Temp(std::forward<Expr>(expr));
  }
};

/// Helper macro for generating element-wise binary operations
#if !defined(DOXYGEN)
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME,                \
                                                        VIENNACL_OPNAME)       \
  /** @brief
      Selector for specialized ViennaCL implementation of
      fdbb::elem_##OPNAME##<A,B>(A&& a, B&& b) function
   */                                                                          \
  template<typename A, typename B>                                             \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    B,                                                                         \
    typename std::enable_if<                                                   \
      std::is_same<decltype(viennacl::linalg::element_##VIENNACL_OPNAME(       \
                     std::declval<A>(), std::declval<B>())),                   \
                   decltype(viennacl::linalg::element_##VIENNACL_OPNAME(       \
                     std::declval<A>(), std::declval<B>()))>::value>::type>    \
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL>                \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(a) function for \a a being of ViennaCL type
   */                                                                          \
  template<typename A, typename B>                                             \
  struct elem_##OPNAME##_impl<A, B, EnumETL::VIENNACL>                         \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept              \
      -> decltype(viennacl::linalg::element_##VIENNACL_OPNAME(                 \
        std::forward<A>(a),                                                    \
        std::forward<B>(b)))                                                   \
    {                                                                          \
      return viennacl::linalg::element_##VIENNACL_OPNAME(std::forward<A>(a),   \
                                                         std::forward<B>(b));  \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME,                \
                                                        VIENNACL_OPNAME)       \
  /** @brief
      Selector for specialized ViennaCL implementation of
      fdbb::elem_##OPNAME##<A,B>(A&& a, B&& b) function
   */                                                                          \
  template<typename A, typename B>                                             \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    B,                                                                         \
    typename std::enable_if<                                                   \
      std::is_same<decltype(viennacl::linalg::element_##VIENNACL_OPNAME(       \
                     std::declval<A>(), std::declval<B>())),                   \
                   decltype(viennacl::linalg::element_##VIENNACL_OPNAME(       \
                     std::declval<A>(), std::declval<B>()))>::value>::type>    \
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL>                \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(a) function for \a a being of ViennaCL type
   */                                                                          \
  template<typename A, typename B>                                             \
  struct elem_##OPNAME##_impl<A, B, EnumETL::VIENNACL>                         \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept              \
    {                                                                          \
      return viennacl::linalg::element_##VIENNACL_OPNAME(std::forward<A>(a),   \
                                                         std::forward<B>(b));  \
    }                                                                          \
  };
#endif

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul, prod)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div, div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow, pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Helper macro for generating element-wise unary operations
#if !defined(DOXYGEN)
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized ViennaCL implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename std::enable_if<std::is_same<                                      \
      decltype(viennacl::linalg::element_##OPNAME(std::declval<A>())),         \
      decltype(viennacl::linalg::element_##OPNAME(                             \
        std::declval<A>()))>::value>::type>                                    \
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL>                \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(a) function for \a a being of ViennaCL type
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::VIENNACL>                            \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(viennacl::linalg::element_##OPNAME(std::forward<A>(a)))      \
    {                                                                          \
      return viennacl::linalg::element_##OPNAME(std::forward<A>(a));           \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief
      Selector for specialized ViennaCL implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename std::enable_if<std::is_same<                                      \
      decltype(viennacl::linalg::element_##OPNAME(std::declval<A>())),         \
      decltype(viennacl::linalg::element_##OPNAME(                             \
        std::declval<A>()))>::value>::type>                                    \
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL>                \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(a) function for \a a being of ViennaCL type
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::VIENNACL>                            \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
    {                                                                          \
      return viennacl::linalg::element_##OPNAME(std::forward<A>(a));           \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Selector for specialized ViennaCL implementation of
    fdbb::elem_abs<A>(A&& a) function
 */
template<typename A>
struct get_elem_abs_impl<
  A,
  typename std::enable_if<std::is_same<
    decltype(viennacl::linalg::element_fabs(std::declval<A>())),
    decltype(viennacl::linalg::element_fabs(std::declval<A>()))>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::VIENNACL>
{};

/** @brief
    Element-wise \a abs(a) function for \a a being of ViennaCL type
 */
template<typename A>
struct elem_abs_impl<A, EnumETL::VIENNACL>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(viennacl::linalg::element_fabs(std::forward<A>(a)))
#endif
  {
    return viennacl::linalg::element_fabs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized ViennaCL implementation of
    fdbb::elem_real<A>(A&& a) function
 */
template<typename A>
struct get_elem_real_impl<
  A,
  typename std::enable_if<std::is_same<
    decltype(viennacl::linalg::element_fabs(std::declval<A>())),
    decltype(viennacl::linalg::element_fabs(std::declval<A>()))>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::VIENNACL>
{};

/** @brief
    Element-wise \a real(a) function for \a a being of ViennaCL type
 */
template<typename A>
struct elem_real_impl<A, EnumETL::VIENNACL>
{
  static FDBB_INLINE auto // constexpr and noexcept not possible
  eval(A&& a)
#if !defined(DOXYGEN)
    -> decltype(viennacl::linalg::element_fabs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return viennacl::linalg::element_fabs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized ViennaCL implementation of
    fdbb::elem_imag<A>(A&& a) function
 */
template<typename A>
struct get_elem_imag_impl<
  A,
  typename std::enable_if<std::is_same<
    decltype(viennacl::linalg::element_fabs(std::declval<A>())),
    decltype(viennacl::linalg::element_fabs(std::declval<A>()))>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::VIENNACL>
{};

/** @brief
    Element-wise \a imag(a) function for \a a being of ViennaCL type
 */
template<typename A>
struct elem_imag_impl<A, EnumETL::VIENNACL>
{
  static FDBB_INLINE auto // constexpr and noexcept not possible
  eval(A&& a)
#if !defined(DOXYGEN)
    -> decltype(viennacl::linalg::element_fabs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return viennacl::linalg::element_fabs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized ViennaCL implementation of
    fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename std::enable_if<std::is_same<
    decltype(viennacl::linalg::element_fabs(std::declval<A>())),
    decltype(viennacl::linalg::element_fabs(std::declval<A>()))>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::VIENNACL>
{};

/** @brief
    Element-wise \a conj(a) function for \a a being of ViennaCL type
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::VIENNACL>
{
  static FDBB_INLINE auto // constexpr and noexcept not possible
  eval(A&& a)
#if !defined(DOXYGEN)
    -> decltype(viennacl::linalg::element_fabs(std::forward<A>(a)))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return viennacl::linalg::element_fabs(std::forward<A>(a));
  }
};

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // FDBB_BACKEND_VIENNACL
#endif // BACKEND_VIENNACL_HPP
