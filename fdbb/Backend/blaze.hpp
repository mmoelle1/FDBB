/** @file fdbb/Backend/blaze.hpp
 *
 *  @brief Implementation details for Blaze library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_BLAZE_HPP
#define BACKEND_BLAZE_HPP

#ifdef FDBB_BACKEND_BLAZE

#include <blaze/Blaze.h>
#include <blaze/system/Version.h>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::BLAZE, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::BLAZE,
  typename std::enable_if<blaze::Or<
    blaze::IsVector<typename fdbb::utils::remove_all<T>::type>,
    blaze::IsMatrix<typename fdbb::utils::remove_all<T>::type>>::value>::type>
  : public std::true_type
{};

/** @brief
    Result type of the expression (Blaze type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::BLAZE>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::ResultType;
};

/** @brief
    Scalar value type of the expression (Blaze type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::BLAZE>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::ElementType;
};

namespace detail {

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::backend::detail::make_temp_impl<Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Blaze terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
  {
    using Temp = typename fdbb::backend::result_type<Expr>::type;
    return Temp(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a fabs(x) for x being of Blaze type

    @note
    Blaze does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a rsqrt(x) for x being of Blaze type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
  {
    return invsqrt(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_sign<A>(A&& a) function
 */
template<typename A>
struct get_elem_sign_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a sign(x) function for x being of Blaze type
 */
template<typename A>
struct elem_sign_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
  {
    return blaze::forEach(
      a, [](typename fdbb::utils::remove_all<A>::type::ElementType x) {
        return (x > 0.0 ? 1.0 : x < 0.0 ? -1.0 : 0.0);
      });
  }
};

#if (BLAZE_MAJOR_VERSION <= 3) && (BLAZE_MINOR_VERSION < 1)
/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_exp10<A>(A&& a) function for Blaze below 3.1
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a exp10(x) function for x being of Blaze type for
    Blaze version below 3.1
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a)
  {
    return blaze::forEach(
      a, [](typename fdbb::utils::remove_all<A>::type::ElementType x) {
        return exp10(x);
      });
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_exp2<A>(A&& a) function for Blaze below 3.1
 */
template<typename A>
struct get_elem_exp2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a exp2(x) function for x being of Blaze type for
    Blaze version below 3.1
 */
template<typename A>
struct elem_exp2_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a)
  {
    return blaze::forEach(
      a, [](typename fdbb::utils::remove_all<A>::type::ElementType x) {
        return exp2(x);
      });
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_log2<A>(A&& a) function for Blaze below 3.1
 */
template<typename A>
struct get_elem_log2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a log2(x) function for x being of Blaze type for
    Blaze version below 3.1
 */
template<typename A>
struct elem_log2_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a)
  {
    return blaze::forEach(
      a, [](typename fdbb::utils::remove_all<A>::type::ElementType x) {
        return log2(x);
      });
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_round<A>(A&& a) function for Blaze below 3.1
 */
template<typename A>
struct get_elem_round_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a round(x) function for x being of Blaze type for
    Blaze version below 3.1
 */
template<typename A>
struct elem_round_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a)
  {
    return blaze::forEach(
      a, [](typename fdbb::utils::remove_all<A>::type::ElementType x) {
        return round(x);
      });
  }
};

/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_trunc<A>(A&& a) function for Blaze below 3.1
 */
template<typename A>
struct get_elem_trunc_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a trunc(x) function for x being of Blaze type for
    Blaze version below 3.1
 */
template<typename A>
struct elem_trunc_impl<A, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a)
  {
    return blaze::forEach(
      a, [](typename fdbb::utils::remove_all<A>::type::ElementType x) {
        return trunc(x);
      });
  }
};
#endif

#if (BLAZE_MAJOR_VERSION <= 3) && (BLAZE_MINOR_VERSION < 3)
/** @brief
    Selector for specialized Blaze implementation of
    fdbb::elem_pow<A,B>(A&& a, B&& b) function for Blaze version below
    3.3
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::BLAZE>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLAZE>
{};

/** @brief
    Element-wise \a pow(x,y) function for x and y being of Blaze type
    for Blaze version below 3.1
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::BLAZE>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b)
  {
    throw "fdbb::elem_pow(a,b) for a and b of Blaze type requires Blaze "
          "version 3.3 of better";
    return a;
  }
};
#endif

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // FDBB_BACKEND_BLAZE
#endif // BACKEND_BLAZE_HPP
