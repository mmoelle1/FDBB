/** @file fdbb/Backend/mtl4.hpp
 *
 *  @brief Implementation details for MTL4/CMTL4 library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_MTL4_HPP
#define BACKEND_MTL4_HPP

#if defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)

#include <boost/numeric/mtl/mtl.hpp>
#include <type_traits>

#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace backend {

/** @brief
    If T is of type EnumETL::MTL4_VECTOR, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::MTL4_VECTOR,
                  typename std::enable_if<mtl::traits::is_vector<
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    If T is of type EnumETL::MTL4_MATRIX, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::MTL4_MATRIX,
                  typename std::enable_if<mtl::traits::is_matrix<
                    typename fdbb::utils::remove_all<T>::type>::value>::type>
  : public std::true_type
{};

/** @brief
    If T is of type EnumETL::MTL4, provides the member constant
    value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::MTL4,
  typename std::enable_if<
    mtl::traits::is_vector<typename fdbb::utils::remove_all<T>::type>::value ||
    mtl::traits::is_matrix<typename fdbb::utils::remove_all<T>::type>::value>::
    type> : public std::true_type
{};

/** @brief
    Result type of the expression (MTL4 vector type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::MTL4_VECTOR>::type>
{
  using type = mtl::vector<typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (MTL4 dense matrix type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::MTL4_MATRIX,
                     !mtl::traits::is_sparse<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type = mtl::matrix<typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Result type of the expression (MTL4 sparse matrix type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of_and_cond<
                     Expr,
                     EnumETL::MTL4_MATRIX,
                     mtl::traits::is_sparse<typename fdbb::utils::remove_all<
                       Expr>::type>::value>::type>
{
  using type =
    mtl::compressed2D<typename fdbb::backend::value_type<Expr>::type>;
};

/** @brief
    Scalar value type of the expression (MTL4 type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::MTL4>::type>
{
  using type = typename fdbb::utils::remove_all<Expr>::type::value_type;
};

namespace detail {

/** @brief
    Selector for specialized MTL4 implementation of
    fdbb::backend::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct get_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::MTL4>::type>
  : public std::integral_constant<EnumETL, EnumETL::MTL4>
{};

/** @brief
    MTL4 terminal type creation from expressions
 */
template<std::size_t Tag, typename Expr>
struct make_temp_impl<Tag, Expr, EnumETL::MTL4>
{
  static FDBB_INLINE auto constexpr eval(Expr&& expr) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::backend::result_type<Expr>::type
#endif
  {
    using Temp = typename fdbb::backend::result_type<Expr>::type;
    return Temp(std::forward<Expr>(expr));
  }
};

/** @brief
    Selector for specialized MTL4 vector implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::MTL4_VECTOR>::type>
  : public std::integral_constant<EnumETL, EnumETL::MTL4_VECTOR>
{};

/** @brief
    Element-wise multiplication of MTL4 vector types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::MTL4_VECTOR>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(mtl::vec::ele_prod(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return mtl::vec::ele_prod(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
    Selector for specialized MTL4 matrix implementation of
    fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::MTL4_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::MTL4_MATRIX>
{};

/** @brief
    Element-wise multiplication of MTL4 matrix types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::MTL4_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(mtl::mat::ele_prod(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return mtl::mat::ele_prod(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
    Selector for specialized MTL4 vector implementation of
    fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_all_type_of<A, B, EnumETL::MTL4_VECTOR>::type>
  : public std::integral_constant<EnumETL, EnumETL::MTL4_VECTOR>
{};

/** @brief
    Element-wise division of MTL4 vector types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::MTL4_VECTOR>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(mtl::vec::ele_quot(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return mtl::vec::ele_quot(std::forward<A>(a), std::forward<B>(b));
  }
};

/// Helper macro for generating element-wise unary operations
#if (__cplusplus <= 201103L) && defined(__clang__)
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
                                                                               \
  /** @brief
      Selector for specialized MTL4 matrix implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::MTL4_MATRIX>::type>           \
    : public std::integral_constant<EnumETL, EnumETL::MTL4_MATRIX>             \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of MTL4 matrix type
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::MTL4_MATRIX>                         \
  {                                                                            \
    static FDBB_INLINE auto eval(A&& a) noexcept -> typename std::enable_if<   \
      mtl::traits::is_vector<A>::value,                                        \
      typename mtl::matrix<                                                    \
        typename fdbb::utils::remove_all<A>::type::value_type>>::type          \
    {                                                                          \
      using namespace std;                                                     \
      using Temp = typename mtl::matrix<                                       \
        typename fdbb::utils::remove_all<A>::type::value_type>;                \
      auto temp = Temp(std::forward<A>(a));                                    \
      _Pragma("omp parallel for shared(temp) collapse(2)") for (               \
        auto i = 0; i < mtl::num_cols(temp);                                   \
        i++) for (auto j = 0; j < mtl::num_rows(temp); j++) temp[i, j] =       \
        OPNAME(temp[i, j]);                                                    \
      return temp;                                                             \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
                                                                               \
  /** @brief
      Selector for specialized MTL4 matrix implementation of
      fdbb::elem_##OPNAME##<A>(A&& a) function
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::MTL4_MATRIX>::type>           \
    : public std::integral_constant<EnumETL, EnumETL::MTL4_MATRIX>             \
  {};                                                                          \
                                                                               \
  /** @brief
      Element-wise \a OPNAME(x) function for x being of MTL4 matrix type
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::MTL4_MATRIX>                         \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept ->                  \
      typename std::enable_if<                                                 \
        mtl::traits::is_vector<A>::value,                                      \
        typename mtl::matrix<                                                  \
          typename fdbb::utils::remove_all<A>::type::value_type>>::type        \
    {                                                                          \
      using namespace std;                                                     \
      using Temp = typename mtl::matrix<                                       \
        typename fdbb::utils::remove_all<A>::type::value_type>;                \
      auto temp = Temp(std::forward<A>(a));                                    \
      _Pragma("omp parallel for shared(temp) collapse(2)") for (               \
        auto i = 0; i < mtl::num_cols(temp);                                   \
        i++) for (auto j = 0; j < mtl::num_rows(temp); j++) temp[i, j] =       \
        OPNAME(temp[i, j]);                                                    \
      return temp;                                                             \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
    Selector for specialized MTL4 vector implementation of
    fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::MTL4>::type>
  : public std::integral_constant<EnumETL, EnumETL::MTL4>
{};

/** @brief
    Element-wise \a fabs(x) function for x being of MTL4 type
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::MTL4>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
    Selector for specialized MTL4 implementation of
    fdbb::elem_sign<A>(A&& a) function
 */
template<typename A>
struct get_elem_sign_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::MTL4>::type>
  : public std::integral_constant<EnumETL, EnumETL::MTL4>
{};

/** @brief
    Element-wise \a sign(x) function for x being of MTL4 type
 */
template<typename A>
struct elem_sign_impl<A, EnumETL::MTL4>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(signum(std::forward<A>(a)))
#endif
  {
    return signum(std::forward<A>(a));
  }
};

} // namespace detail
} // namespace backend
} // namespace fdbb

#endif // SUPPORT CMTL4 || FDBB_BACKEND_MTL4
#endif // BACKEND_MTL4_HPP
