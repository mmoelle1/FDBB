/** @file fdbb/Fluids/Primitive1d.hpp
 *
 *  @brief 1d implementation for primitive variables
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FLUIDS_PRIMITIVE_1D_H
#define FDBB_FLUIDS_PRIMITIVE_1D_H

#include <type_traits>

#include <Core/Config.hpp>
#include <Fluids/Enums.hpp>
#include <Fluids/Traits.hpp>
#include <Fluids/Types.hpp>

namespace fdbb {

namespace fluids {

/** @brief

    Specialization of the Variables structure for primitive
    variables in 1d and arbitrary equation of state

    \f[
    U = \begin{bmatrix}
    u_0\\
    u_1\\
    u_2
    \end{bmatrix}
    = \begin{bmatrix}
    \rho\\
    v_0\\
    p
    \end{bmatrix}
    \f]
    where
    \f$ \rho \f$ is the volumetric mass density,
    \f$ v_0  \f$ is the velocity in x-direction, and
    \f$ p    \f$ is the absolute pressure

    @tparam   EOS      The equation of state type
    @tparam   Traits   The type traits
 */
template<typename EOS, typename Traits>
struct Variables<EOS, 1, EnumForm::primitive, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr INDEX_T dim = 1;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Primitive variables in 1d, ";
    eos::print(os);
    return os;
  }

  /** @brief
      Volumetric mass density variable \f$ \rho = u_0 \f$ for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The volumetric mass density variable

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::rho>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::rho>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Velocity variable \f$ v_0 = u_1 \f$ for primitive
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The x-component of the velocity variable

      @ingroup VariablesPrimary
   */
  template<INDEX_T idim, typename... Vars>
  static FDBB_INLINE auto constexpr v(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    ->
    typename std::enable_if<idim == 0,
                            decltype(traits::template getVariable<EnumVar::v_x>(
                              std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::v_x>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Absolute pressure variable \f$ p = u_2 \f$ for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The absolute pressure variable

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::p>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::p>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Internal energy per unit mass variable \f$ e \f$ for primitive
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The internal energy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr e(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<EOS_pVT, eos>::value,
      decltype(eos::e_rhop(rho(std::forward<Vars>(vars)...),
                           p(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::e_rhop(rho(std::forward<Vars>(vars)...),
                       p(std::forward<Vars>(vars)...));
  }

  /** @brief
      Kinetic energy per unit mass variable \f$ E_kin = \frac12 v_0^2
      \f$ for primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The kinetic energy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
                fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                               v<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                          v<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Total energy per unit mass variable \f$ E = e + \frac12 v_0^2 \f$ for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total energy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(e(std::forward<Vars>(vars)...) +
                E_kin(std::forward<Vars>(vars)...))
#endif
  {
    return e(std::forward<Vars>(vars)...) + E_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
      Total energy per unit volume variable \f$ \rho E = \rho *
      (e + \frac12 v_0^2) \f$ for primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total energy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                               E(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                          E(std::forward<Vars>(vars)...));
  }

  /** @brief
      Momentum variable \f$ \rho v_0 \f$ for primitive
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The x-component of the momentum variable

      @ingroup VariablesSecondary
   */
  template<INDEX_T idim, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<idim>(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(v<idim>(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Magnitude of momentum \f$ |\rho v_0| \f$ for primitive
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the momentum variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_fabs(rhov<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_fabs(rhov<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Magnitude of momentum \f$ |\rho v_0|^2 \f$ squared for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the momentum variable squared

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                               rhov<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                          rhov<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Magnitude of velocity \f$ |v_0| \f$ for primitive variables
      in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the velocity variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_fabs(v<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_fabs(v<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Magnitude of velocity \f$ |v_0|^2 \f$ squared for primitive
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the velocity variable squared

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                               v<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                          v<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Kinetic energy per unit volume variable \f$ \rho E_kin = \frac12 \rho
      v_0^2 \f$ for primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The kinetic energy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
                fdbb::elem_mul(
                  fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                 v<0>(std::forward<Vars>(vars)...)),
                  rho(std::forward<Vars>(vars)...)))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_mul(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                         v<0>(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Internal energy per unit volume variable \f$ \rho e = \rho E -
      \frac12 \rho v_0^2 \f$ for primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The internal energy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoe(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) -
                rhoE_kin(std::forward<Vars>(vars)...))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) -
           rhoE_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
      Total enthalpy per unit volume \f$ \rho H = \rho E + p \f$ for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total enthalpy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoH(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) +
                p(std::forward<Vars>(vars)...))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) + p(std::forward<Vars>(vars)...);
  }

  /** @brief
      Total enthalpy per unit mass \f$ H = E + p/\rho \f$ for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total enthalpy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr H(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(E(std::forward<Vars>(vars)...) +
                fdbb::elem_div(p(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return E(std::forward<Vars>(vars)...) +
           fdbb::elem_div(p(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Enthalpy per unit volume \f$ \rho h = \rho e + p \f$ for
      primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The enthalpy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoh(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoe(std::forward<Vars>(vars)...) +
                p(std::forward<Vars>(vars)...))
#endif
  {
    return rhoe(std::forward<Vars>(vars)...) + p(std::forward<Vars>(vars)...);
  }

  /** @brief
      Enthalpy per unit mass \f$ h = e + p/\rho \f$ for primitive
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The enthalpy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr h(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(e(std::forward<Vars>(vars)...) +
                fdbb::elem_div(p(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return e(std::forward<Vars>(vars)...) +
           fdbb::elem_div(p(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Speed of sound variable \f$ c \f$ for primitve
      variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The speed of sound variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<EOS_pVT, eos>::value,
      decltype(eos::c_rhop(rho(std::forward<Vars>(vars)...),
                           p(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::c_rhop(rho(std::forward<Vars>(vars)...),
                       p(std::forward<Vars>(vars)...));
  }

  /** @brief
      Directional velocty \f$ v_dir = v_x*d_x/|d_x| \f$ for primitive
      variables in 1d.

      @tparam      D_x    The type of the entries of direction vector \f$
      \mathbf{d} \f$
      @tparam      Vars   The variable types

      @param[in]   d_x    The x-component of the vector \f$ \mathbf{d} \f$
      @param[in]   vars   The variables

      @return             The directional velocity variable

      @ingroup VariablesSecondary
   */
  template<typename D_x, typename... Vars>
  static FDBB_INLINE auto constexpr v_dir(D_x&& d_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                              std::forward<D_x>(d_x)),
                               fdbb::elem_fabs(std::forward<D_x>(d_x))))
#endif
  {
    return fdbb::elem_div(
      fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...), std::forward<D_x>(d_x)),
      fdbb::elem_fabs(std::forward<D_x>(d_x)));
  }

  /** @brief
      State vector of conservative variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The vector of conservative variables

      @ingroup VariablesSecondary
   */
  template<bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr conservative(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 3,
                transpose ? 3 : 1,
                _options > (std::move(rho(std::forward<Vars>(vars)...)),
                            std::move(rhov<0>(std::forward<Vars>(vars)...)),
                            std::move(rhoE(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 3, transpose ? 3 : 1,
           _options > (std::move(rho(std::forward<Vars>(vars)...)),
                       std::move(rhov<0>(std::forward<Vars>(vars)...)),
                       std::move(rhoE(std::forward<Vars>(vars)...)));
  }

  /** @brief
      State vector of primitive variables in 1d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The vector of primitive variables

      @ingroup VariablesPrimary
   */
  template<bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr primitive(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 3,
                transpose ? 3 : 1,
                _options > (std::move(rho(std::forward<Vars>(vars)...)),
                            std::move(v<0>(std::forward<Vars>(vars)...)),
                            std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 3, transpose ? 3 : 1,
           _options > (std::move(rho(std::forward<Vars>(vars)...)),
                       std::move(v<0>(std::forward<Vars>(vars)...)),
                       std::move(p(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Vector of characteristic variables in 1d

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the normal vector \f$ n \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ n \f$
      @param[in]   vars   The variables

      @return             The vector of characteristic variables

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr characteristic(N_x&& n_x,
                                                   Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 3,
                transpose ? 3 : 1,
                _options > (std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<Vars>(vars)...) -
                                      c(std::forward<Vars>(vars)...)),
                            std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<Vars>(vars)...)),
                            std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<Vars>(vars)...) +
                                      c(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 3, transpose ? 3 : 1,
           _options > (std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<Vars>(vars)...) -
                                 c(std::forward<Vars>(vars)...)),
                       std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<Vars>(vars)...)),
                       std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<Vars>(vars)...) +
                                 c(std::forward<Vars>(vars)...)));
  }

  /** @brief
      First Riemann invariant \f$ w_1 \f$ for primitive
      variables in 1d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the normal vector \f$ n \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ n \f$
      @param[in]   vars   The variables

      @return             The first Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename N_x, typename... Vars>
  static FDBB_INLINE auto constexpr w_1(N_x&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(v_dir(std::forward<N_x>(n_x), std::forward<Vars>(vars)...) -
                CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
                  c(std::forward<Vars>(vars)...) /
                  (eos::gamma -
                   CONSTANT(1.0, rho(std::forward<Vars>(vars)...))))
#endif
  {
    return v_dir(std::forward<N_x>(n_x), std::forward<Vars>(vars)...) -
           CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
             c(std::forward<Vars>(vars)...) /
             (eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Second Riemann invariant \f$ w_2 \f$ for primitive
      variables in 1d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The second Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(eos::cv * fdbb::elem_log(fdbb::elem_div(
                            p(std::forward<Vars>(vars)...),
                            fdbb::elem_pow(rho(std::forward<Vars>(vars)...),
                                           eos::gamma))))
#endif
  {
    return eos::cv *
           fdbb::elem_log(fdbb::elem_div(
             p(std::forward<Vars>(vars)...),
             fdbb::elem_pow(rho(std::forward<Vars>(vars)...), eos::gamma)));
  }

  /** @brief
      Third Riemann invariant \f$ w_3 \f$ for primitive
      variables in 1d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the normal vector \f$ n \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ n \f$
      @param[in]   vars   The variables

      @return             The third Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename N_x, typename... Vars>
  static FDBB_INLINE auto constexpr w_3(N_x&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(v_dir(std::forward<N_x>(n_x), std::forward<Vars>(vars)...) +
                CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
                  c(std::forward<Vars>(vars)...) /
                  (eos::gamma -
                   CONSTANT(1.0, rho(std::forward<Vars>(vars)...))))
#endif
  {
    return v_dir(std::forward<N_x>(n_x), std::forward<Vars>(vars)...) +
           CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
             c(std::forward<Vars>(vars)...) /
             (eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Vector of Riemann invariants in 1d

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the normal vector \f$ n \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ n \f$
      @param[in]   vars   The variables

      @return             The vector of Riemann invariants

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr riemann(N_x&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 3,
                transpose ? 3 : 1,
                _options > (std::move(w_1(std::forward<N_x>(n_x),
                                          std::forward<Vars>(vars)...)),
                            std::move(w_2(std::forward<Vars>(vars)...)),
                            std::move(w_3(std::forward<N_x>(n_x),
                                          std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 3, transpose ? 3 : 1,
           _options > (std::move(w_1(std::forward<N_x>(n_x),
                                     std::forward<Vars>(vars)...)),
                       std::move(w_2(std::forward<Vars>(vars)...)),
                       std::move(w_3(std::forward<N_x>(n_x),
                                     std::forward<Vars>(vars)...)));
  }
};

} // namespace fluids
} // namespace fdbb

#endif // FDBB_FLUIDS_PRIMITIVE_1D_H
