/** @file fdbb/Fluids/RiemannInvariants3d.hpp
 *
 *  @brief 3d implementation for Riemann invariants
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FLUIDS_RIEMANN_INVARIANTS_3D_H
#define FDBB_FLUIDS_RIEMANN_INVARIANTS_3D_H

#include <type_traits>

#include <Core/Config.hpp>
#include <Fluids/Enums.hpp>
#include <Fluids/Traits.hpp>
#include <Fluids/Types.hpp>

namespace fdbb {

namespace fluids {

/** @brief

    Specialization of the Variables structure for Riemann
    invariants in 3d.

    \f[
    W = \begin{bmatrix}
    w_1\\
    w_2\\
    w_3\\
    w_4\\
    w_5
    \end{bmatrix}
    = \begin{bmatrix}
    v_n-2c/(\gamma-1)\\
    c_v log(p/\rho^\gamma)\\
    v_\xi\\
    v_\tau\\
    v_n+2c/(\gamma-1)
    \end{bmatrix}
    \f]
    where
    \f$ v_n       \f$ is the velocity component normal to the boundary,
    \f$ c         \f$ is the speed of sound,
    \f$ \gamma    \f$ is the adiabatic index,
    \f$ c_v       \f$ is the specific heat at constant volume,
    \f$ p         \f$ is the absolute pressure,
    \f$ \rho      \f$ is the volumetric mass density,
    \f$ v_\xi     \f$ is the first velocity component tangential to the boundary
    (along \f$\xi\f$ vector), and \f$ v_\tau \f$ is the second velocity
    component tangential to the boundary (along \f$\tau\f$ vector).

    @note
    This implementation is valid only for ideal gases!
 */
template<typename EOS, typename Traits>
struct Variables<EOS, 3, EnumForm::Riemann_invariants, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr INDEX_T dim = 3;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Riemann invariants in 3d, ";
    eos::print(os);
    return os;
  }

  /** @brief
      First Riemann invariant \f$ w_1 \f$ in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The first Riemann invariant

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_1(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::w_1>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_1>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Second Riemann invariant \f$ w_2 \f$ in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The second Riemann invariant

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::w_2>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_2>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Third Riemann invariant \f$ w_3 \f$ in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The third Riemann invariant

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_3(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::w_3>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_3>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Fourth Riemann invariant \f$ w_4 \f$ in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The fourth Riemann invariant

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_4(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::w_4>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_4>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Fifth Riemann invariant \f$ w_5 \f$ in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The fifth Riemann invariant

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_5(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::w_5>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_5>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Speed of sound variable \f$ c \f$ for Riemann
      invariants in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The speed of sound variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype((eos::gamma - CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) /
                CONSTANT(4.0, w_1(std::forward<Vars>(vars)...)) *
                (w_5(std::forward<Vars>(vars)...) -
                 w_1(std::forward<Vars>(vars)...)))
#endif
  {
    return (eos::gamma - CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) /
           CONSTANT(4.0, w_1(std::forward<Vars>(vars)...)) *
           (w_5(std::forward<Vars>(vars)...) -
            w_1(std::forward<Vars>(vars)...));
  }

  /** @brief
      Volumetric mass density variable \f$ rho \f$ for Riemann
      invariants in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The volumetric mass density variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_pow(
      fdbb::elem_mul((fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                     c(std::forward<Vars>(vars)...)) /
                      eos::gamma),
                     fdbb::elem_exp(-w_2(std::forward<Vars>(vars)...) /
                                    eos::cv)),
      (CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) /
        (eos::gamma - CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)))))
#endif
  {
    return fdbb::elem_pow(
      fdbb::elem_mul(
        (fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                        c(std::forward<Vars>(vars)...)) /
         eos::gamma),
        fdbb::elem_exp(-w_2(std::forward<Vars>(vars)...) / eos::cv)),
      (CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) /
        (eos::gamma - CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))));
  }

  /** @brief
      Absolute pressure variable \f$ p \f$ for Riemann
      invariants in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The absolute pressure variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                               fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                              c(std::forward<Vars>(vars)...))) /
                eos::gamma)
#endif
  {
    return fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                          fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                         c(std::forward<Vars>(vars)...))) /
           eos::gamma;
  }

  /** @brief
      Velocity variable \f$ v_i, i-0,1,2 \f$ for Riemann
      invariants in 3d

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The i-th velocity variable

      @{
   */

  template<INDEX_T idim,
           typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           typename... Vars>
  static FDBB_INLINE auto constexpr v(N_x&& n_x,
                                      N_y&& n_y,
                                      N_z&& n_z,
                                      Xi_x&& xi_x,
                                      Xi_y&& xi_y,
                                      Xi_z&& xi_z,
                                      Tau_x&& tau_x,
                                      Tau_y&& tau_y,
                                      Tau_z&& tau_z,
                                      Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 0,
      decltype(fdbb::elem_mul(
                 (w_1(std::forward<Vars>(vars)...) +
                  w_5(std::forward<Vars>(vars)...)) /
                   CONSTANT(2.0, w_1(std::forward<Vars>(vars)...)),
                 fdbb::elem_div(
                   std::forward<N_x>(n_x),
                   fdbb::elem_sqrt(fdbb::elem_mul(std::forward<N_x>(n_x),
                                                  std::forward<N_x>(n_x)) +
                                   fdbb::elem_mul(std::forward<N_y>(n_y),
                                                  std::forward<N_y>(n_y)) +
                                   fdbb::elem_mul(std::forward<N_z>(n_z),
                                                  std::forward<N_z>(n_z))))) +
               fdbb::elem_mul(
                 w_3(std::forward<Vars>(vars)...),
                 fdbb::elem_div(
                   std::forward<Xi_x>(xi_x),
                   fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Xi_x>(xi_x),
                                                  std::forward<Xi_x>(xi_x)) +
                                   fdbb::elem_mul(std::forward<Xi_y>(xi_y),
                                                  std::forward<Xi_y>(xi_y)) +
                                   fdbb::elem_mul(std::forward<Xi_z>(xi_z),
                                                  std::forward<Xi_z>(xi_z))))) +
               fdbb::elem_mul(
                 w_4(std::forward<Vars>(vars)...),
                 fdbb::elem_div(
                   std::forward<Tau_x>(tau_x),
                   fdbb::elem_sqrt(
                     fdbb::elem_mul(std::forward<Tau_x>(tau_x),
                                    std::forward<Tau_x>(tau_x)) +
                     fdbb::elem_mul(std::forward<Tau_y>(tau_y),
                                    std::forward<Tau_y>(tau_y)) +
                     fdbb::elem_mul(std::forward<Tau_z>(tau_z),
                                    std::forward<Tau_z>(tau_z))))))>::type
#endif
  {
    return fdbb::elem_mul(
             (w_1(std::forward<Vars>(vars)...) +
              w_5(std::forward<Vars>(vars)...)) /
               CONSTANT(2.0, w_1(std::forward<Vars>(vars)...)),
             fdbb::elem_div(
               std::forward<N_x>(n_x),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<N_x>(n_x),
                                              std::forward<N_x>(n_x)) +
                               fdbb::elem_mul(std::forward<N_y>(n_y),
                                              std::forward<N_y>(n_y)) +
                               fdbb::elem_mul(std::forward<N_z>(n_z),
                                              std::forward<N_z>(n_z))))) +
           fdbb::elem_mul(
             w_3(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<Xi_x>(xi_x),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Xi_x>(xi_x),
                                              std::forward<Xi_x>(xi_x)) +
                               fdbb::elem_mul(std::forward<Xi_y>(xi_y),
                                              std::forward<Xi_y>(xi_y)) +
                               fdbb::elem_mul(std::forward<Xi_z>(xi_z),
                                              std::forward<Xi_z>(xi_z))))) +
           fdbb::elem_mul(
             w_4(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<Tau_x>(tau_x),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Tau_x>(tau_x),
                                              std::forward<Tau_x>(tau_x)) +
                               fdbb::elem_mul(std::forward<Tau_y>(tau_y),
                                              std::forward<Tau_y>(tau_y)) +
                               fdbb::elem_mul(std::forward<Tau_z>(tau_z),
                                              std::forward<Tau_z>(tau_z)))));
  }

  template<INDEX_T idim,
           typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           typename... Vars>
  static FDBB_INLINE auto constexpr v(N_x&& n_x,
                                      N_y&& n_y,
                                      N_z&& n_z,
                                      Xi_x&& xi_x,
                                      Xi_y&& xi_y,
                                      Xi_z&& xi_z,
                                      Tau_x&& tau_x,
                                      Tau_y&& tau_y,
                                      Tau_z&& tau_z,
                                      Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 1,
      decltype(fdbb::elem_mul(
                 (w_1(std::forward<Vars>(vars)...) +
                  w_5(std::forward<Vars>(vars)...)) /
                   CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                 fdbb::elem_div(
                   std::forward<N_y>(n_y),
                   fdbb::elem_sqrt(fdbb::elem_mul(std::forward<N_x>(n_x),
                                                  std::forward<N_x>(n_x)) +
                                   fdbb::elem_mul(std::forward<N_y>(n_y),
                                                  std::forward<N_y>(n_y)) +
                                   fdbb::elem_mul(std::forward<N_z>(n_z),
                                                  std::forward<N_z>(n_z))))) +
               fdbb::elem_mul(
                 w_3(std::forward<Vars>(vars)...),
                 fdbb::elem_div(
                   std::forward<Xi_y>(xi_y),
                   fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Xi_x>(xi_x),
                                                  std::forward<Xi_x>(xi_x)) +
                                   fdbb::elem_mul(std::forward<Xi_y>(xi_y),
                                                  std::forward<Xi_y>(xi_y)) +
                                   fdbb::elem_mul(std::forward<Xi_z>(xi_z),
                                                  std::forward<Xi_z>(xi_z))))) +
               fdbb::elem_mul(
                 w_4(std::forward<Vars>(vars)...),
                 fdbb::elem_div(
                   std::forward<Tau_y>(tau_y),
                   fdbb::elem_sqrt(
                     fdbb::elem_mul(std::forward<Tau_x>(tau_x),
                                    std::forward<Tau_x>(tau_x)) +
                     fdbb::elem_mul(std::forward<Tau_y>(tau_y),
                                    std::forward<Tau_y>(tau_y)) +
                     fdbb::elem_mul(std::forward<Tau_z>(tau_z),
                                    std::forward<Tau_z>(tau_z))))))>::type
#endif
  {
    return fdbb::elem_mul(
             (w_1(std::forward<Vars>(vars)...) +
              w_5(std::forward<Vars>(vars)...)) /
               CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
             fdbb::elem_div(
               std::forward<N_y>(n_y),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<N_x>(n_x),
                                              std::forward<N_x>(n_x)) +
                               fdbb::elem_mul(std::forward<N_y>(n_y),
                                              std::forward<N_y>(n_y)) +
                               fdbb::elem_mul(std::forward<N_z>(n_z),
                                              std::forward<N_z>(n_z))))) +
           fdbb::elem_mul(
             w_3(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<Xi_y>(xi_y),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Xi_x>(xi_x),
                                              std::forward<Xi_x>(xi_x)) +
                               fdbb::elem_mul(std::forward<Xi_y>(xi_y),
                                              std::forward<Xi_y>(xi_y)) +
                               fdbb::elem_mul(std::forward<Xi_z>(xi_z),
                                              std::forward<Xi_z>(xi_z))))) +
           fdbb::elem_mul(
             w_4(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<Tau_y>(tau_y),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Tau_x>(tau_x),
                                              std::forward<Tau_x>(tau_x)) +
                               fdbb::elem_mul(std::forward<Tau_y>(tau_y),
                                              std::forward<Tau_y>(tau_y)) +
                               fdbb::elem_mul(std::forward<Tau_z>(tau_z),
                                              std::forward<Tau_z>(tau_z)))));
  }

  template<INDEX_T idim,
           typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           typename... Vars>
  static FDBB_INLINE auto constexpr v(N_x&& n_x,
                                      N_y&& n_y,
                                      N_z&& n_z,
                                      Xi_x&& xi_x,
                                      Xi_y&& xi_y,
                                      Xi_z&& xi_z,
                                      Tau_x&& tau_x,
                                      Tau_y&& tau_y,
                                      Tau_z&& tau_z,
                                      Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 2,
      decltype(fdbb::elem_mul(
                 (w_1(std::forward<Vars>(vars)...) +
                  w_5(std::forward<Vars>(vars)...)) /
                   CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                 fdbb::elem_div(
                   std::forward<N_z>(n_z),
                   fdbb::elem_sqrt(fdbb::elem_mul(std::forward<N_x>(n_x),
                                                  std::forward<N_x>(n_x)) +
                                   fdbb::elem_mul(std::forward<N_y>(n_y),
                                                  std::forward<N_y>(n_y)) +
                                   fdbb::elem_mul(std::forward<N_z>(n_z),
                                                  std::forward<N_z>(n_z))))) +
               fdbb::elem_mul(
                 w_3(std::forward<Vars>(vars)...),
                 fdbb::elem_div(
                   std::forward<Xi_z>(xi_z),
                   fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Xi_x>(xi_x),
                                                  std::forward<Xi_x>(xi_x)) +
                                   fdbb::elem_mul(std::forward<Xi_y>(xi_y),
                                                  std::forward<Xi_y>(xi_y)) +
                                   fdbb::elem_mul(std::forward<Xi_z>(xi_z),
                                                  std::forward<Xi_z>(xi_z))))) +
               fdbb::elem_mul(
                 w_4(std::forward<Vars>(vars)...),
                 fdbb::elem_div(
                   std::forward<Tau_z>(tau_z),
                   fdbb::elem_sqrt(
                     fdbb::elem_mul(std::forward<Tau_x>(tau_x),
                                    std::forward<Tau_x>(tau_x)) +
                     fdbb::elem_mul(std::forward<Tau_y>(tau_y),
                                    std::forward<Tau_y>(tau_y)) +
                     fdbb::elem_mul(std::forward<Tau_z>(tau_z),
                                    std::forward<Tau_z>(tau_z))))))>::type
#endif
  {
    return fdbb::elem_mul(
             (w_1(std::forward<Vars>(vars)...) +
              w_5(std::forward<Vars>(vars)...)) /
               CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
             fdbb::elem_div(
               std::forward<N_z>(n_z),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<N_x>(n_x),
                                              std::forward<N_x>(n_x)) +
                               fdbb::elem_mul(std::forward<N_y>(n_y),
                                              std::forward<N_y>(n_y)) +
                               fdbb::elem_mul(std::forward<N_z>(n_z),
                                              std::forward<N_z>(n_z))))) +
           fdbb::elem_mul(
             w_3(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<Xi_z>(xi_z),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Xi_x>(xi_x),
                                              std::forward<Xi_x>(xi_x)) +
                               fdbb::elem_mul(std::forward<Xi_y>(xi_y),
                                              std::forward<Xi_y>(xi_y)) +
                               fdbb::elem_mul(std::forward<Xi_z>(xi_z),
                                              std::forward<Xi_z>(xi_z))))) +
           fdbb::elem_mul(
             w_4(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<Tau_z>(tau_z),
               fdbb::elem_sqrt(fdbb::elem_mul(std::forward<Tau_x>(tau_x),
                                              std::forward<Tau_x>(tau_x)) +
                               fdbb::elem_mul(std::forward<Tau_y>(tau_y),
                                              std::forward<Tau_y>(tau_y)) +
                               fdbb::elem_mul(std::forward<Tau_z>(tau_z),
                                              std::forward<Tau_z>(tau_z)))));
  }
  /** @} */

  /** @brief
      Momentum variable \f$ \rho v_i, i=0,1,2 \f$ for Riemann
      invariants in 3d

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The i-th momentum variable

      @ingroup VariablesSecondary
   */
  template<INDEX_T idim,
           typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           typename... Vars>
  static FDBB_INLINE auto constexpr rhov(N_x&& n_x,
                                         N_y&& n_y,
                                         N_z&& n_z,
                                         Xi_x&& xi_x,
                                         Xi_y&& xi_y,
                                         Xi_z&& xi_z,
                                         Tau_x&& tau_x,
                                         Tau_y&& tau_y,
                                         Tau_z&& tau_z,
                                         Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<idim>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(v<idim>(std::forward<N_x>(n_x),
                                  std::forward<N_y>(n_y),
                                  std::forward<N_z>(n_z),
                                  std::forward<Xi_x>(xi_x),
                                  std::forward<Xi_y>(xi_y),
                                  std::forward<Xi_z>(xi_z),
                                  std::forward<Tau_x>(tau_x),
                                  std::forward<Tau_y>(tau_y),
                                  std::forward<Tau_z>(tau_z),
                                  std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Total energy per unit volume variable \f$ \rho E \f$
      for Riemann invariants in 3d

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The total energy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(N_x&& n_x,
                                         N_y&& n_y,
                                         N_z&& n_z,
                                         Xi_x&& xi_x,
                                         Xi_y&& xi_y,
                                         Xi_z&& xi_z,
                                         Tau_x&& tau_x,
                                         Tau_y&& tau_y,
                                         Tau_z&& tau_z,
                                         Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(p(std::forward<Vars>(vars)...) /
                  (eos::gamma -
                   CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) +
                fdbb::elem_mul(
                  rho(std::forward<Vars>(vars)...) /
                    CONSTANT(2.0, w_1(std::forward<Vars>(vars)...)),
                  (fdbb::elem_mul(v<0>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...),
                                  v<0>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...)) +
                   fdbb::elem_mul(v<1>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...),
                                  v<1>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...)) +
                   fdbb::elem_mul(v<2>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...),
                                  v<2>(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Xi_x>(xi_x),
                                       std::forward<Xi_y>(xi_y),
                                       std::forward<Xi_z>(xi_z),
                                       std::forward<Tau_x>(tau_x),
                                       std::forward<Tau_y>(tau_y),
                                       std::forward<Tau_z>(tau_z),
                                       std::forward<Vars>(vars)...)))))
#endif
  {
    return p(std::forward<Vars>(vars)...) /
             (eos::gamma - CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) +
           fdbb::elem_mul(rho(std::forward<Vars>(vars)...) /
                            CONSTANT(2.0, w_1(std::forward<Vars>(vars)...)),
                          (fdbb::elem_mul(v<0>(std::forward<N_x>(n_x),
                                               std::forward<N_y>(n_y),
                                               std::forward<N_z>(n_z),
                                               std::forward<Xi_x>(xi_x),
                                               std::forward<Xi_y>(xi_y),
                                               std::forward<Xi_z>(xi_z),
                                               std::forward<Tau_x>(tau_x),
                                               std::forward<Tau_y>(tau_y),
                                               std::forward<Tau_z>(tau_z),
                                               std::forward<Vars>(vars)...),
                                          v<0>(std::forward<N_x>(n_x),
                                               std::forward<N_y>(n_y),
                                               std::forward<N_z>(n_z),
                                               std::forward<Xi_x>(xi_x),
                                               std::forward<Xi_y>(xi_y),
                                               std::forward<Xi_z>(xi_z),
                                               std::forward<Tau_x>(tau_x),
                                               std::forward<Tau_y>(tau_y),
                                               std::forward<Tau_z>(tau_z),
                                               std::forward<Vars>(vars)...)) +
                           fdbb::elem_mul(v<1>(std::forward<N_x>(n_x),
                                               std::forward<N_y>(n_y),
                                               std::forward<N_z>(n_z),
                                               std::forward<Xi_x>(xi_x),
                                               std::forward<Xi_y>(xi_y),
                                               std::forward<Xi_z>(xi_z),
                                               std::forward<Tau_x>(tau_x),
                                               std::forward<Tau_y>(tau_y),
                                               std::forward<Tau_z>(tau_z),
                                               std::forward<Vars>(vars)...),
                                          v<1>(std::forward<N_x>(n_x),
                                               std::forward<N_y>(n_y),
                                               std::forward<N_z>(n_z),
                                               std::forward<Xi_x>(xi_x),
                                               std::forward<Xi_y>(xi_y),
                                               std::forward<Xi_z>(xi_z),
                                               std::forward<Tau_x>(tau_x),
                                               std::forward<Tau_y>(tau_y),
                                               std::forward<Tau_z>(tau_z),
                                               std::forward<Vars>(vars)...)) +
                           fdbb::elem_mul(v<2>(std::forward<N_x>(n_x),
                                               std::forward<N_y>(n_y),
                                               std::forward<N_z>(n_z),
                                               std::forward<Xi_x>(xi_x),
                                               std::forward<Xi_y>(xi_y),
                                               std::forward<Xi_z>(xi_z),
                                               std::forward<Tau_x>(tau_x),
                                               std::forward<Tau_y>(tau_y),
                                               std::forward<Tau_z>(tau_z),
                                               std::forward<Vars>(vars)...),
                                          v<2>(std::forward<N_x>(n_x),
                                               std::forward<N_y>(n_y),
                                               std::forward<N_z>(n_z),
                                               std::forward<Xi_x>(xi_x),
                                               std::forward<Xi_y>(xi_y),
                                               std::forward<Xi_z>(xi_z),
                                               std::forward<Tau_x>(tau_x),
                                               std::forward<Tau_y>(tau_y),
                                               std::forward<Tau_z>(tau_z),
                                               std::forward<Vars>(vars)...))));
  }

  /** @brief
      State vector of conservative variables in 3d

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The vector of conservative variables

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr conservative(N_x&& n_x,
                                                 N_y&& n_y,
                                                 N_z&& n_z,
                                                 Xi_x&& xi_x,
                                                 Xi_y&& xi_y,
                                                 Xi_z&& xi_z,
                                                 Tau_x&& tau_x,
                                                 Tau_y&& tau_y,
                                                 Tau_z&& tau_z,
                                                 Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 5,
                transpose ? 5 : 1,
                _options > (std::move(rho(std::forward<Vars>(vars)...)),
                            std::move(rhov<0>(std::forward<N_x>(n_x),
                                              std::forward<N_y>(n_y),
                                              std::forward<N_z>(n_z),
                                              std::forward<Xi_x>(xi_x),
                                              std::forward<Xi_y>(xi_y),
                                              std::forward<Xi_z>(xi_z),
                                              std::forward<Tau_x>(tau_x),
                                              std::forward<Tau_y>(tau_y),
                                              std::forward<Tau_z>(tau_z),
                                              std::forward<Vars>(vars)...)),
                            std::move(rhov<1>(std::forward<N_x>(n_x),
                                              std::forward<N_y>(n_y),
                                              std::forward<N_z>(n_z),
                                              std::forward<Xi_x>(xi_x),
                                              std::forward<Xi_y>(xi_y),
                                              std::forward<Xi_z>(xi_z),
                                              std::forward<Tau_x>(tau_x),
                                              std::forward<Tau_y>(tau_y),
                                              std::forward<Tau_z>(tau_z),
                                              std::forward<Vars>(vars)...)),
                            std::move(rhov<2>(std::forward<N_x>(n_x),
                                              std::forward<N_y>(n_y),
                                              std::forward<N_z>(n_z),
                                              std::forward<Xi_x>(xi_x),
                                              std::forward<Xi_y>(xi_y),
                                              std::forward<Xi_z>(xi_z),
                                              std::forward<Tau_x>(tau_x),
                                              std::forward<Tau_y>(tau_y),
                                              std::forward<Tau_z>(tau_z),
                                              std::forward<Vars>(vars)...)),
                            std::move(rhoE(std::forward<N_x>(n_x),
                                           std::forward<N_y>(n_y),
                                           std::forward<N_z>(n_z),
                                           std::forward<Xi_x>(xi_x),
                                           std::forward<Xi_y>(xi_y),
                                           std::forward<Xi_z>(xi_z),
                                           std::forward<Tau_x>(tau_x),
                                           std::forward<Tau_y>(tau_y),
                                           std::forward<Tau_z>(tau_z),
                                           std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 5, transpose ? 5 : 1,
           _options > (std::move(rho(std::forward<Vars>(vars)...)),
                       std::move(rhov<0>(std::forward<N_x>(n_x),
                                         std::forward<N_y>(n_y),
                                         std::forward<N_z>(n_z),
                                         std::forward<Xi_x>(xi_x),
                                         std::forward<Xi_y>(xi_y),
                                         std::forward<Xi_z>(xi_z),
                                         std::forward<Tau_x>(tau_x),
                                         std::forward<Tau_y>(tau_y),
                                         std::forward<Tau_z>(tau_z),
                                         std::forward<Vars>(vars)...)),
                       std::move(rhov<1>(std::forward<N_x>(n_x),
                                         std::forward<N_y>(n_y),
                                         std::forward<N_z>(n_z),
                                         std::forward<Xi_x>(xi_x),
                                         std::forward<Xi_y>(xi_y),
                                         std::forward<Xi_z>(xi_z),
                                         std::forward<Tau_x>(tau_x),
                                         std::forward<Tau_y>(tau_y),
                                         std::forward<Tau_z>(tau_z),
                                         std::forward<Vars>(vars)...)),
                       std::move(rhov<2>(std::forward<N_x>(n_x),
                                         std::forward<N_y>(n_y),
                                         std::forward<N_z>(n_z),
                                         std::forward<Xi_x>(xi_x),
                                         std::forward<Xi_y>(xi_y),
                                         std::forward<Xi_z>(xi_z),
                                         std::forward<Tau_x>(tau_x),
                                         std::forward<Tau_y>(tau_y),
                                         std::forward<Tau_z>(tau_z),
                                         std::forward<Vars>(vars)...)),
                       std::move(rhoE(std::forward<N_x>(n_x),
                                      std::forward<N_y>(n_y),
                                      std::forward<N_z>(n_z),
                                      std::forward<Xi_x>(xi_x),
                                      std::forward<Xi_y>(xi_y),
                                      std::forward<Xi_z>(xi_z),
                                      std::forward<Tau_x>(tau_x),
                                      std::forward<Tau_y>(tau_y),
                                      std::forward<Tau_z>(tau_z),
                                      std::forward<Vars>(vars)...)));
  }

  /** @brief
      State vector of primitive variables in 3d

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The vector of primitive variables

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr primitive(N_x&& n_x,
                                              N_y&& n_y,
                                              N_z&& n_z,
                                              Xi_x&& xi_x,
                                              Xi_y&& xi_y,
                                              Xi_z&& xi_z,
                                              Tau_x&& tau_x,
                                              Tau_y&& tau_y,
                                              Tau_z&& tau_z,
                                              Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 5,
                transpose ? 5 : 1,
                _options > (std::move(rho(std::forward<Vars>(vars)...)),
                            std::move(v<0>(std::forward<N_x>(n_x),
                                           std::forward<N_y>(n_y),
                                           std::forward<N_z>(n_z),
                                           std::forward<Xi_x>(xi_x),
                                           std::forward<Xi_y>(xi_y),
                                           std::forward<Xi_z>(xi_z),
                                           std::forward<Tau_x>(tau_x),
                                           std::forward<Tau_y>(tau_y),
                                           std::forward<Tau_z>(tau_z),
                                           std::forward<Vars>(vars)...)),
                            std::move(v<1>(std::forward<N_x>(n_x),
                                           std::forward<N_y>(n_y),
                                           std::forward<N_z>(n_z),
                                           std::forward<Xi_x>(xi_x),
                                           std::forward<Xi_y>(xi_y),
                                           std::forward<Xi_z>(xi_z),
                                           std::forward<Tau_x>(tau_x),
                                           std::forward<Tau_y>(tau_y),
                                           std::forward<Tau_z>(tau_z),
                                           std::forward<Vars>(vars)...)),
                            std::move(v<2>(std::forward<N_x>(n_x),
                                           std::forward<N_y>(n_y),
                                           std::forward<N_z>(n_z),
                                           std::forward<Xi_x>(xi_x),
                                           std::forward<Xi_y>(xi_y),
                                           std::forward<Xi_z>(xi_z),
                                           std::forward<Tau_x>(tau_x),
                                           std::forward<Tau_y>(tau_y),
                                           std::forward<Tau_z>(tau_z),
                                           std::forward<Vars>(vars)...)),
                            std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 5, transpose ? 5 : 1,
           _options > (std::move(rho(std::forward<Vars>(vars)...)),
                       std::move(v<0>(std::forward<N_x>(n_x),
                                      std::forward<N_y>(n_y),
                                      std::forward<N_z>(n_z),
                                      std::forward<Xi_x>(xi_x),
                                      std::forward<Xi_y>(xi_y),
                                      std::forward<Xi_z>(xi_z),
                                      std::forward<Tau_x>(tau_x),
                                      std::forward<Tau_y>(tau_y),
                                      std::forward<Tau_z>(tau_z),
                                      std::forward<Vars>(vars)...)),
                       std::move(v<1>(std::forward<N_x>(n_x),
                                      std::forward<N_y>(n_y),
                                      std::forward<N_z>(n_z),
                                      std::forward<Xi_x>(xi_x),
                                      std::forward<Xi_y>(xi_y),
                                      std::forward<Xi_z>(xi_z),
                                      std::forward<Tau_x>(tau_x),
                                      std::forward<Tau_y>(tau_y),
                                      std::forward<Tau_z>(tau_z),
                                      std::forward<Vars>(vars)...)),
                       std::move(v<2>(std::forward<N_x>(n_x),
                                      std::forward<N_y>(n_y),
                                      std::forward<N_z>(n_z),
                                      std::forward<Xi_x>(xi_x),
                                      std::forward<Xi_y>(xi_y),
                                      std::forward<Xi_z>(xi_z),
                                      std::forward<Tau_x>(tau_x),
                                      std::forward<Tau_y>(tau_y),
                                      std::forward<Tau_z>(tau_z),
                                      std::forward<Vars>(vars)...)),
                       std::move(p(std::forward<Vars>(vars)...)));
  }
};

} // namespace fluids
} // namespace fdbb

#endif // FDBB_FLUIDS_RIEMANN_INVARIANTS_3D_H
