/** @file fdbb/Fluids/EOS.hpp
 *
 *  @brief Equation of states
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FLUIDS_EOS_HPP
#define FDBB_FLUIDS_EOS_HPP

#include <iostream>
#include <ratio>
#include <type_traits>

#include <Core/Utils.hpp>
#include <Fluids/Traits.hpp>

#ifdef FDBB_WITH_COOLPROP
#include "CoolProp.h"
#endif

namespace fdbb {

namespace fluids {

/** @brief
    Base class for general equations of state

    The equation of state is given in the form
    \f[
    f(p,V,T)=0,
    \f]
    where
    \f$ p \f$ is the absolute pressure,
    \f$ V \f$ is the volume, and
    \f$ T \f$ is the absolute temperature.
 */
struct EOS_pVT
{
  /** @brief
      Print equation of state to output stream

      @param[in] os  The output stream

      @return        The output stream
   */
  static std::ostream& print(std::ostream& os)
  {
    os << "Generic EOS of the form f(p,V,T)=0";
    return os;
  }
};

/** @brief
    Print equation of state to output stream

    @tparam      EOS   The equation of state type

    @param[in]   os    The output stream
    @param[in]   obj   The equation of state

    @return            The output stream
 */
template<typename EOS>
std::ostream&
operator<<(std::ostream& os,
           typename std::enable_if<std::is_base_of<EOS_pVT, EOS>::value,
                                   fdbb::utils::remove_all<EOS>>::type& obj)
{
  return obj.print(os);
}

/** @brief
    Equation of state for an ideal gas

    The equation of state is given in the form
    \f[
    f(p,V,T) = pV-nRT = 0,
    \f]
    where
    \f$ p \f$ is the absolute pressure,
    \f$ V \f$ is the volume,
    \f$ n \f$ is the number of moles of a substance,
    \f$ R \f$ is the ideal gas constant (\f$ 8.314459848 J/(mol\cdot K) \f$),
    and
    \f$ T \f$ is the absolute temperature.

    The ideal gas law is implemented in the form

    \f[
    p=\rho(\gamma-1)e,
    \f]
    where
    \f$ \rho \f$ is the density,
    \f$ \gamma=C_p/C_v \f$ is the adiabatic index (ratio of specific heats),
    \f$ e=C_vT \f$ is the internal energy per unit mass,
    \f$ C_v \f$ is the specific heat at constant volume, and
    \f$ C_p \f$ is the specific heat at constant pressure.

    @note
    The values \f$ C_v \f$ and \f$ C_p \f$ have to be provided as
    std::ratio<N,D> types. Their default values are \f$ C_v=frac{7}{2}
    \f$ and \f$ C_p=\frac{5}{2} \f$ for dry air at sea level so that
    \f$ \gamma=1.400 \f$ (diatomic molecules).

    @tparam   Tcoeff   The coefficient type
    @tparam   Cp       The specific heat at constant pressure value (as
    std::ratio)
    @tparam   Cv       The specific heat at constant volume value (as
    std::ratio)
 */
template<typename Tcoeff,
         typename Cp = std::ratio<7, 2>,
         typename Cv = std::ratio<5, 2>>
struct EOSidealGas : public EOS_pVT
{
  /// @brief ideal gas constant (\f$ 8.314459848 J/(mol\cdot K) \f$)
  static const Tcoeff R;

  /// @brief Specific heat at constant pressure
  static const Tcoeff cp;

  /// @brief Specific heat at constant volume
  static const Tcoeff cv;

  /// @brief Adiabatic index (ratio of specific heats)
  static const Tcoeff gamma;

  /** @brief
      Returns absolute pressure \f$ p = p(\rho, e) \f$ computed from
      volumetric mass density and internal energy per unit mass

      @tparam      Trho   The type of the volumetric mass density
      @tparam      Te     The type of the internal energy per unit mass

      @param[in]   rho    The volumetric mass density
      @param[in]   e      The internal energy per unit mass

      @return             The absolute pressure
 */
  template<typename Trho, typename Te>
  static FDBB_INLINE auto constexpr p_rhoe(Trho&& rho, Te&& e) noexcept
#if !defined(DOXYGEN)
    -> decltype((gamma - static_cast<Tcoeff>(1.0)) * fdbb::elem_mul(rho, e))
#endif
  {
    return (gamma - static_cast<Tcoeff>(1.0)) * fdbb::elem_mul(rho, e);
  }

  /** @brief
      Returns absolute temperature \f$ T = T(\rho, e) \f$ computed
      from volumetric mass density and internal energy per unit mass

      @tparam      Trho   The type of the volumetric mass density
      @tparam      Te     The type of the internal energy per unit mass

      @param[in]   rho    The volumetric mass density
      @param[in]   e      The internal energy per unit mass

      @return             The absolute temperature
 */
  template<typename Trho, typename Te>
  static FDBB_INLINE auto constexpr T_rhoe(Trho&& rho, Te&& e) noexcept
#if !defined(DOXYGEN)
    -> decltype(e / cv)
#endif
  {
    return e / cv;
  }

  /** @brief
      Returns the speed of sound \f$ c = c(\rho, e) \f$ computed from
      volumetric mass density and internal energy per unit mass

      @tparam      Trho   The type of the volumetric mass density
      @tparam      Te     The type of the internal energy per unit mass

      @param[in]   rho    The volumetric mass density
      @param[in]   e      The internal energy per unit mass

      @return             The speed of sound
 */
  template<typename Trho, typename Te>
  static FDBB_INLINE auto constexpr c_rhoe(Trho&& rho, Te&& e) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_sqrt(gamma * fdbb::elem_div(p_rhoe(rho, e), rho)))
#endif
  {
    return fdbb::elem_sqrt(gamma * fdbb::elem_div(p_rhoe(rho, e), rho));
  }

  /** @brief
      Returns absolute pressure \f$ p = p(\rho, T) \f$ computed from
      volumetric mass density and total temperature

      @tparam      Trho   The type of the volumetric mass density
      @tparam      TT     The type of the total temperature

      @param[in]   rho    The volumetric mass density
      @param[in]   T      The total temperature

      @return             The absolute pressure
 */
  template<typename Trho, typename TT>
  static FDBB_INLINE auto constexpr p_rhoT(Trho&& rho, TT&& T) noexcept
#if !defined(DOXYGEN)
    -> decltype((gamma - static_cast<Tcoeff>(1.0)) * cv *
                fdbb::elem_mul(rho, T))
#endif
  {
    return (gamma - static_cast<Tcoeff>(1.0)) * cv * fdbb::elem_mul(rho, T);
  }

  /** @brief
      Returns internal energy per unit mass \f$ e = e(\rho, T) \f$
      computed from volumetric mass density and absolute temperature

      @tparam      Trho   The type of the volumetric mass density
      @tparam      TT     The type of the absolute temperature

      @param[in]   rho   The volumetric mass density
      @param[in]   T     The absolute temperature

      @return            The internal energy per unit mass
 */
  template<typename Trho, typename TT>
  static FDBB_INLINE auto constexpr e_rhoT(Trho&& rho, TT&& T) noexcept
#if !defined(DOXYGEN)
    -> decltype(cv * T)
#endif
  {
    return cv * T;
  }

  /** @brief
      Returns internal energy per unit mass \f$ e = e(\rho, p) \f$
      computed from volumetric mass density and absolute pressure

      @tparam      Trho   The type of the volumetric mass density
      @tparam      Tp     The type of the absolute pressure

      @param[in]   rho    The volumetric mass density
      @param[in]   p      The absolute pressure

      @return             The internal energy per unit mass
 */
  template<typename Trho, typename Tp>
  static FDBB_INLINE auto constexpr e_rhop(Trho&& rho, Tp&& p) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(p, rho*(gamma - static_cast<Tcoeff>(1.0))))
#endif
  {
    return (fdbb::elem_div(p, rho * (gamma - static_cast<Tcoeff>(1.0))));
  }

  /** @brief
      Returns absolute temperature \f$ T = T(\rho, p) \f$ computed
      from volumetric mass density and absolute pressure

      @tparam      Trho  The type of the volumetric mass density
      @tparam      Tp    The type of the absolute pressure

      @param[in]   rho   The volumetric mass density
      @param[in]   p     The absolute pressure

      @return            The absolute temperature
 */
  template<typename Trho, typename Tp>
  static FDBB_INLINE auto constexpr T_rhop(Trho&& rho, Tp&& p) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(p, rho*(gamma - static_cast<Tcoeff>(1.0)) * cv))
#endif
  {
    return (fdbb::elem_div(p, rho * (gamma - static_cast<Tcoeff>(1.0)) * cv));
  }

  /** @brief
      Returns the speed of sound \f$ c = c(\rho, p) \f$ computed from
      volumetric mass density and absolute pressure

      @tparam      Trho   The type of the volumetric mass density
      @tparam      Te     The type of the internal energy per unit mass

      @param[in]   rho    The volumetric mass density
      @param[in]   p      The absolute pressure

      @return             The speed of sound
 */
  template<typename Trho, typename Tp>
  static FDBB_INLINE auto constexpr c_rhop(Trho&& rho, Tp&& p) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_sqrt(gamma * fdbb::elem_div(p, rho)))
#endif
  {
    return fdbb::elem_sqrt(gamma * fdbb::elem_div(p, rho));
  }

  /** @brief
      Print equation of state to output stream

      @param[in]   os    The output stream

      @return            The output stream
   */
  static std::ostream& print(std::ostream& os)
  {
    os << "EOS for ideal gas ( Cv=" << cv << ", Cp=" << cp
       << ", gamma=" << gamma << " )";
    return os;
  }
};

// Initializer (in-class initialization using constexpr is not working with
// clang)
template<typename Tcoeff, typename Cp, typename Cv>
const Tcoeff EOSidealGas<Tcoeff, Cp, Cv>::R = 8.314459848;

// Initializer (in-class initialization using constexpr is not working with
// clang)
template<typename Tcoeff, typename Cp, typename Cv>
const Tcoeff EOSidealGas<Tcoeff, Cp, Cv>::cp = static_cast<Tcoeff>(R* Cp::num) /
                                               static_cast<Tcoeff>(Cp::den);

// Initializer (in-class initialization using constexpr is not working with
// clang)
template<typename Tcoeff, typename Cp, typename Cv>
const Tcoeff EOSidealGas<Tcoeff, Cp, Cv>::cv = static_cast<Tcoeff>(R* Cv::num) /
                                               static_cast<Tcoeff>(Cp::den);

// Initializer (in-class initialization using constexpr is not working with
// clang)
template<typename Tcoeff, typename Cp, typename Cv>
const Tcoeff EOSidealGas<Tcoeff, Cp, Cv>::gamma =
  static_cast<Tcoeff>(Cp::num* Cv::den) / static_cast<Tcoeff>(Cp::den* Cv::num);

} // namespace fluids
} // namespace fdbb

#endif // FDBB_FLUIDS_EOS_H
