/** @file fdbb/Fluids/Conservative3d.hpp
 *
 *  @brief 3d implementation for conservative variables
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FLUIDS_CONSERVATIVE_3D_H
#define FDBB_FLUIDS_CONSERVATIVE_3D_H

#include <type_traits>

#include <Core/Config.hpp>
#include <Fluids/Enums.hpp>
#include <Fluids/Traits.hpp>
#include <Fluids/Types.hpp>

namespace fdbb {

namespace fluids {

/** @brief

    Specialization of the Variables structure for conservative
    variables in 3d and arbitrary equation of state

    \f[
    U = \begin{bmatrix}
    u_0\\
    u_1\\
    u_2\\
    u_3\\
    u_4
    \end{bmatrix}
    = \begin{bmatrix}
    \rho\\
    \rho v_0\\
    \rho v_1\\
    \rho v_2\\
    \rho E
    \end{bmatrix}
    \f]
    where
    \f$ \rho      \f$ is the volumetric mass density,
    \f$ \rho v_0  \f$ is the momentum in x-direction,
    \f$ \rho v_1  \f$ is the momentum in y-direction,
    \f$ \rho v_2  \f$ is the momentum in z-direction, and
    \f$ \rho E    \f$ is the total energy per unit volume

    @tparam   EOS      The equation of state type
    @tparam   Traits   The type traits
 */
template<typename EOS, typename Traits>
struct Variables<EOS, 3, EnumForm::conservative, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr INDEX_T dim = 3;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Conservative variables in 3d, ";
    eos::print(os);
    return os;
  }

  /** @brief
      Volumetric mass density variable \f$ \rho = u_0 \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The volumetric mass density variable

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::rho>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::rho>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Momentum variable \f$ \rho v_i = u_{i+1}, i=0,1,2 \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The i-th component of the momentum variable

      @ingroup VariablesPrimary

      @{
   */
  template<INDEX_T idim, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 0,
      decltype(traits::template getVariable<EnumVar::rhov_x>(
        std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::rhov_x>(
      std::forward<Vars>(vars)...);
  }

  template<INDEX_T idim, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 1,
      decltype(traits::template getVariable<EnumVar::rhov_y>(
        std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::rhov_y>(
      std::forward<Vars>(vars)...);
  }

  template<INDEX_T idim, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 2,
      decltype(traits::template getVariable<EnumVar::rhov_z>(
        std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::rhov_z>(
      std::forward<Vars>(vars)...);
  }
  /** @} */

  /** @brief
      Total energy per unit volume variable \f$ \rho E = u_4 \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total energy per unit volume variable

      @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(traits::template getVariable<EnumVar::rhoE>(
      std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::rhoE>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
      Magnitude of momentum \f$ \|\mathbf{m} \| \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the momentum vector

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_sqrt(
      fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                     rhov<0>(std::forward<Vars>(vars)...)) +
      fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                     rhov<1>(std::forward<Vars>(vars)...)) +
      fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                     rhov<2>(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::elem_sqrt(
      fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                     rhov<0>(std::forward<Vars>(vars)...)) +
      fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                     rhov<1>(std::forward<Vars>(vars)...)) +
      fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                     rhov<2>(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Magnitude of momentum \f$ \|\mathbf{m} \|^2 \f$ squared for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the momentum vector squared

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                               rhov<0>(std::forward<Vars>(vars)...)) +
                fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                               rhov<1>(std::forward<Vars>(vars)...)) +
                fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                               rhov<2>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                          rhov<0>(std::forward<Vars>(vars)...)) +
           fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                          rhov<1>(std::forward<Vars>(vars)...)) +
           fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                          rhov<2>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Total energy per unit mass variable \f$ E = u_4/u_0 \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total energy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      traits::template getVariable<EnumVar::rhoE>(std::forward<Vars>(vars)...),
      traits::template getVariable<EnumVar::rho>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_div(
      traits::template getVariable<EnumVar::rhoE>(std::forward<Vars>(vars)...),
      traits::template getVariable<EnumVar::rho>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Velocity variable \f$ v_i = u_{i+1}, i=0,1,2 \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The i-th component of the velocity variable

      @ingroup VariablesSecondary
   */
  template<INDEX_T idim, typename... Vars>
  static FDBB_INLINE auto constexpr v(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(rhov<idim>(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_div(rhov<idim>(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Magnitude of velocity \f$ \| \mathbf{v} \| \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the velocity vector

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_sqrt(
      fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                    rhov<0>(std::forward<Vars>(vars)...)) +
                       fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                                      rhov<1>(std::forward<Vars>(vars)...)) +
                       fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                                      rhov<2>(std::forward<Vars>(vars)...)),
                     fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                    rho(std::forward<Vars>(vars)...)))))
#endif
  {
    return fdbb::elem_sqrt(
      fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                    rhov<0>(std::forward<Vars>(vars)...)) +
                       fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                                      rhov<1>(std::forward<Vars>(vars)...)) +
                       fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                                      rhov<2>(std::forward<Vars>(vars)...)),
                     fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                    rho(std::forward<Vars>(vars)...))));
  }

  /** @brief
      Magnitude of velocity \f$ \| \mathbf{v} \|^2 \f$ squared for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The magnitude of the velocity vector squared

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                     rhov<0>(std::forward<Vars>(vars)...)) +
        fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                       rhov<1>(std::forward<Vars>(vars)...)) +
        fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                       rhov<2>(std::forward<Vars>(vars)...)),
      fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                     rho(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::elem_div(
      fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                     rhov<0>(std::forward<Vars>(vars)...)) +
        fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                       rhov<1>(std::forward<Vars>(vars)...)) +
        fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                       rhov<2>(std::forward<Vars>(vars)...)),
      fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                     rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Kinetic energy per unit volume variable \f$ \rho E_kin = \frac12 ((\rho
      v_0)^2+(\rho v_1)^2+(\rho v_2)^2)/\rho \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The kinetic energy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
                fdbb::elem_div(
                  fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                 rhov<0>(std::forward<Vars>(vars)...)) +
                    fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                                   rhov<1>(std::forward<Vars>(vars)...)) +
                    fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                                   rhov<2>(std::forward<Vars>(vars)...)),
                  rho(std::forward<Vars>(vars)...)))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_div(
             fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                            rhov<0>(std::forward<Vars>(vars)...)) +
               fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                              rhov<1>(std::forward<Vars>(vars)...)) +
               fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                              rhov<2>(std::forward<Vars>(vars)...)),
             rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Kinetic energy per unit mass variable \f$ E_kin = \frac12 ((\rho
      v_0)^2+(\rho v_1)^2+(\rho v_2)^2)/\rho^2 \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The kinetic energy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
                fdbb::elem_div(
                  fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                 rhov<0>(std::forward<Vars>(vars)...)) +
                    fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                                   rhov<1>(std::forward<Vars>(vars)...)) +
                    fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                                   rhov<2>(std::forward<Vars>(vars)...)),
                  fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                 rho(std::forward<Vars>(vars)...))))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_div(
             fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                            rhov<0>(std::forward<Vars>(vars)...)) +
               fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                              rhov<1>(std::forward<Vars>(vars)...)) +
               fdbb::elem_mul(rhov<2>(std::forward<Vars>(vars)...),
                              rhov<2>(std::forward<Vars>(vars)...)),
             fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                            rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Internal energy per unit volume variable \f$ \rho e = \rho E -
      \frac12 ((\rho v_0)^2+(\rho v_1)^2+(\rho v_2)^2)/\rho \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The internal energy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoe(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) -
                rhoE_kin(std::forward<Vars>(vars)...))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) -
           rhoE_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
      Internal energy per unit mass variable \f$ e = E - \frac12
      ((\rho v_0)^2+(\rho v_1)^2+(\rho v_2)^2)/\rho^2 \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The internal energy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr e(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(E(std::forward<Vars>(vars)...) -
                E_kin(std::forward<Vars>(vars)...))
#endif
  {
    return E(std::forward<Vars>(vars)...) - E_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
      Total enthalpy per unit volume \f$ \rho H = \rho E + p \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total enthalpy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoH(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) +
                eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                            e(std::forward<Vars>(vars)...)))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) +
           eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Total enthalpy per unit mass \f$ H = E + p/\rho \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The total enthalpy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr H(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(rhoE(std::forward<Vars>(vars)...) +
                                 eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                             e(std::forward<Vars>(vars)...)),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_div(rhoE(std::forward<Vars>(vars)...) +
                            eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                        e(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Enthalpy per unit volume \f$ \rho h = \rho e + p \f$ for
      conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The enthalpy per unit volume variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoh(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoe(std::forward<Vars>(vars)...) +
                eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                            e(std::forward<Vars>(vars)...)))
#endif
  {
    return rhoe(std::forward<Vars>(vars)...) +
           eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Enthalpy per unit mass \f$ h = e + p/\rho \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The enthalpy per unit mass variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr h(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(rhoe(std::forward<Vars>(vars)...) +
                                 eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                             e(std::forward<Vars>(vars)...)),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_div(rhoe(std::forward<Vars>(vars)...) +
                            eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                        e(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
      Absolute pressure variable \f$ p \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The absolute pressure variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<EOS_pVT, eos>::value,
      decltype(eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                           e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Speed of sound variable \f$ c \f$ for conservative
      variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The speed of sound variable

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<EOS_pVT, eos>::value,
      decltype(eos::c_rhoe(rho(std::forward<Vars>(vars)...),
                           e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::c_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Directional velocty \f$ v_dir = (v_x*d_x+v_y*d_y+v_z*d_z)/|d| \f$ for
      conservative variables in 3d.

      @tparam      D_x    The type of the entries of the x-component of the
      vector \f$ \mathbf{d} \f$
      @tparam      D_y    The type of the entries of the y-component of the
      vector \f$ \mathbf{d} \f$
      @tparam      D_z    The type of the entries of the z-component of the
      vector \f$ \mathbf{d} \f$
      @tparam      Vars   The variable types

      @param[in]   d_x    The x-component of the vector \f$ \mathbf{d} \f$
      @param[in]   d_y    The y-component of the vector \f$ \mathbf{d} \f$
      @param[in]   d_z    The z-component of the vector \f$ \mathbf{d} \f$
      @param[in]   vars   The variables

      @return             The directional velocity variable

      @ingroup VariablesSecondary
   */
  template<typename D_x, typename D_y, typename D_z, typename... Vars>
  static FDBB_INLINE auto constexpr v_dir(D_x&& d_x,
                                          D_y&& d_y,
                                          D_z&& d_z,
                                          Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      (fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                      std::forward<D_x>(d_x)) +
       fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                      std::forward<D_y>(d_y)) +
       fdbb::elem_mul(v<2>(std::forward<Vars>(vars)...),
                      std::forward<D_z>(d_z))),
      fdbb::elem_sqrt(
        fdbb::elem_mul(std::forward<D_x>(d_x), std::forward<D_x>(d_x)) +
        fdbb::elem_mul(std::forward<D_y>(d_y), std::forward<D_y>(d_y)) +
        fdbb::elem_mul(std::forward<D_z>(d_z), std::forward<D_z>(d_z)))))
#endif
  {
    return fdbb::elem_div(
      (fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                      std::forward<D_x>(d_x)) +
       fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                      std::forward<D_y>(d_y)) +
       fdbb::elem_mul(v<2>(std::forward<Vars>(vars)...),
                      std::forward<D_z>(d_z))),
      fdbb::elem_sqrt(
        fdbb::elem_mul(std::forward<D_x>(d_x), std::forward<D_x>(d_x)) +
        fdbb::elem_mul(std::forward<D_y>(d_y), std::forward<D_y>(d_y)) +
        fdbb::elem_mul(std::forward<D_z>(d_z), std::forward<D_z>(d_z))));
  }

  /** @brief
      State vector of conservative variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The vector of conservative variables

      @ingroup VariablesPrimary
   */
  template<bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr conservative(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 5,
                transpose ? 5 : 1,
                _options > (std::move(rho(std::forward<Vars>(vars)...)),
                            std::move(rhov<0>(std::forward<Vars>(vars)...)),
                            std::move(rhov<1>(std::forward<Vars>(vars)...)),
                            std::move(rhov<2>(std::forward<Vars>(vars)...)),
                            std::move(rhoE(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 5, transpose ? 5 : 1,
           _options > (std::move(rho(std::forward<Vars>(vars)...)),
                       std::move(rhov<0>(std::forward<Vars>(vars)...)),
                       std::move(rhov<1>(std::forward<Vars>(vars)...)),
                       std::move(rhov<2>(std::forward<Vars>(vars)...)),
                       std::move(rhoE(std::forward<Vars>(vars)...)));
  }

  /** @brief
      State vector of primitive variables in 3d

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The vector of primitive variables

      @ingroup VariablesSecondary
   */
  template<bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr primitive(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 5,
                transpose ? 5 : 1,
                _options > (std::move(rho(std::forward<Vars>(vars)...)),
                            std::move(v<0>(std::forward<Vars>(vars)...)),
                            std::move(v<1>(std::forward<Vars>(vars)...)),
                            std::move(v<2>(std::forward<Vars>(vars)...)),
                            std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 5, transpose ? 5 : 1,
           _options > (std::move(rho(std::forward<Vars>(vars)...)),
                       std::move(v<0>(std::forward<Vars>(vars)...)),
                       std::move(v<1>(std::forward<Vars>(vars)...)),
                       std::move(v<2>(std::forward<Vars>(vars)...)),
                       std::move(p(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Vector of characteristic variables in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   vars   The variables

      @return             The vector of characteristic variables

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           typename N_y,
           typename N_z,
           bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr characteristic(N_x&& n_x,
                                                   N_y&& n_y,
                                                   N_z&& n_z,
                                                   Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 5,
                transpose ? 5 : 1,
                _options > (std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<N_y>(n_y),
                                            std::forward<N_z>(n_z),
                                            std::forward<Vars>(vars)...) -
                                      c(std::forward<Vars>(vars)...)),
                            std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<N_y>(n_y),
                                            std::forward<N_z>(n_z),
                                            std::forward<Vars>(vars)...)),
                            std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<N_y>(n_y),
                                            std::forward<N_z>(n_z),
                                            std::forward<Vars>(vars)...)),
                            std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<N_y>(n_y),
                                            std::forward<N_z>(n_z),
                                            std::forward<Vars>(vars)...)),
                            std::move(v_dir(std::forward<N_x>(n_x),
                                            std::forward<N_y>(n_y),
                                            std::forward<N_z>(n_z),
                                            std::forward<Vars>(vars)...) +
                                      c(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 5, transpose ? 5 : 1,
           _options > (std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Vars>(vars)...) -
                                 c(std::forward<Vars>(vars)...)),
                       std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Vars>(vars)...)),
                       std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Vars>(vars)...)),
                       std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Vars>(vars)...)),
                       std::move(v_dir(std::forward<N_x>(n_x),
                                       std::forward<N_y>(n_y),
                                       std::forward<N_z>(n_z),
                                       std::forward<Vars>(vars)...) +
                                 c(std::forward<Vars>(vars)...)));
  }

  /** @brief
      First Riemann invariant \f$ w_1 \f$ for conservative
      variables in 3d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   vars   The variables

      @return             The first Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename N_x, typename N_y, typename N_z, typename... Vars>
  static FDBB_INLINE auto constexpr w_1(N_x&& n_x,
                                        N_y&& n_y,
                                        N_z&& n_z,
                                        Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(v_dir(std::forward<N_x>(n_x),
                      std::forward<N_y>(n_y),
                      std::forward<N_z>(n_z),
                      std::forward<Vars>(vars)...) -
                CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
                  c(std::forward<Vars>(vars)...) /
                  (eos::gamma -
                   CONSTANT(1.0, rho(std::forward<Vars>(vars)...))))
#endif
  {
    return v_dir(std::forward<N_x>(n_x),
                 std::forward<N_y>(n_y),
                 std::forward<N_z>(n_z),
                 std::forward<Vars>(vars)...) -
           CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
             c(std::forward<Vars>(vars)...) /
             (eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Second Riemann invariant \f$ w_2 \f$ for conservative
      variables in 3d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The second Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(eos::cv * fdbb::elem_log(fdbb::elem_div(
                            p(std::forward<Vars>(vars)...),
                            fdbb::elem_pow(rho(std::forward<Vars>(vars)...),
                                           eos::gamma))))
#endif
  {
    return eos::cv *
           fdbb::elem_log(fdbb::elem_div(
             p(std::forward<Vars>(vars)...),
             fdbb::elem_pow(rho(std::forward<Vars>(vars)...), eos::gamma)));
  }

  /** @brief
      Third Riemann invariant \f$ w_3 \f$ for conservative
      variables in 3d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Vars   The variable types

      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   vars   The variables

      @return             The third Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename Xi_x, typename Xi_y, typename Xi_z, typename... Vars>
  static FDBB_INLINE auto constexpr w_3(Xi_x&& xi_x,
                                        Xi_y&& xi_y,
                                        Xi_z&& xi_z,
                                        Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(v_dir(std::forward<Xi_x>(xi_x),
                      std::forward<Xi_y>(xi_y),
                      std::forward<Xi_z>(xi_z),
                      std::forward<Vars>(vars)...))
#endif
  {
    return v_dir(std::forward<Xi_x>(xi_x),
                 std::forward<Xi_y>(xi_y),
                 std::forward<Xi_z>(xi_z),
                 std::forward<Vars>(vars)...);
  }

  /** @brief
      Fourth Riemann invariant \f$ w_4 \f$ for conservative
      variables in 3d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The fourth Riemann invariant

      @ingroup VariablesSecondary
   */
  template<typename Tau_x, typename Tau_y, typename Tau_z, typename... Vars>
  static FDBB_INLINE auto constexpr w_4(Tau_x&& tau_x,
                                        Tau_y&& tau_y,
                                        Tau_z&& tau_z,
                                        Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(v_dir(std::forward<Tau_x>(tau_x),
                      std::forward<Tau_y>(tau_y),
                      std::forward<Tau_z>(tau_z),
                      std::forward<Vars>(vars)...))
#endif
  {
    return v_dir(std::forward<Tau_x>(tau_x),
                 std::forward<Tau_y>(tau_y),
                 std::forward<Tau_z>(tau_z),
                 std::forward<Vars>(vars)...);
  }

  /** @brief
      Fifth Riemann invariant \f$ w_5 \f$ for conservative
      variables in 3d.

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   vars   The variables

      @ingroup VariablesSecondary
   */
  template<typename N_x, typename N_y, typename N_z, typename... Vars>
  static FDBB_INLINE auto constexpr w_5(N_x&& n_x,
                                        N_y&& n_y,
                                        N_z&& n_z,
                                        Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(v_dir(std::forward<N_x>(n_x),
                      std::forward<N_y>(n_y),
                      std::forward<N_z>(n_z),
                      std::forward<Vars>(vars)...) +
                CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
                  c(std::forward<Vars>(vars)...) /
                  (eos::gamma -
                   CONSTANT(1.0, rho(std::forward<Vars>(vars)...))))
#endif
  {
    return v_dir(std::forward<N_x>(n_x),
                 std::forward<N_y>(n_y),
                 std::forward<N_z>(n_z),
                 std::forward<Vars>(vars)...) +
           CONSTANT(2.0, rho(std::forward<Vars>(vars)...)) *
             c(std::forward<Vars>(vars)...) /
             (eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
      Vector of Riemann invariants in 3d

      @note
      Current implementation holds only for ideal gases!

      @tparam      N_x    The type of the entries of the x-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_y    The type of the entries of the y-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      N_z    The type of the entries of the z-component of the
      normal vector \f$ \mathbf{n} \f$
      @tparam      Xi_x   The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_y   The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Xi_z   The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\xi} \f$
      @tparam      Tau_x  The type of the entries of the x-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_y  The type of the entries of the y-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Tau_z  The type of the entries of the z-component of the
      tangential vector \f$ \boldsymbol{\tau} \f$
      @tparam      Vars   The variable types

      @param[in]   n_x    The x-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_y    The y-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   n_z    The z-component of the normal vector \f$ \mathbf{n}
      \f$
      @param[in]   xi_x   The x-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_y   The y-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   xi_z   The z-component of the tangential vector \f$
      \boldsymbol{\xi} \f$
      @param[in]   tau_x  The x-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_y  The y-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   tau_z  The z-component of the tangential vector \f$
      \boldsymbol{\tau} \f$
      @param[in]   vars   The variables

      @return             The vector of Riemann invariants

      @ingroup VariablesSecondary
   */
  template<typename N_x,
           typename N_y,
           typename N_z,
           typename Xi_x,
           typename Xi_y,
           typename Xi_z,
           typename Tau_x,
           typename Tau_y,
           typename Tau_z,
           bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr riemann(N_x&& n_x,
                                            N_y&& n_y,
                                            N_z&& n_z,
                                            Xi_x&& xi_x,
                                            Xi_y&& xi_y,
                                            Xi_z&& xi_z,
                                            Tau_x&& tau_x,
                                            Tau_y&& tau_y,
                                            Tau_z&& tau_z,
                                            Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::make_BlockExpr < transpose ? 1 : 5,
                transpose ? 5 : 1,
                _options > (std::move(w_1(std::forward<N_x>(n_x),
                                          std::forward<N_y>(n_y),
                                          std::forward<N_z>(n_z),
                                          std::forward<Vars>(vars)...)),
                            std::move(w_2(std::forward<Vars>(vars)...)),
                            std::move(w_3(std::forward<Xi_x>(xi_x),
                                          std::forward<Xi_y>(xi_y),
                                          std::forward<Xi_z>(xi_z),
                                          std::forward<Vars>(vars)...)),
                            std::move(w_4(std::forward<Tau_x>(tau_x),
                                          std::forward<Tau_y>(tau_y),
                                          std::forward<Tau_z>(tau_z),
                                          std::forward<Vars>(vars)...)),
                            std::move(w_5(std::forward<N_x>(n_x),
                                          std::forward<N_y>(n_y),
                                          std::forward<N_z>(n_z),
                                          std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 1 : 5, transpose ? 5 : 1,
           _options > (std::move(w_1(std::forward<N_x>(n_x),
                                     std::forward<N_y>(n_y),
                                     std::forward<N_z>(n_z),
                                     std::forward<Vars>(vars)...)),
                       std::move(w_2(std::forward<Vars>(vars)...)),
                       std::move(w_3(std::forward<Xi_x>(xi_x),
                                     std::forward<Xi_y>(xi_y),
                                     std::forward<Xi_z>(xi_z),
                                     std::forward<Vars>(vars)...)),
                       std::move(w_4(std::forward<Tau_x>(tau_x),
                                     std::forward<Tau_y>(tau_y),
                                     std::forward<Tau_z>(tau_z),
                                     std::forward<Vars>(vars)...)),
                       std::move(w_5(std::forward<N_x>(n_x),
                                     std::forward<N_y>(n_y),
                                     std::forward<N_z>(n_z),
                                     std::forward<Vars>(vars)...)));
  }
};

/** @brief
    Specialization of the Fluxes structure for conservative
    variables in 3d and arbitrary equation of state

    Inviscid fluxes
    \f[
    F = \begin{bmatrix}
    f_0\\
    f_1\\
    f_2\\
    f_3\\
    f_4
    \end{bmatrix} =
    \begin{bmatrix}
    \rho v_0\\
    \rho v_0^2 + p\\
    \rho v_0 v_1\\
    \rho v_0 v_2\\
    v_0 (\rho E + p)
    \end{bmatrix},
    \f]
    \f[
    G = \begin{bmatrix}
    g_0\\
    g_1\\
    g_2\\
    g_3\\
    g_4
    \end{bmatrix} =
    \begin{bmatrix}
    \rho v_1\\
    \rho v_1 v_0\\
    \rho v_1^2 + p\\
    \rho v_1 v_2\\
    v_1 (\rho E + p)
    \end{bmatrix},
    \f]
    \f[
    H = \begin{bmatrix}
    h_0\\
    h_1\\
    h_2\\
    h_3\\
    h_4
    \end{bmatrix} =
    \begin{bmatrix}
    \rho v_2\\
    \rho v_2 v_0\\
    \rho v_2 v_1\\
    \rho v_2^2 + p\\
    v_2 (\rho E + p)
    \end{bmatrix}.
    \f]
    where \f$ p \f$ is the pressure.
 */
template<typename Var>
struct Fluxes<Var, 3>
{
  /// @brief Variables
  using variables = Var;

  /// @brief Equation of state
  using eos = typename variables::eos;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Inviscid fluxes in 3d, ";
    variables::print(os);
    return os;
  }

  /** @brief
      Component \f$ f_0 = \rho v_0 \f$  of the inviscid flux in x-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 0-th component of the inviscid fluxes in x-direction
     */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 0,
      decltype(variables::template rhov<0>(std::forward<Vars>(vars)...))>::type
#endif
  {
    return variables::template rhov<0>(std::forward<Vars>(vars)...);
  }

  /** @brief
      Component \f$ f_1 = \rho v_0^2 + p \f$ of the inviscid flux in x-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 1st component of the inviscid fluxes in x-direction
    */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 1 &&
        std::is_base_of<EOS_pVT, eos>::value,
      decltype(fdbb::elem_div(
                 fdbb::elem_mul(
                   variables::template rhov<0>(std::forward<Vars>(vars)...),
                   variables::template rhov<0>(std::forward<Vars>(vars)...)),
                 variables::template rho(std::forward<Vars>(vars)...)) +
               eos::p_rhoe(
                 variables::template rho(std::forward<Vars>(vars)...),
                 variables::template e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_div(
             fdbb::elem_mul(
               variables::template rhov<0>(std::forward<Vars>(vars)...),
               variables::template rhov<0>(std::forward<Vars>(vars)...)),
             variables::template rho(std::forward<Vars>(vars)...)) +
           eos::p_rhoe(variables::template rho(std::forward<Vars>(vars)...),
                       variables::template e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ f_2 = \rho v_0 v_1 \f$  of the inviscid flux in x-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 2nd component of the inviscid fluxes in x-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 2,
      decltype(fdbb::elem_mul(
        variables::template rhov<0>(std::forward<Vars>(vars)...),
        variables::template v<1>(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template rhov<0>(std::forward<Vars>(vars)...),
      variables::template v<1>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ f_3 = \rho v_0 v_2 \f$  of the inviscid flux in x-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 3rd component of the inviscid fluxes in x-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 3,
      decltype(fdbb::elem_mul(
        variables::template rhov<0>(std::forward<Vars>(vars)...),
        variables::template v<2>(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template rhov<0>(std::forward<Vars>(vars)...),
      variables::template v<2>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ f_4 = v_0 (\rho E + p) \f$ of the inviscid flux in
      x-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 4th component of the inviscid fluxes in x-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 4 &&
        std::is_base_of<EOS_pVT, eos>::value,
      decltype(fdbb::elem_mul(
        variables::template v<0>(std::forward<Vars>(vars)...),
        (variables::template rhoE(std::forward<Vars>(vars)...) +
         eos::template p_rhoe(
           variables::template rho(std::forward<Vars>(vars)...),
           variables::template e(std::forward<Vars>(vars)...)))))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template v<0>(std::forward<Vars>(vars)...),
      (variables::template rhoE(std::forward<Vars>(vars)...) +
       eos::template p_rhoe(
         variables::template rho(std::forward<Vars>(vars)...),
         variables::template e(std::forward<Vars>(vars)...))));
  }

  /** @brief
      Component \f$ g_0 = \rho v_1 \f$  of the inviscid flux in y-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 0th component of the inviscid fluxes in y-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::y && ivar == 0,
      decltype(variables::template rhov<1>(std::forward<Vars>(vars)...))>::type
#endif
  {
    return variables::template rhov<1>(std::forward<Vars>(vars)...);
  }

  /** @brief
      Component \f$ g_1 = \rho v_1 v_0 \f$  of the inviscid flux in y-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 1st component of the inviscid fluxes in y-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::y && ivar == 1,
      decltype(fdbb::elem_mul(
        variables::template rhov<1>(std::forward<Vars>(vars)...),
        variables::template v<0>(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template rhov<1>(std::forward<Vars>(vars)...),
      variables::template v<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ g_2 = \rho v_1^2 + p \f$ of the inviscid flux in y-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 2nd component of the inviscid fluxes in y-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::y && ivar == 2 &&
        std::is_base_of<EOS_pVT, eos>::value,
      decltype(fdbb::elem_div(
                 fdbb::elem_mul(
                   variables::template rhov<1>(std::forward<Vars>(vars)...),
                   variables::template rhov<1>(std::forward<Vars>(vars)...)),
                 variables::template rho(std::forward<Vars>(vars)...)) +
               eos::p_rhoe(
                 variables::template rho(std::forward<Vars>(vars)...),
                 variables::template e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_div(
             fdbb::elem_mul(
               variables::template rhov<1>(std::forward<Vars>(vars)...),
               variables::template rhov<1>(std::forward<Vars>(vars)...)),
             variables::template rho(std::forward<Vars>(vars)...)) +
           eos::p_rhoe(variables::template rho(std::forward<Vars>(vars)...),
                       variables::template e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ g_3 = \rho v_1 v_2 \f$  of the inviscid flux in y-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 3rd component of the inviscid fluxes in y-direction
          */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::y && ivar == 3,
      decltype(fdbb::elem_mul(
        variables::template rhov<1>(std::forward<Vars>(vars)...),
        variables::template v<2>(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template rhov<1>(std::forward<Vars>(vars)...),
      variables::template v<2>(std::forward<Vars>(vars)...));
  }

  /** @brief Component \f$ g_4 = v_1 (\rho E + p) \f$ of the inviscid flux in
      y-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 4th component of the inviscid fluxes in y-direction
  */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::y && ivar == 4 &&
        std::is_base_of<EOS_pVT, eos>::value,
      decltype(fdbb::elem_mul(
        variables::template v<1>(std::forward<Vars>(vars)...),
        (variables::template rhoE(std::forward<Vars>(vars)...) +
         eos::template p_rhoe(
           variables::template rho(std::forward<Vars>(vars)...),
           variables::template e(std::forward<Vars>(vars)...)))))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template v<1>(std::forward<Vars>(vars)...),
      (variables::template rhoE(std::forward<Vars>(vars)...) +
       eos::template p_rhoe(
         variables::template rho(std::forward<Vars>(vars)...),
         variables::template e(std::forward<Vars>(vars)...))));
  }

  /** @brief
      Component \f$ h_0 = \rho v_2 \f$  of the inviscid flux in z-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 0th component of the inviscid fluxes in z-direction
  */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::z && ivar == 0,
      decltype(variables::template rhov<2>(std::forward<Vars>(vars)...))>::type
#endif
  {
    return variables::template rhov<2>(std::forward<Vars>(vars)...);
  }

  /** @brief
      Component \f$ h_1 = \rho v_2 v_0 \f$  of the inviscid flux in z-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 1sy component of the inviscid fluxes in z-direction
   */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::z && ivar == 1,
      decltype(fdbb::elem_mul(
        variables::template rhov<2>(std::forward<Vars>(vars)...),
        variables::template v<0>(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template rhov<2>(std::forward<Vars>(vars)...),
      variables::template v<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ h_2 = \rho v_2 v_1 \f$  of the inviscid flux in z-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 2nd component of the inviscid fluxes in z-direction
  */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::z && ivar == 2,
      decltype(fdbb::elem_mul(
        variables::template rhov<2>(std::forward<Vars>(vars)...),
        variables::template v<1>(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template rhov<2>(std::forward<Vars>(vars)...),
      variables::template v<1>(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ h_3 = \rho v_2^2 + p \f$ of the inviscid flux in z-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 3rd component of the inviscid fluxes in z-direction
          */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::z && ivar == 3 &&
        std::is_base_of<EOS_pVT, eos>::value,
      decltype(fdbb::elem_div(
                 fdbb::elem_mul(
                   variables::template rhov<2>(std::forward<Vars>(vars)...),
                   variables::template rhov<2>(std::forward<Vars>(vars)...)),
                 variables::template rho(std::forward<Vars>(vars)...)) +
               eos::p_rhoe(
                 variables::template rho(std::forward<Vars>(vars)...),
                 variables::template e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_div(
             fdbb::elem_mul(
               variables::template rhov<2>(std::forward<Vars>(vars)...),
               variables::template rhov<2>(std::forward<Vars>(vars)...)),
             variables::template rho(std::forward<Vars>(vars)...)) +
           eos::p_rhoe(variables::template rho(std::forward<Vars>(vars)...),
                       variables::template e(std::forward<Vars>(vars)...));
  }

  /** @brief
      Component \f$ h_4 = v_2 (\rho E + p) \f$ of the inviscid flux in
      z-direction

      @tparam   coord   The coordinate enumerator
      @tparam   INDEX_T The variable index
      @tparam   Vars    The variable types

      @param    vars    The variables

      @return           The 4th component of the inviscid fluxes in z-direction
  */
  template<EnumCoord coord, INDEX_T ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::z && ivar == 4 &&
        std::is_base_of<EOS_pVT, eos>::value,
      decltype(fdbb::elem_mul(
        variables::template v<2>(std::forward<Vars>(vars)...),
        (variables::template rhoE(std::forward<Vars>(vars)...) +
         eos::template p_rhoe(
           variables::template rho(std::forward<Vars>(vars)...),
           variables::template e(std::forward<Vars>(vars)...)))))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template v<2>(std::forward<Vars>(vars)...),
      (variables::template rhoE(std::forward<Vars>(vars)...) +
       eos::template p_rhoe(
         variables::template rho(std::forward<Vars>(vars)...),
         variables::template e(std::forward<Vars>(vars)...))));
  }

  /** @brief
      Tensor of the inviscid fluxes in x-, y- and z-direction

      @tparam      Vars   The variable types

      @param[in]   vars   The variables

      @return             The tensor of the inviscid fluxes in x-, y- and
      z-direction
   */

  // Row-major storage
  template<bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      (_options == StorageOptions::RowMajor && !transpose) ||
        (_options == StorageOptions::ColMajor && transpose),
      decltype(fdbb::make_BlockExpr < transpose ? 3 : 5,
               transpose ? 5 : 3,
               _options > (std::move(inviscid<EnumCoord::x, 0>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 0>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 0>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 1>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 1>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 1>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 2>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 2>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 2>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 3>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 3>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 3>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 4>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 4>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 4>(
                             std::forward<Vars>(vars)...))))>::type
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 3 : 5, transpose ? 5 : 3,
           _options >
             (std::move(inviscid<EnumCoord::x, 0>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 0>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 0>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 1>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 1>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 1>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 2>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 2>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 2>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 3>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 3>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 3>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 4>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 4>(std::forward<Vars>(vars)...)),
              std::move(
                inviscid<EnumCoord::z, 4>(std::forward<Vars>(vars)...)));
  }

  // Column-major storage
  template<bool transpose = false,
           StorageOptions _options = StorageOptions::RowMajor,
           typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      (_options == StorageOptions::ColMajor && !transpose) ||
        (_options == StorageOptions::RowMajor && transpose),
      decltype(fdbb::make_BlockExpr < transpose ? 3 : 5,
               transpose ? 5 : 3,
               _options > (std::move(inviscid<EnumCoord::x, 0>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 1>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 2>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 3>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::x, 4>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 0>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 1>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 2>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 3>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::y, 4>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 0>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 1>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 2>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 3>(
                             std::forward<Vars>(vars)...)),
                           std::move(inviscid<EnumCoord::z, 4>(
                             std::forward<Vars>(vars)...))))>::type
#endif
  {
    return fdbb::make_BlockExpr < transpose ? 3 : 5, transpose ? 5 : 3,
           _options >
             (std::move(inviscid<EnumCoord::x, 0>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 1>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 2>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 3>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::x, 4>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 0>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 1>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 2>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 3>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::y, 4>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 0>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 1>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 2>(std::forward<Vars>(vars)...)),
              std::move(inviscid<EnumCoord::z, 3>(std::forward<Vars>(vars)...)),
              std::move(
                inviscid<EnumCoord::z, 4>(std::forward<Vars>(vars)...)));
  }

  /** @brief Viscous fluxes
      @ingroup FluxesViscous
   */
};

/** @brief
    Specialization of the Jacobians structure for conservative
    variables in 3d and arbitrary equation of state
 */
template<typename Var>
struct Jacobians<Var, 3>
{
  /// @brief Variables
  using variables = Var;

  /// @brief Equation of state
  using eos = typename variables::eos;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Inviscid flux-Jacobian in 3d, ";
    variables::print(os);
    return os;
  }
};

} // namespace fluids
} // namespace fdbb

#endif // FDBB_FLUIDS_CONSERVATIVE_3D_H
