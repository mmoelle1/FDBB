/** @file fdbb/Fluids/Types.hpp
 *
 *  @brief Types
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FLUIDS_TYPES_HPP
#define FDBB_FLUIDS_TYPES_HPP

#include <Core/Config.hpp>
#include <Fluids/Enums.hpp>
#include <Fluids/Traits.hpp>

namespace fdbb {

/** @namespace fdbb::fluids
 *
 *  @brief
 *  The \ref fdbb::fluids namespace, containing the fluid dynamics
 *  part of the FDBB library
 *
 *  The \ref fdbb::fluid namespace contains the implementation of the
 *  fluid dynamics part of the FDBB library. Like the functionality in
 *  the \ref fdbb namespace, the fluid dynamics part has a stable API
 *  that does not change over future releases of the FDBB library.
 */
namespace fluids {

/** @struct Variables
 *
 *  @brief
 *  The Variables structure provides member functions to calculate
 *  primary and secondary variables from the vector of variables
 *
 *  The Variables structure requires an Equation-of-state
 *  structure EOS to compute secondary variables
 *
 *  @tparam   EOS      The equation of state type
 *  @tparam   dim      The spatial dimension
 *  @tparam   Form     The formulation type
 *  @tparam   Traits   The type traits
 */
template<typename EOS,
         INDEX_T dim,
         EnumForm Form,
         typename Traits = TraitsPerfectForwarding<dim, Form>>
struct Variables
{
  /**
   * @defgroup   VariablesPrimary     The primary variables
   * @defgroup   VariablesSecondary   The secondary variables
   */
};

/** @struct Fluxes
 *
 *  @brief
 *  The Fluxes structure provides member functions to calculate
 *  inviscid and viscous fluxes from the vector of variables
 *
 *  The Fluxes structure requires a Variables structure to compute
 *  primary and secondary variables
 *
 *  @tparam   Var   The variable type
 *  @tparam   dim   The spatial dimension
 */
template<typename Var, INDEX_T dim = Var::dim>
struct Fluxes
{
  /**
   * @defgroup   FluxesInviscid   The inviscid fluxes
   * @defgroup   FluxesViscous    The viscous fluxes
   */
};

/** @struct Jacobians
 *
 *  @brief
 *  The Jacobians structure provides member functions to calculate
 *  inviscid and viscous flux-Jacobians from the vector of variables
 *
 *  The Jacobians structure requires a Variables structure to
 *  compute primary and secondary variables
 *
 *  @tparam   Var   The variable type
 *  @tparam   dim   The spatial dimension
 */
template<typename Var, INDEX_T dim>
struct Jacobians
{
  /**
   * @defgroup   JacobiansInviscid   The Jacobians for the inviscid fluxes
   * @defgroup   JacobiansViscous    The Jacobians for the viscous fluxes
   */
};

} // namespace fluids
} // namespace fdbb

#endif // FDBB_FLUIDS_TYPES_HPP
