/** @file fdbb/Fluids/Enums.hpp
 *
 *  @brief FDBB enumerators
 *
 *  This file contains the enumerators used by the Fluid Dynamics
 *  Building Block (FDBB) library.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FLUIDS_ENUMS_HPP
#define FDBB_FLUIDS_ENUMS_HPP

#include <map>

#include <Core/Config.hpp>

namespace fdbb {

namespace fluids {

/** @enum EnumForm
 *
 *  @brief
 *  The fdbb::fluids::EnumForm enumeration type, listing all possible
 *  types of formulations supported by the library
 */
enum class EnumForm
{
  invalid,

  /** @brief Conservative variables \cite hirsch2007
      \f[
      U = \left[\rho,
      \rho \mathbf{v},
      \rho E
      \right]^\top
      \f]
      where
      \f$ \rho \f$ is the volumetric mass density,
      \f$ \rho \mathbf{v} \f$ is the momentum, and
      \f$ \rho E \f$ is the total energy per unit volume
   */
  conservative,

  /** @brief Entropy variables \cite diosady2014
      \f[
      V = \left[-\frac{s}{\gamma-1}+
      \frac{\gamma+1}{\gamma-1}-\frac{\rho E}{p},
      \frac{\mathbf{m}}{p},
      -\frac{\rho}{p}
      \right]^\top
      \f]
      where
      \f$ \rho \f$ is the volumetric mass density,
      \f$ \rho \mathbf{v} \f$ is the momentum,
      \f$ p \f$ is the absolute pressure,
      \f$ \rho E \f$ is the total energy per unit volume,
      \f$ s \f$ is the entropy, and
      \f$ \gamma \f$ is the ratio of specific heats (adiabatic index)
   */
  entropy,

  /** @brief Primitive variables \cite hirsch2007
      \f[
      Q = \left[ \rho, \mathbf{v}, p \right]^\top
      \f]
      where
      \f$ \rho \f$ is the density,
      \f$ \mathbf{v} \f$ is the velocity, and
      \f$ p \f$ is the absolute pressure
   */
  primitive,

  /** @brief Riemann invariants \cite wesseling2009principles
      \f[
      W = \left[ \frac{v_n-2c}{\gamma-1},
      c_v log(\frac{p}{\rho^\gamma},
      v_\xi,
      v_\tau,
      \frac{v_n+2c}{\gamma-1}
      \right]^\top
      \f]
      where
      \f$ v_n \f$ is the velocity component normal to the boundary,
      \f$ c \f$ is the speed of sound,
      \f$ \gamma \f$ is the adiabatic index,
      \f$ c_v \f$ is the specific heat at constant volume,
      \f$ p * \f$ is the absolute pressure,
      \f$ \rho \f$ is the volumetric mass density,
      \f$ v_tan = v_\xi + v_\tau \f$ is the velocity vector
      tangential to the boundary (if exists)

      @note
      Current implementation holds only for ideal gases!
   */
  Riemann_invariants
};

/// @brief Map from fdbb::fluids::EnumForm type to std::string
std::map<EnumForm, std::string> EnumFormMap = {
  { EnumForm::invalid, "invalid" },
  { EnumForm::conservative, "conservative" },
  { EnumForm::entropy, "entropy" },
  { EnumForm::primitive, "primitive" },
  { EnumForm::Riemann_invariants, "Riemann_invariants" }
};

/// @brief Map from std::string to fdbb::fluids::EnumForm type
std::map<std::string, EnumForm> EnumFormMapRev = {
  { "invalid", EnumForm::invalid },
  { "conservative", EnumForm::conservative },
  { "entropy", EnumForm::entropy },
  { "primitive", EnumForm::primitive },
  { "Riemann_invariants", EnumForm::Riemann_invariants }
};

/** @brief
    Print fdbb::fluids::EnumForm to output stream

    @param[in] os  The output stream
    @param[in] obj The formulation enumerator

    @return        The output stream with printing of fdbb::fluids::EnumForm
 */
std::ostream&
operator<<(std::ostream& os, const EnumForm& obj)
{
  os << EnumFormMap[obj];
  return os;
}

/** @enum EnumCoord
 *
 *  @brief
 *  The fdbb::fluids::EnumCoord enumeration type, listing all possible types
 *  of coordinate directions supported by the library
 */
enum class EnumCoord
{
  invalid,

  /// @brief x-coordinate direction in Cartesian coordinate system
  x,

  /// @brief y-coordinate direction in Cartesian coordinate system
  y,

  /// @brief z-coordinate direction in Cartesian coordinate system
  z,

  /// @brief temporal direction in space-time formulation
  t,
};

/// @brief Map from fdbb::fluids::EnumCoord type to std::string
std::map<EnumCoord, std::string> EnumCoordMap = { { EnumCoord::invalid,
                                                    "invalid" },
                                                  { EnumCoord::x, "x" },
                                                  { EnumCoord::y, "y" },
                                                  { EnumCoord::z, "z" },
                                                  { EnumCoord::t, "t" } };

/// @brief Map from std::string to fdbb::fluids::EnumCoord type
std::map<std::string, EnumCoord> EnumCoordMapRev = { { "invalid",
                                                       EnumCoord::invalid },
                                                     { "x", EnumCoord::x },
                                                     { "y", EnumCoord::y },
                                                     { "z", EnumCoord::z },
                                                     { "t", EnumCoord::t } };

/** @brief
    Print fdbb::fluids::EnumCoord to output stream

    @param[in] os  The output stream
    @param[in] obj The coordinate enumerator

    @return        The output stream with printing of fdbb::fluids::EnumCoord
 */
std::ostream&
operator<<(std::ostream& os, const EnumCoord& obj)
{
  os << EnumCoordMap[obj];
  return os;
}

/** @enum EnumVar
 *
 *  @brief
 *  The fdbb::fluids::EnumVar enumeration type, listing all possible types of
 *  variables directly supported by the library
 */
enum class EnumVar
{
  invalid,

  /// @brief volumetric mass density: \f$ \rho \f$
  rho,

  /// @brief momentum in x-direction: \f$ \rho v_x \f$
  rhov_x,

  /// @brief momentum in y-direction: \f$ \rho v_y \f$
  rhov_y,

  /// @brief momentum in z-direction: \f$ \rho v_z \f$
  rhov_z,

  /// @brief total energy per unit volume: \f$ \rho E \f$
  rhoE,

  /// @brief internal energy per unit volume: \f$ \rho e \f$
  rhoe,

  /// @brief total enthalpy per unit volume: \f$ \rho H \f$
  rhoH,

  /// @brief enthalpy per unit volume: \f$ \rho h \f$
  rhoh,

  /// @brief velocity in x-direction: \f$ v_x \f$
  v_x,

  /// @brief velocity in y-direction: \f$ v_y \f$
  v_y,

  /// @brief velocity in z-direction: \f$ v_z \f$
  v_z,

  /// @brief total energy per unit mass: \f$ E \f$
  E,

  /// @brief internal energy per unit mass: \f$ e \f$
  e,

  /// @brief total enthalpy per unit volume: \f$ H \f$
  H,

  /// @brief enthalpy per unit volume: \f$ h \f$
  h,

  /// @brief absolute pressure: \f$ p \f$
  p,

  /// @brief first Riemann invariant: \f$ w_1 \f$
  w_1,

  /// @brief second Riemann invariant: \f$ w_2 \f$
  w_2,

  /// @brief third Riemann invariant: \f$ w_3 \f$
  w_3,

  /// @brief fourth Riemann invariant: \f$ w_4 \f$
  w_4,

  /// @brief fifth Riemann invariant: \f$ w_5 \f$
  w_5,
};

/// @brief Map from fdbb::fluids::EnumVar type to std::string
std::map<EnumVar, std::string> EnumVarMap = { { EnumVar::invalid, "invalid" },
                                              { EnumVar::rho, "rho" },
                                              { EnumVar::rhov_x, "rhov_x" },
                                              { EnumVar::rhov_y, "rhov_y" },
                                              { EnumVar::rhov_z, "rhov_z" },
                                              { EnumVar::rhoE, "rhoE" },
                                              { EnumVar::rhoe, "rhoe" },
                                              { EnumVar::rhoH, "rhoH" },
                                              { EnumVar::rhoh, "rhoh" },
                                              { EnumVar::v_x, "v_x" },
                                              { EnumVar::v_y, "v_y" },
                                              { EnumVar::v_z, "v_z" },
                                              { EnumVar::E, "E" },
                                              { EnumVar::e, "e" },
                                              { EnumVar::H, "H" },
                                              { EnumVar::h, "h" },
                                              { EnumVar::p, "p" },
                                              { EnumVar::w_1, "w_1" },
                                              { EnumVar::w_2, "w_2" },
                                              { EnumVar::w_3, "w_3" },
                                              { EnumVar::w_4, "w_4" },
                                              { EnumVar::w_5, "w_5" } };

/// @brief Map from std::string to fdbb::fluids::EnumVar type
std::map<std::string, EnumVar> EnumVarMapRev = { { "invalid",
                                                   EnumVar::invalid },
                                                 { "rho", EnumVar::rho },
                                                 { "rhov_x", EnumVar::rhov_x },
                                                 { "rhov_y", EnumVar::rhov_y },
                                                 { "rhov_z", EnumVar::rhov_z },
                                                 { "rhoE", EnumVar::rhoE },
                                                 { "rhoe", EnumVar::rhoe },
                                                 { "rhoH", EnumVar::rhoH },
                                                 { "rhoh", EnumVar::rhoh },
                                                 { "v_x", EnumVar::v_x },
                                                 { "v_y", EnumVar::v_y },
                                                 { "v_z", EnumVar::v_z },
                                                 { "E", EnumVar::E },
                                                 { "e", EnumVar::e },
                                                 { "H", EnumVar::H },
                                                 { "h", EnumVar::h },
                                                 { "p", EnumVar::p },
                                                 { "w_1", EnumVar::w_1 },
                                                 { "w_2", EnumVar::w_2 },
                                                 { "w_3", EnumVar::w_3 },
                                                 { "w_4", EnumVar::w_4 },
                                                 { "w_5", EnumVar::w_5 } };

/** @brief
    Print fdbb::fluids::EnumVar to output stream

    @param[in] os  The output stream
    @param[in] obj The variable enumerator

    @return        The output stream with printing of fdbb::fluids::EnumVar
 */
std::ostream&
operator<<(std::ostream& os, const EnumVar& obj)
{
  os << EnumVarMap[obj];
  return os;
}

} // namespace fluids
} // namespace fdbb

#endif // FDBB_FLUIDS_ENUMS_HPP
