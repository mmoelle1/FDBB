/** @file fdbb/BlockExpression/BlockMatrixView.hpp
 *
 *  @brief Block matrix view
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BLOCKEXPRESSION_BLOCK_MATRIX_VIEW_HPP
#define FDBB_BLOCKEXPRESSION_BLOCK_MATRIX_VIEW_HPP

#include <cassert>
#include <iostream>
#include <tuple>

#include <BlockExpression/ForwardDeclarations.hpp>
#include <Core/Config.hpp>
#include <Core/Functors.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

/** @brief
    A view on a fixed-size block matrix

    This is a view on a fixed-size block matrix. It supports exactly
    the same functionality but keeps only references to external data.

    @note
    Block matrix views are stored in a std::tuple. The template
    parameter \a _options specifies the storage order. The default
    storage order is row-major. Note that it is not possible to
    perform arithmetic operations between block expressions stored in
    different storage orders.
 */
template<typename T,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options>
struct BlockMatrixView : public BlockMatrixBase
{
private:
  /// @brief Self type
  using self_type = BlockMatrixView<T, _rows, _cols, _options>;

  /// @brief Expression type
  using expr_type = typename fdbb::utils::tuple_t<T*, _rows * _cols>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

  /// @brief Size (rows * columns)
  static constexpr std::size_t size = _rows * _cols;

  /// @brief Storage options
  static constexpr StorageOptions options = _options;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  BlockMatrixView() = delete;

  /// @brief Constructor (store pointers to arguments of parameter pack)
  template<typename... Ts,
           typename = typename std::enable_if<
             !std::is_base_of<
               BlockBase,
               typename fdbb::utils::remove_all<typename std::tuple_element<
                 0,
                 std::template tuple<Ts...>>::type>::type>::value &&
             _cols * _rows == sizeof...(Ts)>::type>
  explicit BlockMatrixView(Ts&&... args) noexcept
    : expr{ &args... }
  {}

  /// @brief Constructor (copy pointers to arguments of parameter pack)
  template<typename... Ts,
           typename = typename std::enable_if<
             !std::is_base_of<
               BlockBase,
               typename fdbb::utils::remove_all<typename std::tuple_element<
                 0,
                 std::template tuple<Ts...>>::type>::type>::value &&
             _cols * _rows == sizeof...(Ts)>::type>
  explicit BlockMatrixView(Ts*... args) noexcept
    : expr{ args... }
  {}

  /// @brief Constructor (copy from expression passed as std::tuple)
  explicit BlockMatrixView(const expr_type& expr)
    : expr{ expr }
  {}

  /// @brief Constructor (move from expression passed as std::tuple)
  explicit BlockMatrixView(expr_type&& expr)
    : expr{ std::move(expr) }
  {}

  /// @brief Constructor (copy from block matrix view)
  BlockMatrixView(
    const BlockMatrixView<T, _rows, _cols, _options>& other) noexcept
    : expr{ other.get() }
  {}

  /// @brief Constructor (move from block matrix view)
  BlockMatrixView(BlockMatrixView<T, _rows, _cols, _options>&& other) noexcept
    : expr{ std::move(other.get()) }
  {}

  /// @brief Constructor (copy from block matrix)
  BlockMatrixView(BlockMatrix<T, _rows, _cols, _options>& other) noexcept
    : expr(fdbb::utils::apply(
        fdbb::functor::make_tuple(),
        fdbb::utils::for_each_return(other.get(), fdbb::functor::ref())))
  {}

  /// @brief Returns the transposed of the block matrix
  constexpr const BlockMatrixView<T, _cols, _rows, _options> transpose()
    const noexcept
  {
    return BlockMatrixView<T, _cols, _rows, _options>(fdbb::utils::apply(
      fdbb::functor::make_tuple(),
      fdbb::utils::for_each_return<0, 1, _cols, 0, 0, 0>(
        fdbb::utils::make_tuple(
          fdbb::utils::make_index_sequence<_rows * _cols>{}),
        fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<_cols + 1>{}),
        fdbb::utils::tie(this->get()),
        fdbb::functor::transpose())));
  }

  /// @brief Returns constant reference to block entry from position
  /// \a idx
  constexpr const T& operator()(const std::size_t& idx) const noexcept
  {
#if __cplusplus > 201103L
    assert(idx < _rows * _cols);
#endif
    return *expr[idx];
  }

  /// @brief Returns reference to block entry from position \a idx
  T& operator()(const std::size_t& idx) noexcept
  {
    assert(idx < _rows * _cols);
    return *expr[idx];
  }

  /// @brief Returns constant reference to Ith block entry
  template<std::size_t I>
  constexpr const T& get() const noexcept
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return *std::get<I>(expr);
  }

  /// @brief Returns reference to Ith block entry
  template<std::size_t I>
  T& get() noexcept
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return *std::get<I>(expr);
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const noexcept { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() noexcept { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const noexcept { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() noexcept { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const noexcept
  {
    os << "Block matrix view[" << _rows << "x" << _cols << ","
       << (_options == StorageOptions::RowMajor ? "RowMajor" : "ColMajor")
       << "]: " << fdbb::utils::for_each_return(expr, fdbb::functor::size());
  }

  /// @brief Applies lambda expression/functor to all block entries
  template<typename F>
  void apply(F f)
  {
    fdbb::utils::for_each(expr, f);
  }
};

/// @brief Prints a short description of the block matrix
template<typename T,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options>
std::ostream&
operator<<(std::ostream& os,
           const BlockMatrixView<T, _rows, _cols, _options>& obj)
{
  obj.print(os);
  return os;
}

namespace utils {

/// @brief Returns reference to the Ith element of the block matrix view
template<std::size_t I,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options>
constexpr T&
get(BlockMatrixView<T, _rows, _cols, _options>& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block matrix view
template<std::size_t I,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options>
constexpr T&&
get(BlockMatrixView<T, _rows, _cols, _options>&& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block matrix
/// view
template<std::size_t I,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options>
constexpr const T&
get(const BlockMatrixView<T, _rows, _cols, _options>& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

} // namespace utils

} // namespace fdbb

namespace std {

/** @brief
    Spezialization of std::tuple_size for type fdbb::BlockMatrixView

    @tparam   T          The type of the scalar expression
    @tparam   _rows      The number of rows
    @tparam   _cols      The number of column
    @tparam   _options   The storage options
 */
template<typename T,
         std::size_t _rows,
         std::size_t _cols,
         ::fdbb::StorageOptions _options>
class tuple_size<fdbb::BlockMatrixView<T, _rows, _cols, _options>>
  : public integral_constant<std::size_t, _rows * _cols>
{};

} // namespace std

#endif // FDBB_BLOCKEXPRESSION_BLOCK_MATRIX_VIEW_HPP
