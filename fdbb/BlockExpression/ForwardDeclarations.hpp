/** @file fdbb/BlockExpression/ForwardDeclarations.hpp
 *
 *  @brief Forward declarations of block expressions
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BLOCKEXPRESSION_FORWARD_DECLARATIONS_HPP
#define FDBB_BLOCKEXPRESSION_FORWARD_DECLARATIONS_HPP

#include <iostream>

namespace fdbb {

/** @enum StorageOptions
 *
 *  @brief
 *  The fdbb::StorageOptions enumeration type, listing all possible
 *  storage options supported by the library
 */
enum class StorageOptions
{
  /** @brief
      Storage order is column major
  */
  ColMajor = 0,

  /** @brief
      Storage order is row major
  */
  RowMajor = 1
};

/** @brief
    Base class of all block-expression objects
 */
struct BlockBase
{};

/** @brief
    Base class of all block-vector objects
 */
struct BlockVectorBase : public BlockBase
{};

/** @brief
    Base class of all block row-vector objects
 */
struct BlockRowVectorBase : public BlockVectorBase
{};

/** @brief
    Base class of all block column-vector objects
 */
struct BlockColVectorBase : public BlockVectorBase
{};

/** @brief
    Base class of all block-matrix objects
 */
struct BlockMatrixBase : public BlockBase
{};

namespace detail {
/** @brief
    Block matrix object (forward declaration)
 */
template<typename T,
         std::size_t _rows = 1,
         std::size_t _cols = 1,
         StorageOptions _options = StorageOptions::RowMajor>
struct BlockMatrix;
} // namespace detail

/** @brief
    Block matrix object (forward declaration)
 */
template<typename T,
         std::size_t _rows = 1,
         std::size_t _cols = 1,
         StorageOptions _options = StorageOptions::RowMajor>
struct BlockMatrix;

/** @brief
    Block matrix view object (forward declaration)
 */
template<typename T,
         std::size_t _rows = 1,
         std::size_t _cols = 1,
         StorageOptions _options = StorageOptions::RowMajor>
struct BlockMatrixView;

/** @brief
    Block expression object (forward declaration)
 */
template<std::size_t _rows = 1,
         std::size_t _cols = 1,
         StorageOptions _options = StorageOptions::RowMajor,
         typename... Ts>
struct BlockExpr;

/** @brief
    Block row-vector object (forward declaration)
 */
template<typename T, std::size_t _rows = 1>
using BlockRowVector = fdbb::BlockMatrix<T, _rows, 1>;

/** @brief
    Block row-vector view object (forward declaration)
 */
template<typename T, std::size_t _rows = 1>
using BlockRowVectorView = fdbb::BlockMatrixView<T, _rows, 1>;

/** @brief
    Block column-vector object (forward declaration)
 */
template<typename T, std::size_t _cols = 1>
using BlockColVector = fdbb::BlockMatrix<T, 1, _cols>;

/** @brief
    Block column-vector view object (forward declaration)
 */
template<typename T, std::size_t _cols = 1>
using BlockColVectorView = fdbb::BlockMatrixView<T, 1, _cols>;

} // namespace fdbb

#endif // FDBB_BLOCKEXPRESSION_FORWARD_DECLARATIONS_HPP
