/** @file fdbb/BlockExpression/BlockOperations.hpp
 *
 *  @brief Block operations
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BLOCKEXPRESSION_BLOCK_OPERATIONS_HPP
#define FDBB_BLOCKEXPRESSION_BLOCK_OPERATIONS_HPP

#include <tuple>

#include <BlockExpression/BlockExpression.hpp>
#include <BlockExpression/ForwardDeclarations.hpp>
#include <Core/Config.hpp>
#include <Core/Functors.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

/// @brief Tags terminals with a unique tag.
template<std::size_t Tag,
         std::size_t TagIncr,
         typename A,
         typename = typename std::enable_if<std::is_base_of<
           BlockBase,
           typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
tag(A&& a) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1>(
                               a.get(),
                               fdbb::functor::tag<Tag, TagIncr>()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1>(
    a.get(), fdbb::functor::tag<Tag, TagIncr>());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Unary operator+ for a type that is derived from the base
/// class fdbb::BlockBase
template<typename A,
         typename = typename std::enable_if<std::is_base_of<
           BlockBase,
           typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator+(A&& a) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<
    fdbb::utils::remove_all<A>::type::rows,
    fdbb::utils::remove_all<A>::type::cols,
    fdbb::utils::remove_all<A>::type::options,
    decltype(fdbb::utils::for_each_return<0, 1>(a.get(),
                                                fdbb::functor::plus()))>::type
#endif
{
  auto ExprType =
    fdbb::utils::for_each_return<0, 1>(a.get(), fdbb::functor::plus());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator+ between two types that are derived from
/// the base class fdbb::BlockBase
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::rows ==
             fdbb::utils::remove_all<B>::type::rows &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::cols>::type>
auto
operator+(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                               a.get(),
                               b.get(),
                               fdbb::functor::add()))>::type
#endif
{
  static_assert(fdbb::utils::remove_all<A>::type::options ==
                  fdbb::utils::remove_all<B>::type::options,
                "A and B must have same storage options");
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
    a.get(), b.get(), fdbb::functor::add());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator+ between an arithmetic type and a type that
/// is derived form the base class fdbb::BlockBase
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<A>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<B>::type>::value>::type>
auto
operator+(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<B>::type::rows,
                             fdbb::utils::remove_all<B>::type::cols,
                             fdbb::utils::remove_all<B>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               b.get(),
                               fdbb::utils::tie(a),
                               fdbb::functor::add()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    b.get(), fdbb::utils::tie(a), fdbb::functor::add());
  return typename BlockExpr_type<fdbb::utils::remove_all<B>::type::rows,
                                 fdbb::utils::remove_all<B>::type::cols,
                                 fdbb::utils::remove_all<B>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator+ between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator+(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::add()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::add());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator+= between two types that are derived from
/// the base class fdbb::BlockBase
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::rows ==
             fdbb::utils::remove_all<B>::type::rows &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::cols>::type>
auto
operator+=(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                               a.get(),
                               b.get(),
                               fdbb::functor::add_assign()))>::type
#endif
{
  static_assert(fdbb::utils::remove_all<A>::type::options ==
                  fdbb::utils::remove_all<B>::type::options,
                "A and B must have same storage options");
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
    a.get(), b.get(), fdbb::functor::add_assign());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator+= between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator+=(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::add_assign()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::add_assign());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Unary operator- for a type that is derived from the base
/// class fdbb::BlockBase
template<typename A,
         typename = typename std::enable_if<std::is_base_of<
           BlockBase,
           typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator-(A&& a) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<
    fdbb::utils::remove_all<A>::type::rows,
    fdbb::utils::remove_all<A>::type::cols,
    fdbb::utils::remove_all<A>::type::options,
    decltype(fdbb::utils::for_each_return<0, 1>(a.get(),
                                                fdbb::functor::minus()))>::type
#endif
{
  auto ExprType =
    fdbb::utils::for_each_return<0, 1>(a.get(), fdbb::functor::minus());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator- between two types that are derived from
/// the base class fdbb::BlockBase
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::rows ==
             fdbb::utils::remove_all<B>::type::rows &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::cols>::type>
auto
operator-(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                               a.get(),
                               b.get(),
                               fdbb::functor::sub()))>::type
#endif
{
  static_assert(fdbb::utils::remove_all<A>::type::options ==
                  fdbb::utils::remove_all<B>::type::options,
                "A and B must have same storage options");
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
    a.get(), b.get(), fdbb::functor::sub());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator- between an arithmetic type and a type that
/// is derived form the base class fdbb::BlockBase
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<A>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<B>::type>::value>::type>
auto
operator-(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<B>::type::rows,
                             fdbb::utils::remove_all<B>::type::cols,
                             fdbb::utils::remove_all<B>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               b.get(),
                               fdbb::utils::tie(a),
                               fdbb::functor::minus_sub()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    b.get(), fdbb::utils::tie(a), fdbb::functor::minus_sub());
  return typename BlockExpr_type<fdbb::utils::remove_all<B>::type::rows,
                                 fdbb::utils::remove_all<B>::type::cols,
                                 fdbb::utils::remove_all<B>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator- between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator-(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::sub()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::sub());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator-= between two types that are derived from
/// the base class fdbb::BlockBase
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::rows ==
             fdbb::utils::remove_all<B>::type::rows &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::cols>::type>
auto
operator-=(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 1>(
                               a.get(),
                               b.get(),
                               fdbb::functor::sub_assign()))>::type
#endif
{
  static_assert(fdbb::utils::remove_all<A>::type::options ==
                  fdbb::utils::remove_all<B>::type::options,
                "A and B must have same storage options");
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 1>(
    a.get(), b.get(), fdbb::functor::sub_assign());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator-= between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator-=(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::sub_assign()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::sub_assign());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator* (matmul) between two types that are
/// derived from the base class fdbb::BlockBase
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::rows>::type>
auto
operator*(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<
    fdbb::utils::remove_all<A>::type::rows,
    fdbb::utils::remove_all<B>::type::cols,
    fdbb::utils::remove_all<A>::type::options,
    decltype(fdbb::utils::for_each_return<
             0,
             1,
             fdbb::utils::remove_all<A>::type::rows,
             0,
             0,
             0,
             0,
             0>(
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows *
                              fdbb::utils::remove_all<B>::type::cols>{}),
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows + 1>{}),
      fdbb::utils::tie(a.get()),
      fdbb::utils::tie(b.get()),
      fdbb::functor::matmul()))>::type
#endif
{
  static_assert(fdbb::utils::remove_all<A>::type::options ==
                  fdbb::utils::remove_all<B>::type::options,
                "A and B must have same storage options");
  auto ExprType =
    fdbb::utils::for_each_return<0,
                                 1,
                                 fdbb::utils::remove_all<A>::type::rows,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0>(
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows *
                              fdbb::utils::remove_all<B>::type::cols>{}),
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows + 1>{}),
      fdbb::utils::tie(a.get()),
      fdbb::utils::tie(b.get()),
      fdbb::functor::matmul());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<B>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator matmul() between two types that are derived
/// from the base class fdbb::BlockBase
///
/// This is an alias to operator*
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::rows>::type>
auto
matmul(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> decltype(std::forward<A>(a) * std::forward<B>(b))
#endif
{
  return std::forward<A>(a) * std::forward<B>(b);
}

/// @brief Binary operator elem_matmul() between two types that are derived
/// from the base class fdbb::BlockBase
///
/// This operator performs a regular matmul() operation on the block
/// level but uses elementwise multiplication for the inner level
template<typename A,
         typename B,
         typename = typename std::enable_if<
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<A>::type>::value &&
           std::is_base_of<BlockBase,
                           typename fdbb::utils::remove_all<B>::type>::value &&
           fdbb::utils::remove_all<A>::type::cols ==
             fdbb::utils::remove_all<B>::type::rows>::type>
auto
elem_matmul(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<
    fdbb::utils::remove_all<A>::type::rows,
    fdbb::utils::remove_all<B>::type::cols,
    fdbb::utils::remove_all<A>::type::options,
    decltype(fdbb::utils::for_each_return<
             0,
             1,
             fdbb::utils::remove_all<A>::type::rows,
             0,
             0,
             0,
             0,
             0>(
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows *
                              fdbb::utils::remove_all<B>::type::cols>{}),
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows + 1>{}),
      fdbb::utils::tie(a.get()),
      fdbb::utils::tie(b.get()),
      fdbb::functor::elem_matmul()))>::type
#endif
{
  static_assert(fdbb::utils::remove_all<A>::type::options ==
                  fdbb::utils::remove_all<B>::type::options,
                "A and B must have same storage options");
  auto ExprType =
    fdbb::utils::for_each_return<0,
                                 1,
                                 fdbb::utils::remove_all<A>::type::rows,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0>(
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows *
                              fdbb::utils::remove_all<B>::type::cols>{}),
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<
                              fdbb::utils::remove_all<A>::type::rows + 1>{}),
      fdbb::utils::tie(a.get()),
      fdbb::utils::tie(b.get()),
      fdbb::functor::elem_matmul());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<B>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator* between an arithmetic type and a type that
/// is derived form the base class fdbb::BlockBase
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<A>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<B>::type>::value>::type>
auto
operator*(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<B>::type::rows,
                             fdbb::utils::remove_all<B>::type::cols,
                             fdbb::utils::remove_all<B>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               b.get(),
                               fdbb::utils::tie(a),
                               fdbb::functor::mul()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    b.get(), fdbb::utils::tie(a), fdbb::functor::mul());
  return typename BlockExpr_type<fdbb::utils::remove_all<B>::type::rows,
                                 fdbb::utils::remove_all<B>::type::cols,
                                 fdbb::utils::remove_all<B>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator* between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator*(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::mul()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::mul());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator*= between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator*=(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::mul_assign()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::mul_assign());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator/ between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator/(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::div()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::div());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

/// @brief Binary operator/= between a type that is derived from the
/// base class fdbb::BlockBase and an arithmetic type
template<
  typename A,
  typename B,
  typename = typename std::enable_if<
    std::is_arithmetic<typename fdbb::utils::remove_all<B>::type>::value &&
    std::is_base_of<BlockBase,
                    typename fdbb::utils::remove_all<A>::type>::value>::type>
auto
operator/=(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
  -> typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                             fdbb::utils::remove_all<A>::type::cols,
                             fdbb::utils::remove_all<A>::type::options,
                             decltype(fdbb::utils::for_each_return<0, 1, 0, 0>(
                               a.get(),
                               fdbb::utils::tie(b),
                               fdbb::functor::div_assign()))>::type
#endif
{
  auto ExprType = fdbb::utils::for_each_return<0, 1, 0, 0>(
    a.get(), fdbb::utils::tie(b), fdbb::functor::div_assign());
  return typename BlockExpr_type<fdbb::utils::remove_all<A>::type::rows,
                                 fdbb::utils::remove_all<A>::type::cols,
                                 fdbb::utils::remove_all<A>::type::options,
                                 decltype(ExprType)>::type{ ExprType };
}

} // namespace fdbb

#endif // FDBB_BLOCKEXPRESSION_BLOCK_OPERATIONS_HPP
