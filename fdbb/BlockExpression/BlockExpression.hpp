/** @file fdbb/BlockExpression/BlockExpression.hpp
 *
 *  @brief Block expression
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BLOCKEXPRESSION_BLOCK_EXPRESSION_HPP
#define FDBB_BLOCKEXPRESSION_BLOCK_EXPRESSION_HPP

#include <iostream>
#include <tuple>

#include <BlockExpression/ForwardDeclarations.hpp>
#include <Core/Config.hpp>
#include <Core/Functors.hpp>
#include <Core/Utils.hpp>

namespace fdbb {

namespace detail {
template<std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename ExprTuple,
         typename U>
struct BlockExpr_type;

template<std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename ExprTuple,
         std::size_t... Is>
struct BlockExpr_type<_rows,
                      _cols,
                      _options,
                      ExprTuple,
                      fdbb::utils::index_sequence<Is...>>
{
  using type =
    fdbb::BlockExpr<_rows,
                    _cols,
                    _options,
                    typename std::tuple_element<Is, ExprTuple>::type...>;
};
} // namespace detail

/** @brief
    Type trait to determine the type of the block expression

    This type trait basically unpacks the \a ExprTuple that is stored
    as \a std::tuple and holds the resulting block expression \a
    ExprType in the type \a type.
 */
template<std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename ExprTuple>
struct BlockExpr_type
{
private:
  using Indices =
    fdbb::utils::make_index_sequence<std::tuple_size<ExprTuple>::value>;

public:
  using type = typename fdbb::detail::
    BlockExpr_type<_rows, _cols, _options, ExprTuple, Indices>::type;
};

/** @brief
    A fixed-size block expression

    This is a block expression container for a fixed number of scalar
    expressions. It supports all arithmetic operations like addition,
    subtraction, element-wise multiplication and division. Moreover,
    it can be transposed by returning a view of a block matrix.

    @note
    Block expressions are stored in a std::tuple. The template
    parameter \a _options specifies the storage order. The default
    storage order is row-major. Note that it is not possible to
    perform arithmetic operations between block expressions stored in
    different storage orders.
 */
template<std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename... Ts>
struct BlockExpr : public BlockBase
{
private:
  /// @brief Self type
  using self_type = BlockExpr<_rows, _cols, _options, Ts...>;

  /// @brief Expression type
  using expr_type = std::tuple<Ts...>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

  /// @brief Size (rows * columns)
  static constexpr std::size_t size = _rows * _cols;

  /// @brief Storage options
  static constexpr StorageOptions options = _options;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  BlockExpr() = delete;

  /// @brief Constructor (copy from expression passed as std::tuple)
  explicit BlockExpr(const expr_type& expr)
    : expr{ expr }
  {}

  /// @brief Constructor (move from expr passed as std::tuple)
  explicit BlockExpr(expr_type&& expr)
    : expr{ std::move(expr) }
  {}

  /// @brief Constructor (copy from block expression)
  template<typename... Ts_>
  BlockExpr(const BlockExpr<_rows, _cols, _options, Ts_...>& other) noexcept
  {
    fdbb::utils::for_each(other.get(), expr, fdbb::functor::assign());
  }

  /// @brief Constructor (copy from block matrix)
  template<typename T>
  BlockExpr(const BlockMatrix<T, _rows, _cols, _options>& other) noexcept
  {
    fdbb::utils::for_each(other.get(), expr, fdbb::functor::assign());
  }

  /// @brief Constructor (copy from block matrix view)
  template<typename T>
  BlockExpr(const BlockMatrixView<T, _rows, _cols, _options>& other) noexcept
  {
    fdbb::utils::for_each(other.get(), expr, fdbb::functor::assign());
  }

  /// @brief Assignment operator (copy from block expression)
  template<typename... Ts_>
  BlockExpr& operator=(
    const BlockExpr<_rows, _cols, _options, Ts_...>& other) noexcept
  {
    fdbb::utils::for_each(other.get(), expr, fdbb::functor::assign());
    return *this;
  }

  /// @brief Assignment operator (copy from block matrix)
  template<typename T>
  BlockExpr& operator=(
    const BlockMatrix<T, _rows, _cols, _options>& other) noexcept
  {
    fdbb::utils::for_each(other.get(), expr, fdbb::functor::assign());
    return *this;
  }

  /// @brief Assignment operator (copy from block matrix view)
  template<typename T>
  BlockExpr& operator=(
    const BlockMatrixView<T, _rows, _cols, _options>& other) noexcept
  {
    fdbb::utils::for_each(other.get(), expr, fdbb::functor::assign());
    return *this;
  }

  /// @brief Returns the transposed of the block matrix
  constexpr auto transpose() const noexcept -> typename fdbb::BlockExpr_type<
    _cols,
    _rows,
    _options,
    decltype(fdbb::utils::for_each_return<0, 1, _cols, 0, 0, 0>(
      fdbb::utils::make_tuple(
        fdbb::utils::make_index_sequence<_rows * _cols>{}),
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<_cols + 1>{}),
      fdbb::utils::tie(expr),
      fdbb::functor::transpose()))>::type
  {
    using ExprType = typename fdbb::BlockExpr_type<
      _cols,
      _rows,
      _options,
      decltype(fdbb::utils::for_each_return<0, 1, _cols, 0, 0, 0>(
        fdbb::utils::make_tuple(
          fdbb::utils::make_index_sequence<_rows * _cols>{}),
        fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<_cols + 1>{}),
        fdbb::utils::tie(expr),
        fdbb::functor::transpose()))>::type;
    return ExprType{ fdbb::utils::for_each_return<0, 1, _cols, 0, 0, 0>(
      fdbb::utils::make_tuple(
        fdbb::utils::make_index_sequence<_rows * _cols>{}),
      fdbb::utils::make_tuple(fdbb::utils::make_index_sequence<_cols + 1>{}),
      fdbb::utils::tie(expr),
      fdbb::functor::transpose()) };
  }

  /// @brief Returns constant reference to Ith scalar expression
  template<std::size_t I>
  constexpr auto get() const noexcept
#if !defined(DOXYGEN)
    -> const decltype(std::get<I>(expr))
#endif
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return (std::get<I>(expr));
  }

  /// @brief Returns reference to Ith scalar expression
  template<std::size_t I>
  auto get() noexcept
#if !defined(DOXYGEN)
    -> decltype(std::get<I>(expr))
#endif
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return (std::get<I>(expr));
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block expression[" << _rows << "x" << _cols << ","
       << (_options == StorageOptions::RowMajor ? "RowMajor" : "ColMajor")
       << "]: " << fdbb::utils::for_each_return(expr, fdbb::functor::size());
  }

  /// @brief Applies lambda expression/functor to all block entries
  template<typename F>
  void apply(F f)
  {
    fdbb::utils::for_each(expr, f);
  }
};

/// @brief Prints a short description of the block expression
template<std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename... Ts>
std::ostream&
operator<<(std::ostream& os,
           const BlockExpr<_rows, _cols, _options, Ts...>& obj)
{
  obj.print(os);
  return os;
}

namespace utils {

/// @brief Returns reference to the Ith element of the block expression
template<std::size_t I,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename... Ts>
constexpr auto
get(BlockExpr<_rows, _cols, _options, Ts...>& obj) noexcept
#if !defined(DOXYGEN)
  -> decltype(obj.template get<I>())
#endif
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block expression
template<std::size_t I,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename... Ts>
constexpr auto
get(BlockExpr<_rows, _cols, _options, Ts...>&& obj) noexcept
#if !defined(DOXYGEN)
  -> decltype(std::move(obj.template get<I>()))
#endif
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block expression
template<std::size_t I,
         std::size_t _rows,
         std::size_t _cols,
         StorageOptions _options,
         typename... Ts>
constexpr auto
get(const BlockExpr<_rows, _cols, _options, Ts...>& obj) noexcept
#if !defined(DOXYGEN)
  -> const decltype(obj.template get<I>())
#endif
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

} // namespace utils

/// @brief Create \ref fdbb::BlockExpr object from tuple
template<std::size_t _rows = 1,
         std::size_t _cols = 1,
         StorageOptions _options = StorageOptions::RowMajor,
         typename... Ts>
constexpr BlockExpr<_rows, _cols, _options, Ts...>
make_BlockExpr(std::tuple<Ts...>&& tuple)
{
  static_assert(_rows * _cols == sizeof...(Ts),
                "Requested col/row combination mismatches size of arguments");
  return BlockExpr<_rows, _cols, _options, Ts...>(
    std::forward<std::tuple<Ts...>>(tuple));
}

/// @brief Create \ref fdbb::BlockExpr object from parameter pack
template<std::size_t _rows = 1,
         std::size_t _cols = 1,
         StorageOptions _options = StorageOptions::RowMajor,
         typename... Ts>
constexpr BlockExpr<_rows, _cols, _options, Ts...>
make_BlockExpr(Ts&&... args)
{
  static_assert(_rows * _cols == sizeof...(args),
                "Requested col/row combination mismatches size of arguments");
  return BlockExpr<_rows, _cols, _options, Ts...>(fdbb::utils::tie(args...));
}

} // namespace fdbb

namespace std {

/** @brief
    Spezialization of std::tuple_size for type fdbb::BlockExpr

    @tparam   _rows      The number of rows
    @tparam   _cols      The number of columns
    @tparam   _options   The storage options
 */
template<std::size_t _rows,
         std::size_t _cols,
         ::fdbb::StorageOptions _options,
         typename... Ts>
class tuple_size<fdbb::BlockExpr<_rows, _cols, _options, Ts...>>
  : public integral_constant<std::size_t, _rows * _cols>
{};

} // namespace std

#endif // FDBB_BLOCKEXPRESSION_BLOCK_EXPRESSION_HPP
