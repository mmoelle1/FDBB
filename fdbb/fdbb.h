/** @file fdbb/fdbb.h
 *
 *  @brief FDBB header
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author: Matthias Moller
 */
#pragma once
#ifndef FDBB_H
#define FDBB_H

/** @namespace fdbb
 *
 *  @brief
 *  The \ref fdbb namespace, containing all functionality of the FDBB
 *  library
 *
 *  The \ref fdbb namespace contains all functionality of the FDBB
 *  library that is exposed to the end-user. All functionality in this
 *  namespace has a stable API over future FDBB library releases.
 */
namespace fdbb {
} // namespace fdbb

/* -------------- Core Components ------------- */
#include <Core/Backend.hpp>
#include <Core/Config.hpp>
#include <Core/Constant.hpp>
#include <Core/Debug.hpp>
#include <Core/Utils.hpp>

/* ------------- Block Expressions ------------ */
#include <BlockExpression/BlockExpression.hpp>
#include <BlockExpression/BlockMatrix.hpp>
#include <BlockExpression/BlockMatrixView.hpp>
#include <BlockExpression/BlockOperations.hpp>
#include <BlockExpression/ForwardDeclarations.hpp>

/* ------------------- Cache ------------------ */
#include <Cache/Cache.hpp>
#include <Cache/Cache2.hpp>
#include <Cache/ForwardDeclarations.hpp>

#include <Fluids/Enums.hpp>
#include <Fluids/Traits.hpp>
#include <Fluids/Types.hpp>

/*  ------------------ Equation of states ----- */
#include <Fluids/EOS.hpp>

/* ---- Conservative variable formulations ---- */
#include <Fluids/Conservative1d.hpp>
#include <Fluids/Conservative2d.hpp>
#include <Fluids/Conservative3d.hpp>

/* ------- Entropy variable formulations ------ */
//#include <Fluids/Entropy1d.hpp>
//#include <Fluids/Entropy2d.hpp>
//#include <Fluids/Entropy3d.hpp>

/* ------ Primitive variable formulations ----- */
#include <Fluids/Primitive1d.hpp>
#include <Fluids/Primitive2d.hpp>
#include <Fluids/Primitive3d.hpp>

/* --- Riemann invariants formulations --- */
#include <Fluids/RiemannInvariants1d.hpp>
#include <Fluids/RiemannInvariants2d.hpp>
#include <Fluids/RiemannInvariants3d.hpp>

#endif // FDBB_H
