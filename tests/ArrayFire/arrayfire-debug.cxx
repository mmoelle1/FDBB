/** @file arrayfire-debug.cxx
 *
 *  @brief Debugging for ArrayFire library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <chrono>
#include <iostream>

#include <arrayfire.h>

#include "fdbb.h"

template<typename T1, typename T2, typename T3, typename T4>
auto
calculate_vanilla(const T1& a1, const T2& a2, const T3& a3, const T4& a4)
#if __cplusplus <= 201103L
  -> decltype((a1 * a1 + a2 * a2 + a3 * a3) / (a4 * a4))
#endif
{
  return (a1 * a1 + a2 * a2 + a3 * a3) / (a4 * a4);
}

template<typename T1, typename T2, typename T3, typename T4>
auto
calculate_fdbb(const T1& a1, const T2& a2, const T3& a3, const T4& a4)
#if __cplusplus <= 201103L
  -> decltype(fdbb::elem_div(fdbb::elem_mul(a1, a1) + fdbb::elem_mul(a2, a2) +
                               fdbb::elem_mul(a3, a3),
                             fdbb::elem_mul(a4, a4)))
#endif
{
  return fdbb::elem_div(fdbb::elem_mul(a1, a1) + fdbb::elem_mul(a2, a2) +
                          fdbb::elem_mul(a3, a3),
                        fdbb::elem_mul(a4, a4));
}

int
main()
{
  typedef float T;
  typedef af::array vector;

  vector density(100, af::dtype(af::dtype_traits<T>::af_type)),
    momentum_x(100, af::dtype(af::dtype_traits<T>::af_type)),
    momentum_y(100, af::dtype(af::dtype_traits<T>::af_type)),
    momentum_z(100, af::dtype(af::dtype_traits<T>::af_type)),
    result(100, af::dtype(af::dtype_traits<T>::af_type));

  density = 2.0;
  momentum_x = 1.0;
  momentum_y = 2.0;
  momentum_z = 3.0;

  // Check has_features
  std::cout << "Check has_make_temp_impl  : "
            << fdbb::detail::has_make_temp_impl<vector>::value << std::endl;
  std::cout << "Check has_tag_impl        : "
            << fdbb::detail::has_tag_impl<vector>::value << std::endl;

  // Check specialized implementations of binary operators
  std::cout << "Check get_elem_mul_impl   : "
            << (int)fdbb::detail::get_elem_mul_impl<vector, vector>::value
            << std::endl;
  std::cout << "Check get_elem_div_impl   : "
            << (int)fdbb::detail::get_elem_div_impl<vector, vector>::value
            << std::endl;
  std::cout << "Check get_elem_pow_impl   : "
            << (int)fdbb::detail::get_elem_pow_impl<vector, vector>::value
            << std::endl;

  // Check specialized implementations of unary operators
  std::cout << "Check get_elem_abs_impl   : "
            << (int)fdbb::detail::get_elem_abs_impl<vector>::value << std::endl;
  std::cout << "Check get_elem_acos_impl  : "
            << (int)fdbb::detail::get_elem_acos_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_acosh_impl : "
            << (int)fdbb::detail::get_elem_acosh_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_asin_impl  : "
            << (int)fdbb::detail::get_elem_asin_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_asinh_impl : "
            << (int)fdbb::detail::get_elem_asinh_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_atan_impl  : "
            << (int)fdbb::detail::get_elem_atan_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_atanh_impl : "
            << (int)fdbb::detail::get_elem_atanh_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_ceil_impl  : "
            << (int)fdbb::detail::get_elem_ceil_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_conj_impl  : "
            << (int)fdbb::detail::get_elem_conj_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_cos_impl   : "
            << (int)fdbb::detail::get_elem_cos_impl<vector>::value << std::endl;
  std::cout << "Check get_elem_cosh_impl  : "
            << (int)fdbb::detail::get_elem_cosh_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_exp_impl   : "
            << (int)fdbb::detail::get_elem_exp_impl<vector>::value << std::endl;
  std::cout << "Check get_elem_exp10_impl : "
            << (int)fdbb::detail::get_elem_exp10_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_exp2_impl  : "
            << (int)fdbb::detail::get_elem_exp2_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_fabs_impl  : "
            << (int)fdbb::detail::get_elem_fabs_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_floor_impl : "
            << (int)fdbb::detail::get_elem_floor_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_imag_impl  : "
            << (int)fdbb::detail::get_elem_imag_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_log_impl   : "
            << (int)fdbb::detail::get_elem_log_impl<vector>::value << std::endl;
  std::cout << "Check get_elem_log10_impl : "
            << (int)fdbb::detail::get_elem_log10_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_log2_impl  : "
            << (int)fdbb::detail::get_elem_log2_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_real_impl  : "
            << (int)fdbb::detail::get_elem_real_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_round_impl : "
            << (int)fdbb::detail::get_elem_round_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_rsqrt_impl : "
            << (int)fdbb::detail::get_elem_rsqrt_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_sign_impl  : "
            << (int)fdbb::detail::get_elem_sign_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_sin_impl   : "
            << (int)fdbb::detail::get_elem_sin_impl<vector>::value << std::endl;
  std::cout << "Check get_elem_sinh_impl  : "
            << (int)fdbb::detail::get_elem_sinh_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_sqrt_impl  : "
            << (int)fdbb::detail::get_elem_sqrt_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_tan_impl   : "
            << (int)fdbb::detail::get_elem_tan_impl<vector>::value << std::endl;
  std::cout << "Check get_elem_tanh_impl  : "
            << (int)fdbb::detail::get_elem_tanh_impl<vector>::value
            << std::endl;
  std::cout << "Check get_elem_trunc_impl : "
            << (int)fdbb::detail::get_elem_trunc_impl<vector>::value
            << std::endl;

  // Vanilla implementation
  result = (momentum_x * momentum_x + momentum_y * momentum_y +
            momentum_z * momentum_z) /
           (density * density);

  af_print(result(0));

  // FDBB implementation
  result = fdbb::elem_div(fdbb::elem_mul(momentum_x, momentum_x) +
                            fdbb::elem_mul(momentum_y, momentum_y) +
                            fdbb::elem_mul(momentum_z, momentum_z),
                          fdbb::elem_mul(density, density));

  af_print(result(0));

  // Function-based implementation
  result = calculate_vanilla(momentum_x, momentum_y, momentum_z, density);

  af_print(result(0));

  // Function-based implementation
  result = calculate_fdbb(momentum_x, momentum_y, momentum_z, density);

  af_print(result(0));

  return 0;
}
