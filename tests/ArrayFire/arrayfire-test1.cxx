/** @file arrayfire-test1.cxx
 *
 *  @brief Test Suite #1 for ArrayFire library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <array>
#include <chrono>
#include <getopt.h>
#include <iostream>

#include <arrayfire.h>

#include "fdbb.h"

#include "suite1.hpp"

template<typename T>
struct ConservativeFixture
{
  // Typedefs
  typedef T type;
  typedef af::array vector;

  // Equation of state
  typedef fdbb::fluids::EOSidealGas<T> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::conservative>
    var1d;
  typedef fdbb::fluids::Variables<eos, 2, fdbb::fluids::EnumForm::conservative>
    var2d;
  typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::conservative>
    var3d;

  // Constructor
  ConservativeFixture()
    : ConservativeFixture(1)
  {}

  // Constructor
  ConservativeFixture(std::size_t N, int dim = 1)
    : density(pow(N, dim), af::dtype(af::dtype_traits<T>::af_type))
    , momentum_x(pow(N, dim), af::dtype(af::dtype_traits<T>::af_type))
    , momentum_y(pow(N, dim), af::dtype(af::dtype_traits<T>::af_type))
    , momentum_z(pow(N, dim), af::dtype(af::dtype_traits<T>::af_type))
    , energy(pow(N, dim), af::dtype(af::dtype_traits<T>::af_type))
    , result(pow(N, dim), af::dtype(af::dtype_traits<T>::af_type))
    , nrows(pow(N, dim))
    , ncols(pow(N, dim))
  {
    density = 2.0;
    momentum_x = 1.0;
    momentum_y = 2.0;
    momentum_z = 3.0;
    energy = 5.0;
    result = 0.0;
  }

  // Destructor
  ~ConservativeFixture() {}

  // Attributes
  vector density;
  vector momentum_x;
  vector momentum_y;
  vector momentum_z;
  vector energy;
  vector result;

  const std::size_t nrows;
  const std::size_t ncols;

  // Time measurement
  auto static getClock() -> decltype(std::chrono::high_resolution_clock::now())
  {
    af::sync();
    return std::chrono::high_resolution_clock::now();
  }
};

int
main(int argc, char** argv)
{
  // Create test configuration
  TestConfiguration config(argc, argv);
  config.info();

  // Run tests
  testAll<ConservativeFixture<float>>(config);
  testAll<ConservativeFixture<double>>(config);

  return 0;
}
