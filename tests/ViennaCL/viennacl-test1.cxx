/** @file viennacl-test1.cxx
 *
 *  @brief Test Suite #1 for ViennaCL library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <array>
#include <chrono>
#include <getopt.h>
#include <iostream>

#include <viennacl/matrix.hpp>
#include <viennacl/vector.hpp>

#include "fdbb.h"

#include "suite1.hpp"

template<typename T>
struct ConservativeFixture
{
  // Typedefs
  typedef T type;
  typedef Matrix1d<T> mat1d;
  typedef Matrix2d<T> mat2d;
  typedef Matrix3d<T> mat3d;
  typedef viennacl::vector<T> vector;
  typedef viennacl::matrix<T> matrix;

  // Equation of state
  typedef fdbb::fluids::EOSidealGas<T> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::conservative>
    var1d;
  typedef fdbb::fluids::Variables<eos, 2, fdbb::fluids::EnumForm::conservative>
    var2d;
  typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::conservative>
    var3d;

  // Constructor
  ConservativeFixture()
    : ConservativeFixture(1)
  {}

  // Constructor
  ConservativeFixture(std::size_t N, int dim = 1)
    : density(pow(N, dim))
    , momentum_x(pow(N, dim))
    , momentum_y(pow(N, dim))
    , momentum_z(pow(N, dim))
    , energy(pow(N, dim))
    , result(pow(N, dim))
    , nrows(pow(N, dim))
    , ncols(pow(N, dim))
  {
    for (auto i = 0; i < density.size(); i++)
      density(i) = 2.0;
    for (auto i = 0; i < momentum_x.size(); i++)
      momentum_x(i) = 1.0;
    for (auto i = 0; i < momentum_y.size(); i++)
      momentum_y(i) = 2.0;
    for (auto i = 0; i < momentum_z.size(); i++)
      momentum_z(i) = 3.0;
    for (auto i = 0; i < energy.size(); i++)
      energy(i) = 5.0;
    for (auto i = 0; i < result.size(); i++)
      result(i) = 0.0;
  }

  // Constructor
  ConservativeFixture(std::size_t N, int dim, std::array<std::size_t, 3> degree)
    : ConservativeFixture(N, dim)
  {
    // TODO
  }

  // Destructor
  ~ConservativeFixture() {}

  // Attributes
  vector density;
  vector momentum_x;
  vector momentum_y;
  vector momentum_z;
  vector energy;
  vector result;
  matrix matA;
  matrix matB;
  matrix matC;

  const std::size_t nrows;
  const std::size_t ncols;

  // Time measurement
  auto static getClock() -> decltype(std::chrono::high_resolution_clock::now())
  {
    viennacl::backend::finish();
    return std::chrono::high_resolution_clock::now();
  }
};

int
main(int argc, char** argv)
{
  // Create test configuration
  TestConfiguration config(argc, argv);
  config.info();

  // Run tests
  testAll<ConservativeFixture<float>>(config);
  testAll<ConservativeFixture<double>>(config);

  return 0;
}
