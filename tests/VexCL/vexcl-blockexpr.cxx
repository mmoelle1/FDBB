/** @file vexcl-blockexpr.cxx
 *
 *  @brief VexCL test for block expressions
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <typeinfo>

#define VEXCL_SHOW_KERNELS

#include "fdbb.h"

// Global VexCL context
#ifdef SUPPORT_DEVICES_CPU
#ifdef SUPPORT_DOUBLE
static vex::Context
ctx(vex::Filter::CPU&& vex::Filter::DoublePrecision);
#else
static vex::Context ctx(vex::Filter::CPU);
#endif
#else
#ifdef SUPPORT_DEVICES_GPU
#ifdef SUPPORT_DOUBLE
static vex::Context
ctx(vex::Filter::GPU&& vex::Filter::DoublePrecision&& vex::Filter::Position(
  SUPPORT_DEVICES_GPU));
#else
static vex::Context
ctx(vex::Filter::GPU&& vex::Filter::Position(SUPPORT_DEVICES_GPU));
#endif
#else
#ifdef SUPPORT_DOUBLE
static vex::Context ctx(vex::Filter::DoublePrecision);
#else
static vex::Context ctx(vex::Filter::Any);
#endif
#endif
#endif

struct _print
{
  template<typename T>
  void operator()(const T& x) const
  {
    std::cout << x << std::endl;
  }
};

int
main(int argc, char** argv)
{
  std::cout << "////////// VexCL-unittest //////////" << std::endl;
  if (!ctx)
    throw std::runtime_error("No devices available.");

  using vector = vex::vector<double>;

  vector u0(ctx, 10), u1(ctx, 10), u2(ctx, 10);

  u0 = 1.0;
  u1 = 2.0;
  u2 = 3.0;

  std::cout << u0 << std::endl;
  std::cout << u1 << std::endl;
  std::cout << u2 << std::endl;

  fdbb::BlockRowVector<0, vector, 3> V1(u0, u1, u2);
  fdbb::BlockRowVector<1, vector, 3> V2(u1, u2, u0);

  std::cout << fdbb::get<0>(V1) << std::endl;
  std::cout << fdbb::get<1>(V1) << std::endl;
  std::cout << fdbb::get<2>(V1) << std::endl;

  auto T = V1 + V2;

  std::cout << typeid(T).name() << std::endl;
  // auto T = V1 - V2 + V1 + V2;

  // fdbb::BlockRowVector<2, vector, 3> V(T);

  // std::cout << fdbb::get<0>(V) << std::endl;
  // std::cout << fdbb::get<1>(V) << std::endl;
  // std::cout << fdbb::get<2>(V) << std::endl;

  return 0;
}
