/** @file suite1.hpp
 *
 *  @brief Testsuite #1
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <array>
#include <getopt.h>
#include <iomanip>
#include <typeinfo>
#include <vector>

#define TIMING(suite, test, type, t0, t1, flops, loads, stores, length, runs)  \
  std::cout << "[" << suite << "." << test << "." << typeid(type).name()       \
            << "] flops,loads,stores  : " << flops << ", " << loads << ", "    \
            << stores << "\n"                                                  \
            << "[" << suite << "." << test << "." << typeid(type).name()       \
            << "] memory consumption  : "                                      \
            << (loads + stores) * length * sizeof(type) << " bytes"            \
            << std::endl;                                                      \
  if (std::chrono::duration_cast<std::chrono::seconds>(t1 - t0).count() >      \
      100) {                                                                   \
    std::cout                                                                  \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] computing time      : "                                            \
      << std::chrono::duration_cast<std::chrono::seconds>(t1 - t0).count()     \
      << " sec\n"                                                              \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] compute performance : "                                            \
      << 1.0e-6 * flops * length * runs /                                      \
           (double)std::chrono::duration_cast<std::chrono::seconds>(t1 - t0)   \
             .count()                                                          \
      << " MFLOPS\n"                                                           \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] memory performance  : "                                            \
      << 1.0e-6 * flops * length * sizeof(type) * runs /                       \
           (double)std::chrono::duration_cast<std::chrono::seconds>(t1 - t0)   \
             .count()                                                          \
      << " MB/s\n";                                                            \
  } else if (std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0)    \
               .count() > 100) {                                               \
    std::cout                                                                  \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] computing time      : "                                            \
      << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0)        \
           .count()                                                            \
      << " millisec\n"                                                         \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] compute performance : "                                            \
      << 1.0e-3 * flops * length * runs /                                      \
           (double)std::chrono::duration_cast<std::chrono::milliseconds>(t1 -  \
                                                                         t0)   \
             .count()                                                          \
      << " MFLOPS\n"                                                           \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] memory performance  : "                                            \
      << 1.0e-3 * (loads + stores) * length * sizeof(type) * runs /            \
           (double)std::chrono::duration_cast<std::chrono::milliseconds>(t1 -  \
                                                                         t0)   \
             .count()                                                          \
      << " MB/s\n";                                                            \
  } else if (std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0)    \
               .count() > 100) {                                               \
    std::cout                                                                  \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] computing time      : "                                            \
      << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0)        \
           .count()                                                            \
      << " microsec\n"                                                         \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] compute performance : "                                            \
      << flops * length * runs /                                               \
           (double)std::chrono::duration_cast<std::chrono::microseconds>(t1 -  \
                                                                         t0)   \
             .count()                                                          \
      << " MFLOPS\n"                                                           \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] memory performance  : "                                            \
      << flops * length * sizeof(type) * runs /                                \
           (double)std::chrono::duration_cast<std::chrono::microseconds>(t1 -  \
                                                                         t0)   \
             .count()                                                          \
      << " MB/s\n";                                                            \
  } else {                                                                     \
    std::cout                                                                  \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] computing time      : "                                            \
      << std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() \
      << " nanosec\n"                                                          \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] compute performance : "                                            \
      << 1.0e3 * flops * length * runs /                                       \
           (double)std::chrono::duration_cast<std::chrono::nanoseconds>(t1 -   \
                                                                        t0)    \
             .count()                                                          \
      << " MFLOPS\n"                                                           \
      << "[" << suite << "." << test << "." << typeid(type).name()             \
      << "] memory performance  : "                                            \
      << 1.0e3 * flops * length * sizeof(type) * runs /                        \
           (double)std::chrono::duration_cast<std::chrono::nanoseconds>(t1 -   \
                                                                        t0)    \
             .count()                                                          \
      << " MB/s\n";                                                            \
  }

// Sparse matrix (any dimension) in CSR format
template<typename val_t,
         typename size_t = std::size_t,
         typename idx_t = std::size_t,
         typename col_t = std::size_t>
struct Matrix
{
  // Default constructor
  Matrix()
    : Matrix(0, 0)
  {}

  // Constructor for N*N matrix
  Matrix(size_t N)
    : Matrix(N, N)
  {}

  // Constructor for N*M matrix
  Matrix(size_t N, size_t M)
    : _rows(N)
    , _cols(M)
    , _nnzs(0)
  {}

  // Return number of rows
  size_t rows() const { return _rows; }

  // Return number of columns
  size_t cols() const { return _cols; }

  // Return number of nonzeros
  size_t nonzeros() const { return _nnzs; }

  // Clear matrix
  void clear()
  {
    _rows = 0;
    _cols = 0;
    _nnzs = 0;
    row.clear();
    col.clear();
    val.clear();
  }

  // Index arrays
  std::vector<idx_t> row;
  std::vector<col_t> col;

  // Data array
  std::vector<val_t> val;

  // Attributes
  size_t _rows;
  size_t _cols;
  size_t _nnzs;
};

// Sparse matrix (1d) in CSR format
template<typename val_t,
         typename size_t = std::size_t,
         typename idx_t = std::size_t,
         typename col_t = std::size_t>
struct Matrix1d : public Matrix<val_t, size_t, idx_t, col_t>
{
  // Constructor
  Matrix1d(size_t n, size_t p)
    : Matrix<val_t, size_t, idx_t, col_t>(n)
  {
    // Loop over all n rows
    for (size_t irow = 0; irow < n; irow++) {
      this->row.push_back((idx_t)this->_nnzs);

      // Loop over all n columns
      for (size_t icol =
             (size_t)std::max((typename std::make_signed<size_t>::type)0,
                              (typename std::make_signed<size_t>::type)irow -
                                (typename std::make_signed<size_t>::type)p);
           icol < std::min(n, irow + p + 1);
           icol++) {
        this->col.push_back((col_t)icol);
        this->val.push_back((val_t)1.0);
        this->_nnzs++;
      }
    }
    this->row.push_back((idx_t)this->_nnzs);
  }
};

// Sparse matrix (2d) in CSR format
template<typename val_t,
         typename size_t = std::size_t,
         typename idx_t = std::size_t,
         typename col_t = std::size_t>
struct Matrix2d : public Matrix<val_t, size_t, idx_t, col_t>
{
  // Constructor
  Matrix2d(size_t n, size_t p)
    : Matrix2d(n, p, p)
  {}

  // Constructor
  Matrix2d(size_t n, size_t p, size_t q)
    : Matrix<val_t, size_t, idx_t, col_t>(n * n)
  {
    // Loop over all n*n rows
    for (size_t irow = 0; irow < n; irow++)
      for (size_t jrow = 0; jrow < n; jrow++) {
        this->row.push_back((idx_t)this->_nnzs);

        // Loop over all n*n columns
        for (size_t icol =
               (size_t)std::max((typename std::make_signed<size_t>::type)0,
                                (typename std::make_signed<size_t>::type)irow -
                                  (typename std::make_signed<size_t>::type)p);
             icol < std::min(n, irow + p + 1);
             icol++)
          for (size_t jcol = (size_t)std::max(
                 (typename std::make_signed<size_t>::type)0,
                 (typename std::make_signed<size_t>::type)jrow -
                   (typename std::make_signed<size_t>::type)q);
               jcol < std::min(n, jrow + q + 1);
               jcol++) {
            this->col.push_back((col_t)icol * n + jcol);
            this->val.push_back((val_t)1.0);
            this->_nnzs++;
          }
      }
    this->row.push_back((idx_t)this->_nnzs);
  }
};

// Sparse matrix (3d) in CSR format
template<typename val_t,
         typename size_t = std::size_t,
         typename idx_t = std::size_t,
         typename col_t = std::size_t>
struct Matrix3d : public Matrix<val_t, size_t, idx_t, col_t>
{
  // Constructor
  Matrix3d(size_t n, size_t p)
    : Matrix3d(n, p, p, p)
  {}

  // Constructor
  Matrix3d(size_t n, size_t p, size_t q, size_t r)
    : Matrix<val_t, size_t, idx_t, col_t>(n * n * n)
  {
    // Loop over all n*n*n rows
    for (size_t irow = 0; irow < n; irow++)
      for (size_t jrow = 0; jrow < n; jrow++)
        for (size_t krow = 0; krow < n; krow++) {
          this->row.push_back((idx_t)this->_nnzs);

          // Loop over all n*n*n columns
          for (size_t icol = (size_t)std::max(
                 (typename std::make_signed<size_t>::type)0,
                 (typename std::make_signed<size_t>::type)irow -
                   (typename std::make_signed<size_t>::type)p);
               icol < std::min(n, irow + p + 1);
               icol++)
            for (size_t jcol = (size_t)std::max(
                   (typename std::make_signed<size_t>::type)0,
                   (typename std::make_signed<size_t>::type)jrow -
                     (typename std::make_signed<size_t>::type)q);
                 jcol < std::min(n, jrow + q + 1);
                 jcol++)
              for (size_t kcol = (size_t)std::max(
                     (typename std::make_signed<size_t>::type)0,
                     (typename std::make_signed<size_t>::type)krow -
                       (typename std::make_signed<size_t>::type)r);
                   kcol < std::min(n, krow + r + 1);
                   kcol++) {
                this->col.push_back((col_t)icol * n * n + jcol * n + kcol);
                this->val.push_back((val_t)1.0);
                this->_nnzs++;
              }
        }
    this->row.push_back((idx_t)this->_nnzs);
  }
};

// Test configuration structure
struct TestConfiguration
{
  // Spatial dimension
  int dim;

  // Factor for test runs
  std::size_t factor_double;
  std::size_t factor_float;

  // Polynomial degrees
  std::array<std::size_t, 3> degrees;

  // Tests to run
  std::array<bool, 3> tests;

  // Number of runs per test size
  std::vector<std::size_t> test_runs;

  // Test sizes
  std::vector<std::size_t> test_sizes;

  // Constructors
  TestConfiguration(int argc, char** argv)
    : dim(3)
    , factor_double(0)
    , factor_float(0)
  {
    degrees.fill(1);
    tests.fill(false);

    int opt;
    int refine_steps = 0;
    std::size_t min_test_size = 100;
    std::size_t max_test_runs = 100;
    while ((opt = getopt(argc, argv, "D:M:R:f:d:m:p:q:r:123")) != EOF)
      switch (opt) {
        case '1':
          tests[0] = true;
          break;
        case '2':
          tests[1] = true;
          break;
        case '3':
          tests[2] = true;
          break;
        case 'D':
          dim = atoi(optarg);
          break;
        case 'M':
          max_test_runs = atoi(optarg);
          break;
        case 'R':
          refine_steps = atoi(optarg);
          break;
        case 'd':
          factor_double = atoi(optarg);
          break;
        case 'f':
          factor_float = atoi(optarg);
          break;
        case 'm':
          min_test_size = atoi(optarg);
          break;
        case 'p':
          degrees[0] = atoi(optarg);
          break;
        case 'q':
          degrees[1] = atoi(optarg);
          break;
        case 'r':
          degrees[2] = atoi(optarg);
          break;
        case '?':
          std::cout
            << "usage " << argv[0] << "\n"
            << "1-3 : enable tests 1-3\n"
            << "D   : dimension\n"
               "M   : maximum number of test runs\n"
            << "R   : number of refinement steps\n"
               "d   : factor by which number of runs is scaled (double)\n"
               "f   : factor by which number of runs is scaled (float)\n"
               "m   : minimum size of tests\n"
               "p-r : polynomial degree in 1-3 dimensions.\n";
      }

    // Create test sizes
    test_sizes.push_back(pow(min_test_size, 1. / dim));
    for (auto i = 0; i < refine_steps; i++)
      test_sizes.push_back(pow(pow(test_sizes.back(), dim) * 2, 1. / dim));

    // Create  test runs
    test_runs.push_back(max_test_runs);
    for (auto i = 0; i < refine_steps; i++)
      test_runs.push_back(std::max(100.0, test_runs.back() / 2.0));
  }

  // Information
  void info() const
  {
    std::cout << "Test configuration\n";
    std::cout << "------------------\n";
    std::cout << "Dimension        : " << dim << std::endl;
    std::cout << "Polynom. degree  : ";
    for (auto i = 0; i < dim; i++)
      std::cout << degrees[i] << " ";
    std::cout << std::endl;
    std::cout << "Tests enabled    : ";
    for (auto i = 0; i < tests.size(); i++)
      if (tests[i])
        std::cout << i + 1 << " ";
    std::cout << std::endl;
    std::cout << "Tests            : Size (double) | Byte (double) | Runs "
                 "(double) |  Size (float) |  Byte (float) |  Runs (float) \n";
    for (auto i = 0; i < test_sizes.size(); i++)
      std::cout << "                   " << std::setw(13) << test_sizes[i]
                << " | " << std::setw(13)
                << pow(test_sizes[i], dim) * sizeof(double) << " | "
                << std::setw(13) << test_runs[i] * factor_double << " | "
                << std::setw(13) << test_sizes[i] << " | " << std::setw(13)
                << pow(test_sizes[i], dim) * sizeof(float) << " | "
                << std::setw(13) << test_runs[i] * factor_float << std::endl;
  }

  template<typename T>
  std::size_t factor()
  {
    return 1;
  }
};

template<>
std::size_t
TestConfiguration::factor<double>()
{
  return factor_double;
}

template<>
std::size_t
TestConfiguration::factor<float>()
{
  return factor_float;
}

// Run all tests
template<typename Fixture>
void
testAll(TestConfiguration config)
{
  for (auto i = 0; i < config.test_sizes.size(); i++) {
    if (config.tests[0]) {
      Fixture fixture(config.test_sizes[i], config.dim);
      switch (config.dim) {
        case 1:
          test1a_1d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          // test1b_1d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          // test1c_1d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          // test1d_1d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          test1e_1d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
        case 2:
          test1a_2d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          // test1b_2d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          // test1c_2d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          // test1d_2d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          test1e_2d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
        case 3:
          test1a_3d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          // test1b_3d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          // test1c_3d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          // test1d_3d(fixture, config.test_runs[i]*config.factor<typename
          // Fixture::type>());
          test1e_3d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
      }
    }

    if (config.tests[1]) {
      Fixture fixture(config.test_sizes[i], config.dim);
      switch (config.dim) {
        case 1:
          test2a_1d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
        case 2:
          test2a_2d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
        case 3:
          test2a_3d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
      }
    }

#if defined(FDBB_BACKEND_ARRAYFIRE) || defined(FDBB_BACKEND_BLITZ) ||          \
  defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    std::cerr << "Sparse matrices are not available" << std::endl;
#else
    if (config.tests[2]) {
      Fixture fixture(config.test_sizes[i], config.dim, config.degrees);
      switch (config.dim) {
        case 1:
          test3e_1d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
        case 2:
          test3e_2d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
        case 3:
          test3e_3d(fixture,
                    config.test_runs[i] *
                      config.factor<typename Fixture::type>());
          break;
      }
    }
#endif
  }
}

// Vanilla implementation using density and momentum from fixture
template<typename Fixture>
void
test1a_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout
    << "\n\nVanilla implementation using density and momentum from fixture"
    << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result = (fixture.momentum_x % fixture.momentum_x) /
                     (fixture.density % fixture.density);
#elif defined(FDBB_BACKEND_ITPP)
    fixture.result =
      itpp::elem_div(itpp::elem_mult(fixture.momentum_x, fixture.momentum_x),
                     itpp::elem_mult(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    fixture.result = mtl::vec::ele_quot(
      mtl::vec::ele_prod(fixture.momentum_x, fixture.momentum_x),
      mtl::vec::ele_prod(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_EIGEN)
    fixture.result = (fixture.momentum_x.array() * fixture.momentum_x.array()) /
                     (fixture.density.array() * fixture.density.array());
#elif defined(FDBB_BACKEND_UBLAS)
    fixture.result = boost::numeric::ublas::element_div(
      boost::numeric::ublas::element_prod(fixture.momentum_x,
                                          fixture.momentum_x),
      boost::numeric::ublas::element_prod(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      (vex::tag<1>(fixture.momentum_x) * vex::tag<1>(fixture.momentum_x)) /
      (vex::tag<3>(fixture.density) * vex::tag<3>(fixture.density));
#elif defined(FDBB_BACKEND_VIENNACL)
    fixture.result = viennacl::linalg::element_div(
      viennacl::linalg::element_prod(fixture.momentum_x, fixture.momentum_x),
      viennacl::linalg::element_prod(fixture.density, fixture.density));
#else
    fixture.result = (fixture.momentum_x * fixture.momentum_x) /
                     (fixture.density * fixture.density);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1a_1d",
         typename Fixture::type,
         t0,
         t1,
         3,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1a_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1a_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1a_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Vanilla implementation using density and momentum from fixture
template<typename Fixture>
void
test1a_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout
    << "\n\nVanilla implementation using density and momentum from fixture"
    << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result = (fixture.momentum_x % fixture.momentum_x +
                      fixture.momentum_y % fixture.momentum_y) /
                     (fixture.density % fixture.density);
#elif defined(FDBB_BACKEND_ITPP)
    fixture.result =
      itpp::elem_div(itpp::elem_mult(fixture.momentum_x, fixture.momentum_x) +
                       itpp::elem_mult(fixture.momentum_y, fixture.momentum_y),
                     itpp::elem_mult(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    fixture.result = mtl::vec::ele_quot(
      mtl::vec::ele_prod(fixture.momentum_x, fixture.momentum_x) +
        mtl::vec::ele_prod(fixture.momentum_y, fixture.momentum_y),
      mtl::vec::ele_prod(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_EIGEN)
    fixture.result = (fixture.momentum_x.array() * fixture.momentum_x.array() +
                      fixture.momentum_y.array() * fixture.momentum_y.array()) /
                     (fixture.density.array() * fixture.density.array());
#elif defined(FDBB_BACKEND_UBLAS)
    fixture.result = boost::numeric::ublas::element_div(
      boost::numeric::ublas::element_prod(fixture.momentum_x,
                                          fixture.momentum_x) +
        boost::numeric::ublas::element_prod(fixture.momentum_y,
                                            fixture.momentum_y),
      boost::numeric::ublas::element_prod(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      (vex::tag<1>(fixture.momentum_x) * vex::tag<1>(fixture.momentum_x) +
       vex::tag<2>(fixture.momentum_y) * vex::tag<2>(fixture.momentum_y)) /
      (vex::tag<3>(fixture.density) * vex::tag<3>(fixture.density));
#elif defined(FDBB_BACKEND_VIENNACL)
    fixture.result = viennacl::linalg::element_div(
      viennacl::linalg::element_prod(fixture.momentum_x, fixture.momentum_x) +
        viennacl::linalg::element_prod(fixture.momentum_y, fixture.momentum_y),
      viennacl::linalg::element_prod(fixture.density, fixture.density));
#else
    fixture.result = (fixture.momentum_x * fixture.momentum_x +
                      fixture.momentum_y * fixture.momentum_y) /
                     (fixture.density * fixture.density);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1a_2d",
         typename Fixture::type,
         t0,
         t1,
         5,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1a_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1a_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1a_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Vanilla implementation using density and momentum from fixture
template<typename Fixture>
void
test1a_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout
    << "\n\nVanilla implementation using density and momentum from fixture"
    << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result = (fixture.momentum_x % fixture.momentum_x +
                      fixture.momentum_y % fixture.momentum_y +
                      fixture.momentum_z % fixture.momentum_z) /
                     (fixture.density % fixture.density);
#elif defined(FDBB_BACKEND_ITPP)
    fixture.result =
      itpp::elem_div(itpp::elem_mult(fixture.momentum_x, fixture.momentum_x) +
                       itpp::elem_mult(fixture.momentum_y, fixture.momentum_y) +
                       itpp::elem_mult(fixture.momentum_z, fixture.momentum_z),
                     itpp::elem_mult(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    fixture.result = mtl::vec::ele_quot(
      mtl::vec::ele_prod(fixture.momentum_x, fixture.momentum_x) +
        mtl::vec::ele_prod(fixture.momentum_y, fixture.momentum_y) +
        mtl::vec::ele_prod(fixture.momentum_z, fixture.momentum_z),
      mtl::vec::ele_prod(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_EIGEN)
    fixture.result = (fixture.momentum_x.array() * fixture.momentum_x.array() +
                      fixture.momentum_y.array() * fixture.momentum_y.array() +
                      fixture.momentum_z.array() * fixture.momentum_z.array()) /
                     (fixture.density.array() * fixture.density.array());
#elif defined(FDBB_BACKEND_UBLAS)
    fixture.result = boost::numeric::ublas::element_div(
      boost::numeric::ublas::element_prod(fixture.momentum_x,
                                          fixture.momentum_x) +
        boost::numeric::ublas::element_prod(fixture.momentum_y,
                                            fixture.momentum_y) +
        boost::numeric::ublas::element_prod(fixture.momentum_z,
                                            fixture.momentum_z),
      boost::numeric::ublas::element_prod(fixture.density, fixture.density));
#elif defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      (vex::tag<1>(fixture.momentum_x) * vex::tag<1>(fixture.momentum_x) +
       vex::tag<2>(fixture.momentum_y) * vex::tag<2>(fixture.momentum_y) +
       vex::tag<3>(fixture.momentum_z) * vex::tag<3>(fixture.momentum_z)) /
      (vex::tag<4>(fixture.density) * vex::tag<4>(fixture.density));
#elif defined(FDBB_BACKEND_VIENNACL)
    fixture.result = viennacl::linalg::element_div(
      viennacl::linalg::element_prod(fixture.momentum_x, fixture.momentum_x) +
        viennacl::linalg::element_prod(fixture.momentum_y, fixture.momentum_y) +
        viennacl::linalg::element_prod(fixture.momentum_z, fixture.momentum_z),
      viennacl::linalg::element_prod(fixture.density, fixture.density));
#else
    fixture.result = (fixture.momentum_x * fixture.momentum_x +
                      fixture.momentum_y * fixture.momentum_y +
                      fixture.momentum_z * fixture.momentum_z) /
                     (fixture.density * fixture.density);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1a_3d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1a_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1a_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1a_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum from fixture + elem_mul and
// elem_div
template<typename Fixture>
void
test1b_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum from fixture + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      fdbb::elem_div(fdbb::elem_mul(vex::tag<1>(fixture.momentum_x),
                                    vex::tag<1>(fixture.momentum_x)),
                     fdbb::elem_mul(vex::tag<4>(fixture.density),
                                    vex::tag<4>(fixture.density)));
#else
    fixture.result =
      fdbb::elem_div(fdbb::elem_mul(fixture.momentum_x, fixture.momentum_x),
                     fdbb::elem_mul(fixture.density, fixture.density));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1b_1d",
         typename Fixture::type,
         t0,
         t1,
         3,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1b_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1b_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1b_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum from fixture + elem_mul and
// elem_div
template<typename Fixture>
void
test1b_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum from fixture + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      fdbb::elem_div(fdbb::elem_mul(vex::tag<1>(fixture.momentum_x),
                                    vex::tag<1>(fixture.momentum_x)) +
                       fdbb::elem_mul(vex::tag<2>(fixture.momentum_y),
                                      vex::tag<2>(fixture.momentum_y)),
                     fdbb::elem_mul(vex::tag<4>(fixture.density),
                                    vex::tag<4>(fixture.density)));
#else
    fixture.result =
      fdbb::elem_div(fdbb::elem_mul(fixture.momentum_x, fixture.momentum_x) +
                       fdbb::elem_mul(fixture.momentum_y, fixture.momentum_y),
                     fdbb::elem_mul(fixture.density, fixture.density));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1b_2d",
         typename Fixture::type,
         t0,
         t1,
         5,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1b_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1b_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1b_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum from fixture + elem_mul and
// elem_div
template<typename Fixture>
void
test1b_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum from fixture + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      fdbb::elem_div(fdbb::elem_mul(vex::tag<1>(fixture.momentum_x),
                                    vex::tag<1>(fixture.momentum_x)) +
                       fdbb::elem_mul(vex::tag<2>(fixture.momentum_y),
                                      vex::tag<2>(fixture.momentum_y)) +
                       fdbb::elem_mul(vex::tag<3>(fixture.momentum_z),
                                      vex::tag<3>(fixture.momentum_z)),
                     fdbb::elem_mul(vex::tag<4>(fixture.density),
                                    vex::tag<4>(fixture.density)));
#else
    fixture.result =
      fdbb::elem_div(fdbb::elem_mul(fixture.momentum_x, fixture.momentum_x) +
                       fdbb::elem_mul(fixture.momentum_y, fixture.momentum_y) +
                       fdbb::elem_mul(fixture.momentum_z, fixture.momentum_z),
                     fdbb::elem_mul(fixture.density, fixture.density));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1b_3d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1b_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1b_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1b_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum by getVariable + elem_mul and
// elem_div
template<typename Fixture>
void
test1c_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum by getVariable + "
               "elem_mul, elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) = fdbb::elem_div(
      fdbb::elem_mul(
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.energy)),
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.energy))),
      fdbb::elem_mul(
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.energy)),
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.energy))));
#else
    fixture.result = fdbb::elem_div(
      fdbb::elem_mul(
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(fixture.density, fixture.momentum_x, fixture.energy),
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(fixture.density, fixture.momentum_x, fixture.energy)),
      fdbb::elem_mul(
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(fixture.density, fixture.momentum_x, fixture.energy),
        fdbb::fluids::Traits<1, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(fixture.density, fixture.momentum_x, fixture.energy)));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1c_1d",
         typename Fixture::type,
         t0,
         t1,
         3,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1c_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1c_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1c_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum by getVariable + elem_mul and
// elem_div
template<typename Fixture>
void
test1c_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum by getVariable + "
               "elem_mul, elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) = fdbb::elem_div(
      fdbb::elem_mul(
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.energy)),
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.energy))) +
        fdbb::elem_mul(
          fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(vex::tag<1>(fixture.density),
                           vex::tag<2>(fixture.momentum_x),
                           vex::tag<3>(fixture.momentum_y),
                           vex::tag<4>(fixture.energy)),
          fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(vex::tag<1>(fixture.density),
                           vex::tag<2>(fixture.momentum_x),
                           vex::tag<3>(fixture.momentum_y),
                           vex::tag<4>(fixture.energy))),
      fdbb::elem_mul(
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.energy)),
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.energy))));
#else
    fixture.result = fdbb::elem_div(
      fdbb::elem_mul(
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.energy),
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.energy)) +
        fdbb::elem_mul(
          fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(fixture.density,
                           fixture.momentum_x,
                           fixture.momentum_y,
                           fixture.energy),
          fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(fixture.density,
                           fixture.momentum_x,
                           fixture.momentum_y,
                           fixture.energy)),
      fdbb::elem_mul(
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.energy),
        fdbb::fluids::Traits<2, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.energy)));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1c_2d",
         typename Fixture::type,
         t0,
         t1,
         5,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1c_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1c_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1c_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum by getVariable + elem_mul and
// elem_div
template<typename Fixture>
void
test1c_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum by getVariable + "
               "elem_mul, elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) = fdbb::elem_div(
      fdbb::elem_mul(
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.momentum_z),
                         vex::tag<5>(fixture.energy)),
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.momentum_z),
                         vex::tag<5>(fixture.energy))) +
        fdbb::elem_mul(
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(vex::tag<1>(fixture.density),
                           vex::tag<2>(fixture.momentum_x),
                           vex::tag<3>(fixture.momentum_y),
                           vex::tag<4>(fixture.momentum_z),
                           vex::tag<5>(fixture.energy)),
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(vex::tag<1>(fixture.density),
                           vex::tag<2>(fixture.momentum_x),
                           vex::tag<3>(fixture.momentum_y),
                           vex::tag<4>(fixture.momentum_z),
                           vex::tag<5>(fixture.energy))) +
        fdbb::elem_mul(
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<3>(vex::tag<1>(fixture.density),
                           vex::tag<2>(fixture.momentum_x),
                           vex::tag<3>(fixture.momentum_y),
                           vex::tag<4>(fixture.momentum_z),
                           vex::tag<5>(fixture.energy)),
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<3>(vex::tag<1>(fixture.density),
                           vex::tag<2>(fixture.momentum_x),
                           vex::tag<3>(fixture.momentum_y),
                           vex::tag<4>(fixture.momentum_z),
                           vex::tag<5>(fixture.energy))),
      fdbb::elem_mul(
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.momentum_z),
                         vex::tag<5>(fixture.energy)),
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(vex::tag<1>(fixture.density),
                         vex::tag<2>(fixture.momentum_x),
                         vex::tag<3>(fixture.momentum_y),
                         vex::tag<4>(fixture.momentum_z),
                         vex::tag<5>(fixture.energy))));
#else
    fixture.result = fdbb::elem_div(
      fdbb::elem_mul(
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.momentum_z,
                         fixture.energy),
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<1>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.momentum_z,
                         fixture.energy)) +
        fdbb::elem_mul(
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(fixture.density,
                           fixture.momentum_x,
                           fixture.momentum_y,
                           fixture.momentum_z,
                           fixture.energy),
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<2>(fixture.density,
                           fixture.momentum_x,
                           fixture.momentum_y,
                           fixture.momentum_z,
                           fixture.energy)) +
        fdbb::elem_mul(
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<3>(fixture.density,
                           fixture.momentum_x,
                           fixture.momentum_y,
                           fixture.momentum_z,
                           fixture.energy),
          fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
            getVariable<3>(fixture.density,
                           fixture.momentum_x,
                           fixture.momentum_y,
                           fixture.momentum_z,
                           fixture.energy)),
      fdbb::elem_mul(
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.momentum_z,
                         fixture.energy),
        fdbb::fluids::Traits<3, fdbb::fluids::EnumForm::conservative>::
          getVariable<0>(fixture.density,
                         fixture.momentum_x,
                         fixture.momentum_y,
                         fixture.momentum_z,
                         fixture.energy)));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1c_3d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1c_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1c_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1c_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum by functions + elem_mul and
// elem_div
template<typename Fixture>
void
test1d_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum by functions + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) = fdbb::elem_div(
      fdbb::elem_mul(
        Fixture::var1d::template momentum<0>(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.energy)),
        Fixture::var1d::template momentum<0>(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.energy))),
      fdbb::elem_mul(Fixture::var1d::density(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.energy)),
                     Fixture::var1d::density(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.energy))));
#else
    fixture.result = fdbb::elem_div(
      fdbb::elem_mul(Fixture::var1d::template momentum<0>(
                       fixture.density, fixture.momentum_x, fixture.energy),
                     Fixture::var1d::template momentum<0>(
                       fixture.density, fixture.momentum_x, fixture.energy)),
      fdbb::elem_mul(Fixture::var1d::density(
                       fixture.density, fixture.momentum_x, fixture.energy),
                     Fixture::var1d::density(
                       fixture.density, fixture.momentum_x, fixture.energy)));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1d_1d",
         typename Fixture::type,
         t0,
         t1,
         3,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1d_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1d_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1d_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum by functions + elem_mul and
// elem_div
template<typename Fixture>
void
test1d_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum by functions + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) = fdbb::elem_div(
      fdbb::elem_mul(
        Fixture::var2d::template momentum<0>(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.energy)),
        Fixture::var2d::template momentum<0>(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.energy))) +
        fdbb::elem_mul(
          Fixture::var2d::template momentum<1>(vex::tag<1>(fixture.density),
                                               vex::tag<2>(fixture.momentum_x),
                                               vex::tag<3>(fixture.momentum_y),
                                               vex::tag<4>(fixture.energy)),
          Fixture::var2d::template momentum<1>(vex::tag<1>(fixture.density),
                                               vex::tag<2>(fixture.momentum_x),
                                               vex::tag<3>(fixture.momentum_y),
                                               vex::tag<4>(fixture.energy))),
      fdbb::elem_mul(Fixture::var2d::density(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.energy)),
                     Fixture::var2d::density(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.energy))));
#else
    fixture.result = fdbb::elem_div(
      fdbb::elem_mul(Fixture::var2d::template momentum<0>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.energy),
                     Fixture::var2d::template momentum<0>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.energy)) +
        fdbb::elem_mul(Fixture::var2d::template momentum<1>(fixture.density,
                                                            fixture.momentum_x,
                                                            fixture.momentum_y,
                                                            fixture.energy),
                       Fixture::var2d::template momentum<1>(fixture.density,
                                                            fixture.momentum_x,
                                                            fixture.momentum_y,
                                                            fixture.energy)),
      fdbb::elem_mul(Fixture::var2d::density(fixture.density,
                                             fixture.momentum_x,
                                             fixture.momentum_y,
                                             fixture.energy),
                     Fixture::var2d::density(fixture.density,
                                             fixture.momentum_x,
                                             fixture.momentum_y,
                                             fixture.energy)));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1d_2d",
         typename Fixture::type,
         t0,
         t1,
         5,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1d_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1d_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1d_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density and momentum by functions + elem_mul and
// elem_div
template<typename Fixture>
void
test1d_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and momentum by functions + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) = fdbb::elem_div(
      fdbb::elem_mul(
        Fixture::var3d::template momentum<0>(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.momentum_z),
                                             vex::tag<5>(fixture.energy)),
        Fixture::var3d::template momentum<0>(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.momentum_z),
                                             vex::tag<5>(fixture.energy))) +
        fdbb::elem_mul(
          Fixture::var3d::template momentum<1>(vex::tag<1>(fixture.density),
                                               vex::tag<2>(fixture.momentum_x),
                                               vex::tag<3>(fixture.momentum_y),
                                               vex::tag<4>(fixture.momentum_z),
                                               vex::tag<5>(fixture.energy)),
          Fixture::var3d::template momentum<1>(vex::tag<1>(fixture.density),
                                               vex::tag<2>(fixture.momentum_x),
                                               vex::tag<3>(fixture.momentum_y),
                                               vex::tag<4>(fixture.momentum_z),
                                               vex::tag<5>(fixture.energy))) +
        fdbb::elem_mul(
          Fixture::var3d::template momentum<2>(vex::tag<1>(fixture.density),
                                               vex::tag<2>(fixture.momentum_x),
                                               vex::tag<3>(fixture.momentum_y),
                                               vex::tag<4>(fixture.momentum_z),
                                               vex::tag<5>(fixture.energy)),
          Fixture::var3d::template momentum<2>(vex::tag<1>(fixture.density),
                                               vex::tag<2>(fixture.momentum_x),
                                               vex::tag<3>(fixture.momentum_y),
                                               vex::tag<4>(fixture.momentum_z),
                                               vex::tag<5>(fixture.energy))),
      fdbb::elem_mul(Fixture::var3d::density(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.momentum_z),
                                             vex::tag<5>(fixture.energy)),
                     Fixture::var3d::density(vex::tag<1>(fixture.density),
                                             vex::tag<2>(fixture.momentum_x),
                                             vex::tag<3>(fixture.momentum_y),
                                             vex::tag<4>(fixture.momentum_z),
                                             vex::tag<5>(fixture.energy))));
#else
    fixture.result = fdbb::elem_div(
      fdbb::elem_mul(Fixture::var3d::template momentum<0>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy),
                     Fixture::var3d::template momentum<0>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy)) +
        fdbb::elem_mul(Fixture::var3d::template momentum<1>(fixture.density,
                                                            fixture.momentum_x,
                                                            fixture.momentum_y,
                                                            fixture.momentum_z,
                                                            fixture.energy),
                       Fixture::var3d::template momentum<1>(fixture.density,
                                                            fixture.momentum_x,
                                                            fixture.momentum_y,
                                                            fixture.momentum_z,
                                                            fixture.energy)) +
        fdbb::elem_mul(Fixture::var3d::template momentum<2>(fixture.density,
                                                            fixture.momentum_x,
                                                            fixture.momentum_y,
                                                            fixture.momentum_z,
                                                            fixture.energy),
                       Fixture::var3d::template momentum<2>(fixture.density,
                                                            fixture.momentum_x,
                                                            fixture.momentum_y,
                                                            fixture.momentum_z,
                                                            fixture.energy)),
      fdbb::elem_mul(Fixture::var3d::density(fixture.density,
                                             fixture.momentum_x,
                                             fixture.momentum_y,
                                             fixture.momentum_z,
                                             fixture.energy),
                     Fixture::var3d::density(fixture.density,
                                             fixture.momentum_x,
                                             fixture.momentum_y,
                                             fixture.momentum_z,
                                             fixture.energy)));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1d_3d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1d_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1d_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1d_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using v_mag2
template<typename Fixture>
void
test1e_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using v_mag2" << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      Fixture::var1d::template v_mag2(vex::tag<1>(fixture.density),
                                      vex::tag<2>(fixture.momentum_x),
                                      vex::tag<3>(fixture.energy));
#else
    fixture.result = Fixture::var1d::template v_mag2(
      fixture.density, fixture.momentum_x, fixture.energy);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1e_1d",
         typename Fixture::type,
         t0,
         t1,
         3,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1e_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1e_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1e_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using v_mag2
template<typename Fixture>
void
test1e_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using v_mag2" << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      Fixture::var2d::template v_mag2(vex::tag<1>(fixture.density),
                                      vex::tag<2>(fixture.momentum_x),
                                      vex::tag<3>(fixture.momentum_y),
                                      vex::tag<4>(fixture.energy));
#else
    fixture.result = Fixture::var2d::template v_mag2(
      fixture.density, fixture.momentum_x, fixture.momentum_y, fixture.energy);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1e_2d",
         typename Fixture::type,
         t0,
         t1,
         5,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1e_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1e_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1e_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using v_mag2
template<typename Fixture>
void
test1e_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using v_mag2" << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      Fixture::var3d::template v_mag2(vex::tag<1>(fixture.density),
                                      vex::tag<2>(fixture.momentum_x),
                                      vex::tag<3>(fixture.momentum_y),
                                      vex::tag<4>(fixture.momentum_z),
                                      vex::tag<5>(fixture.energy));
#else
    fixture.result = Fixture::var3d::template v_mag2(fixture.density,
                                                     fixture.momentum_x,
                                                     fixture.momentum_y,
                                                     fixture.momentum_z,
                                                     fixture.energy);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test1e_3d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test1e_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test1e_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test1e_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Vanilla implementation using density and velocity from fixture
template<typename Fixture>
void
test2a_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nVanilla implementation using velocity from fixture"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result = (fixture.momentum_x / fixture.density) %
                     (fixture.momentum_x / fixture.density);
#elif defined(FDBB_BACKEND_ITPP)
    fixture.result =
      itpp::elem_mult(itpp::elem_div(fixture.momentum_x, fixture.density),
                      itpp::elem_div(fixture.momentum_x, fixture.density));
#elif defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    fixture.result = mtl::vec::ele_prod(
      mtl::vec::ele_quot(fixture.momentum_x, fixture.density),
      mtl::vec::ele_quot(fixture.momentum_x, fixture.density));
#elif defined(FDBB_BACKEND_EIGEN)
    fixture.result = (fixture.momentum_x.array() / fixture.density.array()) *
                     (fixture.momentum_x.array() / fixture.density.array());
#elif defined(FDBB_BACKEND_UBLAS)
    fixture.result = boost::numeric::ublas::element_prod(
      boost::numeric::ublas::element_div(fixture.momentum_x, fixture.density),
      boost::numeric::ublas::element_div(fixture.momentum_x, fixture.density));
#elif defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      (vex::tag<1>(fixture.momentum_x) / vex::tag<2>(fixture.density)) *
      (vex::tag<1>(fixture.momentum_x) / vex::tag<2>(fixture.density));
#elif defined(FDBB_BACKEND_VIENNACL)
    fixture.result = viennacl::linalg::element_prod(
      viennacl::linalg::element_div(fixture.momentum_x, fixture.density),
      viennacl::linalg::element_div(fixture.momentum_x, fixture.density));
#else
    fixture.result = (fixture.momentum_x / fixture.density) *
                     (fixture.momentum_x / fixture.density);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test2a_1d",
         typename Fixture::type,
         t0,
         t1,
         3,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test2a_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test2a_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test2a_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Vanilla implementation using density and velocity from fixture
template<typename Fixture>
void
test2a_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nVanilla implementation using velocity from fixture"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result = (fixture.momentum_x / fixture.density) %
                       (fixture.momentum_x / fixture.density) +
                     (fixture.momentum_y / fixture.density) %
                       (fixture.momentum_y / fixture.density);
#elif defined(FDBB_BACKEND_ITPP)
    fixture.result =
      itpp::elem_mult(itpp::elem_div(fixture.momentum_x, fixture.density),
                      itpp::elem_div(fixture.momentum_x, fixture.density)) +
      itpp::elem_mult(itpp::elem_div(fixture.momentum_y, fixture.density),
                      itpp::elem_div(fixture.momentum_y, fixture.density));
#elif defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    fixture.result =
      mtl::vec::ele_prod(
        mtl::vec::ele_quot(fixture.momentum_x, fixture.density),
        mtl::vec::ele_quot(fixture.momentum_x, fixture.density)) +
      mtl::vec::ele_prod(
        mtl::vec::ele_quot(fixture.momentum_y, fixture.density),
        mtl::vec::ele_quot(fixture.momentum_y, fixture.density));
#elif defined(FDBB_BACKEND_EIGEN)
    fixture.result = (fixture.momentum_x.array() / fixture.density.array()) *
                       (fixture.momentum_x.array() / fixture.density.array()) +
                     (fixture.momentum_y.array() / fixture.density.array()) *
                       (fixture.momentum_y.array() / fixture.density.array());
#elif defined(FDBB_BACKEND_UBLAS)
    fixture.result =
      boost::numeric::ublas::element_prod(
        boost::numeric::ublas::element_div(fixture.momentum_x, fixture.density),
        boost::numeric::ublas::element_div(fixture.momentum_x,
                                           fixture.density)) +
      boost::numeric::ublas::element_prod(
        boost::numeric::ublas::element_div(fixture.momentum_y, fixture.density),
        boost::numeric::ublas::element_div(fixture.momentum_y,
                                           fixture.density));
#elif defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      (vex::tag<1>(fixture.momentum_x) / vex::tag<3>(fixture.density)) *
        (vex::tag<1>(fixture.momentum_x) / vex::tag<3>(fixture.density)) +
      (vex::tag<2>(fixture.momentum_y) / vex::tag<3>(fixture.density)) *
        (vex::tag<2>(fixture.momentum_y) / vex::tag<3>(fixture.density));
#elif defined(FDBB_BACKEND_VIENNACL)
    fixture.result =
      viennacl::linalg::element_prod(
        viennacl::linalg::element_div(fixture.momentum_x, fixture.density),
        viennacl::linalg::element_div(fixture.momentum_x, fixture.density)) +
      viennacl::linalg::element_prod(
        viennacl::linalg::element_div(fixture.momentum_y, fixture.density),
        viennacl::linalg::element_div(fixture.momentum_y, fixture.density));
#else
    fixture.result = (fixture.momentum_x / fixture.density) *
                       (fixture.momentum_x / fixture.density) +
                     (fixture.momentum_y / fixture.density) *
                       (fixture.momentum_y / fixture.density);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test2a_2d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test2a_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test2a_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test2a_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Vanilla implementation using density and velocity from fixture
template<typename Fixture>
void
test2a_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nVanilla implementation using velocity from fixture"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result = (fixture.momentum_x / fixture.density) %
                       (fixture.momentum_x / fixture.density) +
                     (fixture.momentum_y / fixture.density) %
                       (fixture.momentum_y / fixture.density) +
                     (fixture.momentum_z / fixture.density) %
                       (fixture.momentum_z / fixture.density);
#elif defined(FDBB_BACKEND_ITPP)
    fixture.result =
      itpp::elem_mult(itpp::elem_div(fixture.momentum_x, fixture.density),
                      itpp::elem_div(fixture.momentum_x, fixture.density)) +
      itpp::elem_mult(itpp::elem_div(fixture.momentum_y, fixture.density),
                      itpp::elem_div(fixture.momentum_y, fixture.density)) +
      itpp::elem_mult(itpp::elem_div(fixture.momentum_z, fixture.density),
                      itpp::elem_div(fixture.momentum_z, fixture.density));
#elif defined(FDBB_BACKEND_CMTL4) || defined(FDBB_BACKEND_MTL4)
    fixture.result =
      mtl::vec::ele_prod(
        mtl::vec::ele_quot(fixture.momentum_x, fixture.density),
        mtl::vec::ele_quot(fixture.momentum_x, fixture.density)) +
      mtl::vec::ele_prod(
        mtl::vec::ele_quot(fixture.momentum_y, fixture.density),
        mtl::vec::ele_quot(fixture.momentum_y, fixture.density)) +
      mtl::vec::ele_prod(
        mtl::vec::ele_quot(fixture.momentum_z, fixture.density),
        mtl::vec::ele_quot(fixture.momentum_z, fixture.density));
#elif defined(FDBB_BACKEND_EIGEN)
    fixture.result = (fixture.momentum_x.array() / fixture.density.array()) *
                       (fixture.momentum_x.array() / fixture.density.array()) +
                     (fixture.momentum_y.array() / fixture.density.array()) *
                       (fixture.momentum_y.array() / fixture.density.array()) +
                     (fixture.momentum_z.array() / fixture.density.array()) *
                       (fixture.momentum_z.array() / fixture.density.array());
#elif defined(FDBB_BACKEND_UBLAS)
    fixture.result =
      boost::numeric::ublas::element_prod(
        boost::numeric::ublas::element_div(fixture.momentum_x, fixture.density),
        boost::numeric::ublas::element_div(fixture.momentum_x,
                                           fixture.density)) +
      boost::numeric::ublas::element_prod(
        boost::numeric::ublas::element_div(fixture.momentum_y, fixture.density),
        boost::numeric::ublas::element_div(fixture.momentum_y,
                                           fixture.density)) +
      boost::numeric::ublas::element_prod(
        boost::numeric::ublas::element_div(fixture.momentum_z, fixture.density),
        boost::numeric::ublas::element_div(fixture.momentum_z,
                                           fixture.density));
#elif defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      (vex::tag<1>(fixture.momentum_x) / vex::tag<4>(fixture.density)) *
        (vex::tag<1>(fixture.momentum_x) / vex::tag<4>(fixture.density)) +
      (vex::tag<2>(fixture.momentum_y) / vex::tag<4>(fixture.density)) *
        (vex::tag<2>(fixture.momentum_y) / vex::tag<4>(fixture.density)) +
      (vex::tag<3>(fixture.momentum_z) / vex::tag<4>(fixture.density)) *
        (vex::tag<3>(fixture.momentum_z) / vex::tag<4>(fixture.density));
#elif defined(FDBB_BACKEND_VIENNACL)
    fixture.result =
      viennacl::linalg::element_prod(
        viennacl::linalg::element_div(fixture.momentum_x, fixture.density),
        viennacl::linalg::element_div(fixture.momentum_x, fixture.density)) +
      viennacl::linalg::element_prod(
        viennacl::linalg::element_div(fixture.momentum_y, fixture.density),
        viennacl::linalg::element_div(fixture.momentum_y, fixture.density)) +
      viennacl::linalg::element_prod(
        viennacl::linalg::element_div(fixture.momentum_z, fixture.density),
        viennacl::linalg::element_div(fixture.momentum_z, fixture.density));
#else
    fixture.result = (fixture.momentum_x / fixture.density) *
                       (fixture.momentum_x / fixture.density) +
                     (fixture.momentum_y / fixture.density) *
                       (fixture.momentum_y / fixture.density) +
                     (fixture.momentum_z / fixture.density) *
                       (fixture.momentum_z / fixture.density);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test2a_3d",
         typename Fixture::type,
         t0,
         t1,
         11,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test2a_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test2a_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test2a_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// Implementation using density by functions + elem_mul and elem_div
template<typename Fixture>
void
test2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nImplementation using density and velocity by functions + "
               "elem_mul and elem_div"
            << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_VEXCL)
    vex::tag<0>(fixture.result) =
      fdbb::elem_mul(
        Fixture::var3d::template velocity<0>(vex::tag<0>(fixture.density),
                                             vex::tag<0>(fixture.momentum_x),
                                             vex::tag<0>(fixture.momentum_y),
                                             vex::tag<0>(fixture.momentum_z),
                                             vex::tag<0>(fixture.energy)),
        Fixture::var3d::template velocity<0>(vex::tag<0>(fixture.density),
                                             vex::tag<0>(fixture.momentum_x),
                                             vex::tag<0>(fixture.momentum_y),
                                             vex::tag<0>(fixture.momentum_z),
                                             vex::tag<0>(fixture.energy))) +
      fdbb::elem_mul(
        Fixture::var3d::template velocity<1>(vex::tag<0>(fixture.density),
                                             vex::tag<0>(fixture.momentum_x),
                                             vex::tag<0>(fixture.momentum_y),
                                             vex::tag<0>(fixture.momentum_z),
                                             vex::tag<0>(fixture.energy)),
        Fixture::var3d::template velocity<1>(vex::tag<0>(fixture.density),
                                             vex::tag<0>(fixture.momentum_x),
                                             vex::tag<0>(fixture.momentum_y),
                                             vex::tag<0>(fixture.momentum_z),
                                             vex::tag<0>(fixture.energy))) +
      fdbb::elem_mul(
        Fixture::var3d::template velocity<2>(vex::tag<0>(fixture.density),
                                             vex::tag<0>(fixture.momentum_x),
                                             vex::tag<0>(fixture.momentum_y),
                                             vex::tag<0>(fixture.momentum_z),
                                             vex::tag<0>(fixture.energy)),
        Fixture::var3d::template velocity<2>(vex::tag<0>(fixture.density),
                                             vex::tag<0>(fixture.momentum_x),
                                             vex::tag<0>(fixture.momentum_y),
                                             vex::tag<0>(fixture.momentum_z),
                                             vex::tag<0>(fixture.energy)));
#else
    fixture.result =
      fdbb::elem_mul(Fixture::var3d::template velocity<0>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy),
                     Fixture::var3d::template velocity<0>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy)) +
      fdbb::elem_mul(Fixture::var3d::template velocity<1>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy),
                     Fixture::var3d::template velocity<1>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy)) +
      fdbb::elem_mul(Fixture::var3d::template velocity<2>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy),
                     Fixture::var3d::template velocity<2>(fixture.density,
                                                          fixture.momentum_x,
                                                          fixture.momentum_y,
                                                          fixture.momentum_z,
                                                          fixture.energy));
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test2d",
         typename Fixture::type,
         t0,
         t1,
         11,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// SpMatVec multiplication
template<typename Fixture>
void
test3e_1d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result =
      fixture.matA * fdbb::make_temp<0>(Fixture::var1d::template v_mag2(
                       fixture.density, fixture.momentum_x, fixture.energy));
#elif defined(FDBB_BACKEND_VEXCL)
    //        fixture.result = fixture.matA * fixture.density;
    //                fdbb::make_temp<0>(Fixture::var1d::template
    //                v_mag2(vex::tag<1>(fixture.density),
    //                                                                                    vex::tag<2>(fixture.momentum_x),
    //                                                                                    vex::tag<2>(fixture.energy)));
    for (auto i = 0; i < fixture.result.size(); i++)
      std::cout << fixture.result[i] << " " << fixture.density[i] << std::endl;
#else
//        fixture.result = fixture.matA *
//            Fixture::var1d::template v_mag2(fixture.density,
//                                                             fixture.momentum_x,
//                                                             fixture.energy);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test3e_1d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test3e_1d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test3e_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test3e_1d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// SpMatVec multiplication
template<typename Fixture>
void
test3e_2d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nSpMatVec implementation" << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result =
      fixture.matA *
      fdbb::make_temp<0>(Fixture::var2d::template v_mag2(fixture.density,
                                                         fixture.momentum_x,
                                                         fixture.momentum_y,
                                                         fixture.energy));
#elif defined(FDBB_BACKEND_VEXCL)
//        fixture.result = fixture.matA * fixture.density;
//            Fixture::var2d::template
//            v_mag2(vex::tag<1>(fixture.density),
//                                                             vex::tag<2>(fixture.momentum_x),
//                                                             vex::tag<3>(fixture.momentum_y),
//                                                             vex::tag<4>(fixture.momentum_z),
//                                                             vex::tag<5>(fixture.energy));
#else
//        fixture.result = fixture.matA *
//            Fixture::var2d::template v_mag2(fixture.density,
//                                                             fixture.momentum_x,
//                                                             fixture.momentum_y,
//                                                             fixture.momentum_z,
//                                                             fixture.energy);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test3e_2d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test3e_2d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test3e_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test3e_2d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}

// SpMatVec multiplication
template<typename Fixture>
void
test3e_3d(Fixture& fixture, const std::size_t runs)
{
  if (runs == 0)
    return;

  std::cout << "\n\nSpMatVec implementation" << std::endl;
  auto t0 = fixture.getClock();
  for (auto irun = 0; irun < runs; irun++) {
#if defined(FDBB_BACKEND_ARMADILLO)
    fixture.result =
      fixture.matA *
      fdbb::make_temp<0>(Fixture::var3d::template v_mag2(fixture.density,
                                                         fixture.momentum_x,
                                                         fixture.momentum_y,
                                                         fixture.momentum_z,
                                                         fixture.energy));
#elif defined(FDBB_BACKEND_VEXCL)
    fixture.result = fixture.matA * fixture.density;
//            Fixture::var3d::template
//            v_mag2(vex::tag<1>(fixture.density),
//                                                             vex::tag<2>(fixture.momentum_x),
//                                                             vex::tag<3>(fixture.momentum_y),
//                                                             vex::tag<4>(fixture.momentum_z),
//                                                             vex::tag<5>(fixture.energy));
#else
//        fixture.result = fixture.matA  * fixture.density;
//            Fixture::var3d::template v_mag2(fixture.density,
//                                                             fixture.momentum_x,
//                                                             fixture.momentum_y,
//                                                             fixture.momentum_z,
//                                                             fixture.energy);
#ifdef FDBB_BACKEND_ARRAYFIRE
    fixture.result.eval();
#endif
#endif
  }
  auto t1 = fixture.getClock();
  TIMING("suite1",
         "test3e_3d",
         typename Fixture::type,
         t0,
         t1,
         7,
         4,
         1,
         fixture.nrows,
         runs);

#if defined(FDBB_BACKEND_ARRAYFIRE)
  std::cout << "[suite1.test3e_3d." << typeid(typename Fixture::type).name()
            << "] value               : ";
  af_print(fixture.result(0));
  af_print(fixture.result(fixture.nrows - 1));
#elif defined(FDBB_BACKEND_BLAZE) || defined(FDBB_BACKEND_VEXCL)
  std::cout << "[suite1.test3e_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result[0] << " "
            << fixture.result[fixture.nrows - 1] << std::endl;
#else
  std::cout << "[suite1.test3e_3d." << typeid(typename Fixture::type).name()
            << "] value               : " << fixture.result(0) << " "
            << fixture.result(fixture.nrows - 1) << std::endl;
#endif
}
