/** @file ublas-debug-matrix.cxx
 *
 *  @brief Debugging for uBLAS library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <chrono>
#include <iostream>

#include <boost/numeric/ublas/matrix.hpp>

#include "fdbb.h"

template<typename T1, typename T2, typename T3, typename T4>
auto
calculate_vanilla(const T1& a1, const T2& a2, const T3& a3, const T4& a4)
#if __cplusplus <= 201103L
  -> decltype(boost::numeric::ublas::element_div(
    boost::numeric::ublas::element_prod(a1, a1) +
      boost::numeric::ublas::element_prod(a2, a2) +
      boost::numeric::ublas::element_prod(a3, a3),
    boost::numeric::ublas::element_prod(a4, a4)))
#endif
{
  return boost::numeric::ublas::element_div(
    boost::numeric::ublas::element_prod(a1, a1) +
      boost::numeric::ublas::element_prod(a2, a2) +
      boost::numeric::ublas::element_prod(a3, a3),
    boost::numeric::ublas::element_prod(a4, a4));
}

template<typename T1, typename T2, typename T3, typename T4>
auto
calculate_fdbb(const T1& a1, const T2& a2, const T3& a3, const T4& a4)
#if __cplusplus <= 201103L
  -> decltype(fdbb::elem_div(fdbb::elem_mul(a1, a1) + fdbb::elem_mul(a2, a2) +
                               fdbb::elem_mul(a3, a3),
                             fdbb::elem_mul(a4, a4)))
#endif
{
  return fdbb::elem_div(fdbb::elem_mul(a1, a1) + fdbb::elem_mul(a2, a2) +
                          fdbb::elem_mul(a3, a3),
                        fdbb::elem_mul(a4, a4));
}

int
main()
{
  typedef float T;
  typedef boost::numeric::ublas::matrix<T> matrix;

  matrix density(10, 10), momentum_x(10, 10), momentum_y(10, 10),
    momentum_z(10, 10), result(10, 10);

  for (auto i = 0; i < density.size1(); i++)
    for (auto j = 0; j < density.size2(); j++)
      density(i, j) = 2.0;
  for (auto i = 0; i < momentum_x.size1(); i++)
    for (auto j = 0; j < momentum_x.size2(); j++)
      momentum_x(i, j) = 1.0;
  for (auto i = 0; i < momentum_y.size1(); i++)
    for (auto j = 0; j < momentum_y.size2(); j++)
      momentum_y(i, j) = 2.0;
  for (auto i = 0; i < momentum_z.size1(); i++)
    for (auto j = 0; j < momentum_z.size2(); j++)
      momentum_z(i, j) = 3.0;

  // Check has_features
  std::cout << "Check has_make_temp_impl  : "
            << fdbb::detail::has_make_temp_impl<matrix>::value << std::endl;
  std::cout << "Check has_tag_impl        : "
            << fdbb::detail::has_tag_impl<matrix>::value << std::endl;

  // Check specialized implementations of binary operators
  std::cout << "Check get_elem_mul_impl   : "
            << (int)fdbb::detail::get_elem_mul_impl<matrix, matrix>::value
            << std::endl;
  std::cout << "Check get_elem_div_impl   : "
            << (int)fdbb::detail::get_elem_div_impl<matrix, matrix>::value
            << std::endl;
  std::cout << "Check get_elem_pow_impl   : "
            << (int)fdbb::detail::get_elem_pow_impl<matrix, matrix>::value
            << std::endl;

  // Check specialized implementations of unary operators
  std::cout << "Check get_elem_abs_impl   : "
            << (int)fdbb::detail::get_elem_abs_impl<matrix>::value << std::endl;
  std::cout << "Check get_elem_acos_impl  : "
            << (int)fdbb::detail::get_elem_acos_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_acosh_impl : "
            << (int)fdbb::detail::get_elem_acosh_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_asin_impl  : "
            << (int)fdbb::detail::get_elem_asin_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_asinh_impl : "
            << (int)fdbb::detail::get_elem_asinh_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_atan_impl  : "
            << (int)fdbb::detail::get_elem_atan_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_atanh_impl : "
            << (int)fdbb::detail::get_elem_atanh_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_ceil_impl  : "
            << (int)fdbb::detail::get_elem_ceil_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_conj_impl  : "
            << (int)fdbb::detail::get_elem_conj_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_cos_impl   : "
            << (int)fdbb::detail::get_elem_cos_impl<matrix>::value << std::endl;
  std::cout << "Check get_elem_cosh_impl  : "
            << (int)fdbb::detail::get_elem_cosh_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_exp_impl   : "
            << (int)fdbb::detail::get_elem_exp_impl<matrix>::value << std::endl;
  std::cout << "Check get_elem_exp10_impl : "
            << (int)fdbb::detail::get_elem_exp10_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_exp2_impl  : "
            << (int)fdbb::detail::get_elem_exp2_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_fabs_impl  : "
            << (int)fdbb::detail::get_elem_fabs_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_floor_impl : "
            << (int)fdbb::detail::get_elem_floor_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_imag_impl  : "
            << (int)fdbb::detail::get_elem_imag_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_log_impl   : "
            << (int)fdbb::detail::get_elem_log_impl<matrix>::value << std::endl;
  std::cout << "Check get_elem_log10_impl : "
            << (int)fdbb::detail::get_elem_log10_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_log2_impl  : "
            << (int)fdbb::detail::get_elem_log2_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_real_impl  : "
            << (int)fdbb::detail::get_elem_real_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_round_impl : "
            << (int)fdbb::detail::get_elem_round_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_rsqrt_impl : "
            << (int)fdbb::detail::get_elem_rsqrt_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_sign_impl  : "
            << (int)fdbb::detail::get_elem_sign_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_sin_impl   : "
            << (int)fdbb::detail::get_elem_sin_impl<matrix>::value << std::endl;
  std::cout << "Check get_elem_sinh_impl  : "
            << (int)fdbb::detail::get_elem_sinh_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_sqrt_impl  : "
            << (int)fdbb::detail::get_elem_sqrt_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_tan_impl   : "
            << (int)fdbb::detail::get_elem_tan_impl<matrix>::value << std::endl;
  std::cout << "Check get_elem_tanh_impl  : "
            << (int)fdbb::detail::get_elem_tanh_impl<matrix>::value
            << std::endl;
  std::cout << "Check get_elem_trunc_impl : "
            << (int)fdbb::detail::get_elem_trunc_impl<matrix>::value
            << std::endl;

  // Vanilla implementation
  result = boost::numeric::ublas::element_div(
    boost::numeric::ublas::element_prod(momentum_x, momentum_x) +
      boost::numeric::ublas::element_prod(momentum_y, momentum_y) +
      boost::numeric::ublas::element_prod(momentum_z, momentum_z),
    boost::numeric::ublas::element_prod(density, density));

  std::cout << result(0, 0) << std::endl;

  // FDBB implementation
  result = fdbb::elem_div(fdbb::elem_mul(momentum_x, momentum_x) +
                            fdbb::elem_mul(momentum_y, momentum_y) +
                            fdbb::elem_mul(momentum_z, momentum_z),
                          fdbb::elem_mul(density, density));

  std::cout << result(0, 0) << std::endl;

  // Function-based implementation
  result = calculate_vanilla(momentum_x, momentum_y, momentum_z, density);

  std::cout << result(0, 0) << std::endl;

  // Function-based implementation
  result = calculate_fdbb(momentum_x, momentum_y, momentum_z, density);

  std::cout << result(0, 0) << std::endl;

  return 0;
}
