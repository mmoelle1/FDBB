/** @file eigen-primitive.cxx
 *
 *  @brief Test primitive variables for Eigen Library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Andrzej Jaeschke
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <typeinfo>

#include "fdbb.h"

int
main(int argc, char** argv)
{
  using vector = Eigen::Array<double, Eigen::Dynamic, 1>;

  vector u0(1), u1(1), u2(1), u3(1), u4(1);
  u0 = 2.0;
  u1 = 1.0;
  u2 = 2.0;
  u3 = 3.0;
  u4 = 5.0;
  vector u02(1), u12(1), u22(1), u32(1), u42(1), n1(1), n2(1), n3(1);
  u02 = 2.0;
  u12 = 0.5;
  u22 = 0.7;
  u32 = 0.7;
  u42 = 0.6;
  n1 = 1.0;
  n2 = 0.0;
  n3 = 0.0;

  typedef fdbb::fluids::EOSidealGas<double, std::ratio<7, 2>, std::ratio<5, 2>>
    eos;

  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::conservative>
    variables;
  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::primitive>
    variables2;

  // Primary variables
  auto d = variables::rho(u0, u1, u2, u3, u4);
  auto m0 = variables::rhov<0>(u0, u1, u2, u3, u4);
  //  auto m1 = variables::rhov<1>(u0, u1, u2, u3, u4);
  //  auto m2 = variables::rhov<2>(u0, u1, u2, u3, u4);
  auto rhoE = variables::rhoE(u0, u1, u2, u3, u4);
  auto rhov_mag = variables::rhov_mag(u0, u1, u2, u3, u4);
  auto rhov_mag_2 = variables::rhov_mag2(u0, u1, u2, u3, u4);
  auto E = variables::E(u0, u1, u2, u3, u4);
  auto v0 = variables::v<0>(u0, u1, u2, u3, u4);
  // auto v1 = variables::v<1>(u0, u1, u2, u3, u4);
  // auto v2 = variables::v<2>(u0, u1, u2, u3, u4);
  auto v_mag = variables::v_mag(u0, u1, u2, u3, u4);
  auto v_mag_2 = variables::v_mag2(u0, u1, u2, u3, u4);
  auto rhoE_kin = variables::rhoE_kin(u0, u1, u2, u3, u4);
  auto E_kin = variables::E_kin(u0, u1, u2, u3, u4);
  auto rhoe = variables::rhoe(u0, u1, u2, u3, u4);
  auto e = variables::e(u0, u1, u2, u3, u4);
  auto rhoH = variables::rhoH(u0, u1, u2, u3, u4);
  auto H = variables::H(u0, u1, u2, u3, u4);
  auto rhoh = variables::rhoh(u0, u1, u2, u3, u4);
  auto h = variables::h(u0, u1, u2, u3, u4);
  auto p = variables::p(u0, u1, u2, u3, u4);
  auto c = variables::c(u0, u1, u2, u3, u4);
  auto vdir = variables::v_dir(n1, u0, u1, u2, u3, u4);

  auto d2 = variables2::rho(u02, u12, u22, u32, u42);
  auto m02 = variables2::rhov<0>(u02, u12, u22, u32, u42);
  // auto m12 = variables2::rhov<1>(u02, u12, u22, u32, u42);
  //  auto m22 = variables2::rhov<2>(u02, u12, u22, u32, u42);
  auto rhoE2 = variables2::rhoE(u02, u12, u22, u32, u42);
  auto rhov_mag2 = variables2::rhov_mag(u02, u12, u22, u32, u42);
  auto rhov_mag_22 = variables2::rhov_mag2(u02, u12, u22, u32, u42);
  auto E2 = variables2::E(u02, u12, u22, u32, u42);
  auto v02 = variables2::v<0>(u02, u12, u22, u32, u42);
  // auto v12 = variables2::v<1>(u02, u12, u22, u32, u42);
  // auto v22 = variables2::v<2>(u02, u12, u22, u32, u42);
  auto v_mag2 = variables2::v_mag(u02, u12, u22, u32, u42);
  auto v_mag_22 = variables2::v_mag2(u02, u12, u22, u32, u42);
  auto rhoE_kin2 = variables2::rhoE_kin(u02, u12, u22, u32, u42);
  auto E_kin2 = variables2::E_kin(u02, u12, u22, u32, u42);
  auto rhoe2 = variables2::rhoe(u02, u12, u22, u32, u42);
  auto e2 = variables2::e(u02, u12, u22, u32, u42);
  auto rhoH2 = variables2::rhoH(u02, u12, u22, u32, u42);
  auto H2 = variables2::H(u02, u12, u22, u32, u42);
  auto rhoh2 = variables2::rhoh(u02, u12, u22, u32, u42);
  auto h2 = variables2::h(u02, u12, u22, u32, u42);
  auto p2 = variables2::p(u02, u12, u22, u32, u42);
  auto c2 = variables2::c(u02, u12, u22, u32, u42);
  auto vdir2 = variables2::v_dir(n1, u02, u12, u22, u32, u42);

  std::cout << "rho:  conservative:" << d << " primitive: " << d2 << std::endl;
  std::cout << "rhov0: conservative:" << m0 << " primitive: " << m02
            << std::endl;
  // std::cout << "rhov1: conservative:" << m1 << " primitive: " << m12
  //           << std::endl;
  // std::cout << "rhov2: conservative:" << m2 << " primitive: " << m22
  //           << std::endl;
  std::cout << "rhoE:   conservative:" << rhoE << " primitive: " << rhoE2
            << std::endl;
  std::cout << "rhov_mag:  conservative:" << rhov_mag
            << " primitive: " << rhov_mag2 << std::endl;
  std::cout << "rhov_mag_2: conservative:" << rhov_mag_2
            << " primitive: " << rhov_mag_22 << std::endl;
  std::cout << "E:   conservative:" << E << " primitive: " << E2 << std::endl;
  std::cout << "v0:  conservative:" << v0 << " primitive: " << v02 << std::endl;
  // std::cout << "v1:  conservative:" << v1 << " primitive: " << v12 <<
  // std::endl; std::cout << "v2:  conservative:" << v2 << " primitive: " << v22
  // << std::endl;
  std::cout << "v_mag: conservative:" << v_mag << " primitive: " << v_mag2
            << std::endl;
  std::cout << "v_mag_2:   conservative:" << v_mag_2
            << " primitive: " << v_mag_22 << std::endl;
  std::cout << "rhoE_kin:  conservative:" << rhoE_kin
            << " primitive: " << rhoE_kin2 << std::endl;
  std::cout << "E_kin: conservative:" << E_kin << " primitive: " << E_kin2
            << std::endl;
  std::cout << "rhoe:   conservative:" << rhoe << " primitive: " << rhoe2
            << std::endl;
  std::cout << "e:  conservative:" << e << " primitive: " << e2 << std::endl;
  std::cout << "rhoh: conservative:" << rhoh << " primitive: " << rhoh2
            << std::endl;
  std::cout << "h:   conservative:" << h << " primitive: " << h2 << std::endl;
  std::cout << "p:  conservative:" << p << " primitive: " << p2 << std::endl;
  std::cout << "rhoH: conservative:" << rhoH << " primitive: " << rhoH2
            << std::endl;
  std::cout << "H:   conservative:" << H << " primitive: " << H2 << std::endl;
  std::cout << "c:   conservative:" << c << " primitive: " << c2 << std::endl;
  std::cout << "v_dir:   conservative:" << vdir << " primitive: " << vdir2
            << std::endl;

  return 0;
}
