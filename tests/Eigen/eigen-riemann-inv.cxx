/** @file eigen-variables.cxx
 *
 *  @brief Test variables for Eigen Library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <typeinfo>

#include "fdbb.h"

struct __print
{
  template<typename T, std::size_t I0>
  void operator()(const T& x, std::integral_constant<std::size_t, I0>) const
  {
    std::cout << x << std::endl;
  }
};

int
main(int argc, char** argv)
{
  using vector = Eigen::Array<double, Eigen::Dynamic, 1>;

  vector u0(1), u1(1), u2(1), u3(1), u4(1), n1(1), n2(1), n3(1), xi1(1), xi2(1),
    xi3(1), tau1(1), tau2(1), tau3(1), r1(1), r2(1), r3(1), r4(1), r5(1);
  u0 = 2.0;
  u1 = 0.5;
  u2 = 0.7;
  u3 = 1.5;
  u4 = 0.6;
  n1 = 1.0;
  n2 = 0.0;
  n3 = 0.0;
  xi1 = 0.0;
  xi2 = 1.0;
  xi3 = 0.0;
  tau1 = 0.0;
  tau2 = 0.0;
  tau3 = 1.0;
  r1 = -3.0;
  r2 = -27.5849;
  r3 = 4.0;
  r4 = 1.5;
  r5 = 3.74037;

  // Equation of state for ideal gas
  typedef fdbb::fluids::EOSidealGas<double, std::ratio<7, 2>, std::ratio<5, 2>>
    eos;
  /*
    { // Conservative variables in 1D

      typedef fdbb::fluids::Variables<eos, 1,
    fdbb::fluids::EnumForm::conservative> variables;

      auto U = variables::conservative<1>(u0, u1, u2, u3, u4);
      auto V = variables::primitive<2>(u0, u1, u2, u3, u4);
      auto Ri = variables::riemann<3>(n1, u0, u1, u2, u3, u4);
      auto Ch = variables::characteristic<4>(n1, u0, u1, u2, u3, u4);
      typedef fdbb::fluids::Variables<eos, 1,
    fdbb::fluids::EnumForm::Riemann_invariants> variables2; auto U2 =
    variables2::conservative<5>(n1, r1, r2, r3, r4, r5); auto V2 =
    variables2::primitive<6>(n1, r1, r2, r3, r4, r5); std::cout <<
    "Conservative:" << std::endl; fdbb::utils::for_each(U, __print()); std::cout
    << "Primitive:" << std::endl; fdbb::utils::for_each(V, __print()); std::cout
    << "Conservative2:" << std::endl; fdbb::utils::for_each(U2, __print());
      std::cout << "Primitive2:" << std::endl;
      fdbb::utils::for_each(V2, __print());
      std::cout << "Riemann:" << std::endl;
      fdbb::utils::for_each(Ri, __print());
      std::cout << "Characteristic:" << std::endl;
      fdbb::utils::for_each(Ch, __print());

    }
    //*/
  /*
    { // Conservative variables in 2D
      typedef fdbb::fluids::Variables<eos, 2,
  fdbb::fluids::EnumForm::conservative> variables;

      auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
      auto V = variables::primitive<1>(u0, u1, u2, u3, u4);
      auto Ri = variables::riemann<3>(n1, n2, xi1, xi2, u0, u1, u2, u3, u4);
      auto Ch = variables::characteristic<4>(n1, n2, u0, u1, u2, u3, u4);
      typedef fdbb::fluids::Variables<eos, 2,
  fdbb::fluids::EnumForm::Riemann_invariants> variables2; auto U2 =
  variables2::conservative<5>(n1, n2, xi1, xi2, r1, r2, r3, r4, r5); auto V2 =
  variables2::primitive<6>(n1, n2, xi1, xi2, r1, r2, r3, r4, r5); std::cout <<
  "Conservative:" << std::endl; fdbb::utils::for_each(U,
  __print()); std::cout << "Primitive:" << std::endl; fdbb::utils::for_each(V,
  __print()); std::cout << "Conservative2:" << std::endl;
      fdbb::utils::for_each(U2, __print());
      std::cout << "Primitive2:" << std::endl;
      fdbb::utils::for_each(V2, __print());
      std::cout << "Riemann:" << std::endl;
      fdbb::utils::for_each(Ri, __print());
      std::cout << "Characteristic:" << std::endl;
      fdbb::utils::for_each(Ch, __print());
    }
  //*/
  /*
    { // Conservative variables in 3D
      typedef fdbb::fluids::Variables<eos, 3,
  fdbb::fluids::EnumForm::conservative> variables;

      auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
      auto V = variables::primitive<1>(u0, u1, u2, u3, u4);
      auto Ri = variables::riemann<3>(n1, n2, n3, xi1, xi2, xi3, tau1, tau2,
  tau3, u0, u1, u2, u3, u4); auto Ch = variables::characteristic<4>(n1, n2, n3,
  u0, u1, u2, u3, u4); typedef fdbb::fluids::Variables<eos, 3,
  fdbb::fluids::EnumForm::Riemann_invariants> variables2; auto U2 =
  variables2::conservative<5>(n1, n2, n3, xi1, xi2, xi3, tau1, tau2, tau3, r1,
  r2, r3, r4, r5); auto V2 = variables2::primitive<6>(n1, n2, n3, xi1, xi2, xi3,
  tau1, tau2, tau3, r1, r2, r3, r4, r5); std::cout << "Conservative:" <<
  std::endl; fdbb::utils::for_each(U, __print()); std::cout << "Primitive:" <<
  std::endl; fdbb::utils::for_each(V, __print()); std::cout << "Conservative2:"
  << std::endl; fdbb::utils::for_each(U2, __print()); std::cout << "Primitive2:"
  << std::endl; fdbb::utils::for_each(V2, __print()); std::cout << "Riemann:" <<
  std::endl; fdbb::utils::for_each(Ri, __print()); std::cout <<
  "Characteristic:" << std::endl; fdbb::utils::for_each(Ch, __print());
    }
  //*/
  ///*
  { // Primitive variables in 1D
    typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::primitive>
      variables;

    auto U = variables::conservative<1>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<2>(u0, u1, u2, u3, u4);
    auto Ri = variables::riemann<77>(n1, u0, u1, u2, u3, u4);
    auto Ch = variables::characteristic<4>(n1, u0, u1, u2, u3, u4);
    typedef fdbb::fluids::
      Variables<eos, 1, fdbb::fluids::EnumForm::Riemann_invariants>
        variables2;
    auto U2 = variables2::conservative<5>(n1, r1, r2, r3, r4, r5);
    auto V2 = variables2::primitive<6>(n1, r1, r2, r3, r4, r5);
    std::cout << "Conservative:" << std::endl;
    fdbb::utils::for_each(U, __print());
    std::cout << "Primitive:" << std::endl;
    fdbb::utils::for_each(V, __print());
    std::cout << "Conservative2:" << std::endl;
    fdbb::utils::for_each(U2, __print());
    std::cout << "Primitive2:" << std::endl;
    fdbb::utils::for_each(V2, __print());
    std::cout << "Riemann:" << std::endl;
    fdbb::utils::for_each(Ri, __print());
    std::cout << "Characteristic:" << std::endl;
    fdbb::utils::for_each(Ch, __print());
  }
  //*/
  /*
    { // Primitive variables in 2D
      typedef fdbb::fluids::Variables<eos, 2, fdbb::fluids::EnumForm::primitive>
  variables;

      auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
      auto V = variables::primitive<1>(u0, u1, u2, u3, u4);
      auto Ri = variables::riemann<3>(n1, n2, xi1, xi2, u0, u1, u2, u3, u4);
      auto Ch = variables::characteristic<4>(n1, n2, u0, u1, u2, u3, u4);
      typedef fdbb::fluids::Variables<eos, 2,
  fdbb::fluids::EnumForm::Riemann_invariants> variables2; auto U2 =
  variables2::conservative<5>(n1, n2, xi1, xi2, r1, r2, r3, r4, r5); auto V2 =
  variables2::primitive<6>(n1, n2, xi1, xi2, r1, r2, r3, r4, r5); std::cout <<
  "Conservative:" << std::endl; fdbb::utils::for_each(U,
  __print()); std::cout << "Primitive:" << std::endl; fdbb::utils::for_each(V,
  __print()); std::cout << "Conservative2:" << std::endl;
      fdbb::utils::for_each(U2, __print());
      std::cout << "Primitive2:" << std::endl;
      fdbb::utils::for_each(V2, __print());
      std::cout << "Riemann:" << std::endl;
      fdbb::utils::for_each(Ri, __print());
      std::cout << "Characteristic:" << std::endl;
      fdbb::utils::for_each(Ch, __print());
    }
  //*/
  /*
    { // Primitive variables in 3D
      typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::primitive>
    variables;

      auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
      auto V = variables::primitive<1>(u0, u1, u2, u3, u4);
      auto Ri = variables::riemann<3>(n1, n2, n3, xi1, xi2, xi3, tau1, tau2,
    tau3, u0, u1, u2, u3, u4); auto Ch = variables::characteristic<4>(n1, n2,
    n3, u0, u1, u2, u3, u4); typedef fdbb::fluids::Variables<eos, 3,
    fdbb::fluids::EnumForm::Riemann_invariants> variables2; auto U2 =
    variables2::conservative<5>(n1, n2, n3, xi1, xi2, xi3, tau1, tau2, tau3, r1,
    r2, r3, r4, r5); auto V2 = variables2::primitive<6>(n1, n2, n3, xi1, xi2,
    xi3, tau1, tau2, tau3, r1, r2, r3, r4, r5); std::cout << "Conservative:" <<
    std::endl; fdbb::utils::for_each(U, __print()); std::cout << "Primitive:" <<
    std::endl; fdbb::utils::for_each(V, __print()); std::cout <<
    "Conservative2:" << std::endl; fdbb::utils::for_each(U2, __print());
      std::cout << "Primitive2:" << std::endl;
      fdbb::utils::for_each(V2, __print());
      std::cout << "Riemann:" << std::endl;
      fdbb::utils::for_each(Ri, __print());
      std::cout << "Characteristic:" << std::endl;
      fdbb::utils::for_each(Ch, __print());
    }
    //*/
  return 0;
}
