/** @file eigen-variables.cxx
 *
 *  @brief Test variables for Eigen Library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <typeinfo>

#include "fdbb.h"

int
main(int argc, char** argv)
{
  using vector = Eigen::Array<double, Eigen::Dynamic, 1>;

  vector u0(10), u1(10), u2(10), u3(10), u4(10);
  u0 = 2.0;
  u1 = 1.0;
  u2 = 5.0;
  u3 = 0.5;
  u4 = 1.9;

  // Equation of state for ideal gas
  typedef fdbb::fluids::EOSidealGas<double, std::ratio<7, 2>, std::ratio<5, 2>>
    eos;

  { // Conservative variables in 1D
    typedef fdbb::fluids::
      Variables<eos, 1, fdbb::fluids::EnumForm::conservative>
        variables;

    auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<1>(u0, u1, u2, u3, u4);
    // Inviscid fluxes in 1D
    typedef fdbb::fluids::Fluxes<variables> fluxes;

    auto F = fluxes::inviscid<2>(u0, u1, u2, u3, u4);
  }

  { // Conservative variables in 2D
    typedef fdbb::fluids::
      Variables<eos, 2, fdbb::fluids::EnumForm::conservative>
        variables;

    auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<1>(u0, u1, u2, u3, u4);

    // Inviscid fluxes in 2D
    typedef fdbb::fluids::Fluxes<variables> fluxes;

    auto F = fluxes::inviscid<2>(u0, u1, u2, u3, u4);
  }

  { // Conservative variables in 3D
    typedef fdbb::fluids::
      Variables<eos, 3, fdbb::fluids::EnumForm::conservative>
        variables;

    auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<1>(u0, u1, u2, u3, u4);

    // Inviscid fluxes in 3D
    typedef fdbb::fluids::Fluxes<variables> fluxes;

    auto F = fluxes::inviscid<2>(u0, u1, u2, u3, u4);
  }

  { // Primitive variables in 1D
    typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::primitive>
      variables;

    auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<1>(u0, u1, u2, u3, u4);
    // Inviscid fluxes in 1D
    // typedef fdbb::fluids::Fluxes<variables> fluxes;

    // auto F = fluxes::inviscid<2>(u0, u1, u2, u3, u4);;
  }

  { // Primitive variables in 2D
    typedef fdbb::fluids::Variables<eos, 2, fdbb::fluids::EnumForm::primitive>
      variables;

    auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<1>(u0, u1, u2, u3, u4);

    // Inviscid fluxes in 2D
    // typedef fdbb::fluids::Fluxes<variables> fluxes;

    // auto F = fluxes::inviscid<2>(u0, u1, u2, u3, u4);
  }

  { // Primitive variables in 3D
    typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::primitive>
      variables;

    auto U = variables::conservative<0>(u0, u1, u2, u3, u4);
    auto V = variables::primitive<1>(u0, u1, u2, u3, u4);

    // Inviscid fluxes in 3D
    // typedef fdbb::fluids::Fluxes<variables> fluxes;

    // auto F = fluxes::inviscid<2>(u0, u1, u2, u3, u4);
  }
  return 0;
}
