/** @file eigen-test1.cxx
 *
 *  @brief Test Suite #1 for Eigen library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <array>
#include <chrono>
#include <getopt.h>
#include <iostream>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "fdbb.h"

#include "suite1.hpp"

template<typename T>
struct ConservativeFixture
{
  // Typedefs
  typedef T type;
  typedef Matrix1d<T> mat1d;
  typedef Matrix2d<T> mat2d;
  typedef Matrix3d<T> mat3d;
  typedef Eigen::Matrix<T, Eigen::Dynamic, 1> vector;
  typedef Eigen::SparseMatrix<T> matrix;

  // Equation of state
  typedef fdbb::fluids::EOSidealGas<T> eos;

  // Variables
  typedef fdbb::fluids::Variables<eos, 1, fdbb::fluids::EnumForm::conservative>
    var1d;
  typedef fdbb::fluids::Variables<eos, 2, fdbb::fluids::EnumForm::conservative>
    var2d;
  typedef fdbb::fluids::Variables<eos, 3, fdbb::fluids::EnumForm::conservative>
    var3d;

  // Constructor
  ConservativeFixture()
    : ConservativeFixture(1)
  {}

  // Constructor
  ConservativeFixture(std::size_t N, int dim = 1)
    : density(static_cast<std::size_t>(pow(N, dim)))
    , momentum_x(static_cast<std::size_t>(pow(N, dim)))
    , momentum_y(static_cast<std::size_t>(pow(N, dim)))
    , momentum_z(static_cast<std::size_t>(pow(N, dim)))
    , energy(static_cast<std::size_t>(pow(N, dim)))
    , result(static_cast<std::size_t>(pow(N, dim)))
    , nrows(static_cast<std::size_t>(pow(N, dim)))
    , ncols(static_cast<std::size_t>(pow(N, dim)))
    , matA(static_cast<std::size_t>(pow(N, dim)),
           static_cast<std::size_t>(pow(N, dim)))
  {
    density.fill(2.0);
    momentum_x.fill(1.0);
    momentum_y.fill(2.0);
    momentum_z.fill(3.0);
    energy.fill(5.0);
    result.fill(0.0);
  }

  // Constructor
  ConservativeFixture(std::size_t N, int dim, std::array<std::size_t, 3> degree)
    : ConservativeFixture(N, dim)
  {
    // Create sparse matrix in CSR format
    switch (dim) {
      case 1: {
        mat1d mat_h(N, degree[0]);

        typedef Eigen::Triplet<T> Triplet;
        std::vector<Triplet> tripletList;
        tripletList.reserve(mat_h.nonzeros());

        for (auto irow = 0; irow < mat_h.rows(); irow++) {
          for (auto jcol = mat_h.row[irow]; jcol < mat_h.row[irow + 1]; jcol++)
            tripletList.push_back(
              Triplet(irow, mat_h.col[jcol], mat_h.val[jcol]));
        }
        matA.setFromTriplets(tripletList.begin(), tripletList.end());
        mat_h.clear();
        break;
      }
      case 2: {
        mat2d mat_h(N, degree[0], degree[1]);
        matA.reserve(mat_h.nonzeros());

        typedef Eigen::Triplet<T> Triplet;
        std::vector<Triplet> tripletList;
        tripletList.reserve(mat_h.nonzeros());

        for (auto irow = 0; irow < mat_h.rows(); irow++) {
          for (auto jcol = mat_h.row[irow]; jcol < mat_h.row[irow + 1]; jcol++)
            tripletList.push_back(
              Triplet(irow, mat_h.col[jcol], mat_h.val[jcol]));
        }
        matA.setFromTriplets(tripletList.begin(), tripletList.end());
        mat_h.clear();

        result = matA * density.matrix();

        break;
      }
      case 3: {
        mat3d mat_h(N, degree[0], degree[1], degree[2]);
        matA.reserve(mat_h.nonzeros());

        typedef Eigen::Triplet<T> Triplet;
        std::vector<Triplet> tripletList;
        tripletList.reserve(mat_h.nonzeros());

        for (auto irow = 0; irow < mat_h.rows(); irow++) {
          for (auto jcol = mat_h.row[irow]; jcol < mat_h.row[irow + 1]; jcol++)
            tripletList.push_back(
              Triplet(irow, mat_h.col[jcol], mat_h.val[jcol]));
        }
        matA.setFromTriplets(tripletList.begin(), tripletList.end());
        mat_h.clear();
        break;
      }
    }
  }

  // Destructor
  ~ConservativeFixture() {}

  // Attributes
  vector density;
  vector momentum_x;
  vector momentum_y;
  vector momentum_z;
  vector energy;
  vector result;
  matrix matA;
  matrix matB;
  matrix matC;

  const std::size_t nrows;
  const std::size_t ncols;

  // Time measurement
  auto static getClock() -> decltype(std::chrono::high_resolution_clock::now())
  {
    return std::chrono::high_resolution_clock::now();
  }
};

int
main(int argc, char** argv)
{
  // Create test configuration
  TestConfiguration config(argc, argv);
  config.info();

  // Run tests
  testAll<ConservativeFixture<float>>(config);
  testAll<ConservativeFixture<double>>(config);

  return 0;
}
