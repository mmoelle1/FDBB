/** @file eigen-blockexpr.cxx
 *
 *  @brief Test block expressions for Eigen Library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <typeinfo>

#include "fdbb.h"

struct __print
{
  template<typename T, std::size_t I0>
  void operator()(const T& x, std::integral_constant<std::size_t, I0>) const
  {
    std::cout << x << std::endl;
  }
};

template class fdbb::
  BlockRowVector<0, Eigen::Array<double, Eigen::Dynamic, 1>, 3>;
template class fdbb::
  BlockColVector<1, Eigen::Array<double, Eigen::Dynamic, 1>, 3>;

template class fdbb::
  BlockRowVectorView<2, Eigen::Array<double, Eigen::Dynamic, 1>, 3>;
template class fdbb::
  BlockColVectorView<3, Eigen::Array<double, Eigen::Dynamic, 1>, 3>;

template class fdbb::
  BlockMatrix<4, Eigen::Array<double, Eigen::Dynamic, 1>, 3, 2>;
template class fdbb::
  BlockMatrixView<5, Eigen::Array<double, Eigen::Dynamic, 1>, 3, 2>;

template class fdbb::
  BlockMatrix<6, Eigen::Array<double, Eigen::Dynamic, 1>, 3, 2, true>;
template class fdbb::
  BlockMatrixView<7, Eigen::Array<double, Eigen::Dynamic, 1>, 3, 2, true>;

int
main(int argc, char** argv)
{
  using vector = Eigen::Array<double, Eigen::Dynamic, 1>;

  vector u0(1), u1(1), u2(1), u3(1), u4(1), u5(1);
  u0 = 1.0;
  u1 = 2.0;
  u2 = 3.0;
  u3 = 4.0;
  u4 = 5.0;
  u5 = 6.0;

  //  fdbb::BlockMatrixView<0, vector, 2, 3> M1(&u0, &u1, &u2, &u3, &u4,
  //  &u5); fdbb::BlockMatrixView<1, vector, 3, 4> M2(
  //    &u0, &u0, &u0, &u0, &u1, &u1, &u1, &u1, &u2, &u2, &u2, &u2);
  /*
    std::cout << M1 << std::endl;
    fdbb::utils::for_each(M1, __print());
    std::cout << "---\n";
    std::cout << M2 << std::endl;
    fdbb::utils::for_each(M2, __print());
    std::cout << "---\n";

    fdbb::utils::for_each(M2.transpose() * M1.transpose(), __print());
    std::cout << "---\n";*/
  //  fdbb::utils::for_each(M2.transpose() * M1.transpose(), __print());

  std::cout << u0 << std::endl;
  std::cout << u1 << std::endl;
  std::cout << u2 << std::endl;
  std::cout << u3 << std::endl;
  std::cout << u4 << std::endl;
  std::cout << u5 << std::endl;

  fdbb::BlockRowVector<0, vector, 3> V1(u0, u1, u2);
  fdbb::BlockRowVector<1, vector, 3> V2(u1, u2, u0);

  std::cout << fdbb::get<0>(V1) << std::endl;
  std::cout << fdbb::get<1>(V1) << std::endl;
  std::cout << fdbb::get<2>(V1) << std::endl;

  auto T = V1 + V2;

  std::cout << V1 + V2 << std::endl;

  // auto T = V1 - V2 + V1 + V2;

  // fdbb::BlockRowVector<2, vector, 3> V(T);

  // fdbb::BlockRowVector<0, vector, 3> V1(u0, u1, u2);
  // fdbb::BlockRowVector<1, vector, 3> V2(u0, u1, u2);

  // double t = 2.2;

  // auto T = t / V1 / 2.2;

  // fdbb::BlockRowVector<101, vector, 3> Vr(u0, u1, u2);
  // fdbb::BlockRowVector<102, vector, 3> Vc(u0, u1, u2);

  // auto V = Vr * Vc;

  // [. . . . .]   [.]
  // [. . . . .] * [.]
  // [. . . . .]   [.]
  //               [.]
  //               [.]

  return 0;
}
