########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

if (CMAKE_VERSION VERSION_LESS 3.2)
  set(UPDATE_DISCONNECTED_IF_AVAILABLE "")
else()
  set(UPDATE_DISCONNECTED_IF_AVAILABLE "")#"UPDATE_DISCONNECTED 1")
endif()

#
# Option list
#

# Configure device
set(FDBB_BUILD_TESTS_DEVICES "CPU" CACHE STRING "Build tests for selected devices")
set_property(CACHE FDBB_BUILD_TESTS_DEVICES PROPERTY STRINGS ALL CPU GPU0 GPU1 GPU2 GPU3 GPU4 GPU5 GPU6 GPU7)

# Configure precision
set(FDBB_BUILD_TESTS_PRECISIONS "ALL" CACHE STRING "Build tests for selected precisions")
set_property(CACHE FDBB_BUILD_TESTS_PRECISIONS PROPERTY STRINGS ALL FLOAT DOUBLE)

option(FDBB_BUILD_TESTS_ARMADILLO "Build Armadillo tests" OFF)
option(FDBB_BUILD_TESTS_ARRAYFIRE "Build ArrayFire tests" OFF)
option(FDBB_BUILD_TESTS_BLAZE     "Build Blaze tests"     OFF)
option(FDBB_BUILD_TESTS_CMTL4     "Build CMTL4 tests"     OFF)
option(FDBB_BUILD_TESTS_EIGEN     "Build Eigen tests"     OFF)
option(FDBB_BUILD_TESTS_ITPP      "Build IT++ tests"      OFF)
option(FDBB_BUILD_TESTS_MTL4      "Build MTL4 tests"      OFF)
option(FDBB_BUILD_TESTS_UBLAS     "Build uBLAS tests"     OFF)
option(FDBB_BUILD_TESTS_VEXCL     "Build VexCL tests"     OFF)
option(FDBB_BUILD_TESTS_VIENNACL  "Build ViennaCL tests"  OFF)

#
# Summary
#
message("")
message("Tests:")
message("FDBB_BUILD_TESTS_DEVICES...........: ${FDBB_BUILD_TESTS_DEVICES}")
message("FDBB_BUILD_TESTS_PRECISIONS........: ${FDBB_BUILD_TESTS_PRECISIONS}")
message("")
message("FDBB_BUILD_TESTS_ARMADILLO.........: ${FDBB_BUILD_TESTS_ARMADILLO}")
message("FDBB_BUILD_TESTS_ARRAYFIRE.........: ${FDBB_BUILD_TESTS_ARRAYFIRE}")
message("FDBB_BUILD_TESTS_BLAZE.............: ${FDBB_BUILD_TESTS_BLAZE}")
message("FDBB_BUILD_TESTS_CMTL4.............: ${FDBB_BUILD_TESTS_CMTL4}")
message("FDBB_BUILD_TESTS_EIGEN.............: ${FDBB_BUILD_TESTS_EIGEN}")
message("FDBB_BUILD_TESTS_ITPP..............: ${FDBB_BUILD_TESTS_ITPP}")
message("FDBB_BUILD_TESTS_MTL4..............: ${FDBB_BUILD_TESTS_MTL4}")
message("FDBB_BUILD_TESTS_UBLAS.............: ${FDBB_BUILD_TESTS_UBLAS}")
message("FDBB_BUILD_TESTS_VEXCL.............: ${FDBB_BUILD_TESTS_VEXCL}")
message("FDBB_BUILD_TESTS_VIENNACL..........: ${FDBB_BUILD_TESTS_VIENNACL}")
message("")

########################################################################
# Device handling
########################################################################

if(FDBB_BUILD_TESTS_DEVICES STREQUAL "ALL")
  add_definitions(-DSUPPORT_DEVICES_ALL)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "CPU")
  add_definitions(-DSUPPORT_DEVICES_CPU)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU0")
  add_definitions(-DSUPPORT_DEVICES_GPU=0)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU1")
  add_definitions(-DSUPPORT_DEVICES_GPU=1)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU2")
  add_definitions(-DSUPPORT_DEVICES_GPU=2)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU3")
  add_definitions(-DSUPPORT_DEVICES_GPU=3)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU4")
  add_definitions(-DSUPPORT_DEVICES_GPU=4)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU5")
  add_definitions(-DSUPPORT_DEVICES_GPU=5)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU6")
  add_definitions(-DSUPPORT_DEVICES_GPU=6)
elseif(FDBB_BUILD_TESTS_DEVICES STREQUAL "GPU7")
  add_definitions(-DSUPPORT_DEVICES_GPU=7)
endif()

########################################################################
# Precision handling
########################################################################

# Define list of precisions
unset(FDBB_TESTS_PRECISIONS)
if(FDBB_BUILD_TESTS_PRECISIONS STREQUAL "ALL")
  # ALL = float;double
  list(APPEND FDBB_TESTS_PRECISIONS float double)
  add_definitions(-DSUPPORT_FLOAT -DSUPPORT_DOUBLE)
else()
  # Check for admissible values
  foreach(precision ${FDBB_BUILD_TESTS_PRECISIONS})    
    if( ${precision} STREQUAL "float" OR ${precision} STREQUAL "std::float")
      list(APPEND FDBB_TESTS_PRECISIONS ${precision})
      add_definitions(-DSUPPORT_FLOAT)
    elseif(${precision} STREQUAL "double" OR ${precision} STREQUAL "std::double")
      list(APPEND FDBB_TESTS_PRECISIONS ${precision})
      add_definitions(-DSUPPORT_DOUBLE)
    elseif(${precision} STREQUAL "complex<float>" OR ${precision} STREQUAL "std::complex<float>")
      list(APPEND FDBB_TESTS_PRECISIONS ${precision})
      add_definitions(-DSUPPORT_COMPLEX_FLOAT)
    elseif(${precision} STREQUAL "complex<double>" OR ${precision} STREQUAL "std::complex<double>")
      list(APPEND FDBB_TESTS_PRECISIONS ${precision})
      add_definitions(-DSUPPORT_COMPLEX_DOUBLE)
    else()
      message(FATAL_ERROR "Unsupported precision: ${precision}")
    endif()
  endforeach()
endif()

########################################################################
# Add subdirectories
########################################################################

# Armadillo tests
if(FDBB_BUILD_TESTS_ARMADILLO)
  add_subdirectory(Armadillo)
endif()

# ArrayFire tests
if(FDBB_BUILD_TESTS_ARRAYFIRE)
  add_subdirectory(ArrayFire)
endif()

# Blaze
if(FDBB_BUILD_TESTS_BLAZE)
  add_subdirectory(Blaze)
endif()

# CMTL4
if(FDBB_BUILD_TESTS_CMTL4)
  add_subdirectory(CMTL4)
endif()

# Eigen
if(FDBB_BUILD_TESTS_EIGEN)
  add_subdirectory(Eigen)
endif()

# IT++
if(FDBB_BUILD_TESTS_ITPP)
  add_subdirectory(IT++)
endif()

# MTL4
if(FDBB_BUILD_TESTS_MTL4)
  add_subdirectory(MTL4)
endif()

# uBLAS
if(FDBB_BUILD_TESTS_UBLAS)
  add_subdirectory(uBLAS)
endif()

# VexCL
if(FDBB_BUILD_TESTS_VEXCL)
  add_subdirectory(VexCL)
endif()

# ViennaCL
if(FDBB_BUILD_TESTS_VIENNACL)
  add_subdirectory(ViennaCL)
endif()
